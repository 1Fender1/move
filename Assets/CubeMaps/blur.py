import cv2 as cv
import os
import sys

formats = {'bmp', 'dib', 'jpeg', 'jpg', 'jpe', 'jp2', 'png', 'webp', 'pbm', 'pgm', 'ppm', 'sr', 'ras', 'tiff', 'tif'}


def blur(img, kernelSize):
    output = cv.blur(img,(kernelSize,kernelSize))
    return output

def canOpen(name):
    tab = name.split('.')
    format = tab[len(tab)-1]
    if formats.__contains__(format):
        return True
    return False

def printUsage():
    print('Usage : ')
    print(' - blur [KernelSize]')


kSize = 75
if (len(sys.argv) == 2):
    kSize = int(sys.argv[1])
elif (len(sys.argv) > 2):
    print('This script take only 0 or 1 argument')
    printUsage()
    sys.exit(-1)

        


directory = 'blur'

try:
    os.stat(directory)
except:
    os.mkdir(directory)


for subdir, dirs, files in os.walk('.'):
    for file in files:
        if (canOpen(file)):
            img = cv.imread(file)
            blurImg = blur(img, kSize)
            tab = file.split('.')
            del tab[len(tab)-1]
            tab.append('png')
            fileName = '.'.join(tab)
            cv.imwrite(directory + '/' + fileName, blurImg)