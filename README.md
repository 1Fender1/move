Move readme




#######################Dependencies#######################


Move is developped in c++ and use cmake/make tools to compile

 - sudo apt-get install build-essential make cmake

The engine use openGL, GLEW and GLFW3

 - sudo apt-get install glew-utils libglew-dev libglfw3 libglfw3-dev
 
Move include some third-party libraries in submodules, these libraries also have dependencies :

 - sudo apt-get install libtbb-dev

You can also use this command :

- sudo apt-get install build-essential make cmake glew-utils libglew-dev libglfw3 libglfw3-dev libtbb-dev

##########################################################




#######################Compilation#######################

How to compile for linux :

Pull external libraries with 
 - git submodule init
 - git submodule update

mkdir build
cd build
cmake ..
make 


Debug/Release complation mode :


cmake -DCMAKE_BUILD_TYPE=Release -> Release mode also default compilation mode
cmake -DCMAKE_BUILD_TYPE=Debug -> Debug mode



##########################################################





##
