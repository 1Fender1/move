#version 410 core

#include "/Utils/luma.glsl"

float QUALITY(int i) {
    float quality[12] = float[12](0.0, 0.0, 0.0, 0.0, 0.0, 1.5, 2.0, 2.0, 2.0, 2.0, 4.0, 8.0);
    return quality[i];
}

layout (location = 0) out vec3 gColor;

in vec2 texC;
uniform sampler2D inTexture;

uniform int width;
uniform int height;

const float EDGE_THRESHOLD_MIN = 0.0312;
const float EDGE_THRESHOLD_MAX = 0.125;
const float SUBPIXEL_QUALITY = 0.75;
const int ITERATIONS = 12;



void main() {
    vec2 In = texC;//gl_FragCoord.uv;

    vec2 inverseScreenSize = vec2(1.f/width, 1.f/height);

    vec3 center = texelFetch(inTexture, ivec2(gl_FragCoord.xy), 0).rgb;//texture(inTexture, In).rgb;

    float lumaCenter = rgb2luma(center);
    float lumaDown = rgb2luma(textureOffset(inTexture, In, ivec2(0, -1)).rgb);
    float lumaUp = rgb2luma(textureOffset(inTexture, In, ivec2(0, 1)).rgb);
    float lumaLeft = rgb2luma(textureOffset(inTexture, In, ivec2(-1, 0)).rgb);
    float lumaRight = rgb2luma(textureOffset(inTexture, In, ivec2(1, 0)).rgb);


    float lumaMin = min(lumaCenter, min(min(lumaDown, lumaUp), min(lumaLeft, lumaRight)));
    float lumaMax = max(lumaCenter, max(max(lumaDown, lumaUp), max(lumaLeft, lumaRight)));


    float lumaRange = lumaMax - lumaMin;


    if (lumaRange < max(EDGE_THRESHOLD_MIN, lumaMax*EDGE_THRESHOLD_MAX)) {
        gColor = center;
        return;
    }


    float lumaDownLeft = rgb2luma(textureOffset(inTexture, In,ivec2(-1, -1)).rgb);
    float lumaUpRight = rgb2luma(textureOffset(inTexture, In,ivec2(1, 1)).rgb);
    float lumaUpLeft = rgb2luma(textureOffset(inTexture, In,ivec2(-1, 1)).rgb);
    float lumaDownRight = rgb2luma(textureOffset(inTexture, In,ivec2(1, -1)).rgb);


    float lumaDownUp = lumaDown + lumaUp;
    float lumaLeftRight = lumaLeft + lumaRight;

    float lumaLeftCorners = lumaDownLeft + lumaUpLeft;
    float lumaDownCorners = lumaDownLeft + lumaDownRight;
    float lumaRightCorners = lumaDownRight + lumaUpRight;
    float lumaUpCorners = lumaUpRight + lumaUpLeft;

    float edgeHorizontal = abs(-2.0 * lumaLeft + lumaLeftCorners) + abs(-2.0 * lumaCenter + lumaDownUp) * 2.0 + abs(-2.0 * lumaRight + lumaRightCorners);

    float edgeVertical = abs(-2.0 * lumaUp + lumaUpCorners) + abs(-2.0 * lumaCenter + lumaLeftRight) * 2.0 + abs(-2.0 * lumaDown + lumaDownCorners);


    bool isHorizontal = (edgeHorizontal >= edgeVertical);



    float luma1 = isHorizontal ? lumaDown : lumaLeft;
    float luma2 = isHorizontal ? lumaUp : lumaRight;


    float gradient1 = luma1 - lumaCenter;
    float gradient2 = luma2 - lumaCenter;

    bool isSteepest = abs(gradient1) >= abs(gradient2);

    float gradientScaled = 0.25*max(abs(gradient1), abs(gradient2));


    float stepLength = isHorizontal ? inverseScreenSize.y : inverseScreenSize.x;

    float lumaLocalAverage = 0.0;

    if (isSteepest) {
        stepLength = -stepLength;
        lumaLocalAverage = 0.5 * (luma1 + lumaCenter);
    }
    else {
        lumaLocalAverage = 0.5 * (luma2 + lumaCenter);
    }


    vec2 currentUv = In;
    if (isHorizontal) {
        currentUv.y += stepLength * 0.5;
    }
    else {
        currentUv.x += stepLength * 0.5;
    }


    vec2 offset = isHorizontal ? vec2(inverseScreenSize.x, 0.0) : vec2(0.0, inverseScreenSize.y);

    vec2 uv1 = currentUv - offset;
    vec2 uv2 = currentUv + offset;


    float lumaEnd1 = rgb2luma(texture(inTexture, uv1).rgb);
    float lumaEnd2 = rgb2luma(texture(inTexture, uv2).rgb);
    lumaEnd1 -= lumaLocalAverage;
    lumaEnd2 -= lumaLocalAverage;


    bool reached1 = abs(lumaEnd1) >= gradientScaled;
    bool reached2 = abs(lumaEnd2) >= gradientScaled;
    bool reachedBoth = reached1 && reached2;

    if (!reached1)
        uv1 -= offset;
    if(!reached2)
        uv2 += offset;


    if (!reachedBoth) {
        for (int i = 2; i < ITERATIONS; i++) {

            if (!reached1) {
                lumaEnd1 = rgb2luma(texture(inTexture, uv1).rgb);
                lumaEnd1 -= lumaLocalAverage;
            }

            if (!reached2) {
                lumaEnd2 = rgb2luma(texture(inTexture, uv2).rgb);
                lumaEnd2 -= lumaLocalAverage;
            }

            reached1 = abs(lumaEnd1) >= gradientScaled;
            reached2 = abs(lumaEnd2) >= gradientScaled;
            reachedBoth = reached1 && reached2;

            if (!reached1)
                uv1 -= offset * QUALITY(i);

            if (!reached2)
                uv2 += offset * QUALITY(i);


            if (reachedBoth)
                break;

        }

    }

    float distance1 = isHorizontal ? (In.x - uv1.x) : (In.y - uv1.y);
    float distance2 = isHorizontal ? (uv2.x - In.x) : (uv2.y - In.y);

    bool isDirection1 = distance1 < distance2;
    float distanceFinal = min(distance1, distance2);

    float edgeThickness = (distance1 + distance2);

    float pixelOffset = -distanceFinal / edgeThickness + 0.5;

    bool isLumaCenterSmaller = lumaCenter < lumaLocalAverage;
    bool correctVariation = ((isDirection1 ? lumaEnd1 : lumaEnd2) < 0.0) != isLumaCenterSmaller;
    float finalOffset = correctVariation ? pixelOffset : 0.0;


    float lumaAverage = (1.0/12.0) * (2.0 * (lumaDownUp + lumaLeftRight) + lumaLeftCorners + lumaRightCorners);
    float subPixelOffset1 = clamp(abs(lumaAverage - lumaCenter)/lumaRange,0.0,1.0);
    float subPixelOffset2 = (-2.0 * subPixelOffset1 + 3.0) * subPixelOffset1 * subPixelOffset1;
    float subPixelOffsetFinal = subPixelOffset2 * subPixelOffset2 * SUBPIXEL_QUALITY;
    finalOffset = max(finalOffset, subPixelOffsetFinal);


    vec2 finalUv = In;
    if (isHorizontal)
        finalUv.y += finalOffset * stepLength;
    else
        finalUv.x += finalOffset * stepLength;


    vec3 finalColor = texture(inTexture, finalUv).rgb;
    gColor = finalColor;
}
