#version 410 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gTangent;
layout (location = 3) out vec4 gAlbedo;
layout (location = 4) out float gSpecular;
layout (location = 5) out float gDepth;

#include "/Utils/dataStructures.glsl"

in vec2 texC;
in vec3 positionFrag;
in vec3 normal;
in vec3 v;
in mat3 TBN;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;
uniform int hasNormalMap;
uniform int hasSpecularMap;
uniform int hasDiffuseMap;
uniform int hasBumpMap;

uniform float u_near;
uniform float u_far;

uniform Material material;

#include "Utils/constants.glsl"
#include "Utils/diffuse.glsl"
#include "Utils/specular.glsl"
#include "Utils/texturesGetters.glsl"

float linearizeDepth(float depth, float near, float far) {
    float z = depth * 2.f - 1.f;
    return (2.f * near * far) / (far + near - z * (far - near));
}

void main() {
    gAlbedo = getDiffuseColor(texture_diffuse1, texC, hasDiffuseMap, material);
    gSpecular = getSpecularColor(texture_specular1, texC, hasSpecularMap, material).x;
    gNormal = getNormal(texture_normal1, texC, hasNormalMap, TBN, normal);
    gTangent = TBN[0];
    gPosition = v;
    gDepth = linearizeDepth(gl_FragCoord.z, u_near, u_far);
}
