#version 410 core
in vec3 normal;
in vec3 positionFrag;
in vec3 v;
out vec4 color;

#include "Utils/dataStructures.glsl"

uniform Material material;
uniform mat4 model;
uniform float gizmoScale;

uniform bool isPointed;
const vec4 orange = vec4(1.f, 0.647, 0.f, 1.f);

void main() {

    color = material.diffuseColor;

    if (isPointed)
        color = orange;

}
