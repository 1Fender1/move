#version 430 core
layout (location = 0) out vec4 gColor;

#include "Utils/dataStructures.glsl"
#include "Utils/luma.glsl"

out vec4 color;

in vec3 normal;
in vec3 positionFrag;
in vec3 v;
in vec2 texC;
in vec3 tangent;
in vec3 bitangent;
in mat3 TBN;
in vec4 weights;
in vec4 fragLightSpace;
in mat4 mvp;

uniform sampler2D u_inTexture1;
uniform sampler2D u_inTexture2;
uniform int u_blendType;

const int blendADD = 0;
const int blendMULT = 1;


vec4 add(vec4 color1, vec4 color2) {
    return color1 * color1.a + color2 * color2.a;
}

vec4 mult(vec4 color1, vec4 color2) {
    return color1 * color2;
}


void main() {
    vec4 color1 = texelFetch(u_inTexture1, ivec2(gl_FragCoord.xy) , 0);
    vec4 color2 = texelFetch(u_inTexture2, ivec2(gl_FragCoord.xy), 0);

    color = add(color1, color2);
}