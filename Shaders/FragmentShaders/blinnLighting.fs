#version 410 core
layout (location = 0) out vec4 gColor;

#include "Utils/dataStructures.glsl"

in vec3 normal;
in vec3 positionFrag;
in vec3 v;
in vec2 texC;
in vec3 tangent;
in vec3 bitangent;
in mat3 TBN;
in vec4 weights;
in vec4 fragLightSpace;
out vec4 color;
uniform Light light;
uniform vec3 camPos;
uniform mat4 model;

uniform int hasNormalMap;
uniform int hasSpecularMap;
uniform int hasDiffuseMap;
uniform int hasBumpMap;

uniform int hasSSao;

uniform Material material;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;
uniform sampler2D ssaoIn;

uniform sampler2D texture_acc;

uniform sampler2D shadowMap; 

uniform int isShadowMap;

#include "Utils/SMPCF.glsl"
#include "Utils/blinnPhong_Lightning.glsl"


void main() {

    vec3 specularColor;
    vec3 normalT;
    float ambientOcc = 1;
    if (hasSSao == 1)
        ambientOcc = texelFetch(ssaoIn, ivec2(gl_FragCoord.xy), 0).r;

    if (hasDiffuseMap == 1) {
        color = texture(texture_diffuse1, texC);
        //if (color.a == 0.f)
          //  discard;
    }
    else {
        color = material.diffuseColor;
    }

    if (hasSpecularMap == 1)
        specularColor = texture(texture_specular1, texC).rgb;
    else
        specularColor = material.specularColor;


    vec3 normalTexture = normalize(texture(texture_normal1, texC).rgb);
    if (hasNormalMap == 1) {
        if (normalTexture.y == normalTexture.z && normalTexture.x > 0.f) {
            vec3 normalTexture = texture(texture_normal1, texC).rgb;
            float mipmapLevel = textureQueryLod(texture_normal1, texC).x;
            ivec2 texSize = textureSize(texture_normal1, int(0));
            vec2 invSize = vec2((1.f/float(texSize.x), 1.f/float(texSize.y)));

            float center = normalTexture.x;
            float xOffset = texture(texture_normal1, texC+vec2(texSize.x+invSize.x, 0.f)).x;
            float yOffset = texture(texture_normal1, texC+vec2(0.f, texSize.y + invSize.y)).x;

            normalT = vec3(xOffset-center, yOffset-center, 0.01f);
            normalT = normalize(normalT);

            normalT = normalize(TBN * normalT);
            //normalT = vec3(1.f, 0.f, 0.f);
        }
        else {
            vec3 normalTexture = normalize(texture(texture_normal1, texC).rgb);
            //normalT = normalize(texture(texture_normal1, texC).rgb);
            normalT = normalTexture;
            normalT = normalize(normalT * 2.0 - 1.0);

            normalT = normalize(TBN * normalT);
            //normalT = vec3(0.f, 0.f, 1.f);
        }
    } else {
        normalT = normal;
        //normalT = vec3(0.f, 0.f, 0.f);
    }


    Material mat;
    mat.diffuseColor = color;
    vec4 tmpColor = vec4(vec3(0.2 * color * ambientOcc), 1);
    mat.ambientColor = vec3(tmpColor);
    mat.specularColor = specularColor;

    float specularStrength = 0.5;
    
    color = vec4(texelFetch(texture_acc, ivec2(gl_FragCoord.xy), 0).rgb, 1.f) + vec4(computeBlinnPhong(light, mat, normalT), 1);

    gColor = color;


    //gColor.rgb = gColor.rbg;
    //gColor = vec4(normalT*0.5f+0.5f, 1.f);
    //gColor = vec4(specularColor, 1.f);

}
