#version 410 core
out vec4 color;

in vec3 texCoords;

uniform samplerCube skybox;

void main() {
    //color = texture(skybox, texCoords);
    color = vec4(vec3(0.f), 1.f);
}
