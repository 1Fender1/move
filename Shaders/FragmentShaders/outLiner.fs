#version 430 core
layout (location = 0) out vec4 gColor;

#include "Utils/dataStructures.glsl"
#include "Utils/luma.glsl"

out vec4 color;

in vec3 normal;
in vec3 positionFrag;
in vec3 v;
in vec2 texC;
in vec3 tangent;
in vec3 bitangent;
in mat3 TBN;
in vec4 weights;
in vec4 fragLightSpace;
in mat4 mvp;

uniform vec3 u_color;
uniform sampler2D u_inTexture;
uniform int width;
uniform int height;

vec3 gx = vec3(-1.f, 0.f, 1.f);
vec3 gy = vec3(1.f, 0.f, 1.f);
const int filterSize = 3;
float offsetX = 1.f/float(width);
float offsetY = 1.f/float(height);

void main() {

    mat3 samples;

    samples[0][0] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(-1.f, -1.f), 0).r;
    samples[1][0] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(0.f, -1.f), 0).r;
    samples[2][0] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(1.f, -1.f), 0).r;
    samples[0][1] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(-1.f, 0.f), 0).r;
    samples[1][1] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(0.f, 0.f), 0).r;
    samples[2][1] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(1.f, 0.f), 0).r;
    samples[0][2] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(-1.f, 1.f), 0).r;
    samples[1][2] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(0.f, 1.f), 0).r;
    samples[2][2] = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy) - ivec2(1.f, 1.f), 0).r;

    vec3 a = gx * samples;
    vec3 b = gx * transpose(samples);


    vec3 texColor = texelFetch(u_inTexture, ivec2(gl_FragCoord.xy), 0).rgb;

    vec3 color3 = rgb2luma(sqrt(a*a+b*b)) * u_color;
    float alpha = rgb2luma(color3) < 0.01f ? 0.f : 1.f;

    color = vec4(color3, alpha);
}