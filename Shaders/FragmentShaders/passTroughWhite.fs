#version 430 core
layout (location = 0) out vec4 gColor;

#include "Utils/dataStructures.glsl"

out vec4 color;

in vec3 normal;
in vec3 positionFrag;
in vec3 v;
in vec2 texC;
in vec3 tangent;
in vec3 bitangent;
in mat3 TBN;
in vec4 weights;
in vec4 fragLightSpace;
in mat4 mvp;

uniform vec3 u_color;


void main() {

    color = vec4(1.f, 1.f, 1.f, 1.f);
    gColor = color;

}