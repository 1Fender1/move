#version 430 core
layout (location = 0) out vec4 gColor;

#include "Utils/dataStructures.glsl"

in vec3 normal;
in vec3 positionFrag;
in vec3 v;
in vec2 texC;
in vec3 tangent;
in vec3 bitangent;
in mat3 TBN;
in vec4 weights;
in vec4 fragLightSpace;
in mat4 mvp;

out vec4 color;

uniform Light light;
uniform vec3 camPos;
uniform mat4 model;

uniform int hasNormalMap;
uniform int hasSpecularMap;
uniform int hasDiffuseMap;
uniform int hasBumpMap;
uniform int hasSSao;

uniform float u_near;
uniform float u_far;

uniform Material material;

uniform sampler2D u_cameraDepth;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;
uniform sampler2D ssaoIn;

uniform sampler2D texture_acc;

uniform int isShadowMap;
uniform sampler2D shadowMap;

uniform sampler2D areaMag;
uniform sampler2D areaMinV;

uniform int lightCount;


#include "Utils/SMPCF.glsl"
#include "Utils/constants.glsl"
#include "Utils/diffuse.glsl"
#include "Utils/specular.glsl"
#include "Utils/lightsComputation.glsl"
#include "Utils/PrincipledLighting.glsl"
#include "Utils/texturesGetters.glsl"


void initShadingContext(out ShadingData sd) {
    float ambientOcc = getOcclusion(ssaoIn, ivec2(gl_FragCoord.xy), hasSSao);
    vec4 color = getDiffuseColor(texture_diffuse1, texC, hasDiffuseMap, material);
    vec3 specularColor = getSpecularColor(texture_specular1, texC, hasSpecularMap, material);
    vec3 normalT = getNormal(texture_normal1, texC, hasNormalMap, TBN, normal);

    Material mat = material;
    mat.diffuseColor = color;
    vec4 tmpColor = vec4(vec3(0.2 * color * ambientOcc), 1);
    mat.ambientColor = vec3(tmpColor);
    mat.specularColor = specularColor;
    mat.roughness = 1.f - specularColor.r;

    sd.fragment.position = v;
    sd.fragment.normal = normalize(normalT);
    sd.fragment.tangent = tangent;
    sd.fragment.bitangent = bitangent;
    sd.fragment.uv = texC;
    sd.light = light;
    sd.cameraDir = normalize(camPos - sd.fragment.position);
    sd.lightSpacePosition = fragLightSpace;
    sd.lightDir = getLightDir(sd);
    sd.mat = mat;
}


float linearizeDepth(float depth, float near, float far) {
    float z = depth * 2.f - 1.f;
    return (2.f * near * far) / (far + near - z * (far - near));
}

void main() {

    //skip computation if fracgment is not visible from camera
    if (linearizeDepth(gl_FragCoord.z, u_near, u_far) > texelFetch(u_cameraDepth, ivec2(gl_FragCoord.xy), 0).x)
        discard;


    ShadingData sd;
    initShadingContext(sd);

    vec3 lightRadiance = computeLightRadiance(sd);
    vec4 materialResult = computePrincipled(sd, lightRadiance);
    vec4 emissiveColor = vec4(sd.mat.emissiveColor / float(lightCount), 1.f);

    vec4 accumulationBuffer = vec4(texelFetch(texture_acc, ivec2(gl_FragCoord.xy), 0).rgb, 1.f);
    color = accumulationBuffer + materialResult + emissiveColor;
    color.a = 1.f;

    gColor = color;

    //gColor = materialResult;
    //gColor = vec4(lightRadiance, 1.f);
    //gColor = vec4(sd.fragment.normal * 0.5f+0.5f, 1.f);
    //gColor = vec4(sd.fragment.tangent * 0.5f+0.5f, 1.f);
    //gColor = vec4(specularColor, 1.f);
    //gColor = sd.mat.diffuseColor;
    //gColor = vec4(1.f, 0.f, 0.f, 1.f);
}
