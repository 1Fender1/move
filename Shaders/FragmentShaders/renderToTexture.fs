#version 410 core
out vec4 color;

in vec2 texC;
uniform sampler2D texture_diffuse1;


void main() {
    vec3 colorTemp = texture(texture_diffuse1, texC).rgb;
    color = vec4(colorTemp, 1);
}
