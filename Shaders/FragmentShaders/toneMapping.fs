#version 410 core


uniform sampler2D texture_diffuse1;
uniform float exposure;


in vec2 texC;
out vec4 color;

vec3 rgbToSrgb(vec3 color) {

    float r = color.r;
    float g = color.g;
    float b = color.b;

    const float gamma = 2.2f;

    vec3 outColor = pow(color, vec3(1.f / gamma));

    return outColor;
}



void main() {
    vec4 colorTemp = texture2D(texture_diffuse1, texC);

    if (exposure > 0.f)
        colorTemp = vec4(1.f) - exp(-colorTemp * exposure);

    //color = colorTemp;
    
    //color = vec4(1.f, 0.f, 0.f, 1.f);
    
    //color = vec4(texC.x, texC.y, 1.f-texC.x*texC.y, 1.f);
/*
    if (colorTemp.x > 1.f || colorTemp.y > 1.f || colorTemp.z > 1.f) {
        color = vec4(1.f, 0.f, 0.f, 1.f);
        return;
    }
*/

    color = vec4(rgbToSrgb(colorTemp.rgb), 1.f);
   
    
}
