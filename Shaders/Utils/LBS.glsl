

mat4 linearBlendingCalc(mat4 transform, vec4 weigths) {
    mat4 blending = mat4(0);
    blending += mat4(1) * weigths.x;
    blending += transform * weigths.y;
    blending += mat4(1) * weigths.z;
    blending += mat4(1) * weigths.w;

    return blending;
}

//Return the vertex at this new position
vec3 LBSPosCalc(vec3 vertex, mat4 blending, vec4 weigths) {
    vec4 tmpOrig = vec4(vertex, 1);
    vec4 tmpTans = vec4(blending * tmpOrig);
    vec4 tmpFinal = (weigths.y*tmpTans + (1-weigths.y)*tmpOrig);    
    vec3 ret = vec3(tmpFinal/tmpFinal.w);

    return ret;
}

vec3 LBSNormalCalc(vec3 normal, mat4 blending, vec4 weigths) {
    /*vec4 tmpOrig = vec4(normal, 1);
    vec4 tmpTans = vec4(transpose(inverse(blending)) * tmpOrig);
    vec4 tmpFinal = (weigths.y*tmpTans + (1-weigths.y)*tmpOrig);
    vec3 ret = vec3(tmpFinal/tmpFinal.w);*/
    vec3 ret = transpose(inverse(mat3(blending))) * normal;

    return normalize(ret);
}


