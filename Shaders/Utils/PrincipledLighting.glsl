





vec4 computePrincipled(ShadingData sd, vec3 radiance) {

    if (sd.light.type == AREALIGHT) {
        //caution, for the moment area lights only works with lambertian diffuse
        vec4 lightRadiance = vec4(radiance, 1.f);

        return lightRadiance;
    }

    vec4 diffuse = computeDiffuse(sd.fragment.normal, sd.lightDir, sd.cameraDir, sd.mat);

    vec4 specular = computeSpecular(sd.fragment.normal, sd.lightDir, sd.cameraDir, sd.mat);

    vec4 lightRadiance = vec4(radiance, 1.f);

    return (diffuse + specular) * lightRadiance;
}