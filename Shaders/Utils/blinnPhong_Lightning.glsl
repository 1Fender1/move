


const float pi = 3.14159265358979323846;
float calcAttenuation(float constant, float linear, float quadratic, float dist) {
    float attenuation = constant + linear * dist + quadratic * dist * dist;
    return 1.f / attenuation;
}


vec3 blinnPhong(Light light, vec3 direction, vec3 normal, Material mat, float lightIntensity) {
    vec3 specularColor = mat.specularColor;
    vec3 normalT = normalize(normal);
    vec3 diffuseColor = (mat.diffuseColor).rgb;
    vec3 ambientColor = mat.ambientColor;
    
    vec3 viewDir = normalize(camPos - v);
    vec3 lightDir = normalize(direction);

    float diff = max(dot(normalT, lightDir), 0.0);

    vec3 reflectDir = reflect(-lightDir, normalT);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    
    float visibility = 0.f;
    if (isShadowMap == 1)
        visibility = shadowCalcPCF(fragLightSpace, lightDir, normal);
    //float visibility = depthTest(fragLightSpace, normal, lightDir);

    vec3 ambient  = ambientColor;
    vec3 diffuse  = light.color * diff * diffuseColor * lightIntensity;
    vec3 specular = light.color * spec * specularColor * lightIntensity;

    return (/*ambient +*/ (1.f-visibility) * (diffuse + specular));
    //return vec3(1.f-(visibility));
}

vec3 pointLightning(Light light, Material mat, vec3 normalT) {
    
    vec3 direction = normalize(light.pointLight.position - v);
    float dist = length(light.pointLight.position - v);
    
    float attenuation = calcAttenuation(light.pointLight.attenuation.constant, light.pointLight.attenuation.linear, light.pointLight.attenuation.quadratic, dist);
    attenuation = 1.f;
    
    vec3 color = blinnPhong(light, direction, normalT, mat, /*1.f/(dist*dist)*/1.f);
    
    return color * attenuation;

}

vec3 dirLightning(Light dirLight, Material mat, vec3 normalT) {

    vec3 color = blinnPhong(dirLight, -dirLight.dirLight.direction, normal, mat, 1.f);

    return color;
}


vec3 spotLightning(Light light, Material mat, vec3 normalT) {

    vec3 fragToLight = normalize(light.spotLight.position - v);

    float inAngle = light.spotLight.iAngle;
    float outAngle = light.spotLight.oAngle;

    float theta = dot(fragToLight, normalize(-light.spotLight.direction));
    float epsilon = outAngle - inAngle;
    float intensity = clamp((theta - inAngle) / epsilon, 0.0, 1.0);
    

    float dist = length(light.spotLight.position - v);
    float attenuation = calcAttenuation(light.spotLight.attenuation.constant, light.spotLight.attenuation.linear, light.spotLight.attenuation.quadratic, dist);
    attenuation = 1.f;

    vec3 color = blinnPhong(light, fragToLight, normalT, mat, intensity);

    return color * attenuation;
}

vec3 computeBlinnPhong(Light light, Material mat, vec3 normalT) {

    if (light.type == POINTLIGHT)
        return pointLightning(light, mat, normalT);

    if (light.type == SPOTLIGHT)
        return spotLightning(light, mat, normalT);

    if (light.type ==  DIRLIGHT)
        return dirLightning(light, mat, normalT);

    return vec3(0,0,0);
}




