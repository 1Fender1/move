/*
===================================================
            Material
===================================================
*/

struct Material {
    vec3 ambientColor;
    vec4 diffuseColor;
    vec3 specularColor;
    vec3 emissiveColor;
    float transparency;
    float metalness;
    float ior;

    float roughness;
    float diffuseRoughness;

    int specularExp;

    int specType;
};

/*
===================================================
            Lights
===================================================
*/

const int POINTLIGHT = 0;
const int SPOTLIGHT = 1;
const int DIRLIGHT = 2;
const int AREALIGHT = 3;

struct Attenuation {
    float constant;
    float linear;
    float quadratic;
};


struct PointLight {
    vec3 position;
    Attenuation attenuation;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float iAngle;
    float oAngle;
    Attenuation attenuation;
};


struct DirLight {
    vec3 direction;
};

struct AreaLight {
    vec3 position;
    vec3 direction;
    Attenuation attenuation;
    float width;
    float height;
    int doubleSided;
};

struct Light {
    int type;
    vec3 color;
    float intensity;
    DirLight dirLight;
    SpotLight spotLight;
    PointLight pointLight;
    AreaLight areaLight;
};

struct Fragment {
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;
    vec2 uv;
};

struct ShadingData {

    Fragment fragment;
    Light light;
    Material mat;
    vec3 cameraDir;
    vec3 lightDir;
    vec4 lightSpacePosition;

};