
float roughnessMappingSquare(float diffuseRoughness) {
   return diffuseRoughness * diffuseRoughness;
}

float roughnessMappingNone(float diffuseRoughness) {
   return diffuseRoughness;
}


vec4 diffuseColorMetalAware(Material mat) {
   return (1.f-mat.metalness) * mat.diffuseColor;
}


vec4 lambertian(vec3 normal, vec3 lightDir, Material mat) {
   vec4 diffuseColor = diffuseColorMetalAware(mat);
   return diffuseColor * clamp(dot(normal, lightDir), 0.f, 1.f) / PI;
}

//Formulation from http://www.pbr-book.org/3ed-2018/Reflection_Models/Microfacet_Models.html
vec4 orenNayar(vec3 normal, vec3 lightDir, vec3 camDir, Material mat) {
   float alpha = roughnessMappingNone(mat.diffuseRoughness);
   float alpha2 = alpha * alpha;

   float A = 1.f - (alpha2/(2.f*(alpha2 + 0.33f)));
   float B = (0.45f * alpha2) / (alpha2 + 0.09f);

   float LdotV = dot(lightDir, camDir);
   float NdotL = dot(lightDir, normal);
   float NdotV = dot(normal, camDir);

   float s = LdotV - NdotL * NdotV;
   float t = mix(1.f, max(NdotL, NdotV), step(0.f, s));

   vec4 diffuseColor = diffuseColorMetalAware(mat);

   return diffuseColor * max(0.f, NdotL) * (A + B * s / t) / PI;
}

//OrenNayarDiffuse with diffuseRoughness = 0.f is a lambertian
vec4 computeDiffuse(vec3 normal, vec3 lightDir, vec3 camDir, Material mat) {
      return orenNayar(normal, lightDir, camDir, mat);
}