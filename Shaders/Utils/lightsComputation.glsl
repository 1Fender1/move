
#include "Utils/areaLightUtils.glsl"
#include "Utils/mathUtils.glsl"

float calcAttenuation(float constant, float linear, float quadratic, float dist) {
    float attenuation = constant + linear * dist + quadratic * dist * dist;
    float res = attenuation > 0.f ? (1.f / attenuation) : 1.f;
    return res;
}

vec3 pointLighting(ShadingData sd) {

    float dist = length(sd.light.pointLight.position - sd.fragment.position);

    float attenuation = calcAttenuation(sd.light.pointLight.attenuation.constant, sd.light.pointLight.attenuation.linear, sd.light.pointLight.attenuation.quadratic, dist);

    float visibility = 0.f;
    if (isShadowMap == 1)
        visibility = shadowCalcPCF(sd.lightSpacePosition, sd.lightDir, sd.fragment.normal);

    float NdotL = max(0.f, dot(sd.lightDir, sd.fragment.normal));
    vec3 radiance = NdotL * sd.light.color * sd.light.intensity * attenuation /** (1.f-visibility)*/;

    return radiance;
}

vec3 dirLighting(ShadingData sd) {

    float visibility = 0.f;
    if (isShadowMap == 1)
        visibility = shadowCalcPCF(sd.lightSpacePosition, sd.lightDir, sd.fragment.normal);

    float NdotL = max(0.f, dot(sd.lightDir, sd.fragment.normal));
    vec3 radiance = NdotL * light.color * visibility * sd.light.intensity;

    return radiance;
}


vec3 spotLighting(ShadingData sd) {

    vec3 fragToLight = normalize(light.spotLight.position - sd.fragment.position);

    float inAngle = cos(light.spotLight.iAngle);
    float outAngle = cos(light.spotLight.oAngle);

    float theta = dot(fragToLight, normalize(-light.spotLight.direction));
    float epsilon = inAngle-outAngle;
    float intensity = clamp((theta - inAngle) / epsilon, 0.f, 1.f);

    float dist = length(light.spotLight.position - sd.fragment.position);
    float attenuation = calcAttenuation(light.spotLight.attenuation.constant, light.spotLight.attenuation.linear, light.spotLight.attenuation.quadratic, dist);

    vec3 lightDir = sd.lightDir;
    float visibility = 0.f;
    if (isShadowMap == 1)
        visibility = shadowCalcPCF(sd.lightSpacePosition, lightDir, sd.fragment.normal);

    float NdotL = max(0.f, dot(lightDir, sd.fragment.normal));
    vec3 radiance = NdotL * light.color * visibility * attenuation * intensity * sd.light.intensity;

    return radiance;
}


vec3 areaLighting(ShadingData sd) {

    vec3 center = sd.light.areaLight.position;
    vec3 areaLightDir = sd.light.areaLight.direction;
    float width = sd.light.areaLight.width;
    float height = sd.light.areaLight.height;
    vec3 p0, p1, p2, p3;

    generateAreaLightVertex(center, areaLightDir, width, height, p3, p2, p1, p0);

    vec3 points[4] = vec3[](vec3(p0), vec3(p1), vec3(p2), vec3(p3));
    vec3 N = sd.fragment.normal;
    vec3 V = -normalize(sd.fragment.position - camPos);
   // vec3 V = -sd.cameraDir;

    float theta = acos(dot(N, V));
    vec2 uv = vec2(sd.mat.roughness, theta / (0.5f*PI));
    uv = uv * LUT_SCALE + LUT_BIAS;

    vec4 t = texture2D(areaMinV, uv);

    mat3 Minv = mat3(
        vec3(t.x, 0.f, t.y),
        vec3(0.f, t.z, 0.f),
        vec3(t.w, 0.f, 1.f)
    );

    Minv = inverse(Minv);

    bool doubleSided = sd.light.areaLight.doubleSided == 1;

    vec3 spec = LTC_Evaluate(N, V, sd.fragment.position, Minv, points, doubleSided);
    vec4 areaMagEval = texture2D(areaMag, uv);


    vec3 f0;
    if (sd.mat.metalness > 0.001f) {
        vec3 reflectance = (sd.mat.diffuseColor * sd.mat.metalness).rgb;
        f0 = 0.16f * reflectance * reflectance * (1.f - sd.mat.metalness) + sd.mat.diffuseColor.rgb * sd.mat.metalness;
    } else {
        f0 = vec3(IORtoF0(sd.mat.ior));
    }
    spec *= (areaMagEval.x * f0 + (1.f - f0) * areaMagEval.y*areaMagEval.x);

    vec3 diff = LTC_Evaluate(N, V, sd.fragment.position, mat3(1.f), points, doubleSided);
    diff *= (1.f-sd.mat.metalness) * sd.mat.diffuseColor.rgb;

    vec3 radiance = sd.light.color * (spec + diff) * sd.light.intensity;
    radiance /= (2.f * PI);

    return radiance;
}

vec3 computeLightRadiance(ShadingData sd) {

    if (sd.light.type == POINTLIGHT)
        return pointLighting(sd);

    if (sd.light.type == SPOTLIGHT)
        return spotLighting(sd);

    if (sd.light.type == DIRLIGHT)
        return dirLighting(sd);

    if (sd.light.type == AREALIGHT)
        return areaLighting(sd);

    return vec3(0.f);
}


vec3 getLightDir(ShadingData sd) {
    if (sd.light.type == POINTLIGHT)
        return normalize(sd.light.pointLight.position - sd.fragment.position);

    if (sd.light.type == SPOTLIGHT)
        return normalize(-sd.light.spotLight.direction);

    if (sd.light.type == DIRLIGHT)
        return normalize(-sd.light.dirLight.direction);

    if (sd.light.type == AREALIGHT)
        return -normalize(sd.fragment.position - camPos);

    return vec3(0.f);
}