
//UIT-R BT 601 recommandation
float rgb2luma(vec3 rgb) {
    return sqrt(dot(rgb, vec3(0.299, 0.587, 0.114)));
}


//UIT-R BT 709 recommandations
float rgb2luma709(vec3 rgb) {
    return sqrt(dot(rgb, vec3(0.2126, 0.75152, 0.0722)));
}
