mat4 lookAt(vec3 eye, vec3 at, vec3 up) {

    vec3 zaxis = normalize(eye - at);
    vec3 xaxis = normalize(cross(zaxis, up));
    vec3 yaxis = cross(xaxis, zaxis);

    zaxis = -zaxis;

    mat4 viewMatrix;
    viewMatrix[0] = vec4(xaxis.x, xaxis.y, xaxis.z, -dot(xaxis, eye));
    viewMatrix[1] = vec4(yaxis.x, yaxis.y, yaxis.z, -dot(yaxis, eye));
    viewMatrix[2] = vec4(zaxis.x, zaxis.y, zaxis.z, -dot(zaxis, eye));
    viewMatrix[3] = vec4(0.f, 0.f, 0.f, 1.f);


    return viewMatrix;
}