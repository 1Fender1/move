

const int SPEC_VOID = 0;
const int SPEC_BLINNPHONG = 1;
const int SPEC_GGX = 2;
const int SPEC_BECKMANN = 3;

float IORtoF0(float ior) {
    float temp = (1.f - ior) / (1.f + ior);
    return temp * temp;
}

float f0ToIOR(float f0) {
    return (2.f / (1.f-sqrt(f0))) - 1.f;
}

float roughnessMapingSquare(float roughness) {
    return roughness * roughness;
}


vec4 computeBlinnPhong(vec3 normal, vec3 lightDir, vec3 camDir, Material mat) {

    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(camDir, reflectDir), 0.f), 32);

    return vec4(spec * (1.f - mat.roughness) * vec4(mat.specularColor, 1.f));
}

//http://graphicrants.blogspot.com/2013/08/specular-brdf-reference.html && https://learnopengl.com/PBR/Theory

//Normal distrubution function
float DGGX(float NdotH, float a) {
    float a2 = a*a;
    float NdotH2 = NdotH*NdotH;

    float nom = a2;
    float temp = NdotH2 * (a2 - 1.f) + 1.f;
    temp = PI * temp * temp;

    return a2 / temp;
}

//Geometric function
float GeometrySchlickGGX(float NdotX, float k) {
    return NdotX / (NdotX * (1.f - k) + k);
}

float GeometrySmith(float NdotV, float NdotL, float k) {
    float ggx1 = GeometrySchlickGGX(NdotV, k);
    float ggx2 = GeometrySchlickGGX(NdotL, k);

    return ggx1 * ggx2;
}

float GeometryGGX(float NdotV, float NdotL, float k) {
    float G_V = NdotV + sqrt((NdotV - NdotV * k) * NdotV + k);
    float G_L = NdotL + sqrt((NdotL - NdotL * k) * NdotL + k);
    return G_V * G_L;
}

//Fresnel function, is Schick approximation
vec3 fresnel(float HdotV, vec3 f0) {
    return f0 + (1.f - f0) * pow(1.f - HdotV, 5.f);
}


vec4 computeGGX(vec3 normal, vec3 lightDir, vec3 camDir, Material mat) {

    float roughness = roughnessMapingSquare(mat.roughness);

    vec3 h = normalize(camDir + lightDir);
    float NdotH = max(0.f, dot(normal, h));
    float VdotH = max(0.f, dot(camDir, h));
    float NdotV = max(0.f, dot(normal, camDir));
    float NdotL = max(0.f, dot(normal, lightDir));

    vec3 f0;
    if (mat.metalness > 0.001f) {
        vec3 reflectance = (mat.diffuseColor * mat.metalness).rgb;
        f0 = 0.16f * reflectance * reflectance * (1.f - mat.metalness) + mat.diffuseColor.rgb * mat.metalness;
    } else {
        f0 = vec3(IORtoF0(mat.ior));
    }
    //f0 = vec3(0.04);
    //f0      = mix(f0, mat.diffuseColor.rgb, mat.metalness);


    float D = DGGX(NdotH, roughness);
    float G = GeometrySmith(NdotV, NdotL, roughness/2.f);
    vec3 F = fresnel(VdotH, f0);

    vec3 dfg = D * F * G;

    vec3 energieCompensation = vec3(1.f);// 1.f + f0 * (1.f / dfg.y - 1.f);

    vec4 result = vec4(dfg * energieCompensation, 1.f);

    return result;
}

vec4 computeGGX2(vec3 normal, vec3 lightDir, vec3 camDir, Material mat) {

    float roughness = roughnessMapingSquare(mat.roughness);

    vec3 h = normalize(camDir + lightDir);
    float NdotH = max(0.f, dot(normal, h));
    float VdotH = max(0.f, dot(camDir, h));
    float NdotV = max(0.f, dot(normal, camDir));
    float NdotL = max(0.f, dot(normal, lightDir));

    vec3 f0;
    if (mat.metalness > 0.001f) {
        vec3 reflectance = (mat.diffuseColor * mat.metalness).rgb;
        f0 = 0.16f * reflectance * reflectance * (1.f - mat.metalness) + mat.diffuseColor.rgb * mat.metalness;
    } else {
        f0 = vec3(IORtoF0(mat.ior));
    }

    float D = DGGX(NdotH, roughness);
    vec3 F = fresnel(VdotH, f0);
    float G = GeometryGGX(NdotV, NdotL, roughness/2.f);

    vec3 dfg = D * F * G;




    return vec4(dfg, 1.f);

}

vec4 computeSpecular(vec3 normal, vec3 lightDir, vec3 camDir, Material mat) {

    //TODO able to compute blinn-phong or GGX
    if (mat.specType == SPEC_GGX) {
        return computeGGX2(normal, lightDir, camDir, mat);
    }

    if (mat.specType == SPEC_BLINNPHONG) {
        return computeBlinnPhong(normal, lightDir, camDir, mat);
    }

    return vec4(0.f, 0.f, 0.f, 1.f);
}