

vec3 getSpecularColor(sampler2D tex, vec2 uv, int isTextured, Material mat) {

    vec3 specularColor = vec3(0.f);

    if (isTextured == 1)
        specularColor = texture(tex, uv).rgb;
    else
        specularColor = (mat.specType == SPEC_BLINNPHONG) ? mat.specularColor : vec3(1.f-mat.roughness);

    return specularColor;
}

vec4 getDiffuseColor(sampler2D tex, vec2 uv, int isTextured, Material mat) {

    vec4 diffuseColor = vec4(vec3(0.f), 1.f);

    if (isTextured == 1)
        diffuseColor = texture(tex, uv);
    else
        diffuseColor = mat.diffuseColor;

    return diffuseColor;
}

float getOcclusion(sampler2D tex, ivec2 uv, int isTextured) {

    float ambientOcc = 1.f;

    if (isTextured == 1)
        ambientOcc = texelFetch(tex, uv, 0).r;

    return ambientOcc;
}


vec3 getNormal(sampler2D tex, vec2 uv, int isTextured, mat3 TBN, vec3 geomNormal) {

    vec3 normalT = vec3(0.f);

    vec3 normalTexture = normalize(texture(tex, uv).rgb);
    if (isTextured == 1) {
        if (normalTexture.y == normalTexture.z && normalTexture.x > 0.f) {
            vec3 normalTexture = texture(tex, uv).rgb;
            float mipmapLevel = textureQueryLod(tex, uv).x;
            ivec2 texSize = textureSize(tex, int(0));
            vec2 invSize = vec2((1.f/float(texSize.x), 1.f/float(texSize.y)));

            float center = normalTexture.x;
            float xOffset = texture(tex, uv + vec2(texSize.x + invSize.x, 0.f)).x;
            float yOffset = texture(tex, uv + vec2(0.f, texSize.y + invSize.y)).x;

            normalT = vec3(xOffset-center, yOffset-center, 0.01f);
            normalT = normalize(normalT);

            normalT = normalize(TBN * normalT);
            //normalT = vec3(1.f, 0.f, 0.f);
        } else {
            vec3 normalTexture = normalize(texture(tex, uv).rgb);
            //normalT = normalize(texture(texture_normal1, texC).rgb);
            normalT = normalTexture;
            normalT = normalize(normalT * 2.0 - 1.0);

            normalT = normalize(TBN * normalT);
            //normalT = vec3(0.f, 0.f, 1.f);
        }
    } else {
        normalT = geomNormal;
        //normalT = vec3(0.f, 1.f, 0.f);
    }

    return normalT;

}