#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 textCoords;
layout (location = 3) in vec3 itangent;
layout (location = 4) in vec3 ibitangent;
layout (location = 5) in vec4 iweights;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lbsTransform;

out vec3 normal;
out vec3 positionFrag;
out vec3 v;
out vec2 texC;
out vec3 tangent;
out vec3 bitangent;
out mat3 TBN;

uniform int hasTextures;

#include "Utils/LBS.glsl"


void main() {
    mat4 blending = linearBlendingCalc(lbsTransform, iweights);
    vec3 pos = LBSPosCalc(position, blending, iweights);

    // Note that we read the multiplication from right to left
    gl_Position = projection * view * model * vec4(pos, 1.f);
    v = vec3(view * model * vec4(pos, 1.f));
    positionFrag = v;
    texC = textCoords;


    mat3 normalMatrix = mat3(transpose(inverse(model)));
    tangent = normalize(normalMatrix * itangent);
    normal = normalize(normalMatrix * inormal);
    bitangent = normalize(normalMatrix * ibitangent);
    bitangent = -bitangent;

    TBN = mat3(tangent, bitangent, normal);
}
