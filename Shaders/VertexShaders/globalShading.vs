#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 textCoords;
layout (location = 3) in vec3 itangent;
layout (location = 4) in vec3 ibitangent;
layout (location = 5) in vec4 iweights;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lbsTransform;
uniform mat4 lightSpaceMatrix;

out vec3 normal;
out vec3 positionFrag;
out vec3 v;
out vec2 texC;
out vec3 tangent;
out vec3 bitangent;
out mat3 TBN;
out vec4 weights;
out vec4 fragLightSpace;
out vec3 wPosFrag;
out mat4 mvp;

#include "Utils/LBS.glsl"


void main() {
    
    mat4 blending = linearBlendingCalc(lbsTransform, iweights);    


    vec3 pos = LBSPosCalc(position, blending, iweights);
    vec3 normalTemp = LBSNormalCalc(inormal, blending, iweights);
    //pos = position;
    normalTemp = inormal;
    // Note that we read the multiplication from right to left
    gl_Position = projection * view * model * vec4(pos, 1.f);
    v = vec3(model * vec4(pos, 1.f));

    mvp = projection * view * model;

    //v = (projection * view * model * vec4(pos, 1.0f)).xyz;
    positionFrag = pos;
    texC = textCoords;

    wPosFrag = v;

    mat3 normalMatrix = mat3(transpose(inverse(model)));
    tangent = normalize(normalMatrix * itangent);
    normal = normalize(normalMatrix * inormal);

    fragLightSpace = lightSpaceMatrix * model * vec4(position, 1.f);


    //bitangent = normalize(cross(normal, tangent));
    bitangent = normalize(normalMatrix * ibitangent);

    //if (dot(cross(normal, tangent), bitangent) < 0.f) {
       bitangent = -bitangent;
       //tangent = -tangent;
    //}

    TBN = mat3(tangent, bitangent, normal);

    weights = iweights;
}
