include(ExternalProject)


set(SUBMODULE_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/submodule)
# as CMAKE_INSTALL_PREFIX is set to MOVE_BUILD_DIRECTORY by default, we might also use it
set(SUBMODULE_INSTALL_DIRECTORY ${BUILD_DIRECTORY}/submodule)
set(SUBMODULE_BUILD_TYPE Release)

include(SUBGLM)
include(SUBASSIMP)
include(SUBOPENMESH)
include(SUBEMBREE)
#include(SUBOPENMP)

file(GLOB_RECURSE stb_headers "${CMAKE_CURRENT_SOURCE_DIR}/submodule/stb/*.h")
add_custom_target(stb_lib
        COMMAND ${CMAKE_COMMAND} -E make_directory ${SUBMODULE_INSTALL_DIRECTORY}/include/stb/
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${stb_headers} ${SUBMODULE_INSTALL_DIRECTORY}/include/stb/)
set(STB_INCLUDE_DIR ${SUBMODULE_INSTALL_DIRECTORY}/include/)



#
#
#

#file(GLOB_RECURSE imgui_src "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/*.cpp")

set(imgui_src 
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imconfig.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imgui_demo.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imgui_draw.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/examples/imgui_impl_glfw.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/examples/imgui_impl_glfw.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/examples/imgui_impl_opengl3.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/examples/imgui_impl_opengl3.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imgui_internal.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imgui_widgets.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imgui.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imgui.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imstb_rectpack.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imstb_textedit.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/submodule/imgui/imstb_truetype.h"
)



add_custom_target(imgui_lib
        COMMAND ${CMAKE_COMMAND} -E make_directory ${SRC_DIR}/Ui/imgui/
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${imgui_src} ${SRC_DIR}/Ui/imgui/)



#
#
#
