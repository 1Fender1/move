//
// Created by jb on 23/11/2020.
//

#include "Aabb.hpp"
#include <limits>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>

AABB::AABB() :
    min(m_bounds[0]),
    max(m_bounds[1]),
    m_min(m_bounds[0]),
    m_max(m_bounds[1]),
    m_bounds{glm::vec3(std::numeric_limits<float>::max()), glm::vec3(std::numeric_limits<float>::lowest())} {}

AABB::AABB(const glm::vec3& p1, const glm::vec3& p2) : AABB() {
    m_min = glm::vec3(std::min(p1.x, p2.x), std::min(p1.y, p2.y), std::min(p1.z, p2.z));
    m_max = glm::vec3(std::max(p1.x, p2.x), std::max(p1.y, p2.y), std::max(p1.z, p2.z));
}

AABB::AABB(const std::vector<Vertex>& vertices) : AABB() {

    float limitMin = std::numeric_limits<float>::lowest();
    float limitMax = std::numeric_limits<float>::max();

    float minX = limitMax, minY = limitMax, minZ = limitMax;
    float maxX = limitMin, maxY = limitMin, maxZ = limitMin;

    for (const auto& vertex : vertices) {
        const glm::vec3& p = vertex.position;
        float x = p.x, y = p.y, z = p.z;
        minX = minX > x ? x : minX;
        minY = minY > y ? y : minY;
        minZ = minZ > z ? z : minZ;

        maxX = maxX < x ? x : maxX;
        maxY = maxY < y ? y : maxY;
        maxZ = maxZ < z ? z : maxZ;
    }

    m_min = glm::vec3(minX, minY, minZ);
    m_max = glm::vec3(maxX, maxY, maxZ);
}

AABB::AABB(const AABB& box) : AABB() {
    m_min = box.m_min;
    m_max = box.m_max;
}

AABB::~AABB() {
    delete m_layoutMesh;
}

void AABB::drawLayout(Shader& shader) {

    if (!m_layoutMesh) {
        generateLayoutMesh();
        if (!m_layoutMesh)
            return;
    }

    m_layoutMesh->draw(shader);

}

void AABB::generateLayoutMesh() {

    if (m_layoutMesh)
        return;

    m_layoutMesh = new ObjectLine(BaseObjects::aabbBox(m_min , m_max));
    m_layoutMesh->setColor(glm::vec3(0.f, 0.f, 1.f));

}


bool AABB::isInside(const glm::vec3& p) const {

    return (p.x >= m_min.x && p.x <= m_max.x &&
            p.y >= m_min.y && p.y <= m_max.y &&
            p.z >= m_min.z && p.z <= m_max.z);

}

bool AABB::isOverlap(const AABB &box) const {
    bool x = (m_max.x >= box.m_min.x) && (m_min.x <= box.m_max.x);
    bool y = (m_max.y >= box.m_min.y) && (m_min.y <= box.m_max.y);
    bool z = (m_max.z >= box.m_min.z) && (m_min.z <= box.m_max.z);

    return (x && y && z);
}

AABB AABB::intersect(const AABB& box) const {


    glm::vec3 minT = glm::vec3(std::max(m_min.x, box.m_min.x),
                              std::max(m_min.y, box.m_min.y),
                              std::max(m_min.z, box.m_min.z));

    glm::vec3 maxT = glm::vec3(std::min(m_max.x, box.m_max.x),
                              std::min(m_max.y, box.m_max.y),
                              std::min(m_max.z, box.m_max.z));

    return AABB(minT, maxT);

}


glm::vec3& AABB::operator[](uint i) {
    return m_bounds[i];
}

glm::vec3 AABB::corner(uint i) const {
    return glm::vec3(m_bounds[(i & (uint)1)].x,
                     m_bounds[(i & (uint)2) ? 1 : 0].y,
                     m_bounds[(i & (uint)4) ? 1 : 0].z);
}

BoundingSphere AABB::boundingSphere() const {
    glm::vec3 center = (m_min + m_max) / 2.f;
    float radius = isInside(center) ? glm::length(center - m_max) : 0.f;
    return BoundingSphere(center, radius);
}