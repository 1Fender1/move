

#ifndef MOVE_AABB_HPP
#define MOVE_AABB_HPP
#include <Core/Types.hpp>
#include <Engine/Modeling/Mesh/ObjectLine.hpp>
#include <Core/System/Logger.hpp>
#include <Core/Math/BoundingVolumes/BoundingSphere.hpp>

class AABB {

public:
    AABB();
    AABB(const glm::vec3& p1, const glm::vec3& p2);
    explicit AABB(const std::vector<Vertex>& vertices);
    AABB(const AABB& box);

    AABB& operator=(const AABB& box) {

        if (&box == this)
            return *this;

        m_min = box.m_min;
        m_max = box.m_max;

        return *this;
    }

    glm::vec3& operator[](uint i);

    friend bool operator==(const AABB& b1, const AABB& b2) {
        return b1.m_min == b2.m_min && b1.m_max == b2.m_max;
    }

    friend bool operator!=(const AABB& b1, const AABB& b2) {
        return b1.m_min != b2.m_min || b1.m_max != b2.m_max;
    }

    friend AABB operator*(const glm::mat3& transform, const AABB& box) {
        float limitMin = std::numeric_limits<float>::lowest();
        float limitMax = std::numeric_limits<float>::max();
        glm::vec3 corners[8];
        float minX = limitMax, minY = limitMax, minZ = limitMax;
        float maxX = limitMin, maxY = limitMin, maxZ = limitMin;
        for (uint i = 0; i < 8; ++i) {
            corners[i] = transform * box.corner(i);
            float x = corners[i].x;
            float y = corners[i].y;
            float z = corners[i].z;
            minX = minX > x ? x : minX;
            minY = minY > y ? y : minY;
            minZ = minZ > z ? z : minZ;

            maxX = maxX < x ? x : maxX;
            maxY = maxY < y ? y : maxY;
            maxZ = maxZ < z ? z : maxZ;
        }
        return AABB(glm::vec3(minX, minY, minZ), glm::vec3(maxX, maxY, maxZ));
    }

    friend AABB operator*(const glm::mat4& transform, const AABB& box) {
        float limitMin = std::numeric_limits<float>::lowest();
        float limitMax = std::numeric_limits<float>::max();
        glm::vec3 corners[8];
        float minX = limitMax, minY = limitMax, minZ = limitMax;
        float maxX = limitMin, maxY = limitMin, maxZ = limitMin;

        for (uint i = 0; i < 8; ++i) {
            corners[i] = transform * glm::vec4(box.corner(i), 1.f);
            float x = corners[i].x;
            float y = corners[i].y;
            float z = corners[i].z;
            minX = minX > x ? x : minX;
            minY = minY > y ? y : minY;
            minZ = minZ > z ? z : minZ;

            maxX = maxX < x ? x : maxX;
            maxY = maxY < y ? y : maxY;
            maxZ = maxZ < z ? z : maxZ;
        }
        return AABB(glm::vec3(minX, minY, minZ), glm::vec3(maxX, maxY, maxZ));
    }

    friend AABB operator+(const AABB& b1, const AABB& b2) {
        glm::vec3 minT = glm::vec3(std::min(b1.m_min.x, b2.m_min.x),
                                   std::min(b1.m_min.y, b2.m_min.y),
                                   std::min(b1.m_min.z, b2.m_min.z));
        glm::vec3 maxT = glm::vec3(std::max(b1.m_max.x, b2.m_max.x),
                                   std::max(b1.m_max.y, b2.m_max.y),
                                   std::max(b1.m_max.z, b2.m_max.z));
        return AABB(minT, maxT);
    }

    AABB& operator+=(const AABB& b) {
        m_min = glm::vec3(std::min(m_min.x, b.m_min.x),
                                   std::min(m_min.y, b.m_min.y),
                                   std::min(m_min.z, b.m_min.z));
        m_max = glm::vec3(std::max(m_max.x, b.m_max.x),
                                   std::max(m_max.y, b.m_max.y),
                                   std::max(m_max.z, b.m_max.z));
        return *this;
    }

    ~AABB();

    void drawLayout(Shader& shader);

    bool isInside(const glm::vec3& p) const;

    bool isOverlap(const AABB& box) const;

    AABB intersect(const AABB& box) const;

    glm::vec3 corner(uint i) const;

    BoundingSphere boundingSphere() const;

    glm::vec3& min;
    glm::vec3& max;

private:

    void generateLayoutMesh();
    glm::vec3& m_min;
    glm::vec3& m_max;
    glm::vec3 m_bounds[2] = {glm::vec3(std::numeric_limits<float>::max()), glm::vec3(std::numeric_limits<float>::lowest())};

    ObjectLine* m_layoutMesh = nullptr;

};


#endif //MOVE_AABB_HPP
