//
// Created by jb on 23/11/2020.
//

#include "BoundingSphere.hpp"
#include <Engine/Modeling/Geometry/BaseObjects.hpp>


BoundingSphere::BoundingSphere(const std::vector<Vertex>& vertices, SphereType type) {
    if (type == SphereType::Barycenter)
        computeBarycenterMethod(vertices, m_center, m_radius);
    else
        computeMinimalVolumeMethod(vertices, m_center, m_radius);
}

BoundingSphere::BoundingSphere(const glm::vec3& center, float radius) {
    m_center = center;
    m_radius = radius;
}

BoundingSphere::BoundingSphere(const BoundingSphere& sphere) {
    m_center = sphere.m_center;
    m_radius = sphere.m_radius;
}

BoundingSphere::~BoundingSphere() {
    if (m_layoutMesh)
        delete m_layoutMesh;
}

void BoundingSphere::drawLayout(Shader& shader) {

    if (!m_layoutMesh) {
        generateLayoutMesh();
        if (!m_layoutMesh)
            return;
    }

    m_layoutMesh->draw(shader);

}

void BoundingSphere::computeBarycenterMethod(const std::vector<Vertex>& vertices, glm::vec3& center, float& radius) {

    uint vertexCount = vertices.size();
    float norm = 1.f/float(vertexCount);
    center = glm::vec3(0.f);
    for (const auto& vertex : vertices) {
        center += vertex.position * norm;
    }

    float maxRadius = 0.f;
    for (const auto& vertex : vertices) {
        float dist = glm::length(vertex.position - center);
        if (maxRadius < dist)
            maxRadius = dist;
    }
    radius = maxRadius;
}

void BoundingSphere::computeMinimalVolumeMethod(const std::vector<Vertex>& vertices, glm::vec3& center, float& radius) {

    LOG_ERROR("computeMinimalVolumeMethod not implemented yet")

}

void BoundingSphere::generateLayoutMesh() {

    if (m_layoutMesh)
        return;

    Polyline circle1 = BaseObjects::circleLine(m_center, glm::vec3(0.f, 1.f, 0.f), m_radius, 64);
    Polyline circle2 = BaseObjects::circleLine(m_center, glm::vec3(1.f, 0.f, 0.f), m_radius, 64);


    m_layoutMesh = new ObjectLine({circle1, circle2});

}

bool BoundingSphere::intersect(const BoundingSphere& sphere) {
    float dist = glm::length(m_center-sphere.m_center);
    return dist < m_radius+sphere.m_radius;
}

float BoundingSphere::getRadius() const {
    return m_radius;
}

const glm::vec3& BoundingSphere::getCenter() const {
    return m_center;
}