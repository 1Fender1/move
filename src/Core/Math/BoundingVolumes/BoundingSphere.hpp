//
// Created by jb on 23/11/2020.
//

#ifndef MOVE_BOUNDINGSPHERE_HPP
#define MOVE_BOUNDINGSPHERE_HPP
#include <Core/Math/Math.hpp>
#include <vector>
#include <Core/Types.hpp>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Modeling/Mesh/ObjectLine.hpp>


/*
 *
 * Caution test this class before use !!
 *
 */
class BoundingSphere {

public :

    enum SphereType {
        Barycenter,
        MinimalSphereVolume
    };


    BoundingSphere(const glm::vec3& center, float radius);
    BoundingSphere(const std::vector<Vertex>& vertices, SphereType type = SphereType::Barycenter);
    BoundingSphere(const BoundingSphere& sphere);
    ~BoundingSphere();

    BoundingSphere& operator==(const BoundingSphere& sphere) {

        if (&sphere == this)
            return *this;

        m_center = sphere.m_center;
        m_radius = sphere.m_radius;

        return *this;
    }

    friend BoundingSphere operator+(const BoundingSphere& sph1, const BoundingSphere& sph2) {

        float r1 = sph1.m_radius;
        float r2 = sph2.m_radius;
        const glm::vec3& c1 = sph1.m_center;
        const glm::vec3& c2 = sph2.m_center;

        if (c1 == c2)
            return BoundingSphere(c1, glm::max(r1, r2));

        float norm = glm::length(c1 - c2);

        float radius = (r1 + r2 + norm) / 2.f;
        glm::vec3 center = c1 + (c2-c1) * (radius - r1) / norm;

        return BoundingSphere(center, radius);
    }

    BoundingSphere& operator+=(const BoundingSphere& sph) {

        float r1 = m_radius;
        float r2 = sph.m_radius;
        const glm::vec3& c1 = m_center;
        const glm::vec3& c2 = sph.m_center;

        if (c1 == c2) {
            m_radius = glm::max(r1, r2);
            return *this;
        }

        float norm = glm::length(c1 - c2);

        m_radius = (r1 + r2 + norm) / 2.f;
        m_center = c1 + (c2-c1) * (m_radius - r1) / norm;

        return *this;
    }

    bool intersect(const BoundingSphere& sphere);
    void drawLayout(Shader& shader);

    float getRadius() const;
    const glm::vec3& getCenter() const;

private :

    void computeBarycenterMethod(const std::vector<Vertex>& vertices, glm::vec3& center, float& radius);
    void computeMinimalVolumeMethod(const std::vector<Vertex>& vertices, glm::vec3& center, float& radius);

    void generateLayoutMesh();

    glm::vec3 m_center;
    float m_radius;
    ObjectLine *m_layoutMesh = nullptr;



};


#endif //MOVE_BOUNDINGSPHERE_HPP
