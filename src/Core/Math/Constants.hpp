//
// Created by jb on 02/09/2020.
//

#ifndef MOVE_CONSTANTS_HPP
#define MOVE_CONSTANTS_HPP

#include <limits>

constexpr float epsilon = std::numeric_limits<float>::min();
constexpr float infinity = std::numeric_limits<float>::max();

#endif //MOVE_CONSTANTS_HPP
