//
// Created by jb on 06/04/2020.
//

#ifndef MOVE_TMAT_HPP
#define MOVE_TMAT_HPP

#include <glm/glm.hpp>
#include <iostream>

template<uint C, uint R, typename T>
class Tmat : public glm::mat<C, R, T> {

public :

    Tmat(T val) : glm::mat<C, R, T>(val) {}

    Tmat() : glm::mat<C, R, T>(0.f) {}

    friend std::ostream& operator<<(std::ostream& os, const Tmat<C, R, T>& mat) {

        os << "[";
        for (uint i = 0; i < R; ++i) {
            for (uint j = 0; j < C; ++j) {
                if (j < C - 1)
                    os << mat[i][j] << ", ";
                else
                    os << mat[i][j];
            }
            if (i < R-1)
                os << "\n ";
        }
        os << "]";

        return os;
    }

    Tmat<C, R, T> transpose() {
        *this = glm::transpose(*this);
        return *this;
    }




};



#endif //MOVE_TMAT_HPP
