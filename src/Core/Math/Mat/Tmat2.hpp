//
// Created by jb on 06/04/2020.
//

#ifndef MOVE_TMAT2_HPP
#define MOVE_TMAT2_HPP

#include <Core/Math/Mat/Tmat.hpp>

template <typename T>
class Tmat2 : public Tmat<2, 2, T> {

public :

    Tmat2() : Tmat<2, 2, T>() {}
    Tmat2(T val) : Tmat<2, 2, T>(val) {}


};




#endif //MOVE_TMAT2_HPP
