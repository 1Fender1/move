//
// Created by jb on 29/11/2020.
//

#ifndef MOVE_SAMPLERMT_HPP
#define MOVE_SAMPLERMT_HPP
#include <random>

template<typename T>
class SamplerMT {

public :
    SamplerMT(T min, T max, uint seed) {
        m_min = min;
        m_max = max;
        m_seed = seed;

        m_dist = std::uniform_real_distribution<T>(m_min, m_max);
        m_gen.seed(m_seed);
    }

    T rand() {
        return m_dist(m_gen);
    }

private :
    uint m_seed;
    T m_min;
    T m_max;

    std::random_device m_device;
    std::mt19937 m_gen;
    std::uniform_real_distribution<T> m_dist;

};


#endif //MOVE_SAMPLERMT_HPP
