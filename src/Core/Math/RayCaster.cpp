//
// Created by jb on 23/02/19.
//

#include "RayCaster.hpp"

//TODO very naive implementation, to modify
bool RayCaster::intersectRayToScene(std::vector<Object> *scene, glm::vec3 dir, glm::vec3 orig) {
    for (uint i = 0; i < scene->size(); i++) {
        for (uint j = 0; j < scene->at(i).getMeshCount(); j++) {
            Mesh* mesh = scene->at(i).getMeshAt(j);
            if (intersectRayToMesh(*mesh, dir, orig))
                return true;
        }
    }
    return false;
}

bool RayCaster::intersectRayTriangle(const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec3 &v3, const glm::vec3 &dir, const glm::vec3 &orig, glm::vec3& intersectionPoint) {
    glm::vec2 bary;
    float dist;
    intersectionPoint = glm::vec3(0.f);
    if (glm::intersectRayTriangle(orig, dir, v1, v2, v3, bary, dist)) {
        intersectionPoint = v1 * (1.f - bary.x - bary.y) + v2 * bary.x + v3 * bary.y;
        return true;
    }
    return false;
}

bool RayCaster::intersectRayToMesh(Mesh &mesh, glm::vec3 dir, glm::vec3 orig) {
    for (uint k = 0; k < mesh.getIndices().size(); k+=3) {
        glm::vec4 tmp = mesh.getModel(0) * glm::vec4(mesh.getVertices()[mesh.getIndices()[k]].position, 1.f);
        glm::vec3 v1 = glm::vec3(tmp[0], tmp[1], tmp[2]);
        tmp = mesh.getModel(0) * glm::vec4(mesh.getVertices()[mesh.getIndices()[k+1]].position, 1.f);
        glm::vec3 v2 = glm::vec3(tmp[0], tmp[1], tmp[2]);
        tmp = mesh.getModel(0) * glm::vec4(mesh.getVertices()[mesh.getIndices()[k+2]].position, 1.f);
        glm::vec3 v3 = glm::vec3(tmp[0], tmp[1], tmp[2]);
        glm::vec3 pHit;
         if (intersectRayTriangle(v1, v2, v3, dir, orig, pHit))
             return true;
    }
    return false;
}

bool RayCaster::intersectRayToMeshFull(Mesh& mesh, glm::vec3 dir, glm::vec3 orig, std::vector<glm::vec3>& hits) {
    for (uint k = 0; k < mesh.getIndices().size(); k+=3) {
        glm::vec4 tmp = mesh.getModel(0) * glm::vec4(mesh.getVertices()[mesh.getIndices()[k]].position, 1.f);
        glm::vec3 v1 = glm::vec3(tmp.x, tmp.y, tmp.z);
        tmp = mesh.getModel(0) * glm::vec4(mesh.getVertices()[mesh.getIndices()[k+1]].position, 1.f);
        glm::vec3 v2 = glm::vec3(tmp.x, tmp.y, tmp.z);
        tmp = mesh.getModel(0) * glm::vec4(mesh.getVertices()[mesh.getIndices()[k+2]].position, 1.f);
        glm::vec3 v3 = glm::vec3(tmp.x, tmp.y, tmp.z);
        glm::vec3 pHit;
        if (intersectRayTriangle(v1, v2, v3, dir, orig, pHit))
            hits.emplace_back(pHit);
    }
    return hits.size() > 0;
}

bool RayCaster::intersectRayToObject(Object& obj, glm::vec3 dir, glm::vec3 orig) {
    for (uint i = 0; i < obj.getMeshCount(); i++) {
        Mesh* mesh = obj.getMeshAt(i);
        if (intersectRayToMesh(*mesh, dir, orig))
            return true;
    }
    return false;
}

bool RayCaster::intersectRayToObjectFull(Object& obj, glm::vec3 dir, glm::vec3 orig, std::vector<glm::vec3>& hits) {
    for (uint i = 0; i < obj.getMeshCount(); i++) {
        Mesh *mesh = obj.getMeshAt(i);
        intersectRayToMeshFull(*mesh, dir, orig, hits);
    }

    return hits.size() > 0;
}

bool RayCaster::intersectionRayToPlane(glm::vec3 planeOrig, glm::vec3 normal, glm::vec3 &out, glm::vec3 dir, glm::vec3 orig) {
    float dist;
    if (glm::intersectRayPlane(orig, dir, planeOrig, normal, dist)) {
        out = orig + dist * dir;
        return true;
    }
    return false;
}

Object* RayCaster::nearestObject(std::vector<Object> *scene, glm::vec3 dir, glm::vec3 orig) {
    Object* obj = nullptr;
    for (uint i = 0; i < scene->size(); i++) {
        Object& object = scene->at(i);
        for (uint j = 0; j < object.getMeshCount(); j++) {
            Mesh* mesh = object.getMeshAt(j);
            std::vector<glm::vec3> hits;
            RayCaster::intersectRayToMeshFull(*mesh, dir, orig, hits);

            float dist = std::numeric_limits<float>::max();
            if (hits.size() > 0) {
                for (auto hit : hits) {
                    float len = glm::length(hit - orig);
                    if (dist > len) {
                        dist = len;
                        obj = &object;
                    }
                }
            }
        }
    }
    return obj;
}