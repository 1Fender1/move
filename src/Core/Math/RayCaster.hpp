//
// Created by jb on 23/02/19.
//

#ifndef MOVE_RAYCASTER_HPP
#define MOVE_RAYCASTER_HPP
#include <vector>
#include <Engine/Modeling/Mesh/Object.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>
#include <Engine/Camera/Camera.hpp>

class RayCaster {

public:

    //TODO generally when we want to know wich object is touch, we want to know wich instance of this object

    static bool intersectRayToScene(std::vector<Object> *scene, glm::vec3 dir, glm::vec3 orig);
    static bool intersectRayTriangle(const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec3 &v3, const glm::vec3 &dir, const glm::vec3 &orig, glm::vec3& ip);
    //only use if you want a quick check of is an object is hit, else use intersectRayToMeshFull
    static bool intersectRayToMesh(Mesh& mesh, glm::vec3 dir, glm::vec3 orig);
    static bool intersectRayToObject(Object& obj, glm::vec3 dir, glm::vec3 orig);

    static bool intersectRayToMeshFull(Mesh& mesh, glm::vec3 dir, glm::vec3 orig, std::vector<glm::vec3>& hits);
    static bool intersectRayToObjectFull(Object& obj, glm::vec3 dir, glm::vec3 orig, std::vector<glm::vec3>& hits);
    static bool intersectionRayToPlane(glm::vec3 pt, glm::vec3 normal, glm::vec3 &out, glm::vec3 dir, glm::vec3 orig);

    static Object* nearestObject(std::vector<Object> *scene, glm::vec3 dir, glm::vec3 orig);

};


#endif //MOVE_RAYCASTER_HPP
