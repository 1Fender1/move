//
// Created by jb on 10/04/19.
//

#include "Intersection.hpp"

Intersection::Intersection() {

    m_hit = false;
    m_objRef = nullptr;
    m_position = glm::vec3(infinity);
    m_normal = glm::vec3(0.f);
    m_tangent = glm::vec3(0.f);
    m_bitangent = glm::vec3(0.f);
}

Intersection::Intersection(Mesh* objRef, glm::vec3 position, glm::vec3 normal, glm::vec2 texCoord, glm::vec3 tangent = glm::vec3(0.f), glm::vec3 bitangent = glm::vec3(0.f)) {
    m_hit = false;
    m_objRef = nullptr;
    m_position = glm::vec3(infinity);
    m_normal = glm::vec3(0.f);
    m_texCoords = glm::vec2(0.f);
    m_tangent = glm::vec3(0.f);
    m_bitangent = glm::vec3(0.f);

    if (position != glm::vec3(infinity)) {
        m_hit = true;
        m_objRef = objRef;
        m_normal = normal;
        m_texCoords = texCoord;
        m_tangent = tangent;
        m_bitangent = bitangent;
        m_position = position;
    }

}


Intersection::~Intersection() {

}

bool Intersection::isHit() const {
    return m_hit;
}

Mesh *Intersection::getObjRef() const {
    return m_objRef;
}

const glm::vec3 &Intersection::getPosition() const {
    return m_position;
}

const glm::vec3 Intersection::getNormal() const {
    if (!m_objRef)
        return m_normal;

    Material* mat = m_objRef->getMaterial();

    glm::vec3 normal = glm::vec3(0.f);
    if (mat->hasNormalMap())
        normal = mat->getNormalMap()->getNormal(m_texCoords.x, m_texCoords.y, m_tangent, m_bitangent, m_normal, m_objRef->getModel());
    else
        normal = m_normal;

    return normal;
}
/*
glm::vec3 Intersection::getColor() {
    if (!m_objRef)
        return glm::vec3(0.f);

    glm::vec3 color = glm::vec3(0.f);
    if (m_objRef->isDiffuseMaped())
        color = m_objRef->getDiffuseTexture().getColor(m_texCoords.x, m_texCoords.y);
    else
        color = m_objRef->material->getDiffuseColor();

    return color;
}
*/
/*
float Intersection::getSpecularity() {
    if (!m_objRef)
        return 0.f;

    float specularity = 0.f;
    if (m_objRef->isDiffuseMaped())
        specularity = m_objRef->getSpecularTexture().getSpecularity(m_texCoords.x, m_texCoords.y);
    else
        specularity = m_objRef->material->getRoughness();

    return specularity;
}*/

const glm::vec3 Intersection::getColor() const {
    if (!m_objRef)
        return glm::vec3(0.f);

    Material* mat = m_objRef->getMaterial();

    glm::vec4 colorTemp = glm::vec4(0.f);
    if (mat->hasDiffuseMap())
        colorTemp = mat->getDiffuseMap()->getColor(m_texCoords.x, m_texCoords.y);
    else
        colorTemp = m_objRef->getMaterial()->getDiffuseColor();

    return glm::vec3(colorTemp.r, colorTemp.g, colorTemp.b);
}
float Intersection::getSpecularity() const {
    if (!m_objRef)
        return 0.99f;

    Material* mat = m_objRef->getMaterial();

    float specularity = 0.f;
    if (mat->hasSpecularMap())
        specularity = 1.f-mat->getSpecularMap()->getSpecularity(m_texCoords.x, m_texCoords.y);
    else
        specularity = m_objRef->getMaterial()->getRoughness();

    if (specularity <= 0.f)
        specularity = 0.001f;
    if (specularity >= 1.f)
        specularity = 1.f-0.001f;

    return specularity;
}

const glm::vec2 &Intersection::getTexCoords() const {
    return m_texCoords;
}

const glm::vec3 &Intersection::getTangent() const {
    return m_tangent;
}

const glm::vec3 &Intersection::getBiTangent() const {
    return m_bitangent;
}


