//
// Created by jb on 10/04/19.
//

#ifndef MOVE_INTERSECTION_HPP
#define MOVE_INTERSECTION_HPP
//#include <glm/glm.hpp>
#include <Engine/Modeling/Mesh/Mesh.hpp>
#include <Core/Math/Constants.hpp>

class Intersection {

public:
    Intersection(Mesh* objRef, glm::vec3 position, glm::vec3 normal, glm::vec2 texCoord, glm::vec3 tangent, glm::vec3 bitangent);
    Intersection();
    virtual ~Intersection();

    bool isHit() const;

    Mesh *getObjRef() const;

    const glm::vec3 &getPosition() const;

    const glm::vec3 getNormal() const;

    const glm::vec2 &getTexCoords() const;

    const glm::vec3 &getTangent() const;

    const glm::vec3 &getBiTangent() const;

    //glm::vec3 getColor();
    const glm::vec3 getColor() const;
    float getSpecularity() const;



private:

    bool m_hit;
    Mesh* m_objRef;
    glm::vec3 m_position;
    glm::vec3 m_normal;
    glm::vec3 m_tangent;
    glm::vec3 m_bitangent;
    glm::vec2 m_texCoords;

};


#endif //MOVE_INTERSECTION_HPP
