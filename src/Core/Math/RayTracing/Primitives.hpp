//
// Created by jb on 02/09/2020.
//

#ifndef MOVE_PRIMITIVES_HPP
#define MOVE_PRIMITIVES_HPP

#include <glm/glm.hpp>
#include <Core/Math/RayTracing/Ray.hpp>
#include <Core/Math/RayTracing/Intersection.hpp>
#include <Core/Math/Constants.hpp>

struct Plane {
    glm::vec3 orientation;
    glm::vec3 position;

    Plane(const glm::vec3& dir, const glm::vec3& pos) {
        orientation = glm::normalize(dir);
        position = pos;
    }
};

struct Box {

    glm::vec3 vMin;
    glm::vec3 vMax;

    Box(const glm::vec3& min, const glm::vec3& max) {
        vMin = min;
        vMax = max;
    }

};

struct Quad {

    glm::vec3 p0, p1, p2, p3;


    Quad(const glm::vec3& _p0, const glm::vec3& _p1, const glm::vec3& _p2, const glm::vec3& _p3) {
        p0 = _p0;
        p1 = _p1;
        p2 = _p2;
        p3 = _p3;
    }


};

struct Sphere {

    glm::vec3 position;
    float radius;

    Sphere(const glm::vec3& pos, float r) {
        position = pos;
        radius = r;
    }

};

struct Circle {
    glm::vec3 orientation;
    glm::vec3 position;
    float radius;

    Circle(const glm::vec3& dir, const glm::vec3& pos, float r) {
        orientation = glm::normalize(dir);
        position = pos;
        radius = (r >= 0.f) ? r : 0.f;
    }
};


//TODO test
void intersectPlaneLine(const Ray& ray, const Plane& plane, Intersection& intersection) {

    float dot = glm::dot(plane.orientation, ray.getDirection());
    const glm::vec3& n = (dot > 0.f) ? plane.orientation : -plane.orientation;

    const float denom = glm::dot(n, ray.getDirection());
    if (glm::abs(denom) <= epsilon) {
        intersection = Intersection();
    }

    const float t = glm::dot(plane.position - ray.getOrigin(), n) / denom;
    if (t >= 0.f) {
        intersection = Intersection(nullptr, ray.getOrigin() + ray.getDirection() * t, n, glm::vec2(0.f), glm::vec3(0.f), glm::vec3(0.f));
    } else {
        intersection = Intersection();
    }
}


void intersectCircleLine(const Ray&, const Circle& , Intersection& ) {
/*
    Plane f = Plane(circle.orientation, circle.position);
    Ray r = ray;

    Intersection outRF;
    intersectPlaneLine(r, f, outRF);
    if (outRF.isHit()) {
        // get the ray's intersection point on the plane which
        // contains the circle
        const glm::vec3 onPlane = outRF.getPosition();
        // project that point on to the circle's circumference
        glm::vec3 point = circle.position + circle.radius * glm::normalize(onPlane - circle.position);
        intersection = Intersection(nullptr, point, circle.orientation, glm::vec2(0.f), glm::vec3(0.f), glm::vec3(0.f));
    }
    else {
        // the required point on the circle is the one closest to the camera origin
        glm::vec3 point = circle.radius * glm::normalize(reject(ray.getOrigin() - circle.position, circle.orientation));

        return distance_between_point_ray(point, context.camera_ray);
    }
*/
}

bool solveQuadratic(float a, float b, float c, float& x0, float &x1)  {
    float discr = b * b - 4 * a * c;
    if (discr < 0) return false;
    else if (discr == 0) x0 = x1 = - 0.5 * b / a;
    else {
        float q = (b > 0) ?
                  -0.5 * (b + sqrt(discr)) :
                  -0.5 * (b - sqrt(discr));
        x0 = q / a;
        x1 = c / q;
    }
    if (x0 > x1) std::swap(x0, x1);

    return true;
}

void intersectSphere(Ray& ray, const Sphere& sphere, Intersection& intersection) {
    float t0, t1; // solutions for t if the ray intersects

    // analytic solution
    glm::vec3 L = ray.getOrigin() - sphere.position;
    float a = glm::dot(ray.getDirection(), ray.getDirection());
    float b = 2.f * glm::dot(ray.getDirection(), L);
    float c = glm::dot(L, L) - sphere.radius*sphere.radius;
    if (!solveQuadratic(a, b, c, t0, t1)) return;

    if (t0 > t1) std::swap(t0, t1);

    if (t0 < 0) {
        t0 = t1; // if t0 is negative, let's use t1 instead
        if (t0 < 0) return; // both t0 and t1 are negative
    }

    glm::vec3 position = ray.getOrigin() + ray.getDirection() * t0;
    glm::vec3 normal = glm::normalize(position - sphere.position);
    glm::vec2 texCoord = glm::vec2(normal.x, normal.y); // TODO verify

    intersection = Intersection(nullptr, position, normal, texCoord, glm::vec3(0.f), glm::vec3(0.f));
}

void intersectBox(const Ray& ray, const Box& box, Intersection& intersection) {

    float tmin, tmax, tymin, tymax, tzmin, tzmax;

    const glm::vec3& rayDir = ray.getDirection();
    const glm::vec3 invDir = 1.f/rayDir;
    int signX = invDir.x < 0.f;
    int signY = invDir.y < 0.f;
    int signZ = invDir.z < 0.f;
    glm::vec3 bounds[] = {box.vMin, box.vMax};


    tmin = (bounds[signX].x - ray.getOrigin().x) * invDir.x;
    tmax = (bounds[1-signX].x - ray.getOrigin().x) * invDir.x;
    tymin = (bounds[signY].y - ray.getOrigin().y) * invDir.y;
    tymax = (bounds[1-signY].y - ray.getOrigin().y) * invDir.y;

    if ((tmin > tymax) || (tymin > tmax))
        return;
    if (tymin > tmin)
        tmin = tymin;
    if (tymax < tmax)
        tmax = tymax;

    tzmin = (bounds[signZ].z - ray.getOrigin().z) * invDir.z;
    tzmax = (bounds[1-signZ].z - ray.getOrigin().z) * invDir.z;

    if ((tmin > tzmax) || (tzmin > tmax))
        return;
    if (tzmin > tmin)
        tmin = tzmin;
    if (tzmax < tmax)
        tmax = tzmax;

    glm::vec3 pos = ray.getOrigin() + ray.getDirection() * tmax;
    glm::vec3 normal = glm::vec3(0.f); //TODO compute normal
    intersection = Intersection(nullptr, pos, normal, glm::vec2(0.f), glm::vec3(0.f), glm::vec3(0.f));
}



#endif //MOVE_PRIMITIVES_HPP
