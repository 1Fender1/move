//
// Created by jb on 07/04/19.
//

#include "Ray.hpp"

Ray::Ray() {
    m_origin = glm::vec3(0.f);
    m_direction = glm::vec3(0.f);
}

Ray::Ray(const glm::vec3& orig, const glm::vec3& direction) {
    m_origin = orig;
    m_direction = direction;
}


const glm::vec3 &Ray::getOrigin() const {
    return m_origin;
}

const glm::vec3 &Ray::getDirection() const {
    return m_direction;
}


glm::vec3 Ray::posAt(float t) const {
    return m_origin + t * getDirection();
}