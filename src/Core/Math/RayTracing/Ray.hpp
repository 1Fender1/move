//
// Created by jb on 07/04/19.
//

#ifndef MOVE_RAY_HPP
#define MOVE_RAY_HPP

#include <glm/glm.hpp>

class Ray {

public :
    Ray();
    Ray(const glm::vec3& orig, const glm::vec3& direction);

    const glm::vec3 &getOrigin() const;
    const glm::vec3 &getDirection() const;
    glm::vec3 posAt(float t) const;

private :
    glm::vec3 m_origin;
    glm::vec3 m_direction;


};


#endif //MOVE_RAY_HPP
