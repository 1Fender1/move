//
// Created by jb on 02/08/2020.
//

#ifndef MOVE_UTILS_HPP
#define MOVE_UTILS_HPP
#include <glm/glm.hpp>

namespace Core {

    namespace Math {

        namespace Utils {

            static glm::mat3 rotationMatFromDirections(const glm::vec3 baseDirection, const glm::vec3 newDirection) {

                float rotAngle = glm::dot(baseDirection, newDirection)/(glm::length(baseDirection) * glm::length(newDirection));


                if (baseDirection == newDirection)
                    return glm::mat3(1.f);

                glm::vec3 axis = glm::normalize(glm::cross(baseDirection, newDirection));

                glm::mat rmat = glm::mat4(1.f);
                rmat = glm::rotate(rmat, glm::acos(rotAngle), axis);

                return rmat;
            }

            static void decompose(glm::mat4 mat, glm::vec3& translation, glm::vec3& scale, glm::mat4& rotation) {
                translation = mat[3];

                float scaleX = glm::length(mat[0]);
                float scaleY = glm::length(mat[1]);
                float scaleZ = glm::length(mat[2]);

                scale = glm::vec3(scaleX, scaleY, scaleZ);

                rotation = glm::mat4(1.f);

                rotation[0] = mat[0] / scaleX;
                rotation[1] = mat[1] / scaleY;
                rotation[2] = mat[2] / scaleZ;


            }


        }

    }


}



#endif //MOVE_UTILS_HPP
