//
// Created by jb on 03/04/2020.
//

//This file conntain Tvec class, base class for vectors. And operator<< functions for glm vectors


#ifndef MOVE_TVEC_HPP
#define MOVE_TVEC_HPP

#define GLM_FORCE_SWIZZLE
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_SWIZZLE GLM_SWIZZLE_XYZW
#include <glm/glm.hpp>
#include <iostream>


template<uint N, typename T>
class Tvec : public glm::vec<N, T, glm::qualifier::defaultp> {

public :


    Tvec() : glm::vec<N, T>() {}

    Tvec(const T& value) : glm::vec<N, T>(value, value) {}

    Tvec(const Tvec<N, T>& vec) : glm::vec<N, T>(vec) {}


    friend std::ostream& operator<<(std::ostream& os, const Tvec<N, T>& vec) {

        os << "(";
        for (uint i = 0; i < N; ++i) {
            if (i < N-1)
                os << vec.data.data[i] << ", ";
            else
                os << vec.data.data[i];
        }
        os << ")";

        return os;
    }


    T dot(const Tvec<N, T>& vec) {
        const Tvec<N, T>& vec0 = *this;
        return glm::detail::compute_dot<glm::vec<N, T, glm::qualifier::defaultp>, T, glm::detail::is_aligned<glm::qualifier::defaultp>::value>::call(vec0, vec);
    }


private :


};

namespace glm {
    //TODO find why it don't work with template
    inline std::ostream &operator<<(std::ostream &out, const glm::vec2 &g) {
        return out << "(" << g.x << ", " << g.y << ")";
    }
    inline std::ostream &operator<<(std::ostream &out, const glm::vec3 &g) {
        return out << "(" << g.x << ", " << g.y << ", " << g.z << ")";
    }
    inline std::ostream &operator<<(std::ostream &out, const glm::vec4 &g) {
        return out << "(" << g.x << ", " << g.y << ", " << g.z << ", " << g.w << ")";
    }
    inline std::ostream &operator<<(std::ostream &out, const glm::ivec2 &g) {
        return out << "(" << g.x << ", " << g.y << ")";
    }
    inline std::ostream &operator<<(std::ostream &out, const glm::ivec3 &g) {
        return out << "(" << g.x << ", " << g.y << ", " << g.z << ")";
    }
    inline std::ostream &operator<<(std::ostream &out, const glm::ivec4 &g) {
        return out << "(" << g.x << ", " << g.y << ", " << g.z << ", " << g.w << ")";
    }
}


#endif //MOVE_TVEC_HPP
