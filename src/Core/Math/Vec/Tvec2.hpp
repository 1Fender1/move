//
// Created by jb on 03/04/2020.
//

#ifndef MOVE_TVEC2_HPP
#define MOVE_TVEC2_HPP

#include <iostream>
#include <Core/Math/Vec/Tvec.hpp>
#include <type_traits>
#include <utility>

template<typename T>
class Tvec2 : public Tvec<2, T> {

public :

    Tvec2() {
        this->x = 0.f;
        this->y = 0.f;
    }

    Tvec2(const T& v1, const T& v2) {
        this->x = v1;
        this->y = v2;
    }

    Tvec2(const T& v) : Tvec2<T>(v, v) {}

    Tvec2(const Tvec2<T>& vec) : Tvec<2, T>(vec) {
        this->x = vec.x;
        this->y = vec.y;
    }

    Tvec2(const glm::vec<2, T>& vec) {
        this->x = vec.x;
        this->y = vec.y;
    }

    Tvec2<T>& normalize() {
        Tvec2<T> res = glm::normalize(glm::vec<2, T>(this->x, this->y));
        this->x = res.x;
        this->y = res.y;
        return *this;
    }


    Tvec2<T> cross(const Tvec2<T>& vec) {
        const Tvec2<T>& vec0 = *this;
        Tvec2<T> res = glm::cross(*this, vec);
        return res;
    }


private :



};




#endif //MOVE_TVEC2_HPP
