//
// Created by jb on 03/04/2020.
//

#ifndef MOVE_TVEC3_HPP
#define MOVE_TVEC3_HPP


#include <iostream>
#include <Core/Math/Vec/Tvec.hpp>

template<typename T>
class Tvec3 : public Tvec<3, T> {

public :

    Tvec3() {
        this->x = 0.f;
        this->y = 0.f;
        this->z = 0.f;
    }

    Tvec3(const T &v1, const T &v2, const T &v3) {
        this->x = v1;
        this->y = v2;
        this->z = v3;
    }


    Tvec3(const Tvec<2, T> &vec, const T &v) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = v;
    }

    Tvec3(const T &v, const Tvec<2, T> &vec) {
        this->x = v;
        this->y = vec.x;
        this->z = vec.y;
    }

    Tvec3(const T &v) : Tvec3<T>(v, v, v) {}

    Tvec3(const Tvec3<T> &vec) : Tvec<3, T>(vec) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
    }

    Tvec3(const glm::vec<3, T> &vec) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
    }

    Tvec3<T>& normalize() {
        Tvec3<T> res = glm::normalize(glm::vec<3, T>(this->x, this->y, this->z));
        this->x = res.x;
        this->y = res.y;
        this->z = res.z;
        return this;
    }

    Tvec3& operator=(const Tvec3<T>& vec) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
        return *this;
    }

    Tvec3& operator=(const glm::vec<3, T>& vec) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
        return *this;
    }

    Tvec3<T> cross(const Tvec3<T>& vec) {
        const Tvec3<T>& vec0 = *this;
        Tvec3<T> res = glm::cross(*this, vec);
        return res;
    }

/*
    friend Tvec3<T> operator*(const Tvec3<T>& a, const Tvec3<T>& b) {
        return Tvec3<T>(glm::operator*(a, b));
    }
*/

private :

};


#endif //MOVE_TVEC3_HPP
