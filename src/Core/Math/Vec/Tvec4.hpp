//
// Created by jb on 03/04/2020.
//

#ifndef MOVE_TVEC4_HPP
#define MOVE_TVEC4_HPP


#include <iostream>
#include <Core/Math/Vec/Tvec.hpp>

template<typename T>
class Tvec4 : public Tvec<4, T> {

public :

    Tvec4() {
        this->x = 0.f;
        this->y = 0.f;
        this->z = 0.f;
        this->w = 0.f;
    }

    Tvec4(const T &v1, const T &v2, const T &v3, const T& v4) {
        this->x = v1;
        this->y = v2;
        this->z = v3;
        this->w = v3;
    }

    Tvec4(const Tvec<3, T> &vec, const T &v) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
        this->w = v;
    }

    Tvec4(const T &v, const Tvec<3, T> &vec) {
        this->x = v;
        this->y = vec.x;
        this->z = vec.y;
        this->w = vec.z;
    }

    Tvec4(const Tvec<2, T> &vec1, const Tvec<2, T> &vec2) {
        this->x = vec1.x;
        this->y = vec1.y;
        this->z = vec2.x;
        this->w = vec2.y;
    }

    Tvec4(const Tvec<2, T> &vec, const T& v1, const T& v2) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = v1;
        this->w = v2;
    }

    Tvec4(const T& v1, const T& v2, const Tvec<2, T> &vec) {
        this->x = v1;
        this->y = v2;
        this->z = vec.x;
        this->w = vec.y;
    }

    Tvec4(const T& v1, const Tvec<2, T> &vec, const T& v2) {
        this->x = v1;
        this->y = vec.x;
        this->z = vec.y;
        this->w = v2;
    }


    Tvec4(const T &v) : Tvec4<T>(v, v, v, v) {}

    Tvec4(const Tvec4<T> &vec) : Tvec<4, T>(vec) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
        this->w = vec.w;
    }

    Tvec4(const glm::vec<4, T> &vec) {
        this->x = vec.x;
        this->y = vec.y;
        this->z = vec.z;
        this->w = vec.w;
    }

    Tvec4<T>& normalize() {
        Tvec4<T> res = glm::normalize(glm::vec<4, T>(this->x, this->y, this->z, this->w));
        this->x = res.x;
        this->y = res.y;
        this->z = res.z;
        this->w = res.w;
        return *this;
    }



private :

};

#endif //MOVE_TVEC4_HPP
