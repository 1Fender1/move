#ifndef DUALQUATERNION_HPP
#define DUALQUATERNION_HPP

//#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/quaternion.hpp>
//#include <glm/gtx/quaternion.hpp>
//#include <glm/gtx/dual_quaternion.hpp>
#include <glm/glm.hpp>
//#include <glm/ext.hpp>

class DualQuaternion
{
public:
    DualQuaternion();
    DualQuaternion(glm::quat q1, glm::quat q2);
    float dot(DualQuaternion qd1);
    void normalize();
    void conjugate();

    glm::quat rotation();
    glm::quat transform();
    glm::vec3 getTranslation();


    DualQuaternion operator*(DualQuaternion& dq1);
    DualQuaternion operator+(DualQuaternion& dq1);


private:
    glm::quat real;
    glm::quat dual;

};

#endif // DUALQUATERNION_H
