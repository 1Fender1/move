#include "dualquaternion.hpp"

inline DualQuaternion::DualQuaternion() {
    real = glm::quat();
    dual = glm::quat();

}


inline DualQuaternion::DualQuaternion(glm::quat r, glm::quat i) {
    real = glm::normalize(r);
    dual = i;
}

//TODO
//inline DualQuaternion::DualQuaternion(glm::quat r, glm::vec3 t) {
   /* real = glm::normalize(r);
    dual = glm::quat();
    glm::slerp()*/
//}

inline float DualQuaternion::dot(DualQuaternion qd1) {
    return glm::dot(real, dq2.rotation());
}

DualQuaternion DualQuaternion::operator*(DualQuaternion& dq1) {
    return DualQuaternion(real * dq1.rotation(), dual * dq1.rotation() + real * dq1.transform());
}

DualQuaternion DualQuaternion::operator+(DualQuaternion& dq1) {
    return DualQuaternion(dq1.rotation() + real, dq1.transform(), dual);
}

void DualQuaternion::conjugate() {
    glm::conjugate(real);
    glm::conjugate(dual);
}

//?
inline void DualQuaternion::normalize() {
   /* float mag = glm::dot(real, real);
    m_real *= 1.0f / mag;
    m_dual *= 1.0f / mag;
    */
    //return glm::sqrt(real.x*dual.x + real.y*dual.y + real.z*dual.z + real.w*dual.w);
}

glm::quat DualQuaternion::rotation() {
    return real;
}

glm::quat DualQuaternion::transform() {
    return dual;
}

glm::vec3 DualQuaternion::getTranslation() {
    glm::quat t = (dual * 2.0f) * glm::conjugate(real);
    return glm::vec3(t.x, t.y, t.z);
}
