
#include "Logger.hpp"

std::mutex Log::m_mutex;
const Flag Log::FLAGS_ALL = flagAll();
const Flag Log::FLAGS_NONE = 0;
Flag Log::m_flag = FLAGS_ALL;

const Flag Log::FLAG_DEBUG = Log::flags[Log::LogLevel::Debug];
const Flag Log::FLAG_INFORMATION = Log::flags[Log::LogLevel::Information];
const Flag Log::FLAG_WARNING = Log::flags[Log::LogLevel::Warning];
const Flag Log::FLAG_ERROR = Log::flags[Log::LogLevel::Error];
const Flag Log::FLAG_FATAL = Log::flags[Log::LogLevel::Fatal];

String Log::logHeader(LogLevel level, const char* file, int line) {

    String str = LogLevelStr[level];
    String color = m_colorsLinux[level];
    String fileName = Core::String::getFileName(file);
    String out;
    if (level == LogLevel::Debug)
         out = color + str + " [" + fileName + ":" + std::to_string(line) + "] : " + returnDefaultLinux;
    else
        out = color + str + " : " + returnDefaultLinux;

    return out;
}

std::ostream& Log::getOutput(LogLevel level) {

    bool errorOut = (level == LogLevel::Error || level == LogLevel::Fatal);

    return errorOut ? std::cerr : std::cout;
}

constexpr Flag Log::flagAll() {
    Flag flag = 0;
    for (uint i = 0; i < LogLevel::LOG_LEVEL_COUNT; ++i)
        flag |= flags[i];

    return flag;
}

void Log::setFilter(Flag flag) {
    m_flag = flag;
}

void Log::resetFilters() {
    m_flag = FLAGS_ALL;
}

void Log::shutDownLogs() {
    m_flag = FLAGS_NONE;
}

bool Log::is(LogLevel level) {
    return m_flag & flags[level];
}