//
// Created by jb on 31/10/2020.
//

#ifndef MOVE_LOGGER_HPP
#define MOVE_LOGGER_HPP

#include <iostream>
#include <string>
#include <cstdio>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/io.hpp>
#include <glm/glm.hpp>
#include <mutex>
#include <Core/Types.hpp>
#include <sstream>

typedef uint Flag;

class Log {

public :
    enum LogLevel {
        Debug,
        Information,
        Warning,
        Error,
        Fatal,
        LOG_LEVEL_COUNT
    };

    static constexpr Flag flags[] = {

            uint(1) << LogLevel::Debug,
            uint(1) << LogLevel::Information,
            uint(1) << LogLevel::Warning,
            uint(1) << LogLevel::Error,
            uint(1) << LogLevel::Fatal

    };

    static constexpr char *LogLevelStr[]{
            (char*)"Debug",
            (char*)"Information",
            (char*)"Warning",
            (char*)"Error",
            (char*)"Fatal"
    };

    static const Flag FLAG_DEBUG;
    static const Flag FLAG_INFORMATION;
    static const Flag FLAG_WARNING;
    static const Flag FLAG_ERROR;
    static const Flag FLAG_FATAL;

private :
    template <typename Fun>
    static void iteratePack(const Fun&) {}

    template <typename Fun, typename Arg, typename ... Args>
    static void iteratePack(const Fun &fun, Arg &&arg, Args&& ... args) {
        fun(std::forward<Arg>(arg));
        iteratePack(fun, std::forward<Args>(args)...);
    }

    template<typename ... Args>
    static String parseMessage(const char* str, Args ... args) {
        String logStr = String(str);
        size_t prevPos = 0;
        iteratePack([&](auto &arg) {
            std::stringstream stream;
            size_t currentPos = logStr.find("%n", prevPos);
            if (currentPos != std::string::npos) {
                stream << arg;
                logStr.erase(logStr.begin() + currentPos, logStr.begin() + currentPos + 2);
                logStr.insert(currentPos, stream.str());
                prevPos = currentPos;
            }
        }, args...);

        return logStr;
    }


public:


    template<typename ... Args>
    static void log(const char* str, LogLevel level, const char* file, int line, Args ... args) {
        if (!is(level))
            return;

        String outMsg = parseMessage(str, args...);

        m_mutex.lock();
        getOutput(level) << logHeader(level, file, line) << outMsg << std::endl;
        m_mutex.unlock();

       // delete [] logRes;
    }

    static void setFilter(Flag flag);
    static void resetFilters();
    static void shutDownLogs();

private:

    static String logHeader(LogLevel level, const char* file, int line);
    static std::ostream& getOutput(LogLevel level);
    static constexpr Flag flagAll();
    static bool is(LogLevel);

    static std::mutex m_mutex;
    static Flag m_flag;
    static const Flag FLAGS_ALL;
    static const Flag FLAGS_NONE;



    //TODO do it for windows ?
    static constexpr char* m_colorsLinux[] {
        (char*)"\e[1m \e[95m",
        (char*)"\e[1m \e[94m",
        (char*)"\e[1m \e[93m",
        (char*)"\e[1m \e[91m",
        (char*)"\e[1m \e[91m"
    };
    static constexpr char* returnDefaultLinux = (char*)"\e[0m";
};


#define LOG_IMPL(fmt, lvl, file, line, ...) Log::log(fmt, lvl, file, line, ##__VA_ARGS__);

#define LOG(fmt, ...) LOG_IMPL(fmt, Log::LogLevel::Information, __FILE__, __LINE__, ##__VA_ARGS__)
#define LOG_DEBUG(fmt, ...) LOG_IMPL(fmt, Log::LogLevel::Debug, __FILE__, __LINE__, ##__VA_ARGS__)
#define LOG_WARNING(fmt, ...) LOG_IMPL(fmt, Log::LogLevel::Warning, __FILE__, __LINE__, ##__VA_ARGS__)
#define LOG_ERROR(fmt, ...) LOG_IMPL(fmt, Log::LogLevel::Error, __FILE__, __LINE__, ##__VA_ARGS__)
#define LOG_FATAL(fmt, ...) LOG_IMPL(fmt, Log::LogLevel::Fatal, __FILE__, __LINE__, ##__VA_ARGS__)

#endif //MOVE_LOGGER_HPP
