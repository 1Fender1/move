//
// Created by jb on 07/04/2020.
//

#include "SystemParser.hpp"
#include "Logger.hpp"
#include <cstdlib>


SystemParser::SystemParser() {
    m_currentDir = getHome();
}

SystemParser::SystemParser(const String& path) {
    m_currentDir = path;
}

String SystemParser::getHome() {
    //TODO Implement more generic method

    String home = std::getenv("HOME");

//    char path[PATH_MAX];
    //realpath(home.c_str(), path);
    String absolutePath = String(home);

    return absolutePath;
}

void SystemParser::setCurrentPath(const String& newPath) {
    m_currentDir = newPath;
}

String SystemParser::getCurrentPath() {
    return m_currentDir;
}

void SystemParser::open() {
    m_directory = opendir(m_currentDir.c_str());
}
void SystemParser::close() {
    closedir(m_directory);
    m_directory = nullptr;
}

void SystemParser::listCurrentPath(std::vector<File>& files) {
    open();
    if (!m_directory) {
        LOG_ERROR("Directory : %n cannot be open", m_currentDir)
        return;
    }

    struct dirent* file = nullptr;

    while ((file = readdir(m_directory))) {
        char path[PATH_MAX];
        char* res = realpath(file->d_name, path);
        if (res == nullptr) {
            LOG_WARNING("Absolute path cannot be found %n", path)
        }
        //String absolutePath = String(path);
        String absolutePath = m_currentDir + "/" + String(file->d_name);
        String localPath = String(file->d_name);


        bool is_directoy = file->d_type == DT_DIR;

        if (file->d_type != DT_DIR && file->d_type != DT_REG)
            continue;

        if (localPath == "." || localPath == "..")
            continue;

        bool isHide = localPath[0] == '.';


        files.emplace_back(File(absolutePath, is_directoy, isHide));
    }

    close();
}