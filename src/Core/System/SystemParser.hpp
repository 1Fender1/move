//
// Created by jb on 07/04/2020.
//

#ifndef MOVE_SYSTEMPARSER_HPP
#define MOVE_SYSTEMPARSER_HPP

#include <Core/Types.hpp>
#include <vector>
#include <dirent.h>

class SystemParser {

public :

    SystemParser();
    SystemParser(const String& path);
    String getHome();
    void setCurrentPath(const String& newPath);
    String getCurrentPath();
    void listCurrentPath(std::vector<File>& files);



private:

    void open();
    void close();

    String m_currentDir;
    DIR* m_directory = nullptr;

};


#endif //MOVE_SYSTEMPARSER_HPP
