//
// Created by jb on 28/10/2020.
//

#ifndef MOVE_CHRONO_HPP
#define MOVE_CHRONO_HPP

#include <chrono>
#include <iostream>
#include <Core/System/Logger.hpp>

typedef std::chrono::time_point<std::chrono::steady_clock> TimePoint;
typedef std::chrono::duration<float> Duration;

class Chrono {

public :

    Chrono() {
        m_isMesuring = false;
    }

    inline void start() {
        m_isMesuring = true;
        m_start = now();
    }

    //Return time in second
    inline float finish() {
        TimePoint end = now();

        if (!m_isMesuring) {
            LOG_ERROR("Chrono : finish function was called before start function")
            return -1.f;
        }

        Duration diff = end - m_start;
        m_isMesuring = false;
        return diff.count();
    }

private :

    inline TimePoint now() {
        return std::chrono::steady_clock::now();
    }

    TimePoint m_start;
    bool m_isMesuring = false;


};


#endif //MOVE_CHRONO_HPP
