//
// Created by jb on 09/04/2020.
//

#include <Core/System/Utility/FileUtility.hpp>
#include <Core/Types.hpp>

void Core::File::toStrings(const std::vector<::File> &files, std::vector<::String>& strings) {

    strings.clear();
    strings.resize(files.size());
    for (uint i = 0; i < files.size(); ++i) {
        strings[i] = files[i].name;
    }

}


void Core::File::filter(std::vector<::File> &files, ::String filter) {

    if (filter.empty())
        return;

    std::vector<::File> tmp;

    filter = Core::String::toUpper(filter);
    for (uint i = 0; i < files.size(); ++i) {
        const ::File& file = files[i];
        ::String fileName = Core::String::toUpper(file.name);
        if (fileName.find(filter) != std::string::npos) //no contain filter
            tmp.push_back(files[i]);

    }
    files.clear();
    files = tmp;
}

void Core::File::filter(std::vector<::File*> &files, ::String filter) {
    if (filter.empty())
        return;

    std::vector<::File*> tmp;

    filter = Core::String::toUpper(filter);
    for (uint i = 0; i < files.size(); ++i) {
        const ::File& file = *files[i];
        ::String fileName = Core::String::toUpper(file.name);
        if (fileName.find(filter) != std::string::npos) //no contain filter
            tmp.push_back(files[i]);

    }
    files.clear();
    files = tmp;
}


bool Core::File::generateCPPFileFromData(::String outFile, std::vector<float> data) {

    //Initialize data for writing
    ::File file = ::File(outFile, false, false);
    ::String fullName = file.fullPath + file.nameOnly + "." + file.extension;

    ::String name = Core::String::toUpper(file.nameOnly) + "_HPP";
    ::String header = "#ifndef " + name + "\n#define " + name + "\n\n" + "#include <glm/glm.hpp>" + "\n\n";
    ::String foot = "\n\n#endif";


    //Open file
    FILE* toWriteFile = fopen(fullName.c_str(), "w");
    if (toWriteFile == nullptr)
        return false;

    //Write file header
    fwrite(header.c_str(), sizeof(char), header.size(), toWriteFile);

    //write vector initialization
    ::String sizeToWrite = "static const int size = " + std::to_string(data.size()) + ";\n\n" ;
    ::String dataDeclaration = "static const float data[" + std::to_string(data.size()) + "] = {\n";

    fwrite(sizeToWrite.c_str(), sizeof(char), sizeToWrite.size(), toWriteFile);
    fwrite(dataDeclaration.c_str(), sizeof(char), dataDeclaration.size(), toWriteFile);

    //write data
    for (uint i = 0; i < data.size(); ++i) {
        ::String dataString = std::to_string(data[i]);
        if (i == data.size() - 1)
            dataString += "\n";
        else
            dataString += ",\n";
        fwrite(dataString.c_str(), sizeof(char), dataString.size(), toWriteFile);
        dataString.clear();
    }

    //write end data declaration
    ::String endData = "};";
    fwrite(endData.c_str(), sizeof(char), endData.size(), toWriteFile);


    //Write footer
    fwrite(foot.c_str(), sizeof(char), foot.size(), toWriteFile);

    //Free data;
    fclose(toWriteFile);

    return true;
}
