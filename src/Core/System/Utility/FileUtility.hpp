//
// Created by jb on 09/04/2020.
//

#ifndef MOVE_FILEUTILITY_HPP
#define MOVE_FILEUTILITY_HPP

#include <glm/glm.hpp>
#include <vector>
#include <Core/Types.hpp>
#include <dirent.h>

namespace Core {

    namespace File {

        void toStrings(const std::vector<::File> &files, std::vector<::String>& strings);
        void filter(std::vector<::File> &files, ::String filter);
        void filter(std::vector<::File*> &files, ::String filter);

        //Function able to generate data in cpp format (ex : write a std::vector to a header file)

        bool generateCPPFileFromData(::String outFile, std::vector<float>);
        template<int SIZE>
        bool generateCPPFileFromData(::String outFile, std::vector<glm::vec<SIZE, float>> data) {

            //Initialize data for writing
            ::File file = ::File(outFile, false, false);
            ::String fullName = file.fullPath + file.nameOnly + "." + file.extension;

            ::String name = Core::String::toUpper(file.nameOnly) + "_HPP";
            ::String header = "#ifndef " + name + "\n#define " + name + "\n\n" + "#include <glm/glm.hpp>" + "\n\n";
            ::String foot = "\n\n#endif";


            //Open file
            FILE* toWriteFile = fopen(fullName.c_str(), "w");
            if (toWriteFile == nullptr)
                return false;

            //Write file header
            fwrite(header.c_str(), sizeof(char), header.size(), toWriteFile);

            //write vector initialization
            ::String sizeToWrite = "static const int size = " + std::to_string(data.size()) + ";\n\n";
            ::String dataDeclaration = "static const glm::vec" + std::to_string(SIZE) + " data[" + std::to_string(data.size()) + "] = {\n";

            fwrite(sizeToWrite.c_str(), sizeof(char), sizeToWrite.size(), toWriteFile);
            fwrite(dataDeclaration.c_str(), sizeof(char), dataDeclaration.size(), toWriteFile);

            //write data
            for (uint i = 0; i < data.size(); ++i) {
                ::String dataString = "{";
                for (uint j = 0; j < SIZE; ++j) {

                    dataString += std::to_string(data[i][j]);

                    if (j == SIZE-1)
                        dataString += "}";
                    else
                        dataString += ", ";

                }
                if (i == data.size()-1)
                    dataString += "\n";
                else
                    dataString += ",\n";

                fwrite(dataString.c_str(), sizeof(char), dataString.size(), toWriteFile);
                dataString.clear();
            }

            //write end data declaration
            ::String endData = "};";
            fwrite(endData.c_str(), sizeof(char), endData.size(), toWriteFile);


            //Write footer
            fwrite(foot.c_str(), sizeof(char), foot.size(), toWriteFile);

            //Free data;
            fclose(toWriteFile);

            return true;

        }

        template<int ROW, int COLUMN>
        bool generateCPPFileFromData(::String outFile, std::vector<glm::mat<ROW, COLUMN, float>> data) {

            //Initialize data for writing
            ::File file = ::File(outFile, false, false);
            ::String fullName = file.fullPath + file.nameOnly + "." + file.extension;

            ::String name = Core::String::toUpper(file.nameOnly) + "_HPP";
            ::String header = "#ifndef " + name + "\n#define " + name + "\n\n" + "#include <glm/glm.hpp>" + "\n\n";
            ::String foot = "\n\n#endif";


            //Open file
            FILE* toWriteFile = fopen(fullName.c_str(), "w");
            if (toWriteFile == nullptr)
                return false;

            //Write file header
            fwrite(header.c_str(), sizeof(char), header.size(), toWriteFile);

            //write vector initialization
            ::String sizeToWrite = "static const int size = " + std::to_string(data.size()) + ";\n\n";
            ::String dataDeclaration = "static const glm::mat" + std::to_string(ROW) + "x" + std::to_string(COLUMN) + " data[" + std::to_string(data.size()) + "] = {\n";

            fwrite(sizeToWrite.c_str(), sizeof(char), sizeToWrite.size(), toWriteFile);
            fwrite(dataDeclaration.c_str(), sizeof(char), dataDeclaration.size(), toWriteFile);

            //write data
            for (uint i = 0; i < data.size(); ++i) {
                ::String dataString = "{";
                for (uint j = 0; j < ROW; ++j) {

                    for (uint k = 0; k < COLUMN; ++k) {
                        dataString += std::to_string(data[i][j][k]);

                        if (j+(k*COLUMN) == ROW * COLUMN - 1)
                            dataString += "}";
                        else
                            dataString += ",";
                    }

                }
                if (i == data.size()-1)
                    dataString += "\n";
                else
                    dataString += ",\n";

                fwrite(dataString.c_str(), sizeof(char), dataString.size(), toWriteFile);
                dataString.clear();
            }

            //write end data declaration
            ::String endData = "};";
            fwrite(endData.c_str(), sizeof(char), endData.size(), toWriteFile);


            //Write footer
            fwrite(foot.c_str(), sizeof(char), foot.size(), toWriteFile);


            //Free data;
            fclose(toWriteFile);

            return true;

        }
    }

}


#endif //MOVE_FILEUTILITY_CPP_HPP
