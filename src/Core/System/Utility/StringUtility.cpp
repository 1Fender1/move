//
// Created by jb on 08/04/2020.
//

#include <Core/System/Utility/StringUtility.hpp>
#include <iostream>
#include <sstream>
#include <Core/System/Logger.hpp>

String Core::String::getFileName(const String& path) {
    uint back_offset = 0;
    if (path.back() == '/')
        back_offset++;

    uint stringSize =  path.size()-back_offset;

    uint lastSlashPos = -1;
    for (uint i = 0; i < stringSize; ++i) {
        if (path[i] == '/' || path[i] == '\\') {
            lastSlashPos = i;
        }
    }
    if (lastSlashPos == uint(-1))
        return "";

    String outFilename = "";
    outFilename.resize(path.size() - lastSlashPos-1);
    uint count = 0;
    for (uint i = lastSlashPos+1; i < stringSize; ++i) {
        outFilename[count++] = path[i];
    }

    return outFilename;
}

String Core::String::getFileNameWithoutExtension(const String& inName) {

    String name = inName;
    String backupName = name;

    //TODO CAUTION only work in linux

    uint extIndex = 0;
    //Find last extension dot
    for (uint i = 0; i < name.size(); ++i) {
        if (name[i] == '.')
            extIndex = i;
    }

    if (extIndex == 0)
        return name;

    backupName = name;
    name = "";
    for (uint i = 0; i < extIndex; ++i) {
        name += backupName[i];
    }

    return name;
}

String Core::String::getFileExtension(const String& filename) {
    uint lastDotPos = -1;
    for (uint i = 0; i < filename.size(); ++i) {
        if (filename[i] == '.') {
            lastDotPos = i;
        }
    }

    if (lastDotPos == uint(-1))
        return "";

    String outFilename = "";
    outFilename.resize(filename.size() - lastDotPos-1);
    uint count = 0;
    for (uint i = lastDotPos+1; i < filename.size(); ++i) {
        outFilename[count++] = filename[i];
    }

    outFilename = Core::String::toLower(outFilename);

    return outFilename;
}


String Core::String::getPath(const String& fullPath) {

    uint lastSlashPos = -1;
    for (uint i = 0; i < fullPath.size(); ++i) {
        if (fullPath[i] == '/' || fullPath[i] == '\\') {
            lastSlashPos = i;
        }
    }
    if (lastSlashPos == uint(-1))
        return "";

    if (lastSlashPos == fullPath.size()-1)
        return fullPath;

    String outPath = "";
    outPath.resize(lastSlashPos+1);

    for (uint i = 0; i < outPath.size(); ++i) {
        outPath[i] = fullPath[i];
    }

    return outPath;
}


String Core::String::toUpper(const String& string) {
    String outString = string;
    for (uint i = 0; i < string.size(); ++i)
        outString[i] = std::toupper(string[i]);

    return outString;
}

String Core::String::toLower(const String& string) {
    String outString = string;
    for (uint i = 0; i < string.size(); ++i)
        outString[i] = std::tolower(string[i]);

    return outString;
}


void Core::String::split(const String& str, std::vector<String>& splits, char separator) {

    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, separator))
        splits.push_back(token);

}

void Core::String::splitPath(const String& str, std::vector<String>& splits) {
    Core::String::split(str, splits, '/'); //Only works for linux;
}

void Core::String::filter(std::vector<String>& strings, String filter) {

    if (filter.empty())
        return;
    filter = toUpper(filter);

    std::vector<String> tmp;

    for (uint i = 0; i < strings.size(); ++i) {
        const String& word = toUpper(strings[i]);
        if (word.find(filter) != std::string::npos) { //no contain filter
            tmp.push_back(strings[i]);
        }

    }

    strings.clear();
    strings = tmp;

}


String Core::String::removeAccents(const String& string) {

    return string;

    String outStr = string;
    for (uint i = 0; i < string.size(); ++i) {
/*
        int a = int('é');
        LOG_DEBUG("é is %n", a)
        int &c = (int&)outStr.at(i);
        if (c == 'à' || c == 'â' || c == 'ä') {
            c = 'a';
            continue;
        }
        if (c == 'À' || c == 'Â' || c == 'Ä') {
            c = 'A';
            continue;
        }
        if (c == 'é' || c == 'è' || c == 'ê' || c == 'ë') {
            std::cout << c << " to e" << std::endl;
            c = 'e';
            continue;
        }
        if (c == 'É' || c == 'È' || c == 'Ê' || c == 'Ë') {
            c = 'E';
            continue;
        }
        if (c == 'î' || c == 'ï') {
            c = 'i';
            continue;
        }
        if (c == 'Î' || c == 'Ï') {
            c = 'I';
            continue;
        }
        if (c == 'ô' || c == 'ö') {
            c = 'o';
            continue;
        }
        if (c == 'Ô' || c == 'Ö') {
            c = 'O';
            continue;
        }
        if (c == 'û' || c == 'ü' || c == 'ù') {
            c = 'u';
            continue;
        }
        if (c == 'Û' || c == 'Ü' || c == 'Ù') {
            c = 'U';
            continue;
        }
        if (c == 'ŷ' || c == 'ÿ') {
            c = 'y';
            continue;
        }
        if (c == 'Ŷ' || c == 'Ÿ') {
            c = 'Y';
            continue;
        }
        if (c == 'œ') {
            c = 'o';
            outStr.insert(i, String("e"));
            continue;
        }
*/
    }

    return outStr;

}