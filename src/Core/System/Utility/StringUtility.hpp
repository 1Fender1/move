//
// Created by jb on 08/04/2020.
//

#ifndef MOVE_STRINGUTILITY_HPP
#define MOVE_STRINGUTILITY_HPP

#include <string>
#include <vector>

typedef std::string String;

namespace Core {
    namespace String {

        typedef std::string String;

        String getFileName(const String& path);

        String getFileNameWithoutExtension(const String& path);

        String getFileExtension(const String& filename);

        void split(const String& str, std::vector<String>& splits, char separator);

        void splitPath(const String& str, std::vector<String>& splits);

        String getPath(const String& fullPath);

        String toUpper(const String& string);

        String toLower(const String& string);

        void filter(std::vector<String>& strings, String filter);


        String removeAccents(const String& string); //Caution, only works correctly for francophone characters

    }
}





#endif //MOVE_STRINGUTILITY_HPP
