//
// Created by jb on 07/04/2020.
//

#ifndef MOVE_TYPES_HPP
#define MOVE_TYPES_HPP

#include <iostream>
#include <Core/System/Utility/StringUtility.hpp>
#include <memory>
#include <glm/glm.hpp>

typedef std::string String;

//Files types

enum FileType {

    Directory,
    Model3D,
    Audio,
    Code,
    Default

};

//all name, extension and type can be determine from fullPath, I prefer store them than recompute them
struct File {
    String fullPath;
    String name;
    String nameOnly;
    String extension;
    FileType type;
    bool isHide;

    File(String path, bool isDirectory, bool _isHide) {
        fullPath = path;

        isHide = _isHide;
        if (isDirectory) {
            type = FileType::Directory;
            name = Core::String::getFileName(fullPath);
            nameOnly = name;
            extension = "";
        }
        else {
            type = Default;
            name = Core::String::getFileName(fullPath);
            nameOnly = Core::String::getFileNameWithoutExtension(name);
            extension = Core::String::getFileExtension(name);
            fullPath = Core::String::getPath(fullPath);
        }

    }
};

struct Vertex {

    glm::vec3 position = glm::vec3(0.f);
    glm::vec3 normal = glm::vec3(0.f);
    glm::vec2 texCoords = glm::vec2(0.f);
    glm::vec3 tangent = glm::vec3(0.f);
    glm::vec3 bitangent = glm::vec3(0.f);
    glm::vec4 weights = glm::vec4(0.f); //arbitrary four weights max

    Vertex(glm::vec3 pos = glm::vec3(0.f), glm::vec3 normal = glm::vec3(0.f), glm::vec2 texCoord = glm::vec2(0.f), glm::vec3 tangent = glm::vec3(0.f), glm::vec3 bitangent = glm::vec3(0.f), glm::vec4 weight = glm::vec4(0.f)) {
        position = pos;
        normal = normal;
        texCoords = texCoord;
        tangent = tangent;
        bitangent = bitangent;
        weights = weight;
    }

};

#include <Core/System/Utility/FileUtility.hpp>


#endif //MOVE_TYPES_HPP
