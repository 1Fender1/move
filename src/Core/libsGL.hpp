//
// Created by jb on 03/04/2020.
//

#ifndef MOVE_LIBSGL_HPP
#define MOVE_LIBSGL_HPP

#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <Core/glAssert.hpp>

#endif //MOVE_LIBSGL_HPP
