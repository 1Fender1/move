#include "Camera.hpp"

Camera::Camera(glm::vec3 pos, float w, float h) {

    m_frustum.resize(w, h);
    m_position = pos;
    m_target = glm::vec3(0, 0, 0);
    m_direction = glm::normalize(m_position - m_target);
    m_up = glm::vec3(0.f, 1.f, 0.f);
    m_cFront = glm::normalize(glm::vec3(-1.f, 0.f, 0.f));
    m_cRight = glm::normalize(glm::cross(m_up, m_cFront));
    m_cUp = glm::cross(m_cRight, m_cFront);

    m_yaw = -90.f;
    m_pitch = 0.f;
    m_speed = 1.5f;
    m_sensitivity = 0.25f;
    m_distance = 10.f;

    m_frustum.updateView(m_position, m_up, m_target);

    m_isPressed = false;

    m_cameraType = cameraType::NONE;
    m_objType = objectType::CAMERA;

}

Camera::Camera() {

    glm::vec3 pos = glm::vec3(0.f);
    uint w = 800;
    uint h = 600;

    m_frustum.resize(w, h);
    m_position = pos;
    m_target = glm::vec3(0, 0, 0);
    m_direction = glm::normalize(m_position - m_target);
    m_up = glm::vec3(0.f, 1.f, 0.f);
    m_cFront = glm::normalize(glm::vec3(-1.f, 0.f, 0.f));
    m_cRight = glm::normalize(glm::cross(m_up, m_cFront));
    m_cUp = glm::cross(m_cRight, m_cFront);

    m_yaw = -90.f;
    m_pitch = 0.f;
    m_speed = 2.5f;
    m_sensitivity = 0.25f;
    m_distance = 10.f;

    m_frustum.updateView(m_position, m_up, m_target);

    m_isPressed = false;

    m_cameraType = cameraType::NONE;
    m_objType = objectType::CAMERA;

}

Camera::Camera(const Camera& cam) : MoveObject(cam) {

    m_frustum = cam.m_frustum;
    m_position = cam.m_position;
    m_target = cam.m_target;
    m_direction = cam.m_direction;
    m_up = cam.m_up;
    m_cFront = cam.m_cFront;
    m_cRight = cam.m_cRight;
    m_cUp = cam.m_cUp;

    m_yaw = cam.m_yaw;
    m_pitch = cam.m_pitch;
    m_speed = cam.m_speed;
    m_sensitivity = cam.m_sensitivity;
    m_distance = cam.m_distance;

    m_isPressed = cam.m_isPressed;

    m_lastX = cam.m_lastX;
    m_lastY = cam.m_lastY;
    m_lastScroll = cam.m_lastScroll;

    m_cameraType = cam.m_cameraType;
    m_objType = objectType::CAMERA;
}

Camera::~Camera() {

}


void Camera::draw(Shader& shader) {
    shader.use();
    shader.setFloatV3("camPos", m_position);
    shader.setMat4("view", m_frustum.getView());
    shader.setMat4("projection", m_frustum.getProjection());
    shader.setFloat("u_near", m_frustum.getNear());
    shader.setFloat("u_far", m_frustum.getFar());
}

glm::mat4 Camera::getView() {
    return m_frustum.getView();
}

glm::mat4 Camera::getProjection() {
    return m_frustum.getProjection();
}


const glm::vec3& Camera::getPosition() const {
    return m_position;
}


/*Camera::keyDirection Camera::getDirection(char keyInput) {

}
*/

void Camera::processMouseClick(int , GLfloat , GLfloat ) {}

void Camera::processMouseMovement(int , GLfloat , GLfloat , GLboolean, float) {}

void Camera::processMouseScroll(GLfloat ) {}
void Camera::processKeyboard(int  , GLfloat) {}

void Camera::processMouseRelease(int, GLfloat, GLfloat) {}


glm::vec3 Camera::getRayFromScreen(GLfloat x, GLfloat y) {
    uint height = m_frustum.getHeight();
    uint width = m_frustum.getWidth();
    float near = m_frustum.getNear();
    glm::vec3 unprojectPoint = glm::unProject(glm::vec3(x, float(height) - y, near),
                                              m_frustum.getView(), m_frustum.getProjection(),
                                              glm::vec4(0.f, 0.f, width, height));
    glm::vec3 direction = unprojectPoint - m_position;
    return glm::normalize(direction);

}
const glm::vec3& Camera::getDirection() const {
    return m_cFront;
}

const glm::vec3& Camera::getUp() const {
    return m_cUp;
}

const glm::vec3& Camera::getWUp() const {
    return m_up;
}


void Camera::resize(uint width, uint height) {

    m_frustum.resize(width, height);

}

const glm::vec3& Camera::getTarget() {
    return m_target;
}

float Camera::getSpeed() {
    return m_speed;
}

void Camera::setSpeed(float speed) {
    if (speed > 0.f)
        m_speed = speed;
}

void Camera::setTarget(const glm::vec3& target) {
    m_target = target;
    m_direction = glm::normalize(m_target - m_position);
    m_cFront = m_direction;
    m_cRight = glm::normalize(glm::cross(m_cFront, m_up));
    m_cUp = glm::normalize(glm::cross(m_cRight, m_cFront));
    m_frustum.updateView(m_position, m_up, m_target);
    m_pitch = glm::degrees(-glm::atan(m_cFront.y, glm::sqrt(m_cFront.x * m_cFront.x + m_cFront.z * m_cFront.z)));
    m_yaw = glm::degrees(glm::atan(m_cFront.z, m_cFront.x));
    m_frustum.setFront(m_target - m_position);
}

Frustum& Camera::getFrustum() {
    return m_frustum;
}

void Camera::setPosition(const glm::vec3& position) {
    m_position = position;
    m_frustum.setPosition(position);
}

void Camera::setDirection(const glm::vec3& direction) {
    m_direction = direction;
}
void Camera::setUp(const glm::vec3& up) {
    m_up = up;
}
void Camera::setCUp(const glm::vec3& cUp) {
    m_cUp = cUp;
    m_frustum.setUp(m_cUp);
}
void Camera::setCRight(const glm::vec3& cRight) {
    m_cRight = cRight;
}
void Camera::setCFront(const glm::vec3& cFront) {
    m_cFront = cFront;
}
