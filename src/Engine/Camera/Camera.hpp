#ifndef CAMERA_H
#define CAMERA_H
//#include "Math/math.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Core/libsGL.hpp>
#include <iostream>
#include <Engine/Scenes/MoveObject.hpp>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Frustum/Frustum.hpp>

class Camera : public MoveObject {

public:

    enum cameraType {
        NONE,
        FREE,
        ORBIT
    };

    static const unsigned int CAMERA_FREE = 0;
    static const unsigned int CAMERA_ORBIT = 1;

public:


    Camera(glm::vec3 pos, float w, float h);
    Camera(const Camera& cam);
    Camera();
    virtual ~Camera();
    virtual glm::mat4 getView();
    virtual glm::mat4 getProjection();
    const glm::vec3& getPosition() const;
    const glm::vec3& getDirection() const;
    const glm::vec3& getUp() const;
    const glm::vec3& getWUp() const;

    void setTarget(const glm::vec3& target);

    virtual void processMouseClick(int button, GLfloat xpos, GLfloat ypos);
    virtual void processMouseMovement(int , GLfloat , GLfloat , GLboolean, float);
    virtual void processMouseRelease(int, GLfloat, GLfloat);

    virtual void processMouseScroll(GLfloat yoffset);
    virtual void processKeyboard(int key , GLfloat);
    glm::vec3 getRayFromScreen(GLfloat x, GLfloat y);

    void draw(Shader& shader);


    cameraType getCameraType() {return m_cameraType;}

    void resize(uint width, uint height);

    const glm::vec3& getTarget();

    float getSpeed();
    void setSpeed(float speed);

    Frustum& getFrustum();

protected:

    void setPosition(const glm::vec3& position);
    void setDirection(const glm::vec3& direction);
    void setUp(const glm::vec3& up);
    void setCUp(const glm::vec3& cUp);
    void setCRight(const glm::vec3& cRight);
    void setCFront(const glm::vec3& cFront);

    glm::vec3 m_position;
    glm::vec3 m_target;
    glm::vec3 m_direction;
    glm::vec3 m_up;
    glm::vec3 m_cRight;
    glm::vec3 m_cUp;
    glm::vec3 m_cFront;


    GLfloat m_lastX = 0.f;
    GLfloat m_lastY = 0.f;
    GLfloat m_lastScroll = 0.f;

    float m_yaw;
    float m_pitch;
    float m_speed;
    float m_sensitivity;
    float m_distance;

    Frustum m_frustum;

    cameraType m_cameraType;

    enum keyDirection {
        FORWARD = GLFW_KEY_Z,
        BACKWARD = GLFW_KEY_S,
        LEFT = GLFW_KEY_Q,
        RIGHT = GLFW_KEY_D
    };

    bool m_isPressed;

};

typedef std::unique_ptr<Camera> Camera_ptr;


#endif // CAMERA_H
