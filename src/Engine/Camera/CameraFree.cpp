#include "CameraFree.hpp"

CameraFree::CameraFree(glm::vec3 pos, float w, float h) : Camera(pos, w, h) {
    setCFront(glm::normalize(glm::vec3(0.f, 0.f, -1.f)));
    setUp(glm::vec3(0.f, 1.f, 0.f));
    setCRight(glm::normalize(glm::cross(getUp(), m_cFront)));
    setCUp(glm::cross(m_cRight, m_cFront));

    m_frustum.updateView(m_position, m_cUp, m_position + m_cFront);

    m_speed = 30.f;
    updateCameraVectors();

    m_cameraType = cameraType::FREE;

}

CameraFree::CameraFree() : Camera() {
    setCFront(glm::normalize(glm::vec3(0.f, 0.f, -1.f)));
    m_up = glm::vec3(0.f, 1.f, 0.f);
    m_cRight = glm::normalize(glm::cross(m_up, m_cFront));
    m_cUp = glm::cross(m_cRight, m_cFront);

    m_frustum.updateView(m_position, m_cUp, m_position + m_cFront);

    m_speed = 30.f;
    updateCameraVectors();
    m_cameraType = cameraType::FREE;

}

CameraFree::~CameraFree() {

}

CameraFree::CameraFree(const CameraFree& cam) : Camera(cam) {
    m_cameraType = cameraType::FREE;
}

void CameraFree::processKeyboard(int key, GLfloat deltaTime) {
    if (key == FORWARD)
        m_position += m_cFront * m_speed * deltaTime;
    if (key == BACKWARD)
        m_position -= m_cFront * m_speed * deltaTime;
    if (key == RIGHT)
        m_position += m_cRight * m_speed * deltaTime;
    if (key == LEFT)
        m_position -= m_cRight * m_speed * deltaTime;
    m_frustum.updateView(m_position, m_cUp, m_position + m_cFront);
}

void CameraFree::processMouseClick(int , GLfloat xpos, GLfloat ypos) {
    m_isPressed = true;
    m_lastX = xpos;
    m_lastY = ypos;
}

void CameraFree::processMouseRelease(int, GLfloat, GLfloat) {
    m_isPressed = false;
}

void CameraFree::processMouseMovement(int button, GLfloat xpos, GLfloat ypos, GLboolean constrainPitch, float ) {
    if (!m_isPressed)
        return;
    if (button == 1) {
        float xOffset = xpos - m_lastX;
        float yOffset = ypos - m_lastY;
        m_lastX = xpos;
        m_lastY = ypos;

        m_yaw += xOffset * m_sensitivity;
        m_pitch += yOffset * m_sensitivity;

        if (constrainPitch) {
            if (m_pitch > 89)
                m_pitch = 89;
            if (m_pitch < -89)
                m_pitch = -89;
        }
        updateCameraVectors();
    }
}

void CameraFree::updateCameraVectors() {
    glm::vec3 _front;
    float _yaw = glm::radians(m_yaw);
    float _pitch = glm::radians(-m_pitch);
    _front[0] = std::cos(_yaw) * std::cos(_pitch);
    _front[1] = std::sin(_pitch);
    _front[2] = std::sin(_yaw) * std::cos(_pitch);
    setCFront(glm::normalize(_front));
    m_cRight = glm::normalize(glm::cross(m_cFront, m_up));
    m_cUp = glm::normalize(glm::cross(m_cRight, m_cFront));
    m_frustum.updateView(m_position, m_cUp, m_position + m_cFront);

}
