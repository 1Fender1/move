#ifndef CAMERAFREE_HPP
#define CAMERAFREE_HPP
#include <Engine/Camera/Camera.hpp>

class CameraFree : public Camera {

public:
    CameraFree(glm::vec3 pos, float w, float h);
    CameraFree(const CameraFree& cam);
    CameraFree();
    ~CameraFree();
    void processKeyboard(int key, GLfloat a);
    void processMouseClick(int button, GLfloat xpos, GLfloat ypos);
    void processMouseMovement(int button, GLfloat xpos, GLfloat ypos, GLboolean constrainPitch, float deltaTime);
    void processMouseRelease(int, GLfloat, GLfloat);
    void updateCameraVectors();
};

#endif // CAMERAEULER_H
