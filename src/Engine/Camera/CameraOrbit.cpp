#include "CameraOrbit.hpp"

CameraOrbit::CameraOrbit(glm::vec3 pos, float w, float h) : Camera(pos, w, h) {
    m_up = glm::vec3(0.f, 1.f, 0.f);
    m_cFront = glm::normalize(glm::vec3(0.f, 0.f, -1.f));
    m_cRight = glm::normalize(glm::cross(m_up, m_cFront));
    m_cUp = glm::cross(m_cRight, m_cFront);

    m_frustum.setPosition(m_position);
    m_frustum.setFront(m_position + m_cFront);
    m_frustum.setUp(m_cUp);

    m_speed = 1.5f;
    updateCameraVectors();

    m_cameraType = cameraType::ORBIT;

}

CameraOrbit::CameraOrbit() : Camera() {
    m_up = glm::vec3(0.f, 1.f, 0.f);
    m_cFront = glm::normalize(glm::vec3(0.f, 0.f, -1.f));
    m_cRight = glm::normalize(glm::cross(m_up, m_cFront));
    m_cUp = glm::cross(m_cRight, m_cFront);
    m_frustum.setPosition(m_position);
    m_frustum.setFront(m_position + m_cFront);
    m_frustum.setUp(m_cUp);    m_speed = 1.5f;
    updateCameraVectors();

    m_cameraType = cameraType::ORBIT;

}

CameraOrbit::~CameraOrbit() {

}

CameraOrbit::CameraOrbit(const Camera& cam) : Camera(cam) {
    m_cameraType = cameraType::ORBIT;
}


void CameraOrbit::processKeyboard(int key, GLfloat deltaTime) {
    if (key == FORWARD)
        processMouseMovement(0, m_lastX, m_lastY - 20, true, deltaTime);
    if (key == BACKWARD)
        processMouseMovement(0, m_lastX, m_lastY + 20, true, deltaTime);
    if (key == RIGHT)
        processMouseMovement(0, m_lastX - 20, m_lastY, true, deltaTime);
    if (key == LEFT)
        processMouseMovement(0, m_lastX + 20, m_lastY, true, deltaTime);

}

void CameraOrbit::processMouseScroll(GLfloat ypos) {

    m_distance -= m_scrollSpeed * ypos;
    if(m_distance < 0.2f)
        m_distance = 0.2f;
    m_position = m_target - m_cFront * m_distance;
    m_frustum.setPosition(m_position);
    m_frustum.setFront(m_position + m_cFront);
    m_frustum.setUp(m_cUp);
}

void CameraOrbit::processMouseClick(int , GLfloat xpos, GLfloat ypos) {
    m_isPressed = true;
    m_lastX = xpos;
    m_lastY = ypos;
}

void CameraOrbit::processMouseRelease(int, GLfloat, GLfloat) {
    m_isPressed = false;
}

void CameraOrbit::processMouseMovement(int button, GLfloat xpos, GLfloat ypos, GLboolean constrainPitch, float ) {
    if (!m_isPressed) {
        return;
    }
    if (button == 1) {
        float xOffset = xpos - m_lastX;
        float yOffset = ypos - m_lastY;
        m_lastX = xpos;
        m_lastY = ypos;

        m_yaw += xOffset * m_sensitivity;
        m_pitch += yOffset * m_sensitivity;

        if (constrainPitch) {
            if (m_pitch > 89)
                m_pitch = 89;
            if (m_pitch < -89)
                m_pitch = -89;
        }

        updateCameraVectors();
    }
}

void CameraOrbit::updateCameraVectors() {
    glm::vec3 _front;
    float _yaw = glm::radians(m_yaw);
    float _pitch = glm::radians(m_pitch);
    _front[0] = std::cos(_yaw) * std::cos(_pitch);
    _front[1] = std::sin(_pitch);
    _front[2] = std::sin(_yaw) * std::cos(_pitch);
    m_cFront = glm::normalize(_front);
    m_cRight = glm::normalize(glm::cross(m_cFront, m_up));
    m_cUp = glm::normalize(glm::cross(m_cRight, m_cFront));

    m_position = m_target - m_cFront * m_distance;
    m_frustum.updateView(m_position, m_cUp, m_position + m_cFront);
}

float CameraOrbit::getScrollSpeed() {
    return m_scrollSpeed;
}

void CameraOrbit::setScrollSpeed(float speed) {
    if (speed > 0.f)
        m_scrollSpeed = speed;
}
