#ifndef CAMERAORBIT_HPP
#define CAMERAORBIT_HPP
#include "Camera.hpp"

class CameraOrbit : public Camera {


public:
    CameraOrbit(glm::vec3 pos, float w, float h);
    CameraOrbit(const Camera& cam);
    CameraOrbit();
    ~CameraOrbit();

    void processKeyboard(int key, GLfloat deltaTime);
    void processMouseClick(int button, GLfloat xpos, GLfloat ypos);
    void processMouseMovement(int button, GLfloat xpos, GLfloat ypos, GLboolean constrainPitch, float deltaTIme);
    void processMouseScroll(GLfloat ypos);
    void processMouseRelease(int, GLfloat, GLfloat);
    void updateCameraVectors();

    float getScrollSpeed();
    void setScrollSpeed(float speed);

private :
    float m_scrollSpeed = 1.f;


};

#endif // CAMERAORBIT_HPP
