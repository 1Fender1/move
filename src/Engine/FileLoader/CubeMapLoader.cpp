#include <Core/System/Logger.hpp>
#include "CubeMapLoader.hpp"

CubeMapLoader::CubeMapLoader() {

}

CubeMapLoader::~CubeMapLoader() {
    for (uint i = 0; i < 6; i++)
        stbi_image_free(m_textures[i]);

}

/*
unsigned int textureID;
glGenTextures(1, &textureID);
glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
 * */

//name var is the name of the directory which is containing our textures
unsigned int CubeMapLoader::loadCubeMap(std::string name, std::string format) {
    std::vector<std::string> faces = {"right", "left", "top", "bottom", "front", "back"};

    unsigned int textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++) {
        std::string tex = directory + "/" + name + "/" + faces[i] + format;
        unsigned char *data = stbi_load(tex.c_str(), &width, &height, &nrChannels, 0);
        if (data) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                         0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
            );
            m_textures[i] = data;
            m_width = width;
            m_height = height;
            //stbi_image_free(data);
        }
        else {
            LOG_ERROR("Cubemap m_fbTexture failed to load at path : %n", tex)
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID;

}

unsigned char* CubeMapLoader::getTexture(uint texture) {
    if (texture > 5)
        return nullptr;
    return m_textures[texture];
}

void CubeMapLoader::getSize(uint& width, uint& height) {
    width = m_width;
    height = m_height;
}
