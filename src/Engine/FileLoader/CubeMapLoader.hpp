#ifndef CUBEMAPLOADER_HPP
#define CUBEMAPLOADER_HPP
#include <stb/stb_image.h>
#include <string>
#include <vector>
#include <Core/libsGL.hpp>
#include <iostream>

class CubeMapLoader {
public:
    CubeMapLoader();
    ~CubeMapLoader();

    unsigned int loadCubeMap(std::string name, std::string format = ".jpg");

    unsigned char* getTexture(uint texture);

    void getSize(uint& width, uint& height);

private:
    const std::string directory = "../Assets/CubeMaps";
    uint m_width;
    uint m_height;
    unsigned char* m_textures[6];

};

#endif // CUBEMAPLOADER_HPP
