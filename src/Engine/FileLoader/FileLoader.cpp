//
// Created by jb on 01/03/18.
//

#include "FileLoader.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

FileLoader::FileLoader(RenderData* renderData) {
    texFormat = GL_RGB;
    cameraMatrix = glm::mat4(1);
    m_renderData = renderData;
}


std::string FileLoader::getExtension(std::string name) {
    uint index = 0;
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    //get last '.' index
    for (uint i = 0; i < name.size(); i++) {
        if (name[i] == '.')
            index = i;
    }
    std::string extension = "";
    for (uint i = index; i < name.size(); i++) {
        extension.append(sizeof(name[i]), name[i]);
    }
    return extension;
}

void FileLoader::splitPath() {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(lastPath);
    while (std::getline(tokenStream, token, '/')) {
       tokens.emplace_back("/");
       tokens.emplace_back(token);
    }
    lastFileName = token;
    lastDirectory = std::string("");
    //tokens.erase(tokens.end(), tokens.end());
    for (uint i = 1; i < tokens.size()-1; i++)
        lastDirectory += tokens[i];

}

std::string FileLoader::revertSlash(std::string line) {
    std::string out = line;
    for (uint i = 0; i < line.size(); i++) {
        if (line[i] == '\\')
            out[i] = '/';
    }
    return out;
}

glm::mat4 FileLoader::toGlm(aiMatrix4x4 mat) {
    glm::mat4 transformGlm = glm::mat4(1);

    aiVector3t<ai_real> tScaling, tRotationAxis, tTranslation;
    ai_real tAngle;
    mat.Decompose(tScaling, tRotationAxis, tAngle, tTranslation);
    float angle = float(tAngle);
    glm::vec3 scaling, rotationAxis, translation;
    scaling = glm::vec3(tScaling[0], tScaling[1], tScaling[2]);
    rotationAxis = glm::vec3(tRotationAxis[0], tRotationAxis[1], tRotationAxis[2]);
    translation = glm::vec3(tTranslation[0], tTranslation[1], tTranslation[2]);

    transformGlm = glm::rotate(transformGlm, angle, rotationAxis);
    transformGlm = glm::translate(transformGlm, translation);
    transformGlm = glm::scale(transformGlm, scaling);

    /*
    transformGlm[0][0] = (GLfloat)mat.a1; transformGlm[0][1] = (GLfloat)mat.b1;
    transformGlm[0][2] = (GLfloat)mat.c1; transformGlm[0][3] = (GLfloat)mat.d1;
    transformGlm[1][0] = (GLfloat)mat.a2; transformGlm[1][1] = (GLfloat)mat.b2;
    transformGlm[1][2] = (GLfloat)mat.c2; transformGlm[1][3] = (GLfloat)mat.d2;
    transformGlm[2][0] = (GLfloat)mat.a3; transformGlm[2][1] = (GLfloat)mat.b3;
    transformGlm[2][2] = (GLfloat)mat.c3; transformGlm[2][3] = (GLfloat)mat.d3;
    transformGlm[3][0] = (GLfloat)mat.a4; transformGlm[3][1] = (GLfloat)mat.b4;
    transformGlm[3][2] = (GLfloat)mat.c4; transformGlm[3][3] = (GLfloat)mat.d4;
    */

    return transformGlm;
}



bool FileLoader::objLoader(std::string name, bool raytracerVisible) {

    std::string fileName = name;
    this->lastPath = name;

    Assimp::Importer importer;

    /*aiProcess_Triangulate | aiProcess_JoinenticalVertices | aiProcess_FlipUVs | aiProcess_SortByPType | aiProcess_OptimizeMeshes;*/
    uint flag = aiProcess_FlipWindingOrder | aiProcess_GenUVCoords | aiProcess_GenSmoothNormals | aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace | aiProcess_JoinIdenticalVertices;
    const aiScene* scene = importer.ReadFile(fileName, flag);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        LOG_ERROR("Error : %n", importer.GetErrorString())
        return false;
    }

    fileType = getExtension(name);
    splitPath();
    lastPath = revertSlash(lastPath);

    processLights(scene);

    //processSubNode(scene->mRootNode);
    //processNodAnim(scene);
    //globalTransformInv = glm::inverse(toGlm(scene->mRootNode->mTransformation));
    processSubMesh(scene->mRootNode, scene);

    //printObjHierarchy(scene->mRootNode);

    Object obj = Object(meshes, lastFileName);

    obj.setRaytracerVisible(raytracerVisible);
    m_renderData->addObject(obj);

    meshes.clear();
    lastFileName.clear();

    importer.FreeScene();

    return true;
}

// TODO finish this function
void FileLoader::clear() {
    meshes.clear();
}

Object FileLoader::getObj() {

    Object obj = Object(meshes, lastFileName);
    //meshes.clear();

    return obj;
}

aiMatrix4x4 turnColladaModel(aiMatrix4x4 mat) {

    aiMatrix4x4 base;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4;  j++)
            base[i][j] = 0;

    base[0][0] = 1;
    base[1][2] = 1;
    base[2][1] = 1;
    base[3][3] = 1;

    return base * mat;

}

aiMatrix4x4 turnObjModel(aiMatrix4x4 mat) {

    aiMatrix4x4 base;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4;  j++)
            base[i][j] = 0;

    base[0][0] = 1;
    base[1][1] = 1;
    base[2][2] = -1;
    base[3][3] = 1;

    return mat * base;

}



void FileLoader::transformLight(aiNode* subMesh) {
    std::string name = std::string(subMesh->mName.C_Str());
    for (uint i = 0; i < m_renderData->getLightCount(); i++) {
        Light* light = m_renderData->getLight(i);
        if (light->getName() == name) {
            aiVector3D tempPos = aiVector3D(light->getPosition()[0], light->getPosition()[1], light->getPosition()[2]);
            tempPos = subMesh->mTransformation * tempPos;
            glm::vec3 pos = glm::vec3(tempPos.x, tempPos.y, tempPos.z);
            light->setPosition(pos);
        }
    }
}

void FileLoader::printObjHierarchy(aiNode* mesh, uint depth) {

    if (mesh == nullptr)
        return;
    else
        std::cout << "[" << depth << "] " << mesh->mName.C_Str() << std::endl;

    for (uint i = 0; i < mesh->mNumChildren; ++i) {
        printObjHierarchy(mesh->mChildren[i], depth + 1);
    }
}

void FileLoader::processSubMesh(aiNode* subMesh, const aiScene *scene) {



    transformLight(subMesh);

    for (uint i = 0; i < subMesh->mNumMeshes; i++) {
        aiMesh *mesh = scene->mMeshes[subMesh->mMeshes[i]];
        aiMatrix4x4 transform = /*scene->mRootNode->mTransformation * */ subMesh->mTransformation;

        if (fileType == ".dae") {
            transform = /*aiMatrix4x4() **/ subMesh->mTransformation;
            transform = turnColladaModel(transform);
        }
        Mesh appendMesh;
        processMesh(mesh, scene, transform, appendMesh);
        meshes.push_back(appendMesh);
    }

   // aiNodes.emplace_back(subMesh);
    for (uint i = 0; i < subMesh->mNumChildren; i++) {
        processSubMesh(subMesh->mChildren[i], scene);
    }

}

void FileLoader::processLights(const aiScene *scene) {
    for (uint i = 0; i < scene->mNumLights; ++i) {
        aiLight *assimpLight = scene->mLights[i];

        switch(assimpLight->mType) {
            case aiLightSource_POINT : {
                PointLight *light = new PointLight();
                std::string nameTmp = std::string(assimpLight->mName.C_Str());
                if (nameTmp.empty())
                    nameTmp = "PointLight";
                light->setName(nameTmp);
                light->setConstantAttenuation(assimpLight->mAttenuationConstant);
                light->setLinearAttenuation(assimpLight->mAttenuationLinear);
                light->setQuadraticAttenuation(assimpLight->mAttenuationQuadratic);
                aiColor3D color = assimpLight->mColorDiffuse;
                glm::vec3 tmpColor = glm::vec3(color.r, color.g, color.b);
                light->setColor(tmpColor);
                aiVector3D pos = assimpLight->mPosition;
                light->setPosition(glm::vec3(pos.x, pos.y, pos.z));
                //lights.emplace_back(light);
                //lights.emplace_back(dynamic_cast<Light*>(&pointLights[pointLights.size()-1]));
                m_renderData->addObject(*light);
                if (light)
                    delete light;
            }
                break;
            case aiLightSource_SPOT : {
                SpotLight *light = new SpotLight();
                std::string nameTmp = std::string(assimpLight->mName.C_Str());
                if (nameTmp.empty())
                    nameTmp = "SpotLight";
                light->setName(nameTmp);

                light->setConstantAttenuation(assimpLight->mAttenuationConstant);
                light->setLinearAttenuation(assimpLight->mAttenuationLinear);
                light->setQuadraticAttenuation(assimpLight->mAttenuationQuadratic);

                aiColor3D color = assimpLight->mColorAmbient;
                color = assimpLight->mColorDiffuse;
                glm::vec3 colorTemp = glm::vec3(color.r, color.g, color.b);
                light->setColor(colorTemp);
                color = assimpLight->mColorSpecular;

                aiVector3D pos = assimpLight->mPosition;
                light->setPosition(glm::vec3(pos.x, pos.y, pos.z));

                aiVector3D dir = assimpLight->mDirection;
                light->setDirection(glm::vec3(dir.x, dir.y, dir.z));

                float iAngle = assimpLight->mAngleInnerCone;
                float oAngle = assimpLight->mAngleOuterCone;
                light->setIAngle(iAngle);
                light->setOAngle(oAngle);

                //lights.emplace_back(light);
                m_renderData->addObject(*light);
                if (light)
                    delete light;
                //lights.emplace_back(dynamic_cast<Light*>(&spotlights[spotlights.size()-1]));
            }
                break;
            case aiLightSource_DIRECTIONAL : {
                aiColor3D color = assimpLight->mColorDiffuse;
                glm::vec3 glmColor = glm::vec3(color.r, color.g, color.b);

                aiVector3D direction = assimpLight->mDirection;
                glm::vec3 glmDirection = glm::vec3(direction.x, direction.y, direction.z);

                DirectionalLight *light = new DirectionalLight(glmDirection, glmColor);
                std::string nameTmp = std::string(assimpLight->mName.C_Str());
                if (nameTmp.empty())
                    nameTmp = "Directional";
                light->setName(nameTmp);
                //lights.emplace_back(light);
                m_renderData->addObject(*light);
                if (light)
                    delete light;
            }
                break;
            case aiLightSource_AREA :
                break;
            case aiLightSource_AMBIENT :
                break;
            case aiLightSource_UNDEFINED :
                break;
            default :

                break;
        }

    }
}

void FileLoader::processNodAnim(const aiScene* scene) {
    if (scene->mNumAnimations == 0)
        return;

    for (uint i = 0; i < scene->mAnimations[0]->mNumChannels; i++)
        aiNodesAnim.emplace_back(scene->mAnimations[0]->mChannels[i]);

}

void FileLoader::processSubNode(aiNode* node) {
    aiNodes.emplace_back(node);

    for (uint i = 0; i < node->mNumChildren; i++)
        processSubNode(node->mChildren[i]);
}





std::vector<Vertex> FileLoader::calcTangentBiTangent(aiMesh* mesh) {
    std::vector<Vertex> out;
    for (uint i = 0; i < mesh->mNumVertices; i+=3) {

        glm::vec3 v0 = glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
        glm::vec3 v1 = glm::vec3(mesh->mVertices[i+1].x, mesh->mVertices[i+1].y, mesh->mVertices[i+1].z);
        glm::vec3 v2 = glm::vec3(mesh->mVertices[i+2].x, mesh->mVertices[i+2].y, mesh->mVertices[i+2].z);

        glm::vec2 uv0 = glm::vec2(mesh->mTextureCoords[i]->x, mesh->mTextureCoords[i]->y);
        glm::vec2 uv1 = glm::vec2(mesh->mTextureCoords[i+1]->x, mesh->mTextureCoords[i+1]->y);
        glm::vec2 uv2 = glm::vec2(mesh->mTextureCoords[i+2]->x, mesh->mTextureCoords[i+2]->y);

        glm::vec3 dpos1 = v1-v0;
        glm::vec3 dpos2 = v2-v0;

        glm::vec2 dUV1 = uv1-uv0;
        glm::vec2 dUV2 = uv2-uv0;

        float scalar = 1.f / (dUV1[0] * dUV2[1] - dUV1[1] * dUV2[0]);

        glm::vec3 tangent = scalar * (dpos1 * dUV2[1] - dpos2 * dUV1[1]);
        glm::vec3 bitangent = scalar * (dpos2 * dUV1[0] - dpos1 * dUV2[0]);

        Vertex v;
        v.tangent = tangent;
        v.bitangent = bitangent;

        out.emplace_back(v);
        out.emplace_back(v);
        out.emplace_back(v);
    }

    return out;
}


void FileLoader::processMesh(aiMesh *mesh, const aiScene *scene, const aiMatrix4x4 transformation, Mesh& outMesh) {

    //vertices process
    bool isTextured = false;
    std::vector<Vertex> vertices;
    std::vector<Vertex> tanBitanTemp;

    if (!mesh->HasTangentsAndBitangents()) {
        if (mesh->HasTextureCoords(0)) {
            tanBitanTemp = calcTangentBiTangent(mesh);
        }
    }
    else {
        isTextured = true;
    }

    vertices.resize(mesh->mNumVertices);
    tanBitanTemp.resize(mesh->mNumVertices);

    for (uint i = 0; i < mesh->mNumVertices; i++) {
        Vertex vTemp;
        glm::vec3 temp;

        aiVector3D point = mesh->mVertices[i];

        //point = /*transformation **/ point;

        temp[0] = point.x;
        temp[1] = point.y;
        temp[2] = point.z;
        vTemp.position = temp;

        temp[0] = mesh->mNormals[i].x;
        temp[1] = mesh->mNormals[i].y;
        temp[2] = mesh->mNormals[i].z;
        vTemp.normal = temp;

        if (mesh->HasTangentsAndBitangents()) {
            temp[0] = mesh->mTangents[i].x;
            temp[1] = mesh->mTangents[i].y;
            temp[2] = mesh->mTangents[i].z;
            vTemp.tangent = temp;

            temp[0] = mesh->mBitangents[i].x;
            temp[1] = mesh->mBitangents[i].y;
            temp[2] = mesh->mBitangents[i].z;
            vTemp.bitangent = temp;
/*
            vTemp.Tangent = glm::normalize(vTemp.Tangent - vTemp.Normal * glm::dot(vTemp.Normal, vTemp.Tangent));
            vTemp.Bitangent = glm::cross(vTemp.Normal, vTemp.Tangent);

            if (glm::dot(glm::cross(vTemp.Normal, vTemp.Tangent), vTemp.Bitangent) < 0.0f) {
                vTemp.Tangent = -vTemp.Tangent;
            }
*/
        }
        else {
            /*
            if (mesh->HasTextureCoords(0)) {
                vTemp.Tangent = tanBitanTemp[i].Tangent;
                vTemp.Bitangent = tanBitanTemp[i].Bitangent;

                vTemp.Tangent = glm::normalize(vTemp.Tangent - vTemp.Normal * glm::dot(vTemp.Normal, vTemp.Tangent));

                vTemp.Bitangent = glm::cross(vTemp.Normal, vTemp.Tangent);


                if (glm::dot(glm::cross(vTemp.Normal, vTemp.Tangent), vTemp.Bitangent) < 0.0f) {
                    vTemp.Tangent = -vTemp.Tangent;
                }
            }
             */
        }


        if (mesh->mTextureCoords[0]) {
            glm::vec2 temp2;
            temp2[0] = mesh->mTextureCoords[0][i].x;
            temp2[1] = mesh->mTextureCoords[0][i].y;
            vTemp.texCoords = temp2;
        }
        else {
            vTemp.texCoords = glm::vec2(0.f, 0.f);
        }
        vertices[i] = vTemp;

    }

    //indices process
    std::vector<uint> indices;
    for (uint i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        for (uint j = 0; j < face.mNumIndices; j++) {
            indices.emplace_back(face.mIndices[j]);
        }
    }

    bool hasDiffuseMap = false;
    bool hasSpecularMap = false;
    bool hasNormalMap = false;
    //material process
    std::vector<uint> maps, diffuseMaps, specularMaps, normalMaps, heightMaps;
    if (mesh->mMaterialIndex >= 0) {
        aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];

        loadMaterials(material, aiTextureType_DIFFUSE, "texture_diffuse", diffuseMaps);

        if (!diffuseMaps.empty()) {
            hasDiffuseMap = true;
            //maps.insert(maps.end(), diffuseMaps.begin(), diffuseMaps.end());
        }

        loadMaterials(material, aiTextureType_SPECULAR, "texture_specular", specularMaps);

        if (!specularMaps.empty()) {
            hasSpecularMap = true;
            //maps.insert(maps.end(), specularMaps.begin(), specularMaps.end());
        }

        loadMaterials(material, aiTextureType_HEIGHT /*aiTextureType_NORMALS*/, "texture_normal", normalMaps);
        if (!specularMaps.empty()) {
            hasSpecularMap = true;
            //maps.insert(maps.end(), normalMaps.begin(), normalMaps.end());
        }
/*
        heightMaps = loadMaterials(material, aiTextureType_AMBIENT, "texture_height");
        if (!heightMaps.empty())
            maps.insert(maps.end(), heightMaps.begin(), heightMaps.end());
*/
    }

    aiString name;
    PrincipledMaterial material = PrincipledMaterial();

    if(AI_SUCCESS != scene->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_NAME, name))
        LOG_ERROR("Error : material has no name")
    else {
        char* nameTmp = new char[name.length+1];

        strcpy(&nameTmp[0], &name.data[0]);
        nameTmp[name.length] = '\0';
        material.setName(nameTmp);

        delete [] nameTmp;
    }


    aiColor3D color;
    if (AI_SUCCESS != scene->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
        LOG_ERROR("No diffuse color for material : %n", name.C_Str())
        material.setDiffuseColor(glm::vec4(1.f));
    }
    else {
        material.setDiffuseColor(glm::vec4(color.r, color.g, color.b, 1.f));
    }

    if (AI_SUCCESS != scene->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
        LOG_ERROR("No specular color for material : %n", name.C_Str())
        material.setSpecularColor(glm::vec3(0));
    }
    else {
        material.setSpecularColor(glm::vec3(color.r, color.g, color.b));
        float spec = color.r;
        glm::clamp(spec, 0.f, 1.f);
        material.setRoughness(1.f-spec);
    }


    if (AI_SUCCESS != scene->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_AMBIENT, color)) {
        material.setAmbianteColor(glm::vec3(0));
    }
    else {
        material.setAmbianteColor(glm::vec3(color.r, color.g, color.b));
    }

    if (AI_SUCCESS != scene->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_EMISSIVE, color)) {
        material.setEmissiveColor(glm::vec3(0));
    }
    else {
        material.setEmissiveColor(glm::vec3(color.r, color.g, color.b));
    }

    float transparency;
    if (AI_SUCCESS != scene->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_OPACITY, transparency)) {
        material.setTransparency(1.f);
    }
    else {
        material.setTransparency(transparency);
    }

    Material *mat = nullptr;
    if (!m_renderData->isExistMaterial(&material)) {
        mat = m_renderData->addObject(material);
    }
    else {
        mat = m_renderData->getMaterial(material.getName());
    }

    if (!diffuseMaps.empty()) {
        mat->addTexture(m_renderData->getTexture(diffuseMaps[0]));
    }
    if (!specularMaps.empty()) {
        mat->addTexture(m_renderData->getTexture(specularMaps[0]));
    }
    if (!normalMaps.empty()) {
        mat->addTexture(m_renderData->getTexture(normalMaps[0]));
    }

    outMesh = Mesh(vertices, indices, mat);

    outMesh.setIsDiffuseMaped(hasDiffuseMap);
    outMesh.setIsNormalMaped(hasNormalMap);
    outMesh.setIsSpecularMaped(hasSpecularMap);

    glm::mat4 meshModel = glm::make_mat4x4(&transformation[0][0]);
    meshModel = glm::transpose(meshModel);
    outMesh.setTransform(meshModel);

    char* nameTmp = new char[mesh->mName.length+1];

    strcpy(&nameTmp[0], &mesh->mName.data[0]);
    nameTmp[mesh->mName.length] = '\0';
    std::string strName = std::string(nameTmp);
    outMesh.setName(strName);

    delete [] nameTmp;
}


uint FileLoader::textureFromFile(const char *fileName, const std::string &, uint &, uint &, uint& ) {

    std::string filename = std::string(fileName);
    filename = lastDirectory + fileName;/*directory + '/' + filename;*/
    filename = revertSlash(filename);
    LOG("Load Texture : %n", filename);

    Texture texture = Texture(filename);

    texture.setName(filename);

    if (!m_renderData->isExistTexture(texture.getName())) {
        m_renderData->addObject(texture);
        return texture.getID();
    }
    else {
        return m_renderData->getTexture(m_renderData->getTextureID(texture.getName()))->getID();
    }

    return -1;
}


void FileLoader::loadMaterials(aiMaterial *mat, aiTextureType type, std::string typeName, std::vector<uint>& indices) {


    // indices.resize(mat->GetTextureCount(type));
    for (uint i = 0; i < mat->GetTextureCount(type); i++) {

        aiString str;
        mat->GetTexture(type, i, &str);

        std::string texName = lastDirectory + std::string(str.C_Str());/*directory + '/' + filename;*/
        texName = revertSlash(texName);

        bool exist = m_renderData->isExistTexture(texName);

        if (!exist) {

            uint width, height, nbChannel;
            textureFromFile(str.C_Str(), lastPath, width, height, nbChannel);

            uint lastId = m_renderData->getTextureCount()-1;
            m_renderData->getTexture(lastId)->setTextureType(typeName);

            indices.push_back(lastId);

        } else {

            uint texID = m_renderData->getTextureID(str.C_Str());

            if (texID != uint(-1))
                indices.push_back(texID);

        }
    }

}
/*
void FileLoader::loadBones(unsigned int meshInd, aiMesh* mesh, std::vector<Mesh::Bone>& bones) {

    for (unsigned int i = 0; i < mesh->mNumBones; i++) {
        unsigned int boneId = 0;
        std::string name = std::string(mesh->mBones[i]->mName.C_Str());


        if (bones.find(name) == bones.end()) {
            boneId =
        }


    }

}*/

Bone* FileLoader::findBone(std::string name) {

    for (uint i = 0; i < bones.size(); i++) {
        if (bones[i].getName() == name)
            return &bones.at(i);
    }

    return nullptr;
}

aiNode* FileLoader::findNode(std::string name) {
    for (uint i = 0; i < aiNodes.size(); i++) {
        if (aiNodes[i]->mName.data == name )
            return aiNodes.at(i);
    }
    return nullptr;
}
/*
aiNodeAnim* FileLoader::findAiNodeAnim(std::string name) {
    for (unsigned int i = 0; i < aiNodesAnim.size(); i++) {
        if (std::string(aiNodesAnim[i]->mNodeName.C_str()) == name)
            return aiNodesAnim[i];
    }
}
*/



