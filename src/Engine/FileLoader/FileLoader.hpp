//
// Created by jb on 01/03/18.
//
#pragma once
#ifndef HELLOOPENGL_FILELOADER_HPP
#define HELLOOPENGL_FILELOADER_HPP


#include <glm/vec3.hpp>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <Engine/Scenes/RenderData.hpp>

#include <Engine/Modeling/Mesh/Object.hpp>

#include <assimp/mesh.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <sstream>
#include <glm/geometric.hpp>
#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Rendering/Light/Lights.hpp>



class FileLoader {

public:
    FileLoader(RenderData* renderData);
    bool objLoader(std::string name, bool raytracerVisible = true);
    std::vector<glm::vec3> getVertices();
    std::vector<glm::vec3> getTextures();
    std::vector<glm::vec3> getNormals();
    std::vector<int> getIndices();
    Object getObj();
    void clear();

    const std::vector<Material> &getMaterials() const;

private:

    std::string lastPath;
    std::string lastDirectory;
    std::string lastFileName;

    void splitPath();
    std::vector<Mesh> meshes;
    std::vector<Material> materials;
    std::vector<aiNode*> aiNodes;
    std::vector<aiNodeAnim*> aiNodesAnim;
    glm::mat4 globalTransformInv;
    std::vector<Bone> bones;
    //std::vector<Light*> lights;

    glm::mat4 cameraMatrix;

    GLuint texFormat;

    std::string fileType;
    std::string getLineHeader(std::string line);
    std::string fillEmptyValues(std::string line);
    std::string remplaceSeparator(std::string line);
    std::string revertSlash(std::string line);
    std::vector<std::string> splitValues(std::string line);

    void processSubMesh(aiNode* subMesh, const aiScene *scene);
    void processMesh(aiMesh *mesh, const aiScene *scene, const aiMatrix4x4 transformation, Mesh& outMesh);
    void processLights(const aiScene *scene);
    void processNodAnim(const aiScene *scene);
    std::string getExtension(std::string name);
    void loadMaterials(aiMaterial *mat, aiTextureType type, std::string typeName, std::vector<uint>&);
    unsigned int textureFromFile(const char *fileName, const std::string &directory, uint& w, uint& h, uint& nbChannel);
    std::vector<Vertex> calcTangentBiTangent(aiMesh* mesh);
    void standardScale(Object &obj);
    void transformLight(aiNode* subMesh);
    //void loadBones(unsigned int meshInd, aiMesh* mesh, std::vector<Mesh::Bone>& bones);

    Bone* findBone(std::string name);
    aiNode* findNode(std::string name);
    aiNodeAnim* findAiNodeAnim(std::string name);

    //debug function
    void printObjHierarchy(aiNode* mesh, uint depth = 0);


    void processSubNode(aiNode* node);

    glm::mat4 toGlm(aiMatrix4x4 mat);

    //map of m_fbTexture ID and m_fbTexture data used by path tracer
    std::map<uint, unsigned char*> m_textures;

    RenderData* m_renderData = nullptr;
};


#endif //HELLOOPENGL_FILELOADER_H
