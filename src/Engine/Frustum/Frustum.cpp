//
// Created by jb on 24/11/2020.
//

#include <Engine/Modeling/Geometry/BaseObjects.hpp>
#include "Frustum.hpp"

Frustum::Frustum(uint width, uint height) {

    m_width = width;
    m_height = height;
    computeRatio();
    computeFrustum();

}

Frustum::Frustum(const Frustum& frustum) {
    m_far = frustum.m_far;
    m_near = frustum.m_near;
    m_fov = frustum.m_fov;
    m_width = frustum.m_width;
    m_height = frustum.m_height;
    m_ratio = frustum.m_ratio;

    m_position = frustum.m_position;
    m_up = frustum.m_up;
    m_front = frustum.m_front;

    m_projection = frustum.m_projection;
    m_view = frustum.m_view;

    m_type = frustum.m_type;

    for (uint i = 0; i < 6; ++i)
        m_planes[i] = frustum.m_planes[i];

    computePlanes();
    computeFrustum();
}

void Frustum::setNear(float near) {
    if (near > std::numeric_limits<float>::min()) {
        if (near > m_far)
            m_far = near + std::numeric_limits<float>::min();
        m_near = near;
        computeFrustum();
    }
}

void Frustum::setFar(float far) {

    if (far > std::numeric_limits<float>::min()) {
        if (far < m_near && ((m_near - std::numeric_limits<float>::min()) > 0.f))
            m_near = far - std::numeric_limits<float>::min();
        m_far = far;
        computeFrustum();
    }
}

void Frustum::setFOV(float fov) {
    if (fov > 0 && fov < 180) {
        m_fov = fov;
        computeFrustum();
    }
}

void Frustum::setWidth(uint width) {
    m_width = width;
    computeRatio();
    computeFrustum();
}

void Frustum::setHeight(uint height) {
    m_height = height;
    computeRatio();
    computeFrustum();
}

void Frustum::setPosition(const glm::vec3& position) {
    m_position = position;
    computeFrustum();
}
void Frustum::setUp(const glm::vec3& up) {
    m_up = up;
    computeFrustum();
}
void Frustum::setFront(const glm::vec3& front) {
    m_front = front;
    computeFrustum();
}

void Frustum::resize(uint width, uint height) {
    m_width = width;
    m_height = height;
    computeRatio();
    computeFrustum();
}

float Frustum::getNear() const {
    return m_near;
}

float Frustum::getFar() const {
    return m_far;
}

float Frustum::getFOV() const {
    return m_fov;
}

uint Frustum::getWidth() const {
    return m_width;
}

uint Frustum::getHeight() const {
    return m_height;
}

glm::mat4 Frustum::getProjection() const {
    return m_projection;
}

glm::mat4 Frustum::getView() const {
    return m_view;
}

void Frustum::computeRatio() {
    m_ratio = (float)m_width / (float)m_height;
}

void Frustum::computeOrthographicFrustum() {
    m_projection = glm::ortho(-float(m_width)/2.f, float(m_width)/2.f, float(m_height)/2.f, -float(m_height)/2.f);
}

void Frustum::computePerspectiveFrustum() {
    m_projection = glm::perspective(glm::radians(m_fov), m_ratio, m_near, m_far);
}

void Frustum::computeFrustum() {

    if (m_type == FrustumType::Perspective) {
        computePerspectiveFrustum();
    }
    if (m_type == FrustumType::Orthographic) {
        computeOrthographicFrustum();
    }
    m_view = glm::lookAt(m_position, m_front, m_up);
    computePlanes();
}

void Frustum::computePlanes() {

    glm::mat4 mat = glm::transpose(m_projection * m_view);

    m_planes[LEFT]   = mat[3] + mat[0];
    m_planes[RIGHT]  = mat[3] - mat[0];
    m_planes[TOP]    = mat[3] - mat[1];
    m_planes[BOTTOM] = mat[3] + mat[1];
    m_planes[NEAR]   = mat[3] + mat[2];
    m_planes[FAR]    = mat[3] - mat[2];


        for (uint i = 0; i < 6; ++i) {
        float x = m_planes[i].x, y = m_planes[i].y, z = m_planes[i].z;
        float length = std::sqrt(x*x+y*y+z*z);
        m_planes[i] = m_planes[i]/length;
    }

}

bool Frustum::isCull(const AABB& aabb) const {
    return !isInside(aabb);
}


//https://www.iquilezles.org/www/articles/frustumcorrect/frustumcorrect.htm
bool Frustum::isInside(const AABB& box) const {

    int out;

    // check box outside/inside of frustum
    for (uint i = 0; i < 6; ++i) {
        out = 0;
        for (uint j = 0; j < 8; ++j) {
            glm::vec4 point = glm::vec4(box.corner(j), 1.f);
            out += (glm::dot(m_planes[i], point) < 0.f ? 1 : 0);
        }
        if (out == 8) return false;
    }

    // check frustum outside/inside box
    out=0; for (uint i = 0; i < 8; ++i) out += ((corner(i).x > box.max.x) ? 1 : 0); if (out == 8) return false;
    out=0; for (uint i = 0; i < 8; ++i) out += ((corner(i).x < box.min.x) ? 1 : 0); if (out == 8) return false;
    out=0; for (uint i = 0; i < 8; ++i) out += ((corner(i).y > box.max.y) ? 1 : 0); if (out == 8) return false;
    out=0; for (uint i = 0; i < 8; ++i) out += ((corner(i).y < box.min.y) ? 1 : 0); if (out == 8) return false;
    out=0; for (uint i = 0; i < 8; ++i) out += ((corner(i).z > box.max.z) ? 1 : 0); if (out == 8) return false;
    out=0; for (uint i = 0; i < 8; ++i) out += ((corner(i).z < box.min.z) ? 1 : 0); if (out == 8) return false;

    return true;
}

void Frustum::updateView(const glm::vec3& position, const glm::vec3& up, const glm::vec3& front) {
    m_position = position;
    m_up = up;
    m_front = front;
    computeFrustum();
}

glm::vec3 Frustum::corner(uint i) const {
    glm::vec3 point = glm::vec3(m_cornerSigns[i*3], m_cornerSigns[i*3+1], m_cornerSigns[i*3+2]);
    glm::vec4 res = glm::inverse(m_projection * m_view) * glm::vec4(point, 1.f);
    res = res/res.w;
    return res;
}

void Frustum::generateMesh() {

    std::vector<glm::vec3> points;
    points.resize(8);
    for (uint i = 0; i < 8; ++i)
        points[i] = corner(i);

    m_layoutMesh = std::make_unique<ObjectLine>(BaseObjects::boxLine(points));
}


void Frustum::drawLayout(Shader& shader) {

    generateMesh();
    if (m_layoutMesh.get())
        m_layoutMesh->draw(shader);

}