//
// Created by jb on 24/11/2020.
//

#ifndef MOVE_FRUSTUM_HPP
#define MOVE_FRUSTUM_HPP
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Core/Math/BoundingVolumes/Aabb.hpp>

class Frustum {

public :

    enum FrustumType {
        Perspective,
        Orthographic
    };

    Frustum(uint width = 800, uint height = 600);
    Frustum(const Frustum& frustum);
    Frustum& operator=(const Frustum& frustum) {

        if (&frustum == this)
            return *this;

        m_far = frustum.m_far;
        m_near = frustum.m_near;
        m_fov = frustum.m_fov;
        m_width = frustum.m_width;
        m_height = frustum.m_height;
        m_ratio = frustum.m_ratio;

        m_position = frustum.m_position;
        m_up = frustum.m_up;
        m_front = frustum.m_front;

        m_projection = frustum.m_projection;
        m_view = frustum.m_view;

        m_type = frustum.m_type;

        for (uint i = 0; i < 6; ++i)
            m_planes[i] = frustum.m_planes[i];

        computePlanes();
        computeFrustum();

        return *this;
    }

    void setNear(float near);
    void setFar(float far);
    void setFOV(float fov);
    void setWidth(uint width);
    void setHeight(uint height);
    void resize(uint width, uint height);

    void drawLayout(Shader& shader);

    glm::vec3 corner(uint i) const;

    void setPosition(const glm::vec3& position);
    void setUp(const glm::vec3& up);
    void setFront(const glm::vec3& front);

    void updateView(const glm::vec3& position, const glm::vec3& up, const glm::vec3& front);

    float getNear() const;
    float getFar() const;
    float getFOV() const;
    uint getWidth() const;
    uint getHeight() const;

    glm::mat4 getProjection() const;
    glm::mat4 getView() const;

    bool isCull(const AABB& aabb) const;

private :

    enum PlanesName {
        LEFT = 0,
        RIGHT,
        TOP,
        BOTTOM,
        NEAR,
        FAR
    };
    static constexpr const char* m_planesName[] = {
            "LEFT",
            "RIGHT",
            "TOP",
            "BOTTOM",
            "NEAR",
            "FAR"
    };

    static constexpr float m_cornerSigns[] = {
            -1.f, -1.f,  1.f,
            -1.f,  1.f,  1.f,
            1.f,  1.f,  1.f,
            1.f, -1.f,  1.f,
            1.f, -1.f, -1.f,
            -1.f, -1.f, -1.f,
            -1.f,  1.f, -1.f,
            1.f,  1.f, -1.f
    };

    void computeFrustum();
    void computeOrthographicFrustum();
    void computePerspectiveFrustum();
    void computeRatio();
    void computePlanes();
    bool isInside(const AABB& aabb) const;

    void generateMesh();

    float m_far = 32000.f;
    float m_near = 0.1f;
    float m_fov = 45.f;
    uint m_width = 800;
    uint m_height = 600;
    float m_ratio = 800.f/600.f;

    glm::vec3 m_position = glm::vec3(0.f);
    glm::vec3 m_up = glm::vec3(0.f, 1.f, 0.f);
    glm::vec3 m_front = glm::vec3(0.f, 0.f, -1.f);

    glm::mat4 m_projection = glm::mat4(1.f);
    glm::mat4 m_view = glm::mat4(1.f);

    FrustumType m_type = FrustumType::Perspective;

    glm::vec4 m_planes[6];

    std::unique_ptr<ObjectLine> m_layoutMesh;

};


#endif //MOVE_FRUSTUM_HPP
