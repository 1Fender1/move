#include "Bone.hpp"

Bone::Bone(Mesh* mesh, unsigned int id, std::string name = "", glm::mat4 offMat = glm::mat4()) {
    this->mesh = mesh;
    this->boneID = id;
    this->name = name;
    this->offsetMat = offMat;
    parentBone = nullptr;
    node = nullptr;
}

Bone::Bone(unsigned int id) {
    this->boneID = id;
    parentBone = nullptr;
    offsetMat = glm::mat4();
    node = nullptr;
}


Bone::Bone() {
    offsetMat = glm::mat4();
    parentBone = nullptr;
    node = nullptr;
}

std::string Bone::getName() {
    return name;
}

unsigned int Bone::id() {
    return boneID;
}

unsigned int Bone::nbBones() {
    return nbBones(findRoot(this));
}

unsigned int Bone::nbBones(Bone* bone) {
    if (bone == nullptr)
        return 0;
    for (auto b : childBones) {
        return 1 + nbBones(b);
    }
    return 0;
}


glm::mat4 Bone::toGlm(aiMatrix4x4 mat) {
    glm::mat4 transformGlm = glm::mat4(1);

    aiVector3t<ai_real> tScaling, tRotationAxis, tTranslation;
    ai_real tAngle;
    mat.Decompose(tScaling, tRotationAxis, tAngle, tTranslation);
    float angle = float(tAngle);
    glm::vec3 scaling, rotationAxis, translation;
    scaling = glm::vec3(tScaling[0], tScaling[1], tScaling[2]);
    rotationAxis = glm::vec3(tRotationAxis[0], tRotationAxis[1], tRotationAxis[2]);
    translation = glm::vec3(tTranslation[0], tTranslation[1], tTranslation[2]);

    transformGlm = glm::rotate(transformGlm, angle, rotationAxis);
    transformGlm = glm::translate(transformGlm, translation);
    transformGlm = glm::scale(transformGlm, scaling);

    /*
    transformGlm[0][0] = (GLfloat)mat.a1; transformGlm[0][1] = (GLfloat)mat.b1;
    transformGlm[0][2] = (GLfloat)mat.c1; transformGlm[0][3] = (GLfloat)mat.d1;
    transformGlm[1][0] = (GLfloat)mat.a2; transformGlm[1][1] = (GLfloat)mat.b2;
    transformGlm[1][2] = (GLfloat)mat.c2; transformGlm[1][3] = (GLfloat)mat.d2;
    transformGlm[2][0] = (GLfloat)mat.a3; transformGlm[2][1] = (GLfloat)mat.b3;
    transformGlm[2][2] = (GLfloat)mat.c3; transformGlm[2][3] = (GLfloat)mat.d3;
    transformGlm[3][0] = (GLfloat)mat.a4; transformGlm[3][1] = (GLfloat)mat.b4;
    transformGlm[3][2] = (GLfloat)mat.c4; transformGlm[3][3] = (GLfloat)mat.d4;
    */

    return transformGlm;
}

Bone* Bone::findBone(Bone* bone, unsigned int id) {
    if (bone->boneID == id)
        return bone;
    else
        for (unsigned int i = 0; i < bone->childBones.size(); i++)
            return findBone(bone->childBones[i], id);
    return nullptr;
}

Bone* Bone::findBone(unsigned int id) {
    Bone* bone = findRoot(this);
    return findBone(bone, id);
}

Bone* Bone::findRoot(Bone* bone) {
    if (bone->parentBone == nullptr)
        return bone;
    else
        return findRoot(bone->parentBone);
}


glm::mat4 Bone::getParentTransforms() {
    if (parentBone != nullptr)
        return parentBone->offsetMat;
    else
        return glm::mat4();
}

void Bone::addBone(Bone *child) {
    child->parentBone = this;
    this->childBones.emplace_back(child);
}
