#pragma once
#ifndef BONE_HPP
#define BONE_HPP

#include <glm/vec3.hpp>
#include <vector>
#include <assimp/mesh.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/geometric.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


//#include "Mesh.h"

class Mesh;

class Bone {

public:
    Bone();
    Bone(Mesh* mesh, unsigned int id, std::string name, glm::mat4 offMat);
    Bone(unsigned int id);

    std::string getName();
    void addBone(Bone *child);
    unsigned int id();
    Bone* findBone(Bone* bone, unsigned int id);
    Bone* findBone(unsigned int id);

    Bone* findRoot(Bone* bone);
    void rotateBone(glm::mat4 rotationMat);

    glm::mat4 getParentTransforms();
    unsigned int nbBones();

    //TODO make private + make getter & setter
    glm::vec3 position;
    glm::mat4 offsetMat;
    glm::vec3 direction;
    glm::mat4 localTranslation;
    Bone *parentBone;
    float length;




private:

    unsigned int nbBones(Bone* bone);

    std::string name;
    //std::vector<Mesh::Weight> weights;
    std::vector<Bone*> childBones;
    Mesh *mesh;
    unsigned int boneID;
    aiNode* node;
    aiNodeAnim* animNode;
    Bone* getRoot(Bone* curr);
    glm::mat4 toGlm(aiMatrix4x4 mat);

};

#endif // BONE_H
