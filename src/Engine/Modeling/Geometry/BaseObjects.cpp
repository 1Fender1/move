#include "BaseObjects.hpp"

#include <Engine/Modeling/Geometry/Meshtools.hpp>
#include <Core/Math/Utils/Utils.hpp>

BaseObjects::BaseObjects() {

}


void getOrthogonalVectors(glm::vec3& fx, glm::vec3& fy, glm::vec3& fz) {
    //for numerical stability, and seen that z will always be present, take the greatest component between x and y.
    if (std::abs(fx[0]) > std::abs(fx[1])) {
        float inv_len = 1.f / std::sqrt(fx[0] * fx[0] + fx[2] * fx[2]);
        glm::vec3 tmp(-fx[2] * inv_len, 0.f, fx[0] * inv_len);
        fy = tmp;
    }
    else {
        float inv_len = 1.f / sqrtf(fx[1] * fx[1] + fx[2] * fx[2]);
        glm::vec3 tmp(0.f, fx[2] * inv_len, -fx[1] * inv_len);
        fy = tmp;
    }
    //fz = fx.cross(fy);
    fz = glm::cross(fx, fy);
}

Object BaseObjects::cylinder(float length, float radius, uint uRes, uint vRes) {

    Mesh mesh = Mesh();

    //Generate Top and Botton vertex
    glm::vec3 cylinderBottom = glm::vec3(0.f, -length/2.f, 0.f);
    glm::vec3 cylinderTop = glm::vec3(0.f, length/2.f, 0.f);
    Vertex vertexBottom, vertexTop;
    vertexBottom.normal = glm::vec3(0.f, -1.f, 0.f);
    vertexBottom.position = cylinderBottom;

    vertexTop.normal = glm::vec3(0.f, 1.f, 0.f);
    vertexTop.position = cylinderTop;

   // mesh.addVertex(vertexBottom);
    //mesh.addVertex(vertexTop);

    const glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);

    float lengthOffset = length/float(uRes);
    float radiusOffset = 1.f/float(vRes);
    //Generate vertices
    for (uint i = 0; i < uRes; ++i) {
        for (uint j = 0; j < vRes+1; ++j) {

            glm::vec3 centerPos = cylinderBottom + up * lengthOffset * float(i+1);
            float angle = 2.f * float(M_PI) * radiusOffset * float(j+1);
            glm::vec3 diskPos = radius * glm::vec3(glm::cos(angle), 0.f, glm::sin(angle));

            glm::vec3 vertexPos = diskPos + centerPos;

            //TODO verify
            glm::vec3 vertexNormal = glm::normalize(glm::vec3(vertexPos - centerPos));

            //TODO generate textureCoords

            Vertex vertex;
            vertex.position = vertexPos;
            vertex.normal = vertexNormal;

            mesh.addVertex(vertex);
        }
    }

    //Generate triangle without caps
    for (uint i = 0; i < uRes-1; ++i) {

        const uint ringStartIndex = i * (vRes + 1);
        const uint nextRingStartIndex = (i + 1) * (vRes + 1);

        for (uint j = 0; j < vRes; ++j) {

            uint t0 = ringStartIndex + j;
            uint t1 = ringStartIndex + j + 1;
            uint t2 = nextRingStartIndex + j;
            uint t3 = nextRingStartIndex + j + 1;

            mesh.addTriangle(t0, t1, t2);
            mesh.addTriangle(t1, t3, t2);

        }
    }

    uint totalVertex = vRes * uRes;
    mesh.addVertex(vertexBottom);
    mesh.addVertex(vertexTop);
    const uint vertexSize = mesh.getVertices().size();

    //Bottom cap
    for (uint i = 0; i < vRes; ++i) {
        uint center = vertexSize - 2;
        uint t0 = i;
        uint t1 = i+1;

        mesh.addTriangle(t0, center, t1);
    }


    //Top cap
    for (uint i = 0; i < vRes; ++i) {
        uint center = vertexSize - 1;
        uint t0 = vertexSize - i - 2 - 1 -1;
        uint t1 = vertexSize - i - 2 - 1;

        mesh.addTriangle(t0, center, t1);
    }

    std::vector<Mesh> meshes;
    mesh.updateAABB();
    meshes.push_back(mesh);

    return Object(meshes, "Cylinder");
}

Object BaseObjects::cube(float length, float width, float depth) {

    float l = length / 2.f;
    float w = width / 2.f;
    float d = depth / 2.f;


    Mesh mesh = Mesh();

    for(int i = 0; i < 4*6; i+=4) {
        mesh.addTriangle(i, i+1, i+3);
        mesh.addTriangle(i+1, i+2, i+3);
    }

    Vertex tempV;
    tempV.position = glm::vec3(-l,-w,d);
    tempV.normal = glm::vec3(0,0,1);
    tempV.texCoords = glm::vec2(0,1);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l, w,d);
    tempV.normal = glm::vec3(0,0,1);
    tempV.texCoords = glm::vec2(0,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,w,d);
    tempV.normal = glm::vec3(0,0,1);
    tempV.texCoords = glm::vec2(1,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,-w,d);
    tempV.normal = glm::vec3(0,0,1);
    tempV.texCoords = glm::vec2(1,1);
    mesh.getVertices().emplace_back(tempV);

    //===============================

    tempV.position = glm::vec3(l,-w,d);
    tempV.normal = glm::vec3(1,0,0);
    tempV.texCoords = glm::vec2(0,1);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l, w,d);
    tempV.normal = glm::vec3(1,0,0);
    tempV.texCoords = glm::vec2(0,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,w,-d);
    tempV.normal = glm::vec3(1,0,0);
    tempV.texCoords = glm::vec2(1,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,-w,-d);
    tempV.normal = glm::vec3(1,0,0);
    tempV.texCoords = glm::vec2(1,1);
    mesh.getVertices().emplace_back(tempV);

    //=================================

    tempV.position = glm::vec3(l,-w,-d);
    tempV.normal = glm::vec3(0,0,-1);
    tempV.texCoords = glm::vec2(0,1);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l, w,-d);
    tempV.normal = glm::vec3(0,0,-1);
    tempV.texCoords = glm::vec2(0,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l,w,-d);
    tempV.normal = glm::vec3(0,0,-1);
    tempV.texCoords = glm::vec2(1,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l,-w,-d);
    tempV.normal = glm::vec3(0,0,-1);
    tempV.texCoords = glm::vec2(1,1);
    mesh.getVertices().emplace_back(tempV);

    //==================================

    tempV.position = glm::vec3(-l,-w,-d);
    tempV.normal = glm::vec3(-1,0,0);
    tempV.texCoords = glm::vec2(0,1);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l, w,-d);
    tempV.normal = glm::vec3(-1,0,0);
    tempV.texCoords = glm::vec2(0,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l,w,d);
    tempV.normal = glm::vec3(-1,0,0);
    tempV.texCoords = glm::vec2(1,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l,-w,d);
    tempV.normal = glm::vec3(-1,0,0);
    tempV.texCoords = glm::vec2(1,1);
    mesh.getVertices().emplace_back(tempV);

    //=================================

    tempV.position = glm::vec3(-l,w,d);
    tempV.normal = glm::vec3(0,1,0);
    tempV.texCoords = glm::vec2(0,1);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l, w,-d);
    tempV.normal = glm::vec3(0,1,0);
    tempV.texCoords = glm::vec2(0,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,w,-d);
    tempV.normal = glm::vec3(0,1,0);
    tempV.texCoords = glm::vec2(1,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,w,d);
    tempV.normal = glm::vec3(0,1,0);
    tempV.texCoords = glm::vec2(1,1);
    mesh.getVertices().emplace_back(tempV);

    //================================

    tempV.position = glm::vec3(-l,-w,-d);
    tempV.normal = glm::vec3(0,-1,0);
    tempV.texCoords = glm::vec2(0,1);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(-l, -w,d);
    tempV.normal = glm::vec3(0,-1,0);
    tempV.texCoords = glm::vec2(0,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,-w,d);
    tempV.normal = glm::vec3(0,-1,0);
    tempV.texCoords = glm::vec2(1,0);
    mesh.getVertices().emplace_back(tempV);

    tempV.position = glm::vec3(l,-w,-d);
    tempV.normal = glm::vec3(0,-1,0);
    tempV.texCoords = glm::vec2(1,1);
    mesh.getVertices().emplace_back(tempV);

    std::vector<Mesh> meshes;
    mesh.updateAABB();
    meshes.push_back(mesh);
    return Object(meshes, "mesh");
}


Object BaseObjects::cone(float length = 1.f, float radius = 0.5f, int subdivision = 10) {
    Mesh mesh = Mesh();

    std::vector<glm::vec3> vertices = std::vector<glm::vec3>(subdivision + 2);
    std::vector<glm::vec2> uv = std::vector<glm::vec2>(vertices.size());
    std::vector<uint> triangles = std::vector<uint>(subdivision*2*3);

    vertices[0] = glm::vec3(0.f);
    uv[0] = glm::vec2(0.5f, 0.f);
    for (int i = 0, n = subdivision - 1; i < subdivision; i++) {
        float ratio = float(i) / float(n);
        float r = ratio * glm::pi<float>() * 2.f;
        float x = glm::cos(r) * radius;
        float z = glm::sin(r) * radius;
        vertices[i+1] = glm::vec3(x, 0.f, z);

        uv[i+1] = glm::vec2(ratio, 0.f);
    }
    vertices[subdivision + 1] = glm::vec3(0.f, length, 0.f);
    uv[subdivision + 1] = glm::vec2(0.5f, 1.f);

    //bottom
    for (int i = 0, n = subdivision - 1; i < n; i++) {
        int offset = i * 3;
        triangles[offset] = 0;
        triangles[offset + 1] = i + 1;
        triangles[offset + 2] = i + 2;
    }

    //sides
    int bottomOffset = subdivision * 3;
    for (int i = 0, n = subdivision - 1; i < n; i++) {
        int offset = i * 3 + bottomOffset;
        triangles[offset] = i + 1;
        triangles[offset + 1] = subdivision + 1;
        triangles[offset + 2] = i + 2;
    }

    for (uint i = 0; i < vertices.size(); i++) {
        Vertex tmp;
        tmp.position = vertices[i];
        tmp.texCoords = uv[i];
        mesh.addVertex(tmp);
        //mesh.addTriangle(triangles[i], triangles[i+1], triangles[i+2]);
    }
    mesh.getIndices() = triangles;
    mesh.computeSmoothNormals();
    mesh.updateAABB();

    std::vector<Mesh> meshes;
    meshes.push_back(mesh);
    Object obj = Object(meshes, "Cone");

    return obj;
}



Object BaseObjects::icoSphere(float , uint subdivision) {

    const float t = (1.f + glm::sqrt(5.f)) / 2.f;
    Mesh sphere;
    //First step generate a icosahedron
    std::vector<Vertex>& vertices = sphere.getVertices();
//    std::vector<uint>& indices = sphere.getIndices();
    sphere.addVertex(glm::vec3(-1.0,  t, 0.0));
    sphere.addVertex(glm::vec3( 1.0,  t, 0.0));
    sphere.addVertex(glm::vec3(-1.0, -t, 0.0));
    sphere.addVertex(glm::vec3( 1.0, -t, 0.0));
    sphere.addVertex(glm::vec3(0.0, -1.0,  t));
    sphere.addVertex(glm::vec3(0.0,  1.0,  t));
    sphere.addVertex(glm::vec3(0.0, -1.0, -t));
    sphere.addVertex(glm::vec3(0.0,  1.0, -t));
    sphere.addVertex(glm::vec3( t, 0.0, -1.0));
    sphere.addVertex(glm::vec3( t, 0.0,  1.0));
    sphere.addVertex(glm::vec3(-t, 0.0, -1.0));
    sphere.addVertex(glm::vec3(-t, 0.0,  1.0));

    for (uint i = 0; i < vertices.size(); ++i) {
        vertices[i].position = glm::normalize(vertices[i].position);
    }

    sphere.addTriangle(0, 11, 5);
    sphere.addTriangle(0, 5, 1);
    sphere.addTriangle(0, 1, 7);
    sphere.addTriangle(0, 7, 10);
    sphere.addTriangle(0, 10, 11);
    sphere.addTriangle(1, 5, 9);
    sphere.addTriangle(5, 11, 4);
    sphere.addTriangle(11, 10, 2);
    sphere.addTriangle(10, 7, 6);
    sphere.addTriangle(7, 1, 8);
    sphere.addTriangle(3, 9, 4);
    sphere.addTriangle(3, 4, 2);
    sphere.addTriangle(3, 2, 6);
    sphere.addTriangle(3, 6, 8);
    sphere.addTriangle(3, 8, 9);
    sphere.addTriangle(4, 9, 5);
    sphere.addTriangle(2, 4, 11);
    sphere.addTriangle(6, 2, 10);
    sphere.addTriangle(8, 6, 7);
    sphere.addTriangle(9, 8, 1);

    //Second setp subivide
    Mesh meshOut;
    meshOut = MeshTools::subdivide(sphere, subdivision);

    //Compute normals
    meshOut.computeSmoothNormals();
    meshOut.updateAABB();

    //TODO compute texCoords, tangents, bitangents
    std::vector<Mesh> meshes = {meshOut};
    return Object(meshes, "IcoSphere");
}


Object BaseObjects::quad(float width, float height) {

    const glm::vec3 direction = glm::vec3(0.f, 1.f, 0.f);

    const float halfWidth = width / 2.f;
    const float halfHeight = height / 2.f;

    Mesh mesh;
    Vertex v1, v2, v3, v4;
    v1.position = glm::vec3(-halfWidth, 0.f, -halfHeight);
    v1.normal = direction;
    v1.texCoords = glm::vec2(0.f, 0.f);
    mesh.addVertex(v1);

    v2.position = glm::vec3(halfWidth, 0.f, -halfHeight);
    v2.normal = direction;
    v2.texCoords = glm::vec2(1.f, 0.f);
    mesh.addVertex(v2);

    v3.position = glm::vec3(halfWidth, 0.f, halfHeight);
    v3.normal = direction;
    v3.texCoords = glm::vec2(1.f, 1.f);
    mesh.addVertex(v3);

    v4.position = glm::vec3(-halfWidth, 0.f, halfHeight);
    v4.normal = direction;
    v4.texCoords = glm::vec2(0.f, 1.f);
    mesh.addVertex(v4);

    mesh.addTriangle(0, 1, 3);
    mesh.addTriangle(3, 1, 2);

    mesh.updateAABB();
    std::vector<Mesh> meshes = {mesh};
    return Object(meshes, "Plane");

}



Object BaseObjects::torus(float radius, float radiusH, uint uRes, uint vRes) {

    Mesh torus;

    float mainSegmentAngleStep = glm::radians(360.f / float(uRes));
    float tubeSegmentAngleStep = glm::radians(360.f / float(vRes));
    float mainSegmentTextureStep = 2.f / float(uRes);
    float tubeSegmentTextureStep = 1.f / float(vRes);
    float currentMainSegmentAngle = 0.f;
    float currentMainSegmentTexCoordV = 0.f;

    for (uint i = 0; i <= uRes; i++) {
        float sinMain = sin(currentMainSegmentAngle);
        float cosMain = cos(currentMainSegmentAngle);
        float currentTubeSegmentAngle = 0.f;
        float currentTubeSegmentTexCoordU = 0.0f;
        for (uint j = 0; j <= vRes; j++) {
            Vertex vertex;

            float sinTube = sin(currentTubeSegmentAngle);
            float cosTube = cos(currentTubeSegmentAngle);

            vertex.position = glm::vec3((radius + radiusH * cosTube)*cosMain,(radius + radiusH * cosTube)*sinMain,radiusH*sinTube);
            vertex.normal = glm::normalize(glm::vec3(cosMain*cosTube, sinMain*cosTube, sinTube));
            vertex.texCoords = glm::vec2(currentTubeSegmentTexCoordU, currentMainSegmentTexCoordV);

            torus.addVertex(vertex);

            currentTubeSegmentAngle += tubeSegmentAngleStep;
            currentTubeSegmentTexCoordU += tubeSegmentTextureStep;
        }
        currentMainSegmentAngle += mainSegmentAngleStep;
        currentMainSegmentTexCoordV += mainSegmentTextureStep;
    }

    for (uint i = 0; i < uRes; ++i) {

        const uint ringStartIndex = i * (vRes + 1);
        const uint nextRingStartIndex = (i + 1) * (vRes + 1);

        for (uint j = 0; j < vRes; ++j) {

            uint t0 = ringStartIndex + j;
            uint t1 = ringStartIndex + j + 1;
            uint t2 = nextRingStartIndex + j;
            uint t3 = nextRingStartIndex + j + 1;

            torus.addTriangle(t0, t1, t2);
            torus.addTriangle(t1, t3, t2);
        }
    }

    torus.updateAABB();
    std::vector<Mesh> meshes = {torus};
    return Object(meshes, "Torus");
}



Polyline BaseObjects::circleLine(const glm::vec3& center, const glm::vec3& normal, float radius, uint precision) {

    std::vector<Polyline::Vertex> vertices;
    vertices.resize(precision+1);
    float increment = 2.f * M_PI / float(precision);

    uint i = 0;
    for (float currAngle = 0.f; currAngle <= 2.f * M_PI; currAngle += increment, ++i) {
        Polyline::Vertex v;
        v.position = glm::vec3( (radius/2.f) * glm::sin(currAngle), (radius/2.f) * glm::cos(currAngle), 0.f);
        if (i < precision)
            vertices[i] = v;
    }
    vertices[precision] = vertices[0];

    glm::vec3 diskDirection = glm::vec3(0.f, 0.f, 1.f);

    glm::mat3 rmat = Core::Math::Utils::rotationMatFromDirections(diskDirection, normal);

    for (i = 0; i < precision+1; ++i) {
        glm::vec3 newPoint = (rmat * vertices[i].position) + center;
        vertices[i].position = newPoint;
    }

    Polyline obj = Polyline(vertices);
    obj.setName("Circle");
    return obj;
}


Polyline BaseObjects::squareLine(const glm::vec3& center, const glm::vec3& normal, float width, float height) {

    std::vector<Polyline::Vertex> vertices;
    vertices.resize(5);

    float halfWidth = width / 2.f;
    float halfHeight = height / 2.f;
    glm::vec3 direction = glm::vec3(0.f, 0.f, 1.f);

    vertices[0].position = glm::vec3(-halfWidth, -halfHeight, 0.f);
    vertices[1].position = glm::vec3(halfWidth, -halfHeight, 0.f);
    vertices[2].position = glm::vec3(halfWidth, halfHeight, 0.f);
    vertices[3].position = glm::vec3(-halfWidth, halfHeight, 0.f);
    vertices[4].position = vertices[0].position;

    glm::mat3 rmat = Core::Math::Utils::rotationMatFromDirections(direction, normal);
    for (uint i = 0; i < 5; ++i) {
        vertices[i].position = (rmat * vertices[i].position) + center;
    }

    Polyline obj = Polyline(vertices);
    obj.setName("Quad");
    return obj;
}

Polyline BaseObjects::line(const glm::vec3& p1, const glm::vec3& p2) {

    std::vector<Polyline::Vertex> vertices;
    vertices.resize(2);

    vertices[0].position = p1;
    vertices[1].position = p2;

    Polyline obj = Polyline(vertices);
    obj.setName("Line");
    return obj;

}

ObjectLine BaseObjects::aabbBox(const glm::vec3& pMin, const glm::vec3& pMax) {

    Polyline::Vertex p0, p1, p2, p3, p4, p5, p6, p7;

    p0.position = pMin;
    p1.position = glm::vec3(pMin.x, pMax.y, pMin.z);
    p2.position = glm::vec3(pMax.x, pMax.y, pMin.z);
    p3.position = glm::vec3(pMax.x, pMin.y, pMin.z);

    p4.position = glm::vec3(pMax.x, pMin.y, pMax.z);
    p5.position = glm::vec3(pMin.x, pMin.y, pMax.z);
    p6.position = glm::vec3(pMin.x, pMax.y, pMax.z);
    p7.position = pMax;

    std::vector<Polyline::Vertex> frontRectangle;
    frontRectangle.push_back(p0);
    frontRectangle.push_back(p1);
    frontRectangle.push_back(p2);
    frontRectangle.push_back(p3);
    frontRectangle.push_back(p0);

    std::vector<Polyline::Vertex> rightRectangle;
    rightRectangle.push_back(p3);
    rightRectangle.push_back(p4);
    rightRectangle.push_back(p7);
    rightRectangle.push_back(p2);
    rightRectangle.push_back(p3);

    std::vector<Polyline::Vertex> leftRectangle;
    leftRectangle.push_back(p0);
    leftRectangle.push_back(p1);
    leftRectangle.push_back(p6);
    leftRectangle.push_back(p5);
    leftRectangle.push_back(p0);

    std::vector<Polyline::Vertex> upRectangle;
    upRectangle.push_back(p1);
    upRectangle.push_back(p2);
    upRectangle.push_back(p7);
    upRectangle.push_back(p6);
    upRectangle.push_back(p1);

    std::vector<Polyline::Vertex> downRectangle;
    downRectangle.push_back(p0);
    downRectangle.push_back(p5);
    downRectangle.push_back(p4);
    downRectangle.push_back(p3);
    downRectangle.push_back(p0);

    std::vector<Polyline::Vertex> backRectangle;
    backRectangle.push_back(p5);
    backRectangle.push_back(p6);
    backRectangle.push_back(p7);
    backRectangle.push_back(p4);
    backRectangle.push_back(p5);

    Polyline faceFront = Polyline(frontRectangle);
    Polyline faceLeft = Polyline(leftRectangle);
    Polyline faceRight = Polyline(rightRectangle);
    Polyline faceUp = Polyline(upRectangle);
    Polyline faceDown = Polyline(downRectangle);
    Polyline faceBack = Polyline(backRectangle);
    ObjectLine obj = ObjectLine({faceFront, faceLeft, faceRight, faceUp, faceDown, faceBack});
    obj.setName("aabb");

    return obj;
}

ObjectLine BaseObjects::boxLine(const std::vector<glm::vec3>& points) {

    if (points.size() != 8)
        LOG_ERROR("BAD boxLine inputs, %n points instead of 8", points.size())

    Polyline::Vertex p0, p1, p2, p3, p4, p5, p6, p7;
    p0.position = points[0];
    p1.position = points[1];
    p2.position = points[2];
    p3.position = points[3];
    p4.position = points[4];
    p5.position = points[5];
    p6.position = points[6];
    p7.position = points[7];


    std::vector<Polyline::Vertex> frontRectangle;
    frontRectangle.push_back(p0);
    frontRectangle.push_back(p1);
    frontRectangle.push_back(p2);
    frontRectangle.push_back(p3);
    frontRectangle.push_back(p0);

    std::vector<Polyline::Vertex> rightRectangle;
    rightRectangle.push_back(p3);
    rightRectangle.push_back(p4);
    rightRectangle.push_back(p7);
    rightRectangle.push_back(p2);
    rightRectangle.push_back(p3);

    std::vector<Polyline::Vertex> leftRectangle;
    leftRectangle.push_back(p0);
    leftRectangle.push_back(p1);
    leftRectangle.push_back(p6);
    leftRectangle.push_back(p5);
    leftRectangle.push_back(p0);

    std::vector<Polyline::Vertex> upRectangle;
    upRectangle.push_back(p1);
    upRectangle.push_back(p2);
    upRectangle.push_back(p7);
    upRectangle.push_back(p6);
    upRectangle.push_back(p1);

    std::vector<Polyline::Vertex> downRectangle;
    downRectangle.push_back(p0);
    downRectangle.push_back(p5);
    downRectangle.push_back(p4);
    downRectangle.push_back(p3);
    downRectangle.push_back(p0);

    std::vector<Polyline::Vertex> backRectangle;
    backRectangle.push_back(p5);
    backRectangle.push_back(p6);
    backRectangle.push_back(p7);
    backRectangle.push_back(p4);
    backRectangle.push_back(p5);

    Polyline faceFront = Polyline(frontRectangle);
    Polyline faceLeft = Polyline(leftRectangle);
    Polyline faceRight = Polyline(rightRectangle);
    Polyline faceUp = Polyline(upRectangle);
    Polyline faceDown = Polyline(downRectangle);
    Polyline faceBack = Polyline(backRectangle);
    ObjectLine obj = ObjectLine({faceFront, faceLeft, faceRight, faceUp, faceDown, faceBack});
    obj.setName("box");

    return obj;
}