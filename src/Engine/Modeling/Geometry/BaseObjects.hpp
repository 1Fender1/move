#ifndef BASEOBJECTS_HPP
#define BASEOBJECTS_HPP

#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Modeling/Mesh/ObjectLine.hpp>

class BaseObjects {

public:
    static Object cylinder(float length, float radius, uint uRes = 64, uint vRes = 64);
    static Object cube(float length, float width, float depth);
    static Object cone(float length, float radius, int subdivision);
    static Object icoSphere(float radius, uint subdivision);
    static Object quad(float width, float height);
    static Object torus(float radius, float radiusH, uint uRes = 64, uint vRes = 64);


    static Polyline circleLine(const glm::vec3& center, const glm::vec3& normal, float radius, uint precision = 16);
    static Polyline squareLine(const glm::vec3& center, const glm::vec3& normal, float width, float height);
    static Polyline line(const glm::vec3& p1, const glm::vec3& p2);
    static ObjectLine aabbBox(const glm::vec3& pMin, const glm::vec3& pMax);
    static ObjectLine boxLine(const std::vector<glm::vec3>& points);

private:
    BaseObjects();


};

#endif // BASEOBJECTS_H
