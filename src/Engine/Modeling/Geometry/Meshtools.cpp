#include "Meshtools.hpp"
#include <OpenMesh/Tools/Subdivider/Uniform/LoopT.hh>

#define MAX_VALENCE 50

MeshTools::MeshTools() {}

Mesh MeshTools::subdivide(Mesh mesh, uint nbSubdiv) {

    OMesh temp = OpenMeshConverter::toOpenMesh(mesh);
    OpenMesh::Subdivider::Uniform::LoopT<OMesh, float> subdivider;
    subdivider.attach(temp);
    subdivider(nbSubdiv);
    subdivider.detach();

    temp.request_face_normals();
    temp.request_vertex_normals();
    temp.update_normals();
    //temp.request_vertex_texcoords2D();

    temp.request_face_status();
    temp.request_edge_status();
    temp.request_vertex_status();

    Mesh out = OpenMeshConverter::toMeshObj(temp);
    return out;
}

bool MeshTools::subdivideLoop(OMesh& mesh) {

    OpenMesh::VPropHandleT<typename OMesh::Point> vp_pos_;
    OpenMesh::EPropHandleT<typename OMesh::Point> ep_pos_;
    mesh.add_property( vp_pos_ );
    mesh.add_property( ep_pos_ );

    ///TODO:Implement fixed positions
    typename OMesh::FaceIter fit, f_end;
    typename OMesh::EdgeIter eit, e_end;
    typename OMesh::VertexIter vit;

    //move existants vertices
    for (vit = mesh.vertices_begin(); vit != mesh.vertices_end(); ++vit)
        smooth(mesh, *vit, vp_pos_);

    //new vertex calculation
    for (eit = mesh.edges_begin(); eit != mesh.edges_end(); ++eit)
        compute_midpoint(mesh, *eit, ep_pos_);

    //new edges
    e_end = mesh.edges_end();
    for (eit = mesh.edges_begin(); eit != e_end; ++eit)
        split_edge(mesh, *eit, vp_pos_, ep_pos_);

    //new faces
    f_end = mesh.faces_end();
    for (fit = mesh.faces_begin(); fit != f_end; ++fit)
        split_face(mesh, *fit );

    //update mesh properties
    for (vit  = mesh.vertices_begin(); vit != mesh.vertices_end(); ++vit)
        mesh.set_point(*vit, mesh.property(vp_pos_, *vit ) );

  return true;
}

float MeshTools::wheightOrig(int valence) {
    return (1.f/float(valence)*(5.f/8.f - (3.f/8.f + (1.f/4.f)*std::cos(2.f*M_PI/float(valence)))*(3.f/8.f + (1.f/4.f)*std::cos(2.f*M_PI/float(valence)))));
}

float MeshTools::wheightSimple(int valence) {
    return (valence == 3)?(3.f/16.f):(3.f/(8.f*valence));
}

//https://graphics.stanford.edu/~mdfisher/subdivision.html
void MeshTools::smooth(OMesh& mesh, const typename OMesh::VertexHandle& _vh, OpenMesh::VPropHandleT<typename OMesh::Point> &vp_pos_) {
  typename OMesh::Point pos(0.0, 0.0, 0.0);
  //boundary
  if (mesh.is_boundary(_vh)) {
    typename OMesh::HalfedgeHandle heh, prev_heh;
    heh = mesh.halfedge_handle(_vh);

    if (heh.is_valid()) {
        prev_heh = mesh.prev_halfedge_handle(heh);
        typename OMesh::VertexHandle to_vh = mesh.to_vertex_handle(heh);
        typename OMesh::VertexHandle from_vh = mesh.from_vertex_handle(prev_heh);
        pos = mesh.point(_vh);
        pos *= 6.f;
        pos += mesh.point(to_vh);
        pos += mesh.point(from_vh);
        pos *= 1.f/8.f;
    }
    else {
        LOG_ERROR("Error, half-edge not valid")
        return;
    }
  }
  else {
    typename OMesh::VertexVertexIter vvit;
    unsigned int valence = 0;

    for (vvit = mesh.vv_iter(_vh); vvit.is_valid(); ++vvit) {
        valence++;
        pos += mesh.point(*vvit);
    }
    float alpha = wheightSimple(valence);
    pos *= alpha/valence;
    pos += (1.f-alpha) * mesh.point(_vh);
  }
  mesh.property(vp_pos_, _vh) = pos;
}

//https://graphics.stanford.edu/~mdfisher/subdivision.html
void MeshTools::compute_midpoint(OMesh& mesh, const typename OMesh::EdgeHandle& _eh, OpenMesh::EPropHandleT<typename OMesh::Point> &ep_pos_) {
    typename OMesh::HalfedgeHandle heh, opp_heh;

    heh = mesh.halfedge_handle(_eh, 0);
    opp_heh = mesh.halfedge_handle(_eh, 1);

    typename OMesh::Point pos(mesh.point(mesh.to_vertex_handle(heh)));

    pos += mesh.point(mesh.to_vertex_handle(opp_heh));

    //boundary -> take center
    if (mesh.is_boundary(_eh) ) {
        pos *= static_cast<typename OMesh::Point::value_type>(0.5);
    }
    else { // inner edge: add neighbouring Vertices to sum
        pos *= 3.f;
        pos += mesh.point(mesh.to_vertex_handle(mesh.next_halfedge_handle(heh)));
        pos += mesh.point(mesh.to_vertex_handle(mesh.next_halfedge_handle(opp_heh)));
        pos *= 1.f/8.f;
    }
    mesh.property(ep_pos_, _eh) = pos;
}


void MeshTools::split_edge(OMesh& mesh, const typename OMesh::EdgeHandle& _eh, OpenMesh::VPropHandleT<typename OMesh::Point>& vp_pos_, OpenMesh::EPropHandleT<typename OMesh::Point>& ep_pos_) {
    typename OMesh::HalfedgeHandle
    heh = mesh.halfedge_handle(_eh, 0),
    opp_heh = mesh.halfedge_handle(_eh, 1);

    typename OMesh::HalfedgeHandle new_heh, opp_new_heh, t_heh;
    typename OMesh::VertexHandle vh;
    typename OMesh::VertexHandle vh1(mesh.to_vertex_handle(heh));
    typename OMesh::Point midP(mesh.point(mesh.to_vertex_handle(heh)));
    midP += mesh.point(mesh.to_vertex_handle(opp_heh));
    midP *= static_cast<typename OMesh::Point::value_type>(0.5);

    vh = mesh.new_vertex(midP);

    //save position
    mesh.property(vp_pos_, vh) = mesh.property(ep_pos_, _eh);


    //handle elements
    if (mesh.is_boundary(_eh)) {
        t_heh = heh;
        while (mesh.next_halfedge_handle(t_heh) != opp_heh)
            t_heh = mesh.opposite_halfedge_handle(mesh.next_halfedge_handle(t_heh));
    }
    else {
        t_heh = mesh.next_halfedge_handle(opp_heh);
        while (mesh.next_halfedge_handle(t_heh) != opp_heh)
            t_heh = mesh.next_halfedge_handle(t_heh);
    }

    new_heh = mesh.new_edge(vh, vh1);
    opp_new_heh = mesh.opposite_halfedge_handle(new_heh);
    mesh.set_vertex_handle(heh, vh);

    mesh.set_next_halfedge_handle(t_heh, opp_new_heh);
    mesh.set_next_halfedge_handle(new_heh, mesh.next_halfedge_handle(heh));
    mesh.set_next_halfedge_handle(heh, new_heh);
    mesh.set_next_halfedge_handle(opp_new_heh, opp_heh);

    if (mesh.face_handle(opp_heh).is_valid()) {
        mesh.set_face_handle(opp_new_heh, mesh.face_handle(opp_heh));
        mesh.set_halfedge_handle(mesh.face_handle(opp_new_heh), opp_new_heh);
    }

    mesh.set_face_handle(new_heh, mesh.face_handle(heh));
    mesh.set_halfedge_handle(vh, new_heh);
    mesh.set_halfedge_handle(mesh.face_handle(heh), heh);
    mesh.set_halfedge_handle(vh1, opp_new_heh);


    mesh.adjust_outgoing_halfedge(vh);
    mesh.adjust_outgoing_halfedge(vh1);
}

void MeshTools::split_face(OMesh& mesh, const typename OMesh::FaceHandle& _fh) {
    typename OMesh::HalfedgeHandle heh1(mesh.halfedge_handle(_fh));
    typename OMesh::HalfedgeHandle heh2(mesh.next_halfedge_handle(mesh.next_halfedge_handle(heh1)));
    typename OMesh::HalfedgeHandle heh3(mesh.next_halfedge_handle(mesh.next_halfedge_handle(heh2)));

    corner_cutting(mesh, heh1);
    corner_cutting(mesh, heh2);
    corner_cutting(mesh, heh3);
}

void MeshTools::corner_cutting(OMesh& mesh, const typename OMesh::HalfedgeHandle& _he) {

    typename OMesh::HalfedgeHandle heh1(_he);
    typename OMesh::HalfedgeHandle heh5(heh1);
    typename OMesh::HalfedgeHandle heh6(mesh.next_halfedge_handle(heh1));

    //find half-edge
    while (mesh.next_halfedge_handle(mesh.next_halfedge_handle(heh5)) != heh1)
        heh5 = mesh.next_halfedge_handle(heh5);

    typename OMesh::VertexHandle
    vh1 = mesh.to_vertex_handle(heh1),
    vh2 = mesh.to_vertex_handle(heh5);

    typename OMesh::HalfedgeHandle
    heh2(mesh.next_halfedge_handle(heh5)),
    heh3(mesh.new_edge( vh1, vh2)),
    heh4(mesh.opposite_halfedge_handle(heh3));

    typename OMesh::FaceHandle fh_old(mesh.face_handle(heh6));
    typename OMesh::FaceHandle fh_new(mesh.new_face());

    mesh.set_next_halfedge_handle(heh4, heh6);
    mesh.set_next_halfedge_handle(heh5, heh4);

    mesh.set_face_handle(heh4, fh_old);
    mesh.set_face_handle(heh5, fh_old);
    mesh.set_face_handle(heh6, fh_old);
    mesh.set_halfedge_handle(fh_old, heh4);

    mesh.set_next_halfedge_handle(heh1, heh3);
    mesh.set_next_halfedge_handle(heh3, heh2);
    mesh.set_face_handle(heh1, fh_new);
    mesh.set_face_handle(heh2, fh_new);
    mesh.set_face_handle(heh3, fh_new);

    mesh.set_halfedge_handle(fh_new, heh1);
}
