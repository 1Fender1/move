#ifndef MESHTOOLS_HPP
#define MESHTOOLS_HPP

#include "openMeshConverter.hpp"

class MeshTools {
public:

    static Mesh subdivide(Mesh mesh, unsigned int nbSubdiv);


private:
    MeshTools();

    static bool subdivideLoop(OMesh& mesh);
    static bool isPerfectConfig(OMesh::EdgeHandle eh, OMesh* mesh);
    static OMesh::Point edgeNewPoint(OMesh::EdgeHandle eh, OMesh *mesh);
    static OMesh::Point vertexNewPoint(OMesh::VertexHandle vh, OMesh *mesh);

    static void smooth(OMesh& mesh, const typename OMesh::VertexHandle& _vh, OpenMesh::VPropHandleT<typename OMesh::Point> &vp_pos_);
    static void compute_midpoint(OMesh& mesh, const typename OMesh::EdgeHandle& _eh, OpenMesh::EPropHandleT<typename OMesh::Point> &ep_pos_);
    static void split_edge(OMesh& mesh, const typename OMesh::EdgeHandle& _eh, OpenMesh::VPropHandleT<typename OMesh::Point>& vp_pos_, OpenMesh::EPropHandleT<typename OMesh::Point>& ep_pos_);
    static void split_face(OMesh& mesh, const typename OMesh::FaceHandle& _fh);
    static void corner_cutting(OMesh& mesh, const typename OMesh::HalfedgeHandle& _he);
    static float wheightOrig(int valence);
    static float wheightSimple(int valence);


};

#endif // MESHTOOLS_H
