#include "b_spline.hpp"

B_Spline::B_Spline() {
    model = glm::mat4();

    //temporary
    //std::vector<glm::vec3> pc = {glm::vec3(-1, 0, 1.3), glm::vec3(0, 1, 1.3), glm::vec3(1, 0, 1.3), glm::vec3(2,1,1.3), glm::vec3(3, 1, 1.3)};
    //std::vector<glm::vec3> pc = {glm::vec3(-1, 0, 1.3), glm::vec3(0, 1, 1.3), glm::vec3(1, 0, 1.3), glm::vec3(-1, 0, 1.3)};
    std::vector<glm::vec3> pc = {glm::vec3(-1, -0.5, 1.3), glm::vec3(-0.5, 0.5, 1.3), glm::vec3(0, -0.5, 1.3), glm::vec3(0.5, 0.5, 1.3), glm::vec3(1, -0.5, 1.3)};

    vertices = b_spline(pc, 4);


    setupMesh();
}

inline glm::vec3 lerp(glm::vec3 vec1, glm::vec3 vec2, float alpha) {
    return (1-alpha) * vec1 + alpha * vec2;
}


std::vector<glm::vec3> getColumn(std::vector<std::vector<glm::vec3>> vec, unsigned int col) {
    std::vector<glm::vec3> column;
    for (unsigned int i = 0; i < vec.size(); i++)
        column.emplace_back(vec[i][col]);
    return column;
}

std::vector<Vertex> getColumn(std::vector<std::vector<Vertex>> vec, unsigned int col) {
    std::vector<Vertex> column;
    for (unsigned int i = 0; i < vec.size(); i++)
        column.emplace_back(vec[i][col]);
    return column;
}



std::vector<std::vector<glm::vec3>> toVec3(std::vector<std::vector<Vertex>> vec) {
    if (vec.empty())
        return std::vector<std::vector<glm::vec3>>();
    std::vector<std::vector<glm::vec3>> out;
    out.resize(vec.size());

    for (unsigned int i = 0; i < vec.size(); i++) {
        out[i].resize(vec[i].size());
        for (unsigned int j = 0; j < vec[i].size(); j++) {
            out[i][j] = vec[i][j].position;
        }
    }
    return out;
}



glm::vec3 B_Spline::construct(std::vector<glm::vec3> controlPoints, const int order, std::vector<float> nodal, float u) {
    std::vector<glm::vec3> pOut;

    if (nodal.empty())
        for (unsigned int i = 0; i < order + controlPoints.size() + 1; i++)
            nodal.emplace_back(i);

    int i = order;
    int dec = 0;
    while (u > nodal[i++])
        dec++;

    for (int i = 0; i < order; i++)
        pOut.emplace_back(controlPoints[dec+i]);

    for (int i = order; i > 0; i--) {
        for (int j = 0; j < i-1; j++) {
            int u1 = dec + j + 1;
            int u2 = dec + j + i;
            float coef = (u - nodal[u1]) / (nodal[u2] - nodal[u1]);
            pOut[j] = lerp(pOut[j], pOut[j+1], coef);
        }
        dec++;
    }

    return pOut[0];
}

glm::vec3 B_Spline::construct(std::vector<Vertex> controlPoints, const int order, std::vector<float> nodal, float u) {

    std::vector<glm::vec3> vec;
    for (auto p : controlPoints)
        vec.emplace_back(p.position);

    return construct(vec, order, nodal, u);
}


std::vector<Vertex> B_Spline::b_spline(std::vector<glm::vec3> pc, int k) {
    vertices.clear();
    std::vector<float> nodal;
    for (unsigned int i = 0; i < k + pc.size() + 1; i++)
        nodal.emplace_back(i);

    float step = 0.1;
    std::vector<Vertex> bSpline;
    for (float u = nodal[k-1]; u <= nodal[pc.size()]; u+= step) {
        glm::vec3 newP = construct(pc, k, nodal, u);
        Vertex vertex;
        vertex.position = newP;
        bSpline.emplace_back(vertex);
    }

    return bSpline;
}

std::vector<Vertex> B_Spline::b_spline(std::vector<Vertex> pc, int k) {

    std::vector<glm::vec3> vec;
    for (auto p : pc)
        vec.emplace_back(p.position);

    return b_spline(vec, k);
}

Mesh B_Spline::b_spline2D(std::vector<std::vector<glm::vec3>> pc, int k) {
    vertices.clear();
    std::vector<float> nodalU;
    for (unsigned int i = 0; i < k + pc[0].size() + 1; i++)
        nodalU.emplace_back(i);

    std::vector<float> nodalV;
    for (unsigned int i = 0; i < k + pc.size() + 1; i++)
        nodalV.emplace_back(i);

    float stepU = 0.1;
    float stepV = 0.1;


    for (float u = nodalU[k-1]; u <= nodalU[pc[0].size()-1]; u+=stepU) {
        for (float v = nodalV[k-1]; v <= nodalV[pc.size()-1]; v+=stepV) {
            Vertex vertex;
            vertex.position = eval(pc, u, v, k);
            vertices.emplace_back(vertex);
        }
    }

    int i = (nodalU[pc[0].size()-1]-nodalU[k-1])/stepU+1;
    int j = (nodalV[pc.size()-1]-nodalV[k-1])/stepV+1;
    makeIndex(i, j);
    makeNormals();


    Material* m = new Material(std::string("b_splineSurfaceMat01"));
    m->setDiffuseColor(glm::vec4(65.f/255.f, 105.f/255.f, 225.f/255.f, 1.f));
    return Mesh(vertices, indices, m);
}


glm::vec3 B_Spline::eval(std::vector<std::vector<glm::vec3>> splines, float u, float v, int k) {

    std::vector<float> nodalU;
    std::vector<float> nodalV;

    std::vector<glm::vec3> q;

    for (unsigned int i = 0; i < splines.size(); i++)
        q.emplace_back(construct(splines[i], k, nodalV, v));

    return construct(q, k, nodalU, u);
}


void B_Spline::makeNormals() {
    for (unsigned int i = 0; i < indices.size()-3; i+= 3) {
        glm::vec3 a = vertices[indices[i]].position;
        glm::vec3 b = vertices[indices[i+1]].position;
        glm::vec3 c = vertices[indices[i+2]].position;
        glm::vec3 normal = glm::normalize(glm::cross(b - a, c - a));
        vertices[indices[i]].normal += normal;
        vertices[indices[i+1]].normal += normal;
        vertices[indices[i+2]].normal += normal;
    }

    for (unsigned int i = 0; i < vertices.size(); i++) {
        vertices[i].normal = glm::normalize(vertices[i].normal);
    }
}

void B_Spline::makeIndex(int l, int k) {

    for (int i = 0; i < l-1; i++) {
        for (int j = 0; j < k-1; j++) {
            indices.emplace_back(i*k+j);
            indices.emplace_back((i+1)*k+1+j);
            indices.emplace_back(i*k+1+j);

            indices.emplace_back(i*k+j);
            indices.emplace_back((i+1)*k+j);
            indices.emplace_back((i+1)*k+1+j);
        }
    }
}


void B_Spline::draw(GLuint shader) {


    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE_STRIP);

    glBindVertexArray(VAO);


    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glColor3f(1.0f, 0.0f, 1.0f);
    glUniform1i(glGetUniformLocation(shader, "hasTextures"), 0);
    glm::vec3 color = glm::vec3(65.f/255.f, 105.f/255.f, 225.f/255.f);
    glUniform3fv(glGetUniformLocation(shader, "material.m_diffuseColor"), 1, glm::value_ptr(color));

    glDrawArrays(GL_LINE_STRIP, 0, vertices.size());
    glBindVertexArray(0);


    glActiveTexture(GL_TEXTURE0);

    glUniformMatrix4fv( glGetUniformLocation(shader, "model"), 1, GL_FALSE, glm::value_ptr(model));
}


void B_Spline::setupMesh() {

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE_STRIP);

    // Initialize the geometry
    // 1. Generate geometry buffers
    glGenBuffers(1, &VBO);
    glGenVertexArrays(1, &VAO);
    // 2. Bind Vertex Array Object
    glBindVertexArray(VAO);
    // 3. Copy our vertices array in a buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    //vertex attrib
    //Position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);

    glDrawArrays(GL_LINE_STRIP, 0, 3);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

}
