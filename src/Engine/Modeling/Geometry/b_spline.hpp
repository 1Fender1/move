#ifndef B_SPLINE_HPP
#define B_SPLINE_HPP

#include <glm/vec3.hpp>
#include <vector>

#include <Engine/Rendering/Material/Material.hpp>
#include <Core/libsGL.hpp>
#include <Engine/Modeling/Mesh/Mesh.hpp>


class B_Spline {
public:
    B_Spline();
    void draw(GLuint shader);

    Mesh b_spline2D(std::vector<std::vector<glm::vec3>> pc, int k);

private:
    glm::vec3 construct(std::vector<glm::vec3> controlPoints, int k, std::vector<float> nodal, float u);
    glm::vec3 construct(std::vector<Vertex> controlPoints, int k, std::vector<float> nodal, float u);
    std::vector<Vertex> b_spline(std::vector<glm::vec3> points, int k);
    std::vector<Vertex> b_spline(std::vector<Vertex> points, int k);
    glm::vec3 eval(std::vector<std::vector<glm::vec3> > pc, float u, float v, int k);
    void makeNormals();


    void makeIndex(int l, int k);

    void setupMesh();

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    GLuint VAO, VBO;

    glm::mat4 model;
};

#endif // B_SPLINE_H
