#include "openMeshConverter.hpp"


OpenMeshConverter::OpenMeshConverter() {

}

OMesh OpenMeshConverter::toOpenMesh(Mesh mesh) {
    OMesh newMesh = OMesh();

    auto vertices = mesh.getVertices();
    std::vector<OMesh::VertexHandle> vhandle;

    for (uint i = 0; i < vertices.size(); i++) {
        glm::vec3 vertice = vertices[i].position;
        vhandle.push_back(newMesh.add_vertex(OMesh::Point(vertice[0], vertice[1], vertice[2])));
    }

    std::vector<OMesh::VertexHandle> face_vhandles;
    auto triangles = mesh.getIndices();
    for (uint i = 0; i < triangles.size(); i+=3) {
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[triangles[i]]);
        face_vhandles.push_back(vhandle[triangles[i+1]]);
        face_vhandles.push_back(vhandle[triangles[i+2]]);
        newMesh.add_face(face_vhandles);
    }

    return newMesh;
}

Mesh OpenMeshConverter::toMeshObj(OMesh mesh) {

    mesh.request_vertex_normals();
    mesh.request_face_normals();
    mesh.request_face_status();
    mesh.request_edge_status();
    mesh.request_vertex_status();

    OMesh::Point pTemp;
    OMesh::Point nTemp;
    Vertex vTemp;
    std::vector<Vertex> vertice = std::vector<Vertex>(mesh.n_vertices());
    std::vector<uint> indices = std::vector<uint>(mesh.n_faces()*3);
    uint i = 0;

    for (OMesh::FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it) {
        OMesh::FaceVertexIter fv_it;
        for (fv_it = mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it) {

            indices[i] = fv_it->idx();

            pTemp = mesh.point(*fv_it);
            vTemp.position[0] = pTemp[0];
            vTemp.position[1] = pTemp[1];
            vTemp.position[2] = pTemp[2];

            nTemp = mesh.normal(*fv_it);
            vTemp.normal[0] = -nTemp[0];
            vTemp.normal[1] = -nTemp[1];
            vTemp.normal[2] = -nTemp[2];

            vertice[fv_it->idx()] = vTemp;
            i++;
        }
    }

    Mesh obj = Mesh(vertice, indices);
    return obj;
}
