#ifndef OPEN_MESH_CONVERTER_HPP
#define OPEN_MESH_CONVERTER_HPP

#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Tools/Smoother/SmootherT.hh>
#include "Engine/Modeling/Mesh/Mesh.hpp"

typedef OpenMesh::TriMesh_ArrayKernelT<> OMesh;

class OpenMeshConverter {

public :
    static OMesh toOpenMesh(Mesh mesh);
    static Mesh toMeshObj(OMesh mesh);


private :
    OpenMeshConverter();

};





#endif
