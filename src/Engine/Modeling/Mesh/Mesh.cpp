//
// Created by jb on 02/03/18.
//

#include <Engine/Modeling/Mesh/Mesh.hpp>
#include "Core/glAssert.hpp"




Mesh::~Mesh() {
//return;
    if (m_VAO > 0) {
        glDeleteVertexArrays(1, &m_VAO);
        m_VAO = 0;
    }

    if (m_EBO > 0) {
        glDeleteBuffers(1, &m_EBO);
        m_EBO = 0;
    }

    if (m_VBO > 0) {
        glDeleteBuffers(1, &m_VBO);
        m_VBO = 0;
    }

    m_vertices.clear();
    m_indices.clear();

    if (m_skeleton)
        delete m_skeleton;

}

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<uint> indices, Material *material) {

    m_vertices.clear();
    m_vertices.resize(vertices.size());
    for (uint i = 0; i < vertices.size(); ++i) {
        m_vertices[i] = vertices[i];
    }

    m_indices.clear();
    m_indices.resize(indices.size());
    for (uint i = 0; i < indices.size(); ++i) {
        m_indices[i] = indices[i];
    }

    if (material != nullptr) {
        m_isUseDefautltMaterial = false;
        m_material_ptr = material;
    } else {
        initDefaultMaterial();
        m_isUseDefautltMaterial = true;
        m_material_ptr = &m_defaultMaterial;
    }

    m_VAO = 0;
    m_VBO = 0;
    m_EBO = 0;

    //setupMesh();

    //m_models.emplace_back(glm::mat4(1));
    m_skeleton = nullptr;

    m_needUpdate = true;

    m_lbsTransform = glm::mat4(1);
    m_objType = objectType::MESH;
    m_objectName = "mesh";

    m_aabb = AABB(m_vertices);

}

Mesh::Mesh(const Mesh& mesh) : MoveObject(mesh) {


    m_VBO = mesh.m_VBO;
    m_EBO = mesh.m_EBO;
    m_VAO = mesh.m_VAO;


    m_vertices.clear();
    m_vertices.resize(mesh.m_vertices.size());
    for (uint i = 0; i < mesh.m_vertices.size(); ++i) {
        m_vertices[i] = mesh.m_vertices[i];
    }


    m_indices.clear();
    m_indices.resize(mesh.m_indices.size());
    for (uint i = 0; i < mesh.m_indices.size(); ++i) {
        m_indices[i] = mesh.m_indices[i];
    }


    m_skeleton = mesh.m_skeleton;
    m_lbsTransform = mesh.m_lbsTransform;

    if (!mesh.m_isUseDefautltMaterial) {
        m_isUseDefautltMaterial = false;
        m_material_ptr = mesh.m_material_ptr;
    }
    else {
        initDefaultMaterial();
        m_isUseDefautltMaterial = true;
        m_material_ptr = &m_defaultMaterial;
    }

    m_skeleton = mesh.m_skeleton;
    m_needUpdate = true;
    m_objType = objectType::MESH;
    m_objectName = mesh.m_objectName;
    m_aabb = mesh.m_aabb;
}

Mesh::Mesh() {

    initDefaultMaterial();

    m_VAO = 0;
    m_VBO = 0;
    m_EBO = 0;

    //setupMesh();

    //m_models.emplace_back(glm::mat4(1));

    m_needUpdate = true;
    m_isUseDefautltMaterial = true;

    m_material_ptr = &m_defaultMaterial;

    m_skeleton = nullptr;

    m_lbsTransform = glm::mat4(1);
    m_objType = objectType::MESH;
    m_objectName = "mesh";
}


void Mesh::drawGeom(Shader& shader) {

    update();
    shader.use();
    glBindVertexArray(m_VAO);
    glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    shader.setMat4("model", getModel());
}


void Mesh::draw(Shader& shader) {

    update();
    shader.use();

    for (uint j = 0; j < getInstanceCount(); j++) {
        uint diffuseNr  = 1;
        uint specularNr = 1;
        uint normalNr   = 1;
        uint heightNr   = 1;
        uint textureCount = m_material_ptr->getTextureCount();

        for (uint i = 0; i < textureCount; i++) {
            Texture& texture = *m_material_ptr->getTexture(i);

            glActiveTexture(GL_TEXTURE0 + i);
            std::string num;
            std::string type = texture.getTextureType();

            if (type == DIFFUSE_MAP) {
                num = std::to_string(diffuseNr++);
            }
            else if (type == SPECULAR_MAP) {
                num = std::to_string(specularNr++);
            }
            else if (type == NORMAL_MAP) {
                num = std::to_string(normalNr++);
            }
            else if (type == "texture_height") {
                num = std::to_string(heightNr++);
            }

            shader.setInt(type+num, i);
            texture.bind();

        }


        m_material_ptr->draw(shader);

        shader.setMat4("model", getModel(j));
        shader.setMat4("lbsTransform", m_lbsTransform);

        glBindVertexArray(m_VAO);
            glDrawElementsInstanced(GL_TRIANGLES, (GLsizei)m_indices.size(), GL_UNSIGNED_INT, (GLvoid *)nullptr, (GLsizei)getInstanceCount());
        glBindVertexArray(0);
        glActiveTexture(GL_TEXTURE0);

        //GL_CHECK_ERROR

    }
}

void Mesh::drawLayout(Shader& shader) {
    AABB wAABB = getModel() * m_aabb;
    wAABB.drawLayout(shader);
}

void Mesh::setupMesh() {

    glGenVertexArrays(1, &m_VAO);
    glGenBuffers(1, &m_VBO);
    glGenBuffers(1, &m_EBO);

    glBindVertexArray(m_VAO);

        glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
        glBufferData(GL_ARRAY_BUFFER, m_vertices.size()*sizeof (Vertex), &m_vertices[0], GL_DYNAMIC_DRAW);

        //vertex attrib
        //pos
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

        //normals
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));

        //text coords
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));

        //tangent
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));

        //bitangent
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));


        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(GLuint), &m_indices[0], GL_DYNAMIC_DRAW);

    glBindVertexArray(0);
}

bool Mesh::isNormalMaped() {
    return m_hasNormalMap;
}

bool Mesh::isSpecularMaped() {
    return m_hasSpecularMap;
}

bool Mesh::isDiffuseMaped() {
    return m_hasDiffuseMap;
}


void Mesh::setIsNormalMaped(bool value) {
    m_hasNormalMap = value;
}

void Mesh::setIsSpecularMaped(bool value) {
    m_hasSpecularMap = value;
}

void Mesh::setIsDiffuseMaped(bool value) {
    m_hasDiffuseMap = value;
}

void Mesh::transformVertice(glm::mat4 transformation) {
    transform(transformation);
    glm::mat4 trans = getModel() * transformation;
    for (unsigned int i = 0; i < m_vertices.size(); i++) {
        glm::vec4 pos = glm::vec4(m_vertices[i].position, 1);
        pos = trans * pos;
        m_vertices[i].position[0] = pos[0];
        m_vertices[i].position[1] = pos[1];
        m_vertices[i].position[2] = pos[2];

    }
}

void Mesh::computeSmoothNormals() {


    for (unsigned int i = 0; i < m_vertices.size(); i++) {
        m_vertices[i].normal = glm::vec3(0);
    }

    for (unsigned int i = 0; i < m_indices.size(); i+= 3) {
        glm::vec3 a = m_vertices[m_indices[i]].position;
        glm::vec3 b = m_vertices[m_indices[i+1]].position;
        glm::vec3 c = m_vertices[m_indices[i+2]].position;
        glm::vec3 normal = glm::normalize(glm::cross(b - a, c - a));
        m_vertices[m_indices[i]].normal += normal;
        m_vertices[m_indices[i+1]].normal += normal;
        m_vertices[m_indices[i+2]].normal += normal;
    }

    for (unsigned int i = 0; i < m_vertices.size(); i++) {
        m_vertices[i].normal = glm::normalize(m_vertices[i].normal);
        if (m_vertices[i].normal[0] != m_vertices[i].normal[0] || m_vertices[i].normal[1] != m_vertices[i].normal[1] || m_vertices[i].normal[2] != m_vertices[i].normal[2]) {
            m_vertices[i].normal = glm::vec3(0);
        }
    }

}

std::vector<Vertex>& Mesh::getVertices() {
    return m_vertices;
}

std::vector<uint>& Mesh::getIndices() {
    return m_indices;
}



void Mesh::addVertex(glm::vec3 v) {
    Vertex vertex;
    vertex.position = v;
    m_vertices.emplace_back(vertex);
}

void Mesh::addVertex(Vertex v) {
    m_vertices.emplace_back(v);
}

void Mesh::addTriangle(uint a, uint b, uint c) {
    m_indices.emplace_back(a);
    m_indices.emplace_back(b);
    m_indices.emplace_back(c);
}


void Mesh::getTriangle(uint triangleNumber, Vertex& v1, Vertex& v2, Vertex& v3) {
    uint indTriangle = triangleNumber * 3;
    uint indV1 = indTriangle, indV2 = indTriangle + 1, indV3 = indTriangle + 2;

    v1 = m_vertices[m_indices[indV1]];
    v2 = m_vertices[m_indices[indV2]];
    v3 = m_vertices[m_indices[indV3]];
}

void Mesh::toUpdate() {
    m_needUpdate = true;
}

void Mesh::update() {

    if(m_needUpdate) {

        if (m_VAO > 0) {
            glDeleteVertexArrays(1, &m_VAO);
            m_VAO = 0;
        }
        if (m_EBO > 0) {
            glDeleteBuffers(1, &m_EBO);
            m_EBO = 0;
        }
        if (m_VBO > 0) {
            glDeleteBuffers(1, &m_VBO);
            m_VBO = 0;
        }


        if (m_VAO == 0)
            glGenVertexArrays(1, &m_VAO);

        glBindVertexArray(m_VAO);

            if (m_VBO == 0) {
                glGenBuffers(1, &m_VBO);

                glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
                //vertex attrib
                //pos
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),  (void*)offsetof(Vertex, position));
                glEnableVertexAttribArray(0);

                //normals
                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
                glEnableVertexAttribArray(1);

                //text coords
                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
                glEnableVertexAttribArray(2);

                //tangent
                glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
                glEnableVertexAttribArray(3);

                //bitangent
                glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
                glEnableVertexAttribArray(4);

                //Weigths
                glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, weights));
                glEnableVertexAttribArray(5);
            }

            glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
            glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof (Vertex), &m_vertices[0], GL_DYNAMIC_DRAW);

            if (m_EBO == 0)
                glGenBuffers(1, &m_EBO);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(GLuint), &m_indices[0], GL_DYNAMIC_DRAW);

        glBindVertexArray(0);

        m_needUpdate = false;
    }
}
/*
const glm::mat4& Mesh::getModel(uint i) const {
    return m_models[i];
}

glm::mat4& Mesh::getModel(uint i) {
    return m_models[i];
}

void Mesh::setModel(glm::mat4& mat, uint i) {
    if (i > m_models.size())
        return;
    m_models[i] = mat;
}
*/
Material* Mesh::getMaterial() {
    return m_material_ptr;
}

void Mesh::setMaterial(Material* mat) {
    if ((mat != nullptr) && (mat->getName() != "DefaultMaterial")) {
        m_isUseDefautltMaterial = false;
        m_material_ptr = mat;
    }
    else {
        m_material_ptr = &m_defaultMaterial;
        m_isUseDefautltMaterial = true;
    }
}


void Mesh::initDefaultMaterial() {
    m_defaultMaterial = PrincipledMaterial("DefaultMaterial");

    glm::vec3 zero = glm::vec3(0.f);
    m_defaultMaterial.setEmissiveColor(zero);
    m_defaultMaterial.setName("DefaultMaterial");
    m_defaultMaterial.setAmbianteColor(zero);
    m_defaultMaterial.setDiffuseColor(glm::vec4(1.f));
    m_defaultMaterial.setSpecularColor(zero);
}

bool Mesh::isDefaultMaterial() {
    return m_isUseDefautltMaterial;
}

AABB Mesh::getAABB() {
    return getModel() * m_aabb;
}

void Mesh::updateAABB() {
    m_aabb = AABB(m_vertices);
}