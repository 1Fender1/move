//
// Created by jb on 02/03/18.
//
#ifndef HELLOOPENGL_MESHOBJ_HPP
#define HELLOOPENGL_MESHOBJ_HPP


#include <glm/glm.hpp>
#include <vector>

#include <Engine/Rendering/Material/Material.hpp>
#include <Engine/Modeling/Animation/Bone.hpp>
#include <Engine/Shader/Shader.hpp>
#include <Core/libsGL.hpp>
#include <Engine/Scenes/MoveObject.hpp>
#include <Engine/Rendering/Material/PrincipledMaterial.hpp>
#include <Core/Math/BoundingVolumes/Aabb.hpp>

/*
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>
*/
static const std::string UNDEFINED_MAP = "texture_undefined";
static const std::string DIFFUSE_MAP = "texture_diffuse";
static const std::string SPECULAR_MAP = "texture_specular";
static const std::string NORMAL_MAP = "texture_normal";

class Mesh : public MoveObject {

public:

    struct Weight {
        uint vertexId;
        float weight;

        Weight() {
            vertexId = -1;
            weight = 0.f;
        }
    };

    Mesh(std::vector<Vertex>, std::vector<uint>, Material *material = nullptr);
    Mesh(const Mesh& mesh);
    Mesh();
    ~Mesh();

    Mesh& operator=(const Mesh& mesh) {

        MoveObject::operator = (mesh);

        if (&mesh == this)
            return *this;


        m_VBO = mesh.m_VBO;
        m_EBO = mesh.m_EBO;
        m_VAO = mesh.m_VAO;

        m_vertices.clear();
        m_vertices.resize(mesh.m_vertices.size());
        for (uint i = 0; i < mesh.m_vertices.size(); ++i)
            m_vertices[i] = mesh.m_vertices[i];

        m_indices.clear();
        m_indices.resize(mesh.m_indices.size());
        for (uint i = 0; i < mesh.m_indices.size(); ++i)
            m_indices[i] = mesh.m_indices[i];

        m_skeleton = mesh.m_skeleton;
        m_lbsTransform = mesh.m_lbsTransform;

        if (!mesh.m_isUseDefautltMaterial) {
            m_isUseDefautltMaterial = false;
            m_material_ptr = mesh.m_material_ptr;
        }
        else {
            initDefaultMaterial();
            m_isUseDefautltMaterial = true;
            m_material_ptr = &m_defaultMaterial;
        }

        m_hasDiffuseMap = mesh.m_hasDiffuseMap;
        m_hasNormalMap = mesh.m_hasNormalMap;
        m_hasSpecularMap = mesh.m_hasSpecularMap;
        m_needUpdate = true;
        m_objType = objectType::MESH;
        m_objectName = mesh.m_objectName;
        m_skeleton = mesh.m_skeleton;
        m_aabb = mesh.m_aabb;

        return *this;
    }


    void getTriangle(uint triangleNumber, Vertex& v1, Vertex& v2, Vertex& v3);

    bool isColored();

    bool isNormalMaped();
    bool isSpecularMaped();
    bool isDiffuseMaped();

    void setIsNormalMaped(bool value);
    void setIsSpecularMaped(bool value);
    void setIsDiffuseMaped(bool value);

    void setColored(bool _colored);

    void computeSmoothNormals();
    void draw(Shader& shader);
    void drawGeom(Shader& shader);
    void drawLayout(Shader& shader);

    void updateAABB();

    void transformVertice(glm::mat4 transformation);
    std::vector<Vertex>& getVertices();
    std::vector<uint>& getIndices();

/*
    const glm::mat4& getModel(uint i = 0) const;
    glm::mat4& getModel(uint i = 0);
*/
    Material* getMaterial();
    void setMaterial(Material* mat);

  //  void setModel(glm::mat4& mat, uint i = 0);


    void addVertex(glm::vec3 v);
    void addVertex(Vertex v);
    void addTriangle(uint a, uint b, uint c);

    void toUpdate();

    bool isDefaultMaterial();

    AABB getAABB();


    GLuint m_VAO = 0;

    //Temp
    Bone* m_skeleton = nullptr;
    glm::mat4 m_lbsTransform;


private :

    void initDefaultMaterial();

    GLuint m_VBO = 0;
    GLuint m_EBO = 0;

    Material *m_material_ptr = nullptr;
    PrincipledMaterial m_defaultMaterial;

    std::vector<Vertex> m_vertices;
    std::vector<uint> m_indices;



    void update();
    void setupMesh();

    bool m_hasNormalMap;
    bool m_hasSpecularMap;
    bool m_hasDiffuseMap;
    bool m_needUpdate;

    bool m_isUseDefautltMaterial = true;

    //Object space bounding box
    AABB m_aabb;
};


#endif //HELLOOPENGL_MESHOBJ_H
