//
// Created by jb on 02/03/18.
//

#include "Object.hpp"

Object::Object(std::vector<Mesh>& _meshes, std::string objName) : MoveObject(objName) {

    m_meshes.clear();
    m_meshes.reserve(_meshes.size());
    for (uint i = 0; i < _meshes.size(); ++i) {
        m_meshes.push_back(_meshes[i]);
        m_aabb += _meshes[i].getAABB();
    }
    //m_meshes = _meshes;
    m_objectName = objName;

    m_children.clear();
    for (uint i = 0; i < m_meshes.size(); ++i) {
        addChild(&m_meshes[i]);
        m_meshes[i].setParent(this);
    }
    setNameToChildren(getName());

    m_objType = objectType::OBJECT;

   // m_models.emplace_back(glm::mat4(1.f));
}

Object::Object() {
    m_objType = objectType::OBJECT;
    m_objectName = "Object";

    //m_models.emplace_back(glm::mat4(1.f));
}

Object::Object(const Object& obj) : MoveObject(obj) {

    m_meshes.clear();
    m_meshes.reserve(obj.m_meshes.size());

    for (uint i = 0; i < obj.m_meshes.size(); ++i) {
        m_meshes.push_back(obj.m_meshes[i]);
    }

    m_children.clear();
    for (uint i = 0; i < m_meshes.size(); ++i) {
        addChild(&m_meshes[i]);
    }

    //setNameToChildren(getName());
    m_aabb = obj.m_aabb;
    m_isRaytracerVisible = obj.m_isRaytracerVisible;
    m_isForwardVisible = obj.m_isForwardVisible;

    m_objType = objectType::OBJECT;
}

void Object::draw(Shader& shader) {

    for (uint i = 0; i < m_meshes.size(); i++)
        m_meshes[i].draw(shader);

}

void Object::drawGeom(Shader& shader) {
    for (uint i = 0; i < m_meshes.size(); i++)
        m_meshes[i].drawGeom(shader);
}

void Object::drawLayout(Shader& shader) {
    AABB wAABB = getModel() * m_aabb;
    wAABB.drawLayout(shader);
}


void Object::addMesh(Mesh& mesh) {
    m_meshes.push_back(mesh);
    setNameToChildren(getName());
    addChild(&m_meshes.back());
}


void Object::setNameToChildren(std::string _name) {
    uint tmp_size = m_meshes.size();
    tmp_size /= 10;
    std::string padding = "";
    int paddingCount = 0;
    while (tmp_size > 0) {
        //padding += "0";
        paddingCount++;
        tmp_size /= 10;
    }

    for (uint i = 0; i < m_meshes.size(); ++i) {
        Mesh& mesh = m_meshes[i];
        padding = "";
        uint idTmp = i;
        int currentPaddingCount = paddingCount;
        while (idTmp > 0) {
            currentPaddingCount--;
            idTmp /= 10;
        }
        if (i != 0)
            currentPaddingCount++;
        for (int j = 0; j < currentPaddingCount; ++j)
            padding += '0';

        if (mesh.getName() == "mesh" || mesh.getName() == "" || mesh.getName() == "defaultobject") {
            std::string tmpName = _name;
            tmpName += "_" + padding;
            std::string name = tmpName + std::to_string(i);
            mesh.setName(name);
        }
    }
}

void Object::setName(std::string objectName) {

    m_objectName = objectName;
    if (m_objectName != "mesh" && m_objectName != "" && m_objectName != "defaultobject")
        setNameToChildren(objectName);
}

Mesh* Object::getMeshAt(uint index) {
    if (index >= m_meshes.size())
        return nullptr;
    return &m_meshes[index];
}

uint Object::getMeshCount() {
    return m_meshes.size();
}

void Object::removeMeshAt(uint i) {
    if (i >= m_meshes.size())
        return;

    m_meshes.erase(m_meshes.begin()+i);
    m_children.erase(m_children.begin()+i);
    m_aabb = AABB();
    for (uint j = 0; j < m_children.size(); ++j) {
        m_children[j] = &m_meshes[j];
        m_aabb += m_meshes[j].getAABB();
    }

}

/*

const glm::mat4& Object::getModel(uint i) {
    if (i > m_model.size())
        return m_model[0];
    return m_model[i];
}
*/
/*
void Object::setModel(glm::mat4 mat, uint i) {
    if (i > m_model.size()) {
        m_model[0] = mat;
        for (uint j = 0; j < m_meshes.size(); ++j) {
            m_meshes[j].setModel(mat, 0);
        }
    }

    m_model[i] = mat;
    for (uint j = 0; j < m_meshes.size(); ++j) {
        m_meshes[j].setModel(mat, j);
    }
}
*/
void Object::setRaytracerVisible(bool visible) {
    m_isRaytracerVisible = visible;
}

void Object::setForwardVisible(bool visible) {
    m_isForwardVisible = visible;
}

bool Object::isRaytracerVisible() {
    return m_isRaytracerVisible;
}

bool Object::isForwardVisible() {
    return m_isForwardVisible;
}