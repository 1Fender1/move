//
// Created by jb on 02/03/18.
//

#ifndef HELLOOPENGL_CLASSICOBJ_HPP
#define HELLOOPENGL_CLASSICOBJ_HPP


#include <glm/glm.hpp>
#include <Engine/Modeling/Mesh/Mesh.hpp>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Scenes/MoveObject.hpp>


class Object : public MoveObject {

public:

    Object();
    Object(std::vector<Mesh>& _meshes, std::string name);
    Object(const Object& obj);
    Object& operator=(const Object& obj) {

        MoveObject::operator=(obj);

        if (&obj == this)
            return *this;

        m_meshes.clear();
        m_meshes.resize(obj.m_meshes.size());

        for (uint i = 0; i < obj.m_meshes.size(); ++i) {
            m_meshes[i] = obj.m_meshes[i];
        }

        m_children.clear();
        for (uint i = 0; i < m_meshes.size(); ++i) {
            addChild(&m_meshes[i]);
        }

        m_objectName = obj.m_objectName;
        m_aabb = obj.m_aabb;
        m_isRaytracerVisible = obj.m_isRaytracerVisible;
        m_isForwardVisible = obj.m_isForwardVisible;
        //setNameToChildren(m_objectName);

        m_objType = objectType::OBJECT;
        return *this;
    }

    void draw(Shader& shader);
    void drawGeom(Shader& shader);
    void drawLayout(Shader& shader);

    void setName(std::string objectName);

    void instance();

    void addMesh(Mesh& mesh);
    Mesh* getMeshAt(uint index);
    uint getMeshCount();

    void removeMeshAt(uint i);

    //const glm::mat4& getModel(uint i = 0);
    //void setModel(glm::mat4 mat, uint i = 0);

    void setRaytracerVisible(bool);
    void setForwardVisible(bool);
    bool isRaytracerVisible();
    bool isForwardVisible();

private:

    void setNameToChildren(std::string name);

    std::vector<Mesh> m_meshes;

    bool m_isRaytracerVisible = true;
    bool m_isForwardVisible = true;

    AABB m_aabb;

};

typedef std::unique_ptr<Object> Object_ptr;



#endif //HELLOOPENGL_CLASSICOBJ_H
