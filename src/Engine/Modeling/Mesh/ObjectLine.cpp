//
// Created by jb on 31/07/2020.
//

#include "ObjectLine.hpp"

ObjectLine::ObjectLine() {
    m_objType = objectType::OBJECT;
    m_objectName = "Object";
}

ObjectLine::ObjectLine(const std::vector<Polyline>& polylines) : MoveObject("Object", nullptr) {
    m_objType = objectType::OBJECT;

    uint linesSize = polylines.size();
    m_lines.resize(linesSize);
    for (uint i = 0; i < linesSize; ++i) {
        m_lines[i] = polylines[i];
        addChild(&m_lines[i]);
    }

}

ObjectLine::ObjectLine(const ObjectLine& obj) : MoveObject(obj) {
    m_objType = objectType::OBJECT;

    uint linesSize = obj.m_lines.size();
    m_lines.resize(linesSize);
    m_objectName = obj.m_objectName;

    for (uint i = 0; i < linesSize; ++i) {
        m_lines[i] = obj.m_lines[i];
        addChild(&m_lines[i]);
    }

    m_color = obj.m_color;
    if (m_color != glm::vec3(-1.f)) {
        for (uint i = 0; i < linesSize; ++i) {
            m_lines[i].setColor(m_color);
        }
    }
}

ObjectLine::~ObjectLine() {

}

void ObjectLine::draw(Shader& shader) {

    for (uint i = 0; i < m_lines.size(); ++i) {
        m_lines[i].draw(shader);
    }

}

void ObjectLine::setFillDraw(uint index, bool fill) {
    if (index > m_lines.size()-1)
        return;
    m_lines[index].setFillDrawing(fill);
}

void ObjectLine::setColor(const glm::vec3& color) {
    m_color = color;

    for (uint i = 0; i < m_lines.size(); ++i) {
        m_lines[i].setColor(m_color);
    }
}