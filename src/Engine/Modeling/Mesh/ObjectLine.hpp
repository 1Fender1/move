//
// Created by jb on 31/07/2020.
//

#ifndef MOVE_OBJECTLINE_HPP
#define MOVE_OBJECTLINE_HPP

#include <Engine/Modeling/Mesh/Polyline.hpp>

class ObjectLine : public MoveObject {


public :
    ObjectLine();
    ObjectLine(const std::vector<Polyline>& polyline);
    ObjectLine(const ObjectLine& obj);
    ~ObjectLine();
    ObjectLine& operator=(const ObjectLine& obj) {
        if (&obj == this)
            return *this;
        m_objectName = obj.m_objectName;

        uint linesSize = obj.m_lines.size();
        m_lines.resize(linesSize);
        for (uint i = 0; i < linesSize; ++i) {
            m_lines[i] = obj.m_lines[i];
            addChild(&m_lines[i]);
        }
        m_color = obj.m_color;
        if (m_color != glm::vec3(-1.f)) {
            for (uint i = 0; i < linesSize; ++i) {
                m_lines[i].setColor(m_color);
            }
        }

        return *this;
    }

    void draw(Shader& shader);

    void setFillDraw(uint index, bool fill);

    void setColor(const glm::vec3& color);

private :

    glm::vec3 m_color = glm::vec3(-1.f);
    std::vector<Polyline> m_lines;



};


#endif //MOVE_OBJECTLINE_HPP
