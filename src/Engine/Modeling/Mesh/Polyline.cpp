//
// Created by jb on 31/07/2020.
//

#include "Polyline.hpp"
#include <Core/glAssert.hpp>

Polyline::Polyline() {
    m_needUpdate = true;
    m_VBO = 0;
}

Polyline::Polyline(std::vector<Vertex>& vertices) : Polyline() {

    m_objectName = "Polyline";

    uint inVerticeSize = vertices.size();
    m_vertices.resize(inVerticeSize);
    for (uint i = 0; i < inVerticeSize; ++i) {
        const Vertex& v = vertices[i];
        m_vertices[i] = v;
    }

}

Polyline::Polyline(const Polyline& polyline) :  MoveObject(polyline) {

//    bool m_needUpdate = true;
    m_VBO = 0;
    m_objectName = polyline.m_objectName;
    m_color = polyline.m_color;

    uint inVerticeSize = polyline.m_vertices.size();
    m_vertices.resize(inVerticeSize);
    for (uint i = 0; i < inVerticeSize; ++i) {
        const Vertex& v = polyline.m_vertices[i];
        m_vertices[i] = v;
    }

}

Polyline::~Polyline() {

    if (m_VAO > 0) {
        glDeleteVertexArrays(1, &m_VAO);
        m_VAO = 0;
    }

    if (m_VBO > 0) {
        glDeleteBuffers(1, &m_VBO);
        m_VBO = 0;
    }

    m_vertices.clear();

}

const std::vector<Polyline::Vertex>& Polyline::getVertices() {
    return m_vertices;
}


void Polyline::draw(Shader& shader) {
    if (m_needUpdate) {
        setupMesh();
        m_needUpdate = false;
    }

    shader.use();

    shader.setFloatV3("u_color", m_color);

    shader.setMat4("model", getModel());

    glBindVertexArray(m_VAO);
    glDrawArrays(GL_LINE_STRIP, 0, m_vertices.size());
    glBindVertexArray(0);

    GL_CHECK_ERROR
}

void Polyline::setColor(const glm::vec3& color) {
    m_color = color;
}

void Polyline::setFillDrawing(bool fill) {
    m_isFillLines = fill;
}


void Polyline::setupMesh() {

    // Initialize the geometry
    // 1. Generate geometry buffers
    glGenBuffers(1, &m_VBO);
    glGenVertexArrays(1, &m_VAO);
    // 2. Bind Vertex Array Object
    glBindVertexArray(m_VAO);
    // 3. Copy our vertices array in a buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    glBufferData(GL_ARRAY_BUFFER, m_vertices.size()*sizeof(Polyline::Vertex), &m_vertices[0], GL_STATIC_DRAW);

    //vertex attrib
    //Position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Polyline::Vertex), (void*)offsetof(Polyline::Vertex, position));
    glEnableVertexAttribArray(0);

    //glDrawArrays(GL_LINE_STRIP, 0, 3);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    GL_CHECK_ERROR

}