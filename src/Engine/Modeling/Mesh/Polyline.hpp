//
// Created by jb on 31/07/2020.
//

#ifndef MOVE_POLYLINE_HPP
#define MOVE_POLYLINE_HPP

#include <Engine/Scenes/MoveObject.hpp>
#include <Core/libsGL.hpp>
#include <Engine/Shader/Shader.hpp>

class Polyline : public MoveObject {

public:
    struct Vertex {
        glm::vec3 position;
    };

    Polyline(std::vector<Vertex>& vertices);
    Polyline(const Polyline& polyline);
    Polyline();
    ~Polyline();

    Polyline& operator=(const Polyline& polyline) {

        if (&polyline == this)
            return *this;

//        bool m_needUpdate = true;
//        GLuint m_VBO = 0;
//        GLuint m_EBO = 0;
        m_objectName = polyline.m_objectName;
        m_color = polyline.m_color;

        uint inVerticeSize = polyline.m_vertices.size();
        m_vertices.resize(inVerticeSize);
        for (uint i = 0; i < inVerticeSize; ++i) {
            const Vertex& v = polyline.m_vertices[i];
            m_vertices[i] = v;
        }

        return *this;
    }

    const std::vector<Vertex>& getVertices();

    void draw(Shader& shader);

    void setColor(const glm::vec3& color);

    void setFillDrawing(bool);


private :

    void setupMesh();

    GLuint m_VBO = 0;
    GLuint m_VAO = 0;
    bool m_needUpdate = true;

    glm::vec3 m_color = glm::vec3(1.f, 0.3686f, 0.0039f);

    std::vector<Vertex> m_vertices; //Why use a struct vertex and not vec3, this structure can be modify with more attributes

    bool m_isFillLines = true;


};


#endif //MOVE_POLYLINE_HPP
