//
// Created by jb on 05/04/19.
//

#include "ForwardRenderer.hpp"
#include <Core/glAssert.hpp>

ForwardRenderer::ForwardRenderer(uint w, uint h, RenderData* renderData) {
    //framebuffers.emplace_back(FrameBuffer(w, h));
    m_width = w;
    m_height = h;
    m_renderData = renderData;

    uint smWidth = 1024;
    uint smHeight = 1024;

    m_shadowMap = new ShadowMapPCF(smWidth, smHeight);

    m_gBuffer = new GBuffer(w, h);
    m_ssaoBuf = new SSAO(w, h);
    m_blurBuf = new Blur(w, h);
    m_fxaaBuf = new FXAA(w, h);

    m_tempFrameBuffer = new FrameBuffer(w, h);
    m_outLinerBuffer = new OutLinerBuffer(w, h);

    m_pingPongBuffer[0] = new FrameBuffer(w, h);
    m_pingPongBuffer[1] = new FrameBuffer(w, h);

    CubeMapLoader cml = CubeMapLoader();
    m_cubeMapId = cml.loadCubeMap(std::string("seaBlur"), std::string(".jpg"));
    //cubeMapId = cml.loadCubeMap(std::string("stormDays"), std::string(".tga"));

    m_skyBox = BaseObjects::cube(350, 350, 350);

    Shader::ShaderNames files;
    files.vextexShader = "../Shaders/VertexShaders/globalShading.vs";
    files.fragmentShader = "../Shaders/FragmentShaders/lineDebug.fs";
    m_shaderLayoutLine = new Shader(files, "LineDebugShader");

    files.fragmentShader = "../Shaders/FragmentShaders/passTroughWhite.fs";
    Shader tmpPassThrough = Shader(files,  "PassTroughWhiteShader");
    m_passTroughWhiteShader = m_renderData->addShader(tmpPassThrough);

    files.vextexShader = "../Shaders/VertexShaders/renderToTexture.vs";
    files.fragmentShader = "../Shaders/FragmentShaders/outLiner.fs";
    Shader tmpm_outLinerShader= Shader(files,  "outLinerShader");
    m_outLinerShader = m_renderData->addShader(tmpm_outLinerShader);


    files.vextexShader = "../Shaders/VertexShaders/renderToTexture.vs";
    files.fragmentShader = "../Shaders/FragmentShaders/blend.fs";
    Shader tmpBlender = Shader(files,  "blendShader");
    m_blendShader = m_renderData->addShader(tmpBlender);


}


ForwardRenderer::~ForwardRenderer() {
    delete m_gBuffer;
    delete m_ssaoBuf;
    delete m_blurBuf;
    delete m_fxaaBuf;

    delete m_shadowMap;

    delete m_pingPongBuffer[0];
    delete m_pingPongBuffer[1];

    delete m_shaderLayoutLine;
    //delete m_passTroughWhiteShader;
   // delete m_outLinerShader;

    delete m_tempFrameBuffer;
    delete m_outLinerBuffer;

}



uint ForwardRenderer::draw() {


    //GL_ASSERT("Forward renderer draw call");
    glViewport(0, 0, m_width, m_height);

    //  ===============================================
    //                    Geometry pass
    //  ===============================================

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    m_gBuffer->bind();

    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_gBuffShader->use();
    m_camera->draw(*m_gBuffShader);

    uint objCount = m_renderData->getObjCount();

    for (uint i = 0; i < objCount; ++i) {
        Object& obj = *(m_renderData->getObject(i));
        for (uint j = 0; j < obj.getMeshCount(); ++j) {
            Mesh& mesh = *obj.getMeshAt(j);
            if (!m_camera->getFrustum().isCull(mesh.getAABB()))
                mesh.draw(*m_gBuffShader);
        }
    }

    m_gBuffer->unbind();
    GL_CHECK_ERROR

    //  ===============================================
    //                    SSAO pass
    //  ===============================================
/*
    ssaoBuf->bind();

    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);
    ssaoShader->use();
    for (uint i = 0; i < 64; i++) {
        std::string name = "samples[" + std::to_string(i) + "]";
        ssaoShader->setFloatV3(name, ssaoBuf->kernel[i]);
    }

    ssaoShader->setInt("width", m_width);
    ssaoShader->setInt("height", m_height);
    ssaoShader->setFloat("radius", 0.5f);

    ssaoShader->setFloatV3("camPos", camera->getPosition());
    ssaoShader->setMat4("view", camera->getView());
    ssaoShader->setMat4("projection", *(projection));
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gBuffer->gPosition);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gBuffer->gNormal);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, ssaoBuf->noiseTexture);

    ssaoBuf->drawGeom(ssaoShader);

    ssaoBuf->unbind();

    //  ===============================================
    //                    Blur pass
    //  ===============================================


    blurBuf->bind();

    glClear(GL_COLOR_BUFFER_BIT);
    ssaoBlurShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoBuf->ssaoColorBuffer);

    blurBuf->drawGeom(ssaoBlurShader);

    blurBuf->unbind();
*/
    //  ===============================================
    //                    Lighting pass
    //  ===============================================


    for (int i = 0; i < 2; ++i) {
        m_pingPongBuffer[i]->bind();
        if (i==0)
            glClearColor(0.f, 0.f, 0.f, 0);
        else
            glClearColor(0,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
    }


    m_currentBuffer = 0;
    uint nbLights = m_renderData->getLightCount();
    Frustum cameraFrustum = m_camera->getFrustum();
    for (uint i = 0; i < nbLights; ++i) {

        Light* light = m_renderData->getLight(i);

        if (!light) {
            continue;
        }

        m_shadowMap->computeShadowMap(light, m_renderData);

        m_pingPongBuffer[m_currentBuffer]->bind();
        glCullFace(GL_BACK);

        glViewport(0, 0, m_width, m_height);

        if (m_isFillGeometry)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        glClear(GL_DEPTH_BUFFER_BIT);

        //drawObjects(*defaultShader);
        for (uint j = 0; j < m_renderData->getObjCount(); ++j) {
            Object &obj = *m_renderData->getObject(j);
            for (uint k = 0; k < obj.getMeshCount(); ++k) {
                Mesh &mesh = *obj.getMeshAt(k);
                m_camera->getFrustum().resize(m_width, m_height);

                if (cameraFrustum.isCull(mesh.getAABB()))
                    continue;

                Shader &shader = mesh.getMaterial()->getShader();

                shader.use();

                shader.setInt("lightCount", nbLights);

                light->draw(shader);
                m_shadowMap->draw(&shader);

                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, m_blurBuf->ssaoColorBufferBlur);
                shader.setInt("ssaoIn", 3);
                shader.setBool("hasSSao", false); //TODO think about set to false

                glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, m_pingPongBuffer[(m_currentBuffer + 1) % 2]->texId());
                shader.setInt("texture_acc", 4);

                glActiveTexture(GL_TEXTURE5);
                glBindTexture(GL_TEXTURE_2D, m_shadowMap->getTexID());
                shader.setInt("shadowMap", 5);

                glActiveTexture(GL_TEXTURE6);
                glBindTexture(GL_TEXTURE_2D, m_gBuffer->getTexture(GBuffer::G_DEPTH));
                shader.setInt("u_cameraDepth", 6);

                m_camera->draw(shader);

                mesh.draw(shader);

            }
        }

        //m_pingPongBuffer[m_currentBuffer]->unbind();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        m_currentBuffer = (m_currentBuffer + 1) % 2;
    }
    m_currentBuffer = (m_currentBuffer + 1) % 2;

    //  ===============================================
    //                    CubeMap pass
    //  ===============================================
/*
    m_pingPongBuffer[m_currentBuffer]->bind();
    glDepthFunc(GL_LEQUAL);
    glDepthMask(GL_FALSE);
    m_cubeMapShader->use();
    m_camera->draw(*m_cubeMapShader);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubeMapId);
    m_cubeMapShader->setInt("skybox",0);
    m_skyBox.draw(*m_cubeMapShader);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LESS);
    m_pingPongBuffer[m_currentBuffer]->unbind();
    */

    //===============================================
    //                Gizmo pass
    //===============================================



    glDisable(GL_DEPTH_TEST);
    m_pingPongBuffer[m_currentBuffer]->bind();
    if (m_gizmo != nullptr) {
        Shader *gizShader = m_gizmo->getShader();
        gizShader->use();
        m_camera->draw(*gizShader);
        m_gizmo->draw();
    }
    m_pingPongBuffer[m_currentBuffer]->unbind();
    glEnable(GL_DEPTH_TEST);

    //  ===============================================
    //                    Outline pass
    //  ===============================================

    m_tempFrameBuffer->bind();

    glDisable(GL_DEPTH_TEST);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_passTroughWhiteShader->use();
    m_camera->draw(*m_passTroughWhiteShader);
    for (uint i = 0; i < m_renderData->getObjCount(); ++i) {
        Object& obj = *m_renderData->getObject(i);
        if (obj.isSelected()) {
            obj.draw(*m_passTroughWhiteShader);
        } else {
            for (uint j = 0; j < obj.getMeshCount(); ++j) {
                Mesh &mesh = *obj.getMeshAt(j);
                if (mesh.isSelected()) {
                    mesh.draw(*m_passTroughWhiteShader);
                }
            }
        }
    }
    m_tempFrameBuffer->unbind();

    m_outLinerBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
    m_outLinerShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_tempFrameBuffer->texId());
    m_outLinerShader->setInt("u_inTexture", 0);
    m_outLinerShader->setInt("width", m_width);
    m_outLinerShader->setInt("height", m_height);
    m_outLinerShader->setFloatV3("u_color", glm::vec3(1.f, 0.3686f, 0.0039f));
    m_outLinerBuffer->drawGeom(m_outLinerShader);
    m_outLinerBuffer->unbind();


    //Blend currentRender and outline pass
    m_tempFrameBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_blendShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_pingPongBuffer[m_currentBuffer]->texId());
    m_blendShader->setInt("u_inTexture1", 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_outLinerBuffer->texId());
    m_blendShader->setInt("u_inTexture2", 1);
    m_blendShader->setInt("u_blendType", 0);
    m_tempFrameBuffer->drawGeom(m_blendShader);
    m_tempFrameBuffer->unbind();


    //  ===============================================
    //                    FXAA pass
    //  ===============================================

    m_fxaaBuf->bind();

    glClear(GL_COLOR_BUFFER_BIT);
    m_FXAAshader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, /*m_pingPongBuffer[m_currentBuffer]->texId()*/m_tempFrameBuffer->texId());
    m_FXAAshader->setInt("inTexture", 0);

    m_FXAAshader->setInt("width", m_width);
    m_FXAAshader->setInt("height", m_height);

    m_fxaaBuf->drawGeom(m_FXAAshader);

    m_fxaaBuf->unbind();

    //  ===============================================
    //                    Layout pass
    //  ===============================================

    m_fxaaBuf->bind();
    m_shaderLayoutLine->use();
    m_camera->draw(*m_shaderLayoutLine);
    for (uint i = 0; i < m_renderData->getLightCount(); ++i) {
        m_renderData->getLight(i)->setCamera(m_camera);
        m_renderData->getLight(i)->drawLayout(*m_shaderLayoutLine);
    }

    for (uint j = 0; j < m_renderData->getObjCount(); ++j) {
        Object& obj = *m_renderData->getObject(j);
        m_camera->draw(*m_shaderLayoutLine);
        if (obj.isSelected())
            obj.drawLayout(*m_shaderLayoutLine);
        for (uint k = 0; k < obj.getMeshCount(); ++k) {
            Mesh& mesh = *obj.getMeshAt(k);
            if (mesh.isSelected())
                mesh.drawLayout(*m_shaderLayoutLine);
        }
    }

    for (uint j = 0; j < m_renderData->getCameraCount(); ++j) {
        Camera& cam = *m_renderData->getCamera(j);
        if (cam.isSelected() && (&cam!=m_camera))
            cam.getFrustum().drawLayout(*m_shaderLayoutLine);
    }
    m_fxaaBuf->unbind();





        //  ===============================================
    //                    Final pass
    //  ===============================================

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    m_pingPongBuffer[m_currentBuffer]->bind();
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_frameShader->use();

    GLint tex = m_fxaaBuf->texId();
    //GLint tex = m_outLinerBuffer->texId();//m_tempFrameBuffer->texId();
    //GLint tex = m_shadowMap->getSMBuffer()->texId();
    m_frameShader->setFloat("exposure", m_outExposure);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    m_frameShader->setInt("texture_diffuse1", 0);

    m_pingPongBuffer[m_currentBuffer]->drawGeom(m_frameShader);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //GL_CHECK_ERROR
    GLint texId = m_pingPongBuffer[m_currentBuffer]->texId();

    ++m_drawCallCount;

    GL_CHECK_ERROR

    return texId;
}

void ForwardRenderer::resize(uint w, uint h) {
    m_width = w;
    m_height = h;
    m_shadowMap->resize(1024, 1024);

    m_ssaoBuf->resize(w, h);
    m_gBuffer->resize(w, h);
    m_blurBuf->resize(w, h);
    m_fxaaBuf->resize(w, h);

    m_pingPongBuffer[0]->resize(w, h);
    m_pingPongBuffer[1]->resize(w, h);


    m_outLinerBuffer->resize(w, h);
    m_tempFrameBuffer->resize(w, h);
}


void ForwardRenderer::setMainShader(Shader* shader) {
    m_defaultShader = shader;
}

void ForwardRenderer::setFrameShader(Shader* shader) {
    m_frameShader = shader;
}

void ForwardRenderer::setGBufShader(Shader* shader) {
    m_gBuffShader = shader;
}

void ForwardRenderer::setSSAOShader(Shader* shader) {
    m_ssaoShader = shader;
}

void ForwardRenderer::setSSAOBlurBuffer(Shader* shader) {
    m_ssaoBlurShader = shader;
}

void ForwardRenderer::setFXAAShader(Shader* shader) {
    m_FXAAshader = shader;
}

void ForwardRenderer::setCubeMapShader(Shader* shader) {
    m_cubeMapShader = shader;
}

void ForwardRenderer::setDepthMapStandardShader(Shader* shader) {
    m_depthMapStandardShader = shader;
    m_shadowMap->setShaderStandard(m_depthMapStandardShader);
}

void ForwardRenderer::setDepthMapPointShader(Shader* shader) {
    m_depthMapPointShader = shader;
    m_shadowMap->setShaderPoint(m_depthMapPointShader);
}

void ForwardRenderer::setGizmo(Gizmo* giz) {
    m_gizmo = giz;
}


void ForwardRenderer::drawObjects(Shader& shader) {
    uint objCount = m_renderData->getObjCount();

    for (uint i = 0; i < objCount; ++i) {
        Object& obj = *(m_renderData->getObject(i));
        obj.draw(shader);
    }

}