//
// Created by jb on 05/04/19.
//

#ifndef MOVE_FORWARDRENDERER_HPP
#define MOVE_FORWARDRENDERER_HPP

#include <Engine/Rendering/Renderer.hpp>
#include <cstdio>
#include <iostream>
#include <vector>

#include <Engine/Modeling/Mesh/Mesh.hpp>
#include <Engine/Rendering/FrameBuffer/FrameBuffer.hpp>
#include <Engine/Rendering/FrameBuffer/Gbuffer.hpp>
#include <Engine/Rendering/FrameBuffer/Ssao.hpp>
#include <Engine/Rendering/FrameBuffer/Blur.hpp>
#include <Engine/Rendering/FrameBuffer/Fxaa.hpp>
#include <Engine/FileLoader/CubeMapLoader.hpp>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>
#include <Engine/Rendering/ShadowMap/ShadowMapPcf.hpp>

#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Camera/Camera.hpp>
#include <Engine/Rendering/Light/Lights.hpp>
#include <Ui/Gizmos/Gizmo.hpp>
#include <Engine/Modeling/Mesh/ObjectLine.hpp>
#include <Engine/Rendering/FrameBuffer/OutLinerBuffer.hpp>

class ForwardRenderer : public Renderer {


public:
    ForwardRenderer(uint w, uint h, RenderData* renderData);
    ForwardRenderer() {};
    ~ForwardRenderer();

    uint draw();
    void resize(unsigned int w, unsigned int h);


    void setMainShader(Shader* shader);
    void setFrameShader(Shader* shader);
    void setGBufShader(Shader* shader);
    void setSSAOShader(Shader* shader);
    void setSSAOBlurBuffer(Shader* shader);
    void setFXAAShader(Shader* shader);
    void setCubeMapShader(Shader* shader);

    void setDepthMapStandardShader(Shader* shader);
    void setDepthMapPointShader(Shader* shader);

    void setGizmo(Gizmo* giz);


    //Temp
    //std::vector<Object> objsTemp;

private :
    //std::vector<FrameBuffer> framebuffers;
    GBuffer *m_gBuffer = nullptr;
    SSAO *m_ssaoBuf = nullptr;
    Blur *m_blurBuf = nullptr;
    FXAA *m_fxaaBuf = nullptr;
    ShadowMapPCF *m_shadowMap = nullptr;
    FrameBuffer *m_pingPongBuffer[2] = {nullptr, nullptr};

    FrameBuffer* m_tempFrameBuffer = nullptr;
    OutLinerBuffer* m_outLinerBuffer = nullptr;



    uint m_currentBuffer = 0;



    uint m_width;
    uint m_height;

    uint m_cubeMapId;

    Shader* m_frameShader = nullptr;
    Shader* m_defaultShader = nullptr;
    Shader* m_gBuffShader = nullptr;
    Shader* m_ssaoShader = nullptr;
    Shader* m_ssaoBlurShader = nullptr;
    Shader* m_FXAAshader = nullptr;
    Shader* m_cubeMapShader = nullptr;
    Shader* m_depthMapStandardShader = nullptr;
    Shader* m_depthMapPointShader = nullptr;

    Shader* m_passTroughWhiteShader = nullptr;
    Shader* m_outLinerShader = nullptr;

    Shader* m_blendShader = nullptr;

    void drawObjects(Shader& shader);

    Object m_skyBox;
    Gizmo *m_gizmo = nullptr;

    Shader* m_shaderLayoutLine = nullptr;


    //debug
private :
    uint m_drawCallCount = 0;
};


#endif //MOVE_FORWARDRENDERER_HPP
