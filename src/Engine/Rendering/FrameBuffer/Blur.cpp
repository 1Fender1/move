#include "Blur.hpp"

Blur::Blur(unsigned int w, unsigned int h) : FrameBuffer(w, h) {

    glGenFramebuffers(1, &m_fbo);
    glGenTextures(1, &ssaoColorBufferBlur);
    genScreen();
    resize(w, h);
}


void Blur::resize(unsigned int w, unsigned h) {
    m_width = w;
    m_height = h;

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur, 0);

    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    checkFrameBuffer("ssao blur m_fbo");
}
