#ifndef BLUR_HPP
#define BLUR_HPP

#include "FrameBuffer.hpp"

class Blur : public FrameBuffer {
public:
    Blur(unsigned int w, unsigned h);
    void resize(unsigned int w, unsigned h);

    unsigned int ssaoColorBufferBlur;
};

#endif // BLUR_H
