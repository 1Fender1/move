#include "FrameBuffer.hpp"
#include <Core/glAssert.hpp>


FrameBuffer::FrameBuffer(uint w, uint h) {

    m_width  = w;
    m_height = h;

    glGenFramebuffers(1, &m_fbo);
    glGenTextures(1, &m_fbTexture);
    glGenRenderbuffers(1, &m_rbo);

    resize(w, h);

    Mesh obj = genScreen();
    m_screenMesh = new Mesh(obj.getVertices(), obj.getIndices(), obj.getMaterial());

    m_texture.setTextureType("texture_diffuse");
    m_texture.setPath("");
    m_texture.setID(m_fbTexture);
    m_screenMesh->getMaterial()->addTexture(&m_texture);

}

FrameBuffer::FrameBuffer() {

}

FrameBuffer::~FrameBuffer() {
    glDeleteFramebuffers(1, &m_fbo);
    glDeleteTextures(1, &m_fbTexture);
    glDeleteRenderbuffers(1, &m_rbo);

    if (m_frameBufferMat)
        delete m_frameBufferMat;

    if (m_screenMesh)
        delete m_screenMesh;
}

void FrameBuffer::setTexture(uint nTex) {
    /*
    m_fbTexture = nTex;
    Mesh::Texture tex;
    tex.type = "texture_diffuse";
    tex.path = "";
    tex.id = m_fbTexture;
    std::vector<Mesh::Texture> texs = {tex};
    m_screenMesh->setTextures(texs);
*/
    LOG_ERROR("Function not implemented yet")
}


void FrameBuffer::bind(GLint bindValue) {
    glBindFramebuffer(GL_FRAMEBUFFER, bindValue);
}

void FrameBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
}

void FrameBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::unbind(GLint bindValue) {
    glBindFramebuffer(GL_FRAMEBUFFER, bindValue);
}

void FrameBuffer::resize(uint w, uint h) {

    m_width = w;
    m_height = h;

    bind();

    glBindTexture(GL_TEXTURE_2D, m_fbTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbTexture, 0);

    glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);


    checkFrameBuffer("basic m_fbo");

    unbind();
}

unsigned int FrameBuffer::id() { return m_fbo; }

bool FrameBuffer::checkFrameBuffer(std::string bufferName) {
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if(status != GL_FRAMEBUFFER_COMPLETE) {
        switch (status) {

            case GL_FRAMEBUFFER_UNDEFINED :
                LOG_ERROR("Error::FrameBuffer frameBuffer undefined ! %n", bufferName);
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT :
                LOG_ERROR("Error::FrameBuffer incomplete attachment ! %n", bufferName);
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT :
                LOG_ERROR("Error::FrameBuffer missing attachment ! %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER :
                LOG_ERROR("Error::FrameBuffer incomplete draw buffer ! %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER :
                LOG_ERROR("Error::FrameBuffer incomplete read buffer ! %n", bufferName)
                break;

            case GL_FRAMEBUFFER_UNSUPPORTED :
                LOG_ERROR("Error::FrameBuffer framebuffer unsuported ! %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE :
                LOG_ERROR("Error::FrameBuffer incomplete multisample ! %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS :
                LOG_ERROR("Error::FrameBuffer incomplete layer targets ! %n", bufferName)
                break;

            default :
                LOG_ERROR("Framebuffer is not complete ! %n", bufferName)
                break;

        }
        return false;
    }
    return true;
}

Mesh FrameBuffer::genScreen() {

    std::vector<Vertex> vertices;
    Vertex vertex;
    vertex.position = glm::vec3(-1, 1, 0);
    vertex.texCoords = glm::vec2(0, 1);
    vertices.emplace_back(vertex);

    vertex.position = glm::vec3(-1, -1, 0);
    vertex.texCoords = glm::vec2(0, 0);
    vertices.emplace_back(vertex);

    vertex.position = glm::vec3(1, -1, 0);
    vertex.texCoords = glm::vec2(1, 0);
    vertices.emplace_back(vertex);

    vertex.position = glm::vec3(1, 1, 0);
    vertex.texCoords = glm::vec2(1, 1);
    vertices.emplace_back(vertex);


    std::vector<uint> indices = {0, 1, 2, 0, 2, 3};

    m_frameBufferMat = new Material("quadFrameBuffer");

    Mesh obj = Mesh(vertices, indices, m_frameBufferMat);

    obj.setIsDiffuseMaped(true);
    obj.setIsNormalMaped(false);
    obj.setIsSpecularMaped(false);

    return obj;
}

void FrameBuffer::draw(Shader& shader) {
    m_screenMesh->draw(shader);
}

void FrameBuffer::draw(Shader* shader) {
    m_screenMesh->draw(*shader);
}

void FrameBuffer::drawGeom(Shader shader) {
    m_screenMesh->drawGeom(shader);
}

void FrameBuffer::drawGeom(Shader* shader) {
    m_screenMesh->drawGeom(*shader);
}

void FrameBuffer::draw(Shader shader, GLint bindValue) {
   glBindFramebuffer(GL_FRAMEBUFFER, bindValue);

    m_screenMesh->draw(shader);

    unbind();
}

void FrameBuffer::draw(Shader* shader, GLint bindValue) {
    glBindFramebuffer(GL_FRAMEBUFFER, bindValue);

    m_screenMesh->draw(*shader);

    unbind();
}

uint FrameBuffer::texId() {
    return m_fbTexture;
}
