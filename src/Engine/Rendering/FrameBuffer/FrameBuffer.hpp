#ifndef FRAMEBUFFER_HPP
#define FRAMEBUFFER_HPP


#include <cstdio>
#include <iostream>
#include <vector>

#pragma once

#include "Engine/Modeling/Mesh/Mesh.hpp"
#include <Engine/Rendering/Texture/Texture.hpp>



class FrameBuffer {

public:
    FrameBuffer(unsigned int w, unsigned int h);
    FrameBuffer();
    ~FrameBuffer();
    Mesh genScreen();
    void draw(Shader& shader);
    void draw(Shader shader, GLint bindValue);
    void drawGeom(Shader shader);
    void draw(Shader* shader);
    void draw(Shader* shader, GLint bindValue);
    void drawGeom(Shader* shader);
    void bind(GLint bindValue);
    void bind();
    void unbind();
    void unbind(GLint bindValue);
    void resize(unsigned int w, unsigned int h);
    uint texId();
    uint id();
    unsigned char *pixels = NULL;
    void setTexture(unsigned int nTex);

protected:
    uint m_fbo;
    uint m_rbo;
    uint m_fbTexture;


    Mesh *m_screenMesh = nullptr;
    Material* m_frameBufferMat = nullptr;
    Texture m_texture;
    uint m_idTex;

    uint m_width;
    uint m_height;

    bool checkFrameBuffer(std::string bufferName);

};

#endif // FRAMEBUFFER_H
