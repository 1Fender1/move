#include "Fxaa.hpp"

FXAA::FXAA(unsigned int w, unsigned int h) : FrameBuffer(w, h) {
    glGenFramebuffers(1, &m_fbo);
    glGenTextures(1, &m_fbTexture);

    resize(w, h);

    genScreen();
}

void FXAA::resize(unsigned int w, unsigned int h) {

    m_width = w;
    m_height = h;

    bind();

    glBindTexture(GL_TEXTURE_2D, m_fbTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbTexture, 0);
/*
    glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
*/

    checkFrameBuffer("FXAA m_fbo");

    unbind();

}
