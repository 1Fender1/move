#ifndef FXAA_HPP
#define FXAA_HPP

#include "FrameBuffer.hpp"

class FXAA : public FrameBuffer {
public:
    FXAA(unsigned int w, unsigned int h);
    void resize(unsigned int w, unsigned int h);
};

#endif // FXAA_H
