#include "Gbuffer.hpp"

GBuffer::GBuffer(unsigned int width, unsigned int height) : FrameBuffer(width, height) {
    this->m_width = width;
    this->m_height = height;

    glGenTextures(1, &m_gPosition);
    glGenTextures(1, &m_gNormal);
    glGenTextures(1, &m_gTangent);
    glGenTextures(1, &m_gSpecular);
    glGenTextures(1, &m_gAlbedo);
    glGenTextures(1, &m_gDepth);

    glGenRenderbuffers(1, &m_rboDepth);
    glGenFramebuffers(1, &m_fbo);

    resize(width, height);
}


GBuffer::~GBuffer() {
    if (m_gPosition > 0) {
        glDeleteTextures(1, &m_gPosition);
        m_gPosition = 0;
    }

    if (m_gNormal > 0) {
        glDeleteTextures(1, &m_gNormal);
        m_gNormal = 0;
    }

    if (m_gTangent > 0) {
        glDeleteTextures(1, &m_gTangent);
        m_gTangent = 0;
    }

    if (m_gSpecular > 0) {
        glDeleteTextures(1, &m_gSpecular);
        m_gSpecular = 0;
    }

    if (m_gAlbedo > 0) {
        glDeleteTextures(1, &m_gAlbedo);
        m_gAlbedo = 0;
    }

    if (m_gDepth > 0) {
        glDeleteTextures(1, &m_gDepth);
        m_gDepth = 0;
    }

    if (m_rboDepth > 0) {
        glDeleteRenderbuffers(1, &m_rboDepth);
        m_rboDepth = 0;
    }

    if (m_fbo > 0) {
        glDeleteFramebuffers(1, &m_fbo);
        m_fbo = 0;
    }
    updateTextures();
}


void GBuffer::resize(uint w, uint h) {
    m_width = w;
    m_height = h;

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    // - position color buffer
    glBindTexture(GL_TEXTURE_2D, m_gPosition);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_gPosition, 0);

    // - normal color buffer
    glBindTexture(GL_TEXTURE_2D, m_gNormal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_gNormal, 0);

    // - tangent color buffer
    glBindTexture(GL_TEXTURE_2D, m_gTangent);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_gTangent, 0);

    // - albedo color buffer
    glBindTexture(GL_TEXTURE_2D, m_gAlbedo);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_gAlbedo, 0);

    // - specular color buffer
    glBindTexture(GL_TEXTURE_2D, m_gSpecular);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, m_width, m_height, 0, GL_RED, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, m_gSpecular, 0);

    // - depth buffer
    glBindTexture(GL_TEXTURE_2D, m_gDepth);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, m_width, m_height, 0, GL_RED, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, m_gDepth, 0);


    // - tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
    uint attachments[6] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5 };
    glDrawBuffers(6, attachments);

    glBindRenderbuffer(GL_RENDERBUFFER, m_rboDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_width, m_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_rboDepth);


    checkFrameBuffer("GBuffer");

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    updateTextures();
    GL_CHECK_ERROR
}


uint GBuffer::getTexture(GTextureType type) {
    if (type >= G_NONE)
        return 0;

    return m_textures[type];
}


void GBuffer::updateTextures() {
    m_textures[G_POSITION] = m_gPosition;
    m_textures[G_DEPTH] = m_gDepth;
    m_textures[G_NORMAL] = m_gNormal;
    m_textures[G_TANGENT] = m_gTangent;
    m_textures[G_ALBEDO] = m_gAlbedo;
    m_textures[G_SPECULAR] = m_gSpecular;
}