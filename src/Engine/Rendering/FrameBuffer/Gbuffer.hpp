#ifndef GBUFFER_HPP
#define GBUFFER_HPP

#include "FrameBuffer.hpp"

class GBuffer : public FrameBuffer {
public:

    enum GTextureType {
        G_POSITION,
        G_DEPTH,
        G_NORMAL,
        G_TANGENT,
        G_ALBEDO,
        G_SPECULAR,
        G_NONE
    };

    GBuffer(uint width, uint height);
    ~GBuffer();

    void resize(uint w, uint h);


    uint getTexture(GTextureType type);


private:
    uint m_gPosition;
    uint m_gNormal;
    uint m_gTangent;
    uint m_gSpecular;
    uint m_gAlbedo;
    uint m_gDepth;

    uint m_rboDepth;


    uint m_textures[6];

    void updateTextures();
};



#endif // GBUFFER_H
