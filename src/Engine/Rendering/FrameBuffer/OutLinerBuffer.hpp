//
// Created by jb on 11/11/2020.
//

#ifndef MOVE_OUTLINERBUFFER_HPP
#define MOVE_OUTLINERBUFFER_HPP
#include <Engine/Rendering/FrameBuffer/FrameBuffer.hpp>

class OutLinerBuffer : public FrameBuffer {

public :
    OutLinerBuffer(uint w, uint h);
    void resize(uint w, uint h);


};


#endif //MOVE_OUTLINERBUFFER_HPP
