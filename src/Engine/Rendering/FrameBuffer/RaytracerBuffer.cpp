//
// Created by jb on 05/04/19.
//

#include "RaytracerBuffer.hpp"


RaytracerBuffer::RaytracerBuffer(uint w, uint h) {

    width  = w;
    height = h;

    //glGenFramebuffers(1, &m_fbo);
    glGenTextures(1, &texture);
    //glGenRenderbuffers(1, &m_rbo);

    //resize(w, h);

    Mesh obj = genScreen();
    screenMesh = new Mesh(obj.getVertices(), obj.getIndices(), obj.getMaterial());

    m_texture.setTextureType("texture_diffuse");
    m_texture.setPath("");
    m_texture.setID(texture);
    screenMesh->getMaterial()->addTexture(&m_texture);
}

void RaytracerBuffer::resize(uint w, uint h) {

    width = w;
    height = h;

    //glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);


    // glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbTexture, 0);
/*
    glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
    //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, m_width, m_height);
    //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
*/
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbTexture, 0);
  //  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_rbo);


    checkFrameBuffer("Raytrace framebuffer");

    //glViewport(0, 0, w, h);
  //  unbind();
}


RaytracerBuffer::~RaytracerBuffer() {
   // glDeleteFramebuffers(1, &m_fbo);
    glDeleteTextures(1, &texture);
   // glDeleteRenderbuffers(1, &m_rbo);
   delete screenMesh;
}

Mesh RaytracerBuffer::genScreen() {

    std::vector<Vertex> vertices;
    Vertex vertex;
    vertex.position = glm::vec3(-1, 1, 0);
    vertex.texCoords = glm::vec2(0, 1);
    vertices.emplace_back(vertex);

    vertex.position = glm::vec3(-1, -1, 0);
    vertex.texCoords = glm::vec2(0, 0);
    vertices.emplace_back(vertex);

    vertex.position = glm::vec3(1, -1, 0);
    vertex.texCoords = glm::vec2(1, 0);
    vertices.emplace_back(vertex);

    vertex.position = glm::vec3(1, 1, 0);
    vertex.texCoords = glm::vec2(1, 1);
    vertices.emplace_back(vertex);


    std::vector<unsigned int> indices = {0, 1, 2, 0, 2, 3};

    Material *mat = new Material("quadFrameBuffer");

    Mesh obj = Mesh(vertices, indices, mat);

    obj.setIsDiffuseMaped(true);
    obj.setIsNormalMaped(false);
    obj.setIsSpecularMaped(false);

    return obj;
}

void RaytracerBuffer::bind() {
    //glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
}

void RaytracerBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

bool RaytracerBuffer::checkFrameBuffer(std::string bufferName) {
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if(status != GL_FRAMEBUFFER_COMPLETE){
        switch (status) {

            case GL_FRAMEBUFFER_UNDEFINED :
                LOG_ERROR("Error::FrameBuffer frameBuffer undefined : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT :
                LOG_ERROR("Error::FrameBuffer incomplete attachment : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT :
                LOG_ERROR("Error::FrameBuffer missing attachment : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER :
                LOG_ERROR("Error::FrameBuffer incomplete draw buffer : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER :
                LOG_ERROR("Error::FrameBuffer incomplete read buffer : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_UNSUPPORTED :
                LOG_ERROR("Error::FrameBuffer framebuffer unsuported : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE :
                LOG_ERROR("Error::FrameBuffer incomplete multisample : %n", bufferName)
                break;

            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS :
                LOG_ERROR("Error::FrameBuffer incomplete layer targets : %n", bufferName)
                break;

            default :
                LOG_ERROR("Framebuffer is not complete : %n", bufferName)
                break;

        }
        return false;
    }
    return true;
}

void RaytracerBuffer::setTexture(uint ) {
    /*
    m_fbTexture = nTex;
    Mesh::Texture tex;
    tex.type = "texture_diffuse";
    tex.path = "";
    tex.id = m_fbTexture;
    std::vector<Mesh::Texture> texs = {tex};
    m_screenMesh->setTextures(texs);
     */
    LOG_ERROR("Function not implemented yet")
}

void RaytracerBuffer::draw(Shader shader) {
    screenMesh->drawGeom(shader);
}

void RaytracerBuffer::draw(Shader* shader) {
    screenMesh->drawGeom(*shader);
}

void RaytracerBuffer::setTextureFromData(float* textureData) {

    //useless, implament faster way
    updateTextureFromData(textureData);
}

void RaytracerBuffer::updateTextureFromData(float* data) {
 //   if (m_fbTexture == 0)
   //     return;

   // bind();

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, (GLvoid*)data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbTexture, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    //unbind();

}

uint RaytracerBuffer::getWidth() {
    return width;
}


uint RaytracerBuffer::getHeight() {
    return height;
}

uint RaytracerBuffer::getTexture() const {
    return texture;
}
