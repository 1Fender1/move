//
// Created by jb on 05/04/19.
//

#ifndef MOVE_RAYTRACERBUFFER_HPP
#define MOVE_RAYTRACERBUFFER_HPP

#include <cstdio>
#include <iostream>
#include <Engine/Modeling/Mesh/Mesh.hpp>
#include <vector>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Rendering/Texture/Texture.hpp>

class RaytracerBuffer {

public :
    RaytracerBuffer(uint w, uint h);
    ~RaytracerBuffer();
    void setTexture(uint nTex);
    void resize(uint w, uint h);
    void setTextureFromData(float* textureData);
    void updateTextureFromData(float* textureData);
    void bind();
    void unbind();
    void draw(Shader);
    void draw(Shader*);

    uint getWidth();
    uint getHeight();

    uint getTexture() const;

private :
    Mesh genScreen();
    bool checkFrameBuffer(std::string bufferName);


    uint fbo;
    uint rbo;
    uint texture;


    Mesh *screenMesh = nullptr;
    Texture m_texture;


    uint width;
    uint height;

};


#endif //MOVE_RAYTRACERBUFFER_HPP
