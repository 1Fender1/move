//
// Created by jb on 22/09/2019.
//

#include "ShadowMapPCFBuffer.hpp"
#include <Core/glAssert.hpp>

ShadowMapPCFBuffer::ShadowMapPCFBuffer(uint w, uint h) : FrameBuffer(w, h) {
    glGenFramebuffers(1, &m_fbo);
    glGenTextures(1, &m_fbTexture);

    m_width = w;
    m_height = h;

    resize(w, h);

}

ShadowMapPCFBuffer::~ShadowMapPCFBuffer() {

    glDeleteFramebuffers(1, &m_fbo);
    glDeleteTextures(1, &m_fbTexture);

}


void ShadowMapPCFBuffer::resize(uint w, uint h) {

    m_width = w;
    m_height = h;

    glBindTexture(GL_TEXTURE_2D, m_fbTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_width, m_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_fbTexture, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    checkFrameBuffer("Depth map framebuffer");
    unbind();


    glBindTexture(GL_TEXTURE_2D, 0);


}

void ShadowMapPCFBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glViewport(0, 0, m_width, m_height);

}