//
// Created by jb on 22/09/2019.
//

#ifndef MOVE_SHADOWMAPPCFBUFFER_HPP
#define MOVE_SHADOWMAPPCFBUFFER_HPP

#include <Engine/Rendering/FrameBuffer/FrameBuffer.hpp>
//#include <Core/glassert.hpp>

//Class used to store depthBuffer

class ShadowMapPCFBuffer : public FrameBuffer {

public :

    ShadowMapPCFBuffer(uint w, uint h);
    ~ShadowMapPCFBuffer();

    void resize(uint w, uint h);

    void bind();

};


#endif //MOVE_SHADOWMAPPCFBUFFER_HPP
