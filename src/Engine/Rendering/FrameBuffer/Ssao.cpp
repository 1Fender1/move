#include "Ssao.hpp"



float lerp(float a, float b, float coef) {
    return a + coef * (b - a);
}

SSAO::SSAO(unsigned int w, unsigned int h) : FrameBuffer(w, h)
{
    glGenFramebuffers(1, &m_fbo);
    glGenTextures(1, &ssaoColorBuffer);
    glGenTextures(1, &noiseTexture);
    //generateNoiseTexture();
    //kernel = generateKernel(64);

    std::vector<glm::vec3> ssaoNoise;
    std::uniform_real_distribution<GLfloat> randomFloats(0.f, 1.f);
    std::default_random_engine generator;

    for (unsigned int i=0; i < 16; i++) {
        glm::vec3 noise(randomFloats(generator) * 2.f - 1.f, randomFloats(generator) * 2.f - 1.f, 0.f);
        ssaoNoise.emplace_back(noise);
    }

    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    unsigned int nbSamples = 64;

    for (unsigned int i = 0; i < nbSamples; i++) {
        glm::vec3 sample(randomFloats(generator) * 2.f - 1.f, randomFloats(generator) * 2.f - 1.f, randomFloats(generator));
        sample = glm::normalize(sample);
        sample *= randomFloats(generator);

        float scale = float(i) / nbSamples;

        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        kernel.emplace_back(sample);
    }



    resize(w, h);
}


void SSAO::resize(unsigned int w, unsigned int h) {
    m_width = w;
    m_height = h;

   glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

   glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBuffer, 0);

   glDrawBuffer(GL_COLOR_ATTACHMENT0);

   checkFrameBuffer("ssao m_fbo");

   glBindFramebuffer(GL_FRAMEBUFFER, 0);

}



std::vector<glm::vec3> SSAO::generateKernel(unsigned int nbSamples) {
    std::uniform_real_distribution<GLfloat> randomFloats(0.f, 1.f);
    std::default_random_engine generator;
    std::vector<glm::vec3> ssaoKernel;

    for (unsigned int i = 0; i < nbSamples; i++) {
        glm::vec3 sample(randomFloats(generator) * 2.f - 1.f, randomFloats(generator) * 2.f - 1.f, randomFloats(generator));
        sample = glm::normalize(sample);
        sample *= randomFloats(generator);

        float scale = float(i) / nbSamples;

        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel.emplace_back(sample);
    }

    return ssaoKernel;
}


void SSAO::generateNoiseTexture() {
    std::vector<glm::vec3> ssaoNoise;
    std::uniform_real_distribution<GLfloat> randomFloats(0.f, 1.f);
    std::default_random_engine generator;

    for (unsigned int i=0; i < 16; i++) {
        glm::vec3 noise(randomFloats(generator) * 2.f - 1.f, randomFloats(generator) * 2.f - 1.f, 0.f);
        ssaoNoise.emplace_back(noise);
    }


    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


}

