#ifndef SSAO_HPP
#define SSAO_HPP

#include "FrameBuffer.hpp"
#include <random>

class SSAO : public FrameBuffer {
    public:
        SSAO(unsigned int w, unsigned int h);

        void resize(unsigned int w, unsigned int h);

        std::vector<glm::vec3> generateKernel(unsigned int nbSamples);
        void generateNoiseTexture();

        unsigned int ssaoColorBuffer;

        //m_fbTexture id
        unsigned int noiseTexture;

        std::vector<glm::vec3> kernel;


};

#endif // SSAO_H
