//
// Created by jb on 28/05/2020.
//

#include "AreaLight.hpp"
#include <Core/libsGL.hpp>
#include <Engine/Rendering/Light/LightUtils/ggx_areaLight_profile.hpp>
#include <Core/Math/Utils/Utils.hpp>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>

AreaLight::AreaLight() : Light() {

    m_constantAttenuation = 0.f;
    m_linearAttenuation = 0.f;
    m_quadraticAttenuation = 1.f;

    m_color = glm::vec3(1.f);

    m_width = 1.f;
    m_height = 1.f;


    initLTC();


    m_type = LightType::AREA;

}

AreaLight::AreaLight(const AreaLight& light) : Light(light) {
    m_constantAttenuation = light.m_constantAttenuation;
    m_linearAttenuation = light.m_linearAttenuation;
    m_quadraticAttenuation = light.m_quadraticAttenuation;

    m_color = light.m_color;

    m_width = light.m_width;
    m_height = light.m_height;

    m_isDoubleSided = light.m_isDoubleSided;

    initLTC();

    m_type = LightType::AREA;
}

AreaLight::AreaLight(glm::vec3 ) : AreaLight() {
    m_constantAttenuation = 0.f;
    m_linearAttenuation = 0.f;
    m_quadraticAttenuation = 1.f;

    m_color = glm::vec3(1.f);

    m_width = 1.f;
    m_height = 1.f;

    m_isDoubleSided = false;

    m_type = LightType::AREA;
}

AreaLight::~AreaLight() {

    if (m_magLTC != 0)
        glDeleteTextures(1, &m_magLTC);

    if (m_minVLTC != 0)
        glDeleteTextures(1, &m_minVLTC);

    if (m_minVCoeff)
        delete [] m_minVCoeff;

    if (m_magnitude)
        delete [] m_magnitude;

}

void AreaLight::draw(Shader& shader) {

    glm::vec3 position = getPosition();
    glm::vec3 direction = getDirection();

    shader.setFloatV3("light.areaLight.position", position);
    shader.setInt("light.type", m_type);
    shader.setFloatV3("light.areaLight.direction", direction);
    shader.setFloat("light.areaLight.attenuation.constant", m_constantAttenuation);
    shader.setFloat("light.areaLight.attenuation.linear", m_linearAttenuation);
    shader.setFloat("light.areaLight.attenuation.quadratic", m_quadraticAttenuation);
    shader.setFloatV3("light.color", m_color);
    shader.setFloat("light.intensity", m_intensity);
    shader.setFloat("light.areaLight.m_width", m_width);
    shader.setFloat("light.areaLight.m_height", m_height);
    shader.setBool("light.areaLight.doubleSided", m_isDoubleSided);


    glActiveTexture(GL_TEXTURE0 + 14);
    glBindTexture(GL_TEXTURE_2D, m_minVLTC);
    shader.setInt("areaMinV", 14);

    glActiveTexture(GL_TEXTURE0 + 15);
    glBindTexture(GL_TEXTURE_2D, m_magLTC);
    shader.setInt("areaMag", 15);

}

void AreaLight::drawLayout(Shader& shader) {
    if (!m_isSelected)
        return;
    generateLayoutMesh();
    if (!m_layoutMesh)
        return;
    m_layoutMesh->draw(shader);
}




float AreaLight::calcAttenutaion(float distance) {
    return 1.f/(m_constantAttenuation + m_linearAttenuation * distance + m_quadraticAttenuation * distance * distance);
}

glm::vec3 AreaLight::sample(ContextSceneEmbree& , Ray& , float& ) {
    LOG_WARNING("AreaLight::sample not implemented")
    return glm::vec3(0.f, -1.f, 0.f);
}

glm::vec3 AreaLight::getPower() {

    return glm::vec3(1.f);
}

glm::vec3 AreaLight::getDirection(glm::vec3) {
    glm::vec3 direction = glm::vec3(glm::inverse(getModel()) * glm::vec4(m_refDirection, 0.f));
    direction = glm::normalize(direction);

    return direction;
}

glm::vec3 AreaLight::getDirection() {
    glm::vec3 direction = glm::vec3(glm::inverse(getModel()) * glm::vec4(m_refDirection, 0.f));
    direction = glm::normalize(direction);

    return direction;
}



bool AreaLight::isDoubleSided() {
    return m_isDoubleSided;
}

void AreaLight::setDoubleSided(bool doubleSided) {
    m_isDoubleSided = doubleSided;
}

//Function getDirection must be called before this function
glm::vec3 AreaLight::eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& outPdf) {

    glm::vec3 areaDir = getDirection();

    float area = m_height * m_width;
    glm::vec3 position = getPosition();
    glm::vec3 lightSample = m_sampler.getRecSample(position, areaDir, m_width, m_height);
    //glm::vec3 lightSample = m_sampler.getRecSample(glm::inverse(getModel()), m_width, m_height);
    glm::vec3 direction = (lightSample - samplePosition);
    float dist = glm::length(direction);
    direction /= dist;
    outDirection = -direction;

    float attenuation = 1.f/computeDistAttenuation(dist);

    Ray shadowRay = Ray(samplePosition, direction);
    float visibility = computeVisibility(shadowRay, context);


    float NdotL = glm::dot(-direction, areaDir);
    float incidence = m_isDoubleSided ? glm::abs(NdotL) : glm::max(0.f, NdotL);
    float pdf =  attenuation / (area * incidence);
    outPdf = pdf;

    float cosTheta = glm::max(0.f, glm::dot(direction, normal));

    glm::vec3 radiance = ((m_color * m_intensity) / pdf) * cosTheta * visibility;

    return radiance;
}

float AreaLight::computeVisibility(Ray shadowRay, ContextSceneEmbree& context) {
    float visibility = 0.f;

    Intersection intersection = context.intersectRay(shadowRay);
    float intersectionDist = glm::length(intersection.getPosition() - shadowRay.getOrigin());
    float lightDist = /*glm::length(position - shadowRay.getOrigin())*/intersectionDist;
    if (!intersection.isHit() || (intersection.isHit() && (intersectionDist > lightDist)))
        visibility = 1.f;

    return visibility;
}

void AreaLight::initLTC() {

    if (m_magLTC != 0)
        glDeleteTextures(1, &m_magLTC);

    if (m_minVLTC != 0)
        glDeleteTextures(1, &m_minVLTC);

    if (m_minVCoeff)
        delete [] m_minVCoeff;

    if (m_magnitude)
        delete [] m_magnitude;

    m_minVCoeff = new float[ltc_size * ltc_size * 4];
    m_magnitude = new float[ltc_size * ltc_size * 2];
    uint magSize = ltc_size * ltc_size;
    for (uint i = 0; i < magSize; ++i) {
        uint j = i * 2;
        m_magnitude[j] = tabAmplitude[i][0];
        m_magnitude[j+1] = tabAmplitude[i][1];

    }


    for (uint i = 0; i < magSize; ++i) {
        uint j = i*4;
        glm::vec4 minV = tabM[i];

        m_minVCoeff[j] = minV[0];
        m_minVCoeff[j+1] = minV[1];
        m_minVCoeff[j+2] = minV[2];
        m_minVCoeff[j+3] = minV[3];
    }



    glGenTextures(1, &m_magLTC);
    glGenTextures(1, &m_minVLTC);

    glBindTexture(GL_TEXTURE_2D, m_magLTC);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, 64, 64, 0, GL_RG, GL_FLOAT, m_magnitude);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, m_minVLTC);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 64, 64, 0, GL_RGBA, GL_FLOAT, m_minVCoeff);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


}


float AreaLight::getWidth() {
    return m_width;
}
float AreaLight::getHeight() {
    return m_height;
}
void AreaLight::setWidth(float width) {
    m_width = width;
}
void AreaLight::setHeight(float height) {
    m_height = height;
}

void AreaLight::generateLayoutMesh() {
    glm::vec3 position = getPosition();
//    glm::mat3 rot = Core::Math::Utils::rotationMatFromDirections(m_refDirection, getDirection());
    glm::vec3 direction = getDirection();
//    glm::vec3 p0, p1, p2, p3;
    float fovY = m_camera->getFrustum().getFOV();
    float fov = fovY / 2.f * float(M_PI) / 180.f;
    float dist = glm::length(m_camera->getPosition() - position);


    Polyline square = BaseObjects::squareLine(position, getDirection(), m_width, m_height);
    float lineLength = 1.f/(15.f * (glm::sin(fov) / glm::cos(fov)) / dist);
    Polyline directionLine = BaseObjects::line(position, position + direction * lineLength);


    square.setFillDrawing(false);

    if (m_layoutMesh)
        delete m_layoutMesh;

    if (isDoubleSided())
        m_layoutMesh = new ObjectLine({square});
    else
        m_layoutMesh = new ObjectLine({square, directionLine});

}