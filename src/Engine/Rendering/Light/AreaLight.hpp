//
// Created by jb on 28/05/2020.
//

#ifndef MOVE_AREALIGHT_HPP
#define MOVE_AREALIGHT_HPP

#include <Engine/Rendering/Light/Light.hpp>
#include <Engine/Rendering/Light/LightUtils/AreaLightSampler.hpp>

class AreaLight : public Light {

public :
    AreaLight();
    AreaLight(glm::vec3 pos);
    AreaLight(const AreaLight& light);
    ~AreaLight();


    void draw(Shader& shader) override;
    void drawLayout(Shader& shader) override;


    float calcAttenutaion(float distance);

    glm::vec3 sample(ContextSceneEmbree& context, Ray& ray, float& pdf);
    glm::vec3 getPower() override;

    glm::vec3 getDirection(glm::vec3 samplePos) override;
    glm::vec3 getDirection();


    bool isDoubleSided();
    void setDoubleSided(bool doubleSided);


    glm::vec3 eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& pdf) override;

    void generateLayoutMesh() override;


    float getWidth();
    float getHeight();
    void setWidth(float width);
    void setHeight(float height);

private :

    float computeVisibility(Ray shadowRay, ContextSceneEmbree& context) override;

    void initLTC();

    float m_width = 1.f;
    float m_height = 1.f;

    bool m_isDoubleSided = false;

    GLuint m_magLTC = 0;
    GLuint m_minVLTC = 0;

    float* m_minVCoeff = nullptr;
    float* m_magnitude = nullptr;

    AreaLightSampler m_sampler;

};


#endif //MOVE_AREALIGHT_HPP
