//
// Created by jb on 19/09/2019.
//

#include <Engine/Rendering/Material/MaterialUtils.hpp>
#include "DirectionalLight.hpp"
#include <Engine/Modeling/Geometry/BaseObjects.hpp>


DirectionalLight::DirectionalLight() : Light() {

    m_color = glm::vec3(1.f);
    m_intensity = 1.f;
    m_objType = objectType::LIGHT;
    m_type = Light::LightType::DIR;


}

DirectionalLight::DirectionalLight(glm::vec3 direction, glm::vec3 color) : Light() {
    m_color = color;
    m_objType = objectType::LIGHT;
    m_type = Light::LightType::DIR;
}

DirectionalLight::DirectionalLight(const DirectionalLight& light) : Light(light) {
    m_color = light.m_color;
    m_objType = objectType::LIGHT;
    m_type = Light::LightType::DIR;
}

void DirectionalLight::draw(Shader& shader) {
    glm::vec3 direction = getDirection();

    shader.setFloatV3("light.dirLight.direction", direction);
    shader.setFloatV3("light.color", m_color);
    shader.setInt("light.type", m_type);
    shader.setFloat("light.intensity", m_intensity);

}

void DirectionalLight::drawLayout(Shader& shader) {
    if (!m_isSelected)
        return;
    generateLayoutMesh();
    if (!m_layoutMesh)
        return;
    m_layoutMesh->draw(shader);
}

float DirectionalLight::computeVisibility(Ray shadowRay, ContextSceneEmbree& context) {
    float visibility = 0.f;

    Intersection intersection = context.intersectRay(shadowRay);
    if (!intersection.isHit())
        visibility = 1.f;

    return visibility;
}

glm::vec3 DirectionalLight::eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& outPdf) {


    glm::vec3 lightDir = glm::normalize(-getDirection());

    outDirection = getDirection();
    outPdf = 1.f;

    float NdotL = glm::max(0.f, dot(lightDir, normal));
    glm::vec3 lightIntensity = NdotL * m_color * m_intensity;

    if (MaterialUtils::Utils::colorToLuma(lightIntensity) < glm::epsilon<float>())
        return glm::vec3(0.f);

    Ray shadowRay = Ray(samplePosition, lightDir);
    float visibility = computeVisibility(shadowRay, context);

    glm::vec3 radiance = lightIntensity * visibility;

    return radiance;

}



glm::vec3 DirectionalLight::sample(ContextSceneEmbree& context, Ray& ray, float& pdf) {
    pdf = 1.f;
    glm::vec3 direction = getDirection();

    Ray visibilityRay = Ray(ray.getOrigin(), -direction);
    Intersection intersection = context.intersectRay(visibilityRay);
    float visibility = 0.f;
    if (!intersection.isHit())
        visibility++;

    glm::vec3 lightDir = glm::normalize(ray.getDirection());

    float theta = dot(lightDir, normalize(-direction));
    if (theta <= 0.f)
        return glm::vec3(0.f);
    //float attenuation = glm::clamp(theta, 0.f, theta);
//    float attenuation = theta;

    return visibility * 1.f * m_color;
}

glm::vec3 DirectionalLight::getPower() {
    return glm::vec3(0.f);
}

glm::vec3 DirectionalLight::getDirection(glm::vec3) {
    glm::vec3 direction = glm::vec3(glm::inverse(getModel()) * glm::vec4(m_refDirection, 0.f));
    direction = glm::normalize(direction);
    return direction;
}

void DirectionalLight::setDirection(glm::vec3 ) {
    LOG_ERROR("DirectionalLight::setDirection not implemented")
}

void DirectionalLight::generateLayoutMesh() {
    glm::vec3 position = getPosition();
    glm::vec3 direction = getDirection();

    float dist = glm::length(m_camera->getPosition() - position);
//    const float scaleRadius = m_layoutBaseCircle;
    glm::vec3 camDir = glm::normalize(position - m_camera->getPosition());

    float fovY = m_camera->getFrustum().getFOV();
    float fov = fovY / 2.f * float(M_PI) / 180.f;


    float radius2 = 30.f * (glm::sin(fov) / glm::cos(fov)) / dist;
    float lineLength = 1.f/(m_layoutDirectionLine * (glm::sin(fov) / glm::cos(fov)) / dist);

    Polyline circle = BaseObjects::circleLine(position, camDir,1.f/radius2, 32);
    Polyline lineDir = BaseObjects::line(position, position + direction * lineLength);
    circle.setFillDrawing(false);

    if (m_layoutMesh)
        delete m_layoutMesh;

    m_layoutMesh = new ObjectLine({circle, lineDir});
}