//
// Created by jb on 19/09/2019.
//

#ifndef MOVE_DIRECTIONALLIGHT_HPP
#define MOVE_DIRECTIONALLIGHT_HPP

#include <glm/glm.hpp>
#include <string>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Rendering/Light/Light.hpp>

class DirectionalLight : public Light {

public:

    DirectionalLight();
    DirectionalLight(glm::vec3 direction, glm::vec3 color);
    DirectionalLight(const DirectionalLight& light);

    DirectionalLight& operator=(const DirectionalLight& light) {

        if (&light == this) {
            return *this;
        }

        m_objectName = light.m_objectName;
        m_color = light.m_color;
        m_intensity = light.m_intensity;
        m_objType = objectType::LIGHT;
        m_type = Light::LightType::DIR;

        return *this;

    }

    glm::vec3 eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& outPdf) override;
    void draw(Shader& shader) override;
    void drawLayout(Shader& shader) override;

    float computeVisibility(Ray shadowRay, ContextSceneEmbree& context) override;

    glm::vec3 sample(ContextSceneEmbree& context, Ray& ray, float& pdf) override;

    glm::vec3 getPower();

    glm::vec3 getDirection(glm::vec3 samplePos = glm::vec3(0.f)) override;
    void setDirection(glm::vec3 direction);

    void generateLayoutMesh() override;

private:
    const float m_layoutBaseCircle = 0.5f;
    const float m_layoutDirectionLine = 5.f;



};


#endif //MOVE_DIRECTIONALLIGHT_HPP
