//
// Created by jb on 02/03/18.
//

#include "Light.hpp"

Light::Light() {
    m_type = LightType::UNKNOW;
    m_objType = objectType::LIGHT;
    m_color = glm::vec3(1.f);
    m_intensity = 1.f;
    m_constantAttenuation = 0.f;
    m_linearAttenuation = 0.f;
    m_quadraticAttenuation = 1.f;
    m_nbSamples = 1;

}

Light::Light(const Light& light) : MoveObject(light) {
    m_type = light.m_type;
    m_objType = light.m_objType;
    pdf = light.pdf;
    m_objectName = light.m_objectName;
    m_color = light.m_color;
    m_intensity = light.m_intensity;
    m_nbSamples = light.m_nbSamples;

    m_constantAttenuation = light.m_constantAttenuation;
    m_linearAttenuation = light.m_linearAttenuation;
    m_quadraticAttenuation = light.m_quadraticAttenuation;
}

void Light::setType(unsigned int _type) {
    m_type = _type;
}

unsigned int Light::getLightType() {
    return m_type;
}

glm::vec3 Light::getPosition() {
    return glm::vec3(getModel()[3]);
}

void Light::setPosition(glm::vec3 pos) {
    glm::mat4 model = getModel();
    model[3] = glm::vec4(pos, 1.f);
    setTransform(model);
}


float Light::getConstantAttenuation() {
    return m_constantAttenuation;
}

float Light::getLinearAttenuation() {
    return m_linearAttenuation;
}

float Light::getQuadraticAttenuation() {
    return m_quadraticAttenuation;
}

void Light::setConstantAttenuation(float constant) {
    m_constantAttenuation = constant;
}

void Light::setLinearAttenuation(float linear) {
    m_linearAttenuation = linear;
}

void Light::setQuadraticAttenuation(float quadratic) {
    m_quadraticAttenuation = quadratic;
}


uint Light::getSampleCount() { return m_nbSamples; }


void Light::setSampleCount(uint sampleCount) {
    m_nbSamples = (sampleCount == 0) ? 1 : sampleCount;
}

void Light::setAttenuationPhysical(bool att) {

    if (m_isAttenuationPhysical == att) {
        return;
    }

    m_isAttenuationPhysical = att;

    if (att) {
        m_backupConstAtt = m_constantAttenuation;
        m_backupLinearConstAtt = m_linearAttenuation;
        m_backupQuadraticConstAtt = m_quadraticAttenuation;
        m_constantAttenuation = 0.f;
        m_linearAttenuation = 0.f;
        m_quadraticAttenuation = 1.f;
    }
    else {
        m_constantAttenuation = m_backupConstAtt;
        m_linearAttenuation = m_backupLinearConstAtt;
        m_quadraticAttenuation = m_backupQuadraticConstAtt;
        m_backupConstAtt = m_constantAttenuation;
        m_backupLinearConstAtt = m_linearAttenuation;
        m_backupQuadraticConstAtt = m_quadraticAttenuation;
    }

}


float Light::computeDistAttenuation(float dist) {
    float attenuation = m_constantAttenuation + m_linearAttenuation * dist + m_quadraticAttenuation * dist * dist;
    if (attenuation < glm::epsilon<float>())
        return 1.f;
    return 1.f / attenuation;
}


void Light::setCamera(Camera* camera) {
    m_camera = camera;
}