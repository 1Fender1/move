//
// Created by jb on 02/03/18.
//

#ifndef HELLOOPENGL_LIGHTOBJECT_HPP
#define HELLOOPENGL_LIGHTOBJECT_HPP

#include <Core/System/Logger.hpp>
#include <Ui/libs_ui.hpp>
#include <Core/Math/RayTracing/Ray.hpp>
#include <Core/Math/RayTracing/Intersection.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
#include <Engine/Modeling/Mesh/ObjectLine.hpp>

#include <string>
#include <cstdio>
#include <iostream>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>
#include <Engine/Scenes/MoveObject.hpp>
#include <Engine/Camera/Camera.hpp>

class ContextSceneEmbree;

/*
Default light is a point light
*/
class Light : public MoveObject {

public:

    enum LightType {
        POINT = 0,
        SPOT = 1,
        DIR = 2,
        AREA = 3,
        UNKNOW
    };

    Light();
    Light(const Light& light);
    Light& operator=(Light& light) {

        if (&light == this)
            return *this;

        m_objectName = light.m_objectName;
        m_type = light.m_type;
        m_objType = light.m_objType;
        pdf = light.pdf;
        m_color = light.m_color;
        m_nbSamples = light.m_nbSamples;

        return *this;
    }

    virtual ~Light() {}

    void translate(glm::vec3);
    virtual void draw(Shader&) {LOG_WARNING("Default draw function, does not be call : %n", m_objectName)};
    virtual void drawLayout(Shader&) {LOG_WARNING("Default drawLayout function, does not be call : %n", m_objectName)};
    virtual glm::vec3 eval(const glm::vec3&, const glm::vec3&, ContextSceneEmbree&, glm::vec3&, float&) {LOG_WARNING("Default eval function, does not be call : %n", m_objectName); return glm::vec3(0.f);};
    virtual float computeVisibility(Ray, ContextSceneEmbree&) {LOG_WARNING("Default computeVisibility function, does not be call : %n", m_objectName); return 0.f;};

    void setType(uint _type);
    uint getLightType();
    float getIntensity() {return m_intensity;}
    void setIntensity(float intensity) {m_intensity = intensity;}

    glm::vec3 getPosition();
    virtual glm::vec3 getDirection(glm::vec3 samplePos = glm::vec3(0.f)) {LOG_WARNING("Default getDirection function, does not be call : %n", m_objectName);return samplePos;};

    void setPosition(glm::vec3);

    glm::vec3 getColor() {return m_color;}
    void setColor(glm::vec3 color) {m_color = color;}

     virtual glm::vec3 sample(ContextSceneEmbree&, Ray&, float&) {
         LOG_WARNING("Default sample function, does not be call : %n", m_objectName)
          return glm::vec3(0.f);
    };
    virtual glm::vec3 getPower() {LOG_WARNING("Default getPower function, does not be call : %n", m_objectName) return glm::vec3(0.f);};

    bool isAttenuationPhysical() {return m_isAttenuationPhysical;}
    void setAttenuationPhysical(bool att);


    float getConstantAttenuation();
    float getLinearAttenuation();
    float getQuadraticAttenuation();
    void setConstantAttenuation(float constant);
    void setLinearAttenuation(float linear);
    void setQuadraticAttenuation(float quadratic);

    float computeDistAttenuation(float dist);

    uint getSampleCount();
    void setSampleCount(uint sampleCount);

    void setCamera(Camera* camera);


private:

    float m_backupConstAtt = 0.f;
    float m_backupLinearConstAtt = 0.f;
    float m_backupQuadraticConstAtt = 0.f;

protected:

    int m_type;
    float pdf = 1.f;

    const glm::vec3 m_refDirection = glm::normalize(glm::vec3(0.f, -1.f, 0.f));

    //Only used for path tracer
    uint m_nbSamples = 1;
    virtual void generateLayoutMesh() {LOG_WARNING("Default generateLayoutMesh function, does not be call : %n", m_objectName)}


protected:

    glm::vec3 m_color;
    float m_intensity = 1.f;
    float m_constantAttenuation = 0.f;
    float m_linearAttenuation = 0.f;
    float m_quadraticAttenuation = 0.f;

    bool m_isAttenuationPhysical = true;

    ObjectLine *m_layoutMesh = nullptr;

    //Useful to generate layout mesh with screen space size
    Camera* m_camera = nullptr;


};

typedef std::unique_ptr<Light> Light_ptr;


#endif //HELLOOPENGL_LIGHTOBJECT_H
