//
// Created by jb on 10/07/2020.
//

#include "AreaLightSampler.hpp"
#include <iostream>

AreaLightSampler::AreaLightSampler() {

    m_generator = std::mt19937(m_randomDev());
    m_uvDist = std::uniform_real_distribution<float>(0.f, 1.f);

}

glm::vec3 AreaLightSampler::getRecSample(const glm::mat4& rmat, float width, float height) {
    float u = m_uvDist(m_generator);
    float v = m_uvDist(m_generator);

    float halfWidth = width / 2.f;
    float halfHeight = height / 2.f;

    glm::vec3 newPoint = glm::vec3(u * width - halfWidth, 0.f, v * height - halfHeight);

    return glm::vec3(rmat * glm::vec4(newPoint, 1.f));
}


glm::vec3 AreaLightSampler::getRecSample(const glm::vec3& center, const glm::vec3& normal, float width, float height) {

    float u = m_uvDist(m_generator);
    float v = m_uvDist(m_generator);

    float halfWidth = width / 2.f;
    float halfHeight = height / 2.f;

    glm::vec3 newPoint = glm::vec3(u * width - halfWidth, 0.f, v * height - halfHeight);

    glm::vec3 basePlaneNormal = glm::normalize(glm::vec3(0.f, -1.f, 0.f));

    if (basePlaneNormal == normal)
        return newPoint + center;

    float rotAngle = glm::dot(basePlaneNormal, normal)/(glm::length(basePlaneNormal) * glm::length(normal));

    glm::vec3 axis = glm::normalize(glm::cross(basePlaneNormal, normal));

    float c = rotAngle;
    float s = glm::sqrt(1.f - c * c);
    float C = 1.f - c;
    float x = axis.x;
    float y = axis.y;
    float z = axis.z;
    glm::mat3 rmat = glm::mat3(glm::vec3(x * x * C + c, x * y * C - z * s, x * z * C + y * s),
                          glm::vec3(y * x * C + z * s, y * y * C + c, y * z * C - x * s),
                          glm::vec3(z * x * C - y * s, z * y * C + x * s, z * z * C + c));

    newPoint = (rmat * newPoint) + center;


    return newPoint;
}