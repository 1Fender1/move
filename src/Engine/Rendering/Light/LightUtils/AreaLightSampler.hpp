//
// Created by jb on 10/07/2020.
//

#ifndef MOVE_AREALIGHTSAMPLER_HPP
#define MOVE_AREALIGHTSAMPLER_HPP

#include <glm/glm.hpp>
#include <random>
class AreaLightSampler {

public :
    enum SampleType {
        UNIFORM,
        STRATIFIED

    };

    AreaLightSampler();
    glm::vec3 getRecSample(const glm::vec3& center, const glm::vec3& normal, float width, float height);
    glm::vec3 getRecSample(const glm::mat4& rmat, float width, float height);


private :


    SampleType m_sampleType = UNIFORM;
    std::random_device m_randomDev;
    std::mt19937 m_generator;
    std::uniform_real_distribution<float> m_uvDist;


};


#endif //MOVE_AREALIGHTSAMPLER_HPP
