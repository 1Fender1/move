//
// Created by jb on 19/06/2020.
//

#ifndef MOVE_COMPUTELTC_GGX_HPP
#define MOVE_COMPUTELTC_GGX_HPP

#include <Core/Math/Optimizer/nelder_mead.hpp>
#include <Engine/Rendering/Material/Materials.hpp>
#include <Core/System/Utility/FileUtility.hpp>
#include <omp.h>

//This file exist to precompute BRDF LTC fitting

//Main LTC fitting code is from come from Eric Heitz paper work (https://eheitzresearch.wordpress.com/415-2/)
//LTC Fresnel fitting method come from Stephen Hill paper work (https://blog.selfshadow.com/publications/s2016-advances/s2016_ltc_fresnel.pdf)


// size of precomputed table (theta, alpha)
const int N = 64;
// number of samples used to compute the error during fitting
const int Nsample = 50;
// minimal roughness (avoid singularities)
const float MIN_ALPHA = 0.0001f;

namespace LightUtils {

    namespace LTC {

        struct LTC {

            // lobe amplitude
            float amplitude;
            float fresnel;

            // parametric representation
            float m11, m22, m13, m23;
            glm::vec3 X, Y, Z;

            // matrix representation
            glm::mat3 M;
            glm::mat3 invM;
            float detM;

            LTC() {
                amplitude = 1.f;
                fresnel = 1.f;
                m11 = 1.f;
                m22 = 1.f;
                m13 = 0.f;
                m23 = 0.f;
                X = glm::vec3(1.f, 0.f, 0.f);
                Y = glm::vec3(0.f, 1.f, 0.f);
                Z = glm::vec3(0.f, 0.f, 1.f);
                update();
            }

            void copy(const LTC& ltc) {
                this->amplitude = ltc.amplitude;
                this->fresnel = ltc.fresnel;
                this->m11 = ltc.m11;
                this->m22 = ltc.m22;
                this->m13 = ltc.m13;
                this->m23 = ltc.m23;
                this->X = ltc.X;
                this->Y = ltc.Y;
                this->Z = ltc.Z;
                this->M = ltc.M;
                this->invM = ltc.invM;
                this->detM = ltc.detM;
            }

            void update() { // compute matrix from parameters
                M = glm::mat3(X, Y, Z) *
                    glm::mat3(m11, 0.f, 0.f,
                              0.f, m22, 0.f,
                              m13, m23, 1.f);
                invM = glm::inverse(M);
                detM = glm::abs(glm::determinant(M));
            }

            void update2() {
                invM = glm::inverse(M);
                detM = glm::abs(glm::determinant(M));
            }

            float eval(const glm::vec3& L) const {
                glm::vec3 Loriginal = glm::normalize(invM * L);
                glm::vec3 L_ = M * Loriginal;

                float l = glm::length(L_);
                float Jacobian = detM / (l*l*l);

                float D = 1.f /float(M_PI) * glm::max<float>(0.f, Loriginal.z);

                float res = amplitude * D / Jacobian;
                return res;
            }

            glm::vec3 sample(const float U1, const float U2) const {
                const float theta = glm::cos(glm::sqrt(U1));
                const float phi = 2.f*float(M_PI) * U2;
                const glm::vec3 L = glm::normalize(M * glm::vec3(glm::sin(theta)*glm::cos(phi), glm::sin(theta)*glm::sin(phi), glm::cos(theta)));
                return L;
            }

            void testNormalization() const {
                double sum = 0.0;
                float dtheta = 0.005f;
                float dphi = 0.005f;
                for (float theta = 0.f ; theta <= float(M_PI) ; theta+=dtheta) {
                    for (float phi = 0.f; phi <= 2.f * float(M_PI); phi += dphi) {
                        glm::vec3 L(glm::cos(phi) * glm::sin(theta), glm::sin(phi) * glm::sin(theta), glm::cos(theta));

                        sum += glm::sin(theta) * eval(L);
                    }
                }
                sum *= dtheta * dphi;
                LOG("LTC normalization test : %n", sum)
                LOG("LTC normalization expected : %n", amplitude)
            }
        };


// compute the error between the BRDF and the LTC
// using Multiple Importance Sampling
        float computeError(const LTC& ltc, const Material& brdf, const glm::vec3& V, const float alpha) {
            double error = 0.0;

            for(int j = 0 ; j < Nsample ; ++j) {
                for (int i = 0; i < Nsample; ++i) {
                    const float U1 = (i + 0.5f) / (float) Nsample;
                    const float U2 = (j + 0.5f) / (float) Nsample;

                    // importance sample LTC
                    {
                        // sample
                        const glm::vec3 L = ltc.sample(U1, U2);

                        // error with MIS weight
                        float pdf_brdf;
                        float eval_brdf = brdf.eval_BRDF_LTC(V, L, alpha, pdf_brdf);
                        float eval_ltc = ltc.eval(L);
                        float pdf_ltc = eval_ltc / ltc.amplitude;
                        double error_ = fabsf(eval_brdf - eval_ltc);
                        error_ = error_ * error_ * error_;
                        error += error_ / (pdf_ltc + pdf_brdf);
                    }

                    // importance sample BRDF
                    {
                        // sample
                        const glm::vec3 L = brdf.sample_LTC(V, alpha, U1, U2);

                        // error with MIS weight
                        float pdf_brdf;
                        float eval_brdf = brdf.eval_BRDF_LTC(V, L, alpha, pdf_brdf);
                        float eval_ltc = ltc.eval(L);
                        float pdf_ltc = eval_ltc / ltc.amplitude;
                        double error_ = fabsf(eval_brdf - eval_ltc);
                        error_ = error_ * error_ * error_;
                        error += error_ / (pdf_ltc + pdf_brdf);
                    }
                }
            }

            return (float)error / (float)(Nsample*Nsample);
        }

        struct FitLTC {
            FitLTC(LightUtils::LTC::LTC& ltc_, const Material& brdf, bool isotropic_, const glm::vec3& V_, float alpha_) :
                    brdf(brdf), ltc(ltc_), isotropic(isotropic_), V(V_), alpha(alpha_) {
            }

            void update(const float * params) {
                if (params == nullptr)
                    return;

                float m11 = std::max<float>(params[0], MIN_ALPHA);
                float m22 = std::max<float>(params[1], MIN_ALPHA);
                float m13 = params[2];
                float m23 = params[3];

                if(isotropic) {
                    ltc.m11 = m11;
                    ltc.m22 = m11;
                    ltc.m13 = 0.f;
                    ltc.m23 = 0.f;
                } else {
                    ltc.m11 = m11;
                    ltc.m22 = m22;
                    ltc.m13 = m13;
                    ltc.m23 = m23;
                }
                ltc.update();
            }

            float operator()(const float * params)
            {
                update(params);
                return computeError(ltc, brdf, V, alpha);
            }

            const Material& brdf;
            LightUtils::LTC::LTC& ltc;
            bool isotropic;

            const glm::vec3& V;
            float alpha;
        };

        // compute the norm (albedo) of the BRDF
        float computeNorm(const Material& brdf, const glm::vec3& V, const float alpha) {
            float norm = 0.0;

            for(int j = 0 ; j < Nsample ; ++j) {
                for (int i = 0; i < Nsample; ++i) {
                    const float U1 = (i + 0.5f) / (float) Nsample;
                    const float U2 = (j + 0.5f) / (float) Nsample;

                    // sample
                    const glm::vec3 L = brdf.sample_LTC(V, alpha, U1, U2);

                    // eval
                    float pdf;
                    float eval = brdf.eval_BRDF_LTC(V, L, alpha, pdf);

                    // accumulate
                    norm += (pdf > 0) ? eval / pdf : 0.0f;
                }
            }

            return norm / (float)(Nsample*Nsample);
        }

        // compute the norm (albedo) of the BRDF
        float computeNormF0(const Material& brdf, const glm::vec3& V, float alpha) {
            float norm = 0.0;

            for(int j = 0 ; j < Nsample ; ++j) {
                for (int i = 0; i < Nsample; ++i) {
                    const float U1 = (i + 0.5f) / (float) Nsample;
                    const float U2 = (j + 0.5f) / (float) Nsample;

                    // sample
                    const glm::vec3 L = brdf.sample_LTC(V, alpha, U1, U2);

                    // eval
                    float pdf;
                    float eval = brdf.eval_BRDF_LTC(V, L, alpha, pdf);
                    float evalFresnel = brdf.eval_fresnel_LTC(V, L);
                    float value = eval * evalFresnel;

                    // accumulate
                    norm += (pdf > 0) ? value / pdf : 0.0f;
                }
            }

            return norm / (float)(Nsample*Nsample);
        }

// compute the average direction of the BRDF
        glm::vec3 computeAverageDir(const Material& brdf, const glm::vec3& V, const float alpha) {
            glm::vec3 averageDir = glm::vec3(0.f, 0.f, 0.f);

            for(int j = 0 ; j < Nsample ; ++j) {
                for (int i = 0; i < Nsample; ++i) {
                    const float U1 = (i + 0.5f) / (float) Nsample;
                    const float U2 = (j + 0.5f) / (float) Nsample;

                    // sample
                    const glm::vec3 L = brdf.sample_LTC(V, alpha, U1, U2);

                    // eval
                    float pdf;
                    float eval = brdf.eval_BRDF_LTC(V, L, alpha, pdf);

                    // accumulate
                    averageDir += (pdf > 0.f) ? eval / pdf * L : glm::vec3(0.f, 0.f, 0.f);
                }
            }

            // clear y component, which should be zero with isotropic BRDFs
            averageDir.y = 0.f;

            return normalize(averageDir);
        }


// fit brute force
// refine first guess by exploring parameter space
        void fit(LTC& ltc, const Material& brdf, const glm::vec3& V, const float alpha, const bool isotropic=false) {
         //   float startFit[4] = { ltc.m11, ltc.m22, ltc.m13, ltc.m23 };
            float resultFit[4] = {0.f, 0.f, 0.f, 0.f};

            FitLTC fitter(ltc, brdf, isotropic, V, alpha);

            // Find best-fit LTC lobe (scale, alphax, alphay)
//            float error = NelderMead<4>(resultFit, startFit, epsilon, 1e-5f, 100, fitter);

            // Update LTC with best fitting values
            fitter.update(resultFit);
        }

        // fit data
        void fitTab(glm::mat3 * tab, glm::vec2 * tabAmplitude, const int N, const Material& brdf) {
            LTC ltc;

            LOG("Start fittig LTC")

            // loop over theta and alpha
            for (int a = N-1 ; a >= 0; --a) {
               // #pragma omp parallel for
                for (int t = 0; t <= N - 1; ++t) {
                    float theta = std::min<float>(1.57f, t / float(N - 1) * 1.57079f);
                    const glm::vec3 V = glm::vec3(sinf(theta), 0, cosf(theta));

                    // alpha = roughness^2
                    float roughness = a / float(N - 1);
                    float alpha = std::max<float>(roughness * roughness, MIN_ALPHA);

                    ltc.amplitude = computeNorm(brdf, V, alpha);
                    ltc.fresnel = computeNormF0(brdf, V, alpha);
                    const glm::vec3 averageDir = computeAverageDir(brdf, V, alpha);
                    bool isotropic;

                    // 1. first guess for the fit
                    // init the hemisphere in which the distribution is fitted
                    // if theta == 0 the lobe is rotationally symmetric and aligned with Z = (0 0 1)
                    if (t == 0) {
                        ltc.X = glm::vec3(1.f, 0.f, 0.f);
                        ltc.Y = glm::vec3(0.f, 1.f, 0.f);
                        ltc.Z = glm::vec3(0.f, 0.f, 1.f);

                        if (a == N - 1) { // roughness = 1
                            ltc.m11 = 1.f;
                            ltc.m22 = 1.f;
                        } else {  // init with roughness of previous fit
                            ltc.m11 = std::max<float>(tab[a + 1 + t * N][0][0], MIN_ALPHA);
                            ltc.m22 = std::max<float>(tab[a + 1 + t * N][1][1], MIN_ALPHA);
                        }

                        ltc.m13 = 0.f;
                        ltc.m23 = 0.f;
                        ltc.update();

                        isotropic = true;
                    } else { // otherwise use previous configuration as first guess
                        glm::vec3 L = normalize(averageDir);
                        glm::vec3 T1(L.z, 0.f, -L.x);
                        glm::vec3 T2(0.f, 1.f, 0.f);
                        ltc.X = T1;
                        ltc.Y = T2;
                        ltc.Z = L;

                        ltc.update();

                        isotropic = false;
                    }

                    // 2. fit (explore parameter space and refine first guess)
                    fit(ltc, brdf, V, alpha, isotropic);

                    // copy data
                    tab[a + t * N] = ltc.M;
                    tabAmplitude[a + t * N][0] = ltc.amplitude;
                    tabAmplitude[a + t * N][1] = ltc.fresnel;

                    // kill useless coefs in matrix and normalize
                    tab[a + t * N][0][1] = 0.f;
                    tab[a + t * N][1][0] = 0.f;
                    tab[a + t * N][2][1] = 0.f;
                    tab[a + t * N][1][2] = 0.f;
                    tab[a + t * N] = 1.f / tab[a + t * N][2][2] * tab[a + t * N];

                }
                LOG("%n%", float(N-1 - a) / (float(N-1)) * 100.f)
            }
            LOG("LTC correctly fitted")
        }
    }
}


namespace LightUtils {

    namespace LTC {

        void compute_LTC_PrincipledMaterial() {
            PrincipledMaterial mat;
            glm::mat3 *minVs = new glm::mat3[N*N];
            glm::vec2 *amplitudes = new glm::vec2[N*N];

            fitTab(minVs, amplitudes, N, mat);

            std::vector<glm::vec4> toWriteMinVs;
            toWriteMinVs.resize(N*N);
            std::vector<glm::vec2> toWriteAmplitudes;
            toWriteAmplitudes.resize(N*N);

            for (uint i = 0; i < N*N; ++i) {
                glm::mat3 MinV = minVs[i];

                toWriteMinVs[i][0] = MinV[0][0];
                toWriteMinVs[i][1] = MinV[0][2];
                toWriteMinVs[i][2] = MinV[1][1];
                toWriteMinVs[i][3] = MinV[2][0];

                toWriteAmplitudes[i] = amplitudes[i];
            }

            LOG("Write data on disk")
            Core::File::generateCPPFileFromData("./MinV_PrincipledMat.hpp", toWriteMinVs);
            Core::File::generateCPPFileFromData("./Amplitude_PrincipledMat.hpp", toWriteAmplitudes);
            LOG("Precompute terminated")

            delete minVs;
            delete amplitudes;
        }

    }
}


#endif //MOVE_COMPUTELTC_GGX_HPP
