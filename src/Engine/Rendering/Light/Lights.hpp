//
// Created by jb on 19/09/2019.
//

#ifndef MOVE_LIGHTS_HPP
#define MOVE_LIGHTS_HPP

#pragma once
//#include <Engine/Rendering/Light/Light.hpp>
#include <Engine/Rendering/Light/Pointlight.hpp>
#include <Engine/Rendering/Light/SpotLight.hpp>
#include <Engine/Rendering/Light/DirectionalLight.hpp>
#include <Engine/Rendering/Light/AreaLight.hpp>

#endif //MOVE_LIGHTS_HPP
