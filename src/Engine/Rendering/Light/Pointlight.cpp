#include <Engine/Modeling/Geometry/BaseObjects.hpp>
#include "Pointlight.hpp"



PointLight::PointLight() : Light() {

    m_constantAttenuation = 0.f;
    m_linearAttenuation = 0.f;
    m_quadraticAttenuation = 1.f;

    m_color = glm::vec3(1.f);

    m_type = LightType::POINT;
}

PointLight::PointLight(const PointLight& light) : Light(light) {
    m_constantAttenuation = light.m_constantAttenuation;
    m_linearAttenuation = light.m_linearAttenuation;
    m_quadraticAttenuation = light.m_quadraticAttenuation;

    m_type = LightType::POINT;

}

void PointLight::draw(Shader& shader) {
    glm::vec3 position = getPosition();

    shader.setFloatV3("light.pointLight.position", position);
    shader.setInt("light.type", m_type);
    shader.setFloat("light.pointLight.attenuation.constant", m_constantAttenuation);
    shader.setFloat("light.pointLight.attenuation.linear", m_linearAttenuation);
    shader.setFloat("light.pointLight.attenuation.quadratic", m_quadraticAttenuation);
    shader.setFloatV3("light.color", m_color);
    shader.setFloat("light.intensity", m_intensity);
}

void PointLight::drawLayout(Shader& shader) {
    if (!m_isSelected)
        return;
    generateLayoutMesh();
    if (!m_layoutMesh)
        return;
    m_layoutMesh->draw(shader);
}

glm::vec3 PointLight::sample(ContextSceneEmbree& context, Ray& ray, float& pdf) {
    pdf = 1.f;
    glm::vec3 position = getPosition();
    Ray visibilityRay = Ray(ray.getOrigin(), position - ray.getOrigin());
    Intersection intersection = context.intersectRay(visibilityRay);
    float visibility = 0.f;
    float intersectionDist = glm::length(intersection.getPosition() - ray.getOrigin());
    float lightDist = glm::length(position - ray.getOrigin());
    if (!intersection.isHit() || (intersection.isHit() && (intersectionDist > lightDist)))
        visibility++;

    return visibility * (m_color/(lightDist*lightDist));

}

glm::vec3 PointLight::getDirection(glm::vec3 samplePos) {
    return glm::normalize(samplePos - getPosition());
}

float PointLight::computeVisibility(Ray shadowRay, ContextSceneEmbree& context) {
    float visibility = 0.f;
    glm::vec3 position = getPosition();

    Intersection intersection = context.intersectRay(shadowRay);
    float intersectionDist = glm::length(intersection.getPosition() - shadowRay.getOrigin());
    float lightDist = glm::length(position - shadowRay.getOrigin());
    if (!intersection.isHit() || (intersection.isHit() && (intersectionDist > lightDist)))
        visibility = 1.f;

    return visibility;
}

glm::vec3 PointLight::eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& outPdf) {
    glm::vec3 position = getPosition();

    float dist = glm::length(position - samplePosition);
    glm::vec3 direction = (position - samplePosition) / dist;
    outDirection = -direction;
    outPdf = 1.f;

    float attenuation = computeDistAttenuation(dist);

    Ray shadowRay = Ray(samplePosition, glm::normalize(position - samplePosition));
    float visibility = computeVisibility(shadowRay, context);

    float NdotL = glm::max(0.f, glm::dot(direction, normal));
    glm::vec3 radiance = NdotL * m_color * m_intensity * attenuation * visibility;


    return radiance;
}


glm::vec3 PointLight::getPower() {
    return 4 * float(M_PI) * m_color;
}

float PointLight::calcAttenutaion(float distance) {
    return 1.f/(m_constantAttenuation + m_linearAttenuation * distance + m_quadraticAttenuation * distance * distance);
}

void PointLight::generateLayoutMesh() {
    glm::vec3 position = getPosition();

    float dist = glm::length(m_camera->getPosition() - position);
//    const float scaleRadius = m_layoutBaseCircle;
    glm::vec3 camDir = glm::normalize(position - m_camera->getPosition());

    float fovY = m_camera->getFrustum().getFOV();
    float fov = fovY / 2.f * float(M_PI) / 180.f;


    float radius2 = 30.f * (glm::sin(fov) / glm::cos(fov)) / dist;

    Polyline circle = BaseObjects::circleLine(position, camDir,1.f/radius2, 32);
    Polyline lineToY = BaseObjects::line(position, glm::vec3(position.x, 0.f, position.z));
    circle.setFillDrawing(false);

    if (m_layoutMesh)
        delete m_layoutMesh;

    m_layoutMesh = new ObjectLine({circle, lineToY});
}
