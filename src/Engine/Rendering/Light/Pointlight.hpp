#ifndef POINTLIGHT_HPP
#define POINTLIGHT_HPP
#include "Light.hpp"

class PointLight : public Light {

public:
    PointLight();
    PointLight(const PointLight& light);
    ~PointLight() {};

    void draw(Shader& shader) override;
    void drawLayout(Shader& shader) override;


    float calcAttenutaion(float distance);

    glm::vec3 sample(ContextSceneEmbree& context, Ray& ray, float& pdf);
    glm::vec3 getPower() override;

    glm::vec3 getDirection(glm::vec3 samplePos) override;

    glm::vec3 eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& direction, float& pdf) override;

    void generateLayoutMesh() override;


private :

    const float m_layoutBaseCircle = 0.5f;

    float computeVisibility(Ray shadowRay, ContextSceneEmbree& context) override;


};

#endif // POINTLIGHT_H
