#include "SpotLight.hpp"
#include <Engine/Rendering/Material/MaterialUtils.hpp>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>
#include <Core/Math/Utils/Utils.hpp>

SpotLight::SpotLight() {
    m_angle = M_PI_2;
    m_oAngle = M_PI / 4.f;
    m_iAngle = M_PI / 4.f;


    m_constantAttenuation = 0.f;
    m_linearAttenuation = 0.f;
    m_quadraticAttenuation = 1.f;


    m_type = LightType::SPOT;
    m_objType = MoveObject::LIGHT;
}

SpotLight::SpotLight(const SpotLight& light) : Light(light) {
    m_angle = light.m_angle;
    m_oAngle = light.m_oAngle;
    m_iAngle = light.m_iAngle;

    m_constantAttenuation = light.m_constantAttenuation;
    m_linearAttenuation = light.m_linearAttenuation;
    m_quadraticAttenuation = light.m_quadraticAttenuation;

    m_type = LightType::SPOT;
    m_objType = MoveObject::LIGHT;
}


void SpotLight::draw(Shader& shader) {
    glm::vec3 position = getPosition();
    glm::vec3 direction = getDirection();
    shader.setFloatV3("light.spotLight.position", position);
    shader.setInt("light.type", m_type);
    shader.setFloat("light.spotLight.attenuation.constant", m_constantAttenuation);
    shader.setFloat("light.spotLight.attenuation.linear", m_linearAttenuation);
    shader.setFloat("light.spotLight.attenuation.quadratic", m_quadraticAttenuation);
    shader.setFloatV3("light.color", m_color);
    shader.setFloat("light.spotLight.iAngle", m_iAngle);
    shader.setFloat("light.spotLight.oAngle", m_oAngle);
    shader.setFloatV3("light.spotLight.direction", direction);
    shader.setFloat("light.intensity", m_intensity);
}

void SpotLight::drawLayout(Shader& shader) {
    if (!m_isSelected)
        return;
    generateLayoutMesh();
    if (!m_layoutMesh)
        return;
    m_layoutMesh->draw(shader);
}

void SpotLight::generateLayoutMesh() {

    glm::vec3 position = getPosition();
    glm::vec3 direction = getDirection();

    float dist = glm::length(m_camera->getPosition() - position);
    const float scaleRadius = m_layoutBaseCircle;
    glm::vec3 camDir = glm::normalize(position - m_camera->getPosition());

    float fovY = m_camera->getFrustum().getFOV();
    float fov = fovY / 2.f * float(M_PI) / 180.f;
    float projectedRadius = 1.f / glm::tan(fov) * scaleRadius / glm::sqrt(glm::abs(dist * dist - scaleRadius*scaleRadius));
    Polyline circlePos = BaseObjects::circleLine(position, camDir, 1.f/projectedRadius, 32);
    float angle = m_oAngle * 2.f;
    float h = m_layoutConeHeight * glm::sqrt(1.f - (angle*angle)/(M_PI*M_PI));

    float circleOutRadius = angle * m_layoutConeHeight;
    float circleInRadius = m_iAngle * 2.f * m_layoutConeHeight;
    Polyline circleOut = BaseObjects::circleLine(position + h * direction, direction, circleOutRadius, 32);
    Polyline circleIn = BaseObjects::circleLine(position + h * direction, direction, circleInRadius, 32);



    Polyline lineCenter = BaseObjects::line(position, position + direction * (m_layoutConeHeight * 2.f));

    uint circleOutSize = circleOut.getVertices().size();

    float minDist = std::numeric_limits<float>::max();
    float maxDist = std::numeric_limits<float>::min();
    uint minId = 0, maxId = 0;

    for (uint i = 0; i < circleOutSize; ++i) {
        const glm::vec3& pos = circleOut.getVertices()[i].position;
        float dist = glm::length(pos - m_camera->getPosition());
        if (minDist > dist) {
            minDist = dist;
            minId = i;
        }
        if (maxDist < dist) {
            maxDist = dist;
            maxId = i;
        }
    }
    uint id1 = (minId + circleOutSize / 4) % circleOutSize;
    uint id2 = (id1 + circleOutSize/2) % circleOutSize;

    Polyline line1 = BaseObjects::line(position, circleOut.getVertices()[id1].position);
    Polyline line2 = BaseObjects::line(position, circleOut.getVertices()[id2].position);


    circlePos.setFillDrawing(false);
    if (m_layoutMesh)
        delete m_layoutMesh;
    m_layoutMesh = new ObjectLine({circlePos, circleOut, circleIn, lineCenter, line1, line2});

}


glm::vec3 SpotLight::sample(ContextSceneEmbree& context, Ray& ray, float& pdf) {
    pdf = 1.f;
    glm::vec3 position = getPosition();
    glm::vec3 direction = getDirection();

    Ray visibilityRay = Ray(ray.getOrigin(), position - ray.getOrigin());
    Intersection intersection = context.intersectRay(visibilityRay);
    float visibility = 0.f;
    float intersectionDist = glm::length(intersection.getPosition() - ray.getOrigin());
    float lightDist = glm::length(position - ray.getOrigin());
    if (!intersection.isHit() || (intersection.isHit() && (intersectionDist > lightDist)))
        visibility++;

    glm::vec3 lightDir = -glm::normalize(ray.getOrigin() - position);

    float theta = dot(lightDir, normalize(-direction));
    float intensity = 0.f;
    if (theta > m_iAngle) {
        float epsilon = m_oAngle - m_iAngle;
        intensity = glm::clamp((theta - m_iAngle) / epsilon, 0.f, 1.f);
    }
    return visibility /** intensity*/ /** computeDistAttenuation(lightDist)*/ * m_color;

    //glm::vec3 res = m_color * m_intensity;

    //return res;

}

float SpotLight::computeVisibility(Ray shadowRay, ContextSceneEmbree& context) {
    float visibility = 0.f;
    glm::vec3 position = getPosition();

    Intersection intersection = context.intersectRay(shadowRay);
    float intersectionDist = glm::length(intersection.getPosition() - shadowRay.getOrigin());
    float lightDist = glm::length(position - shadowRay.getOrigin());
    if (!intersection.isHit() || (intersection.isHit() && (intersectionDist > lightDist)))
        visibility = 1.f;

    return visibility;
}

glm::vec3 SpotLight::eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& outPdf) {
    glm::vec3 position = getPosition();
    glm::vec3 direction = getDirection();

    glm::vec3 fragToLight = normalize(position - samplePosition);

    float inAngle = glm::cos(m_iAngle);
    float outAngle = glm::cos(m_oAngle);

    float theta = glm::dot(fragToLight, normalize(-direction));
    float epsilon = inAngle-outAngle;
    float coneIntensity = glm::clamp((theta - inAngle) / epsilon, 0.f, 1.f);

    float dist = length(getPosition() - samplePosition);
    float attenuation = computeDistAttenuation(dist);

    glm::vec3 lightDir = -direction;
    outDirection = direction;
    outPdf = 1.f;

    float NdotL = glm::max(0.f, dot(lightDir, normal));

    glm::vec3 lightIntensity = attenuation * coneIntensity * m_intensity * NdotL * m_color;
    //Avoid to cast shadow ray if light not contribute
    if (MaterialUtils::Utils::colorToLuma(lightIntensity) < glm::epsilon<float>())
        return glm::vec3(0.f);

    Ray shadowRay = Ray(samplePosition, glm::normalize(position - samplePosition));
    float visibility = computeVisibility(shadowRay, context);

    glm::vec3 radiance = lightIntensity * visibility;

    return radiance;
}

glm::vec3 SpotLight::getPower() {
    return glm::vec3(0.f);
}


float SpotLight::getAngle() {
    return m_angle;
}

void SpotLight::setAngle(float nAngle) {
    m_angle = nAngle;
}


float SpotLight::getIAngle() {
    return m_iAngle;
}

void SpotLight::setIAngle(float nAngle) {
    m_iAngle = nAngle;
}

float SpotLight::getOAngle() {
    return m_oAngle;
}

void SpotLight::setOAngle(float nAngle) {
    m_oAngle = nAngle;
}

void SpotLight::setDirection(glm::vec3) {
   // m_direction = glm::normalize(nDirection);
   LOG_ERROR("SpotLight::setDirection not implemented")
}

glm::vec3 SpotLight::getDirection(glm::vec3) {

    glm::vec3 direction = glm::vec3(glm::inverse(getModel()) * glm::vec4(m_refDirection, 0.f));
    direction = glm::normalize(direction);

    return direction;
}
