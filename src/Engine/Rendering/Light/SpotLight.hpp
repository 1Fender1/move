#ifndef SPOTLIGHT_HPP
#define SPOTLIGHT_HPP

#include "Light.hpp"
#include <cmath>

class SpotLight : public Light {

public :
    SpotLight();
    SpotLight(const SpotLight& light);

    float getAngle();
    void setAngle(float nAngle);

    float getIAngle();
    void setIAngle(float nAngle);

    float getOAngle();
    void setOAngle(float nAngle);

    void draw(Shader& shader) override;
    void drawLayout(Shader& shader) override;

    void setDirection(glm::vec3 nDirection);
    glm::vec3 getDirection(glm::vec3 samplePos = glm::vec3(0.f)) override;

    glm::vec3 sample(ContextSceneEmbree& context, Ray& ray, float& pdf);
    glm::vec3 getPower();

    glm::vec3 eval(const glm::vec3& normal, const glm::vec3& samplePosition, ContextSceneEmbree& context, glm::vec3& outDirection, float& outPdf) override;
    float computeVisibility(Ray shadowRay, ContextSceneEmbree& context) override;


private :

    void generateLayoutMesh() override;
    const float m_layoutBaseCircle = 10.f;
    const float m_layoutConeHeight = 5.f;

    //Angles in radians
    float m_angle;
    float m_iAngle;
    float m_oAngle;
};



#endif
