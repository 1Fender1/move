//
// Created by jb on 02/03/18.
//

#include "Material.hpp"
#include <Core/Math/RayTracing/Intersection.hpp>
#include <random>

Material::Material(std::string id) {
    m_diffuseColor = glm::vec4(0.5, 0.5, 0.5, 1.f);
    m_emissiveColor = glm::vec3(0);
    m_specularColor = glm::vec3(0.8, 0.8, 0.8);
    specularExp = 0;
    opticDensity = 0;
    lumParam = 0;
    transparency = 1;
    m_rng = std::mt19937(m_dev());
    m_dist01 = std::uniform_real_distribution<float>(0.f, 1.f);

    m_objectName = id;
    m_objType = objectType::MATERIAL;
    m_materialType = materialType::NONE;
    m_specType = specularType::BLINNPHONG;
    initShader();
}

Material::Material() {
    m_diffuseColor = glm::vec4(0.5, 0.5, 0.5, 1.f);
    m_emissiveColor = glm::vec3(0);
    m_specularColor = glm::vec3(0.8, 0.8, 0.8);
    specularExp = 0;
    opticDensity = 0;
    lumParam = 0;
    transparency = 1;
    m_rng =  std::mt19937(m_dev());
    m_dist01 = std::uniform_real_distribution<float>(0.f, 1.f);

    m_objType = objectType::MATERIAL;
    m_materialType = materialType::NONE;
    m_objectName = "defaultMaterial";
    m_specType = specularType::BLINNPHONG;
    initShader();

}

Material::Material(const Material& mat) : MoveObject(mat) {
    m_diffuseColor = mat.m_diffuseColor;
    m_emissiveColor = mat.m_emissiveColor;
    m_specularColor = mat.m_specularColor;
    specularExp = mat.specularExp;
    opticDensity = mat.opticDensity;
    lumParam = mat.lumParam;
    transparency = mat.transparency;
    m_rng = mat.m_rng;
    m_dist01 = mat.m_dist01;
    m_objType = mat.m_objType;
    m_pdf = mat.m_pdf;
    m_roughness = mat.m_roughness;

    m_objType = mat.m_objType;

    m_materialType = mat.m_materialType;
    m_objectName = mat.m_objectName;
    m_shader = mat.m_shader;
    m_specType = mat.m_specType;

}


void Material::draw(const Shader& shader){
    shader.setBool("hasNormalMap", hasNormalMap());
    shader.setBool("hasBumpMap", hasNormalMap());
    shader.setBool("hasSpecularMap", hasSpecularMap());
    shader.setBool("hasDiffuseMap", hasDiffuseMap());
    shader.setFloatV4("material.diffuseColor", getDiffuseColor());
    shader.setFloat("material.transparency", getTransparency());
    shader.setFloatV3("material.specularColor", getSpecularColor());
    shader.setInt("material.specType", (int)m_specType);
}

const glm::vec3 &Material::getAmbianteColor() const {
    return m_ambianteColor;
}

void Material::setAmbianteColor(const glm::vec3 &ambianteColor) {
    m_ambianteColor = ambianteColor;
}

glm::vec4 &Material::getDiffuseColor() {
    return m_diffuseColor;
}

void Material::setDiffuseColor(const glm::vec4 &diffuseColor) {
    m_diffuseColor = diffuseColor;
}

glm::vec3 &Material::getSpecularColor() {
    return m_specularColor;
}

void Material::setSpecularColor(const glm::vec3 &specularColor) {
    this->m_specularColor = specularColor;
}

const glm::vec3 &Material::getEmissiveColor() const {
    return m_emissiveColor;
}

void Material::setEmissiveColor(const glm::vec3 &emissiveColor) {
    m_emissiveColor = emissiveColor;
}

float Material::getOpticDensity() const {
    return opticDensity;
}

void Material::setOpticDensity(float opticDensity) {
    this->opticDensity = opticDensity;
}

int Material::getSpecularExp() const {
    return specularExp;
}

void Material::setSpecularExp(int specularExp) {
    this->specularExp = specularExp;
}

float Material::getTransparency() const {
    return transparency;
}

void Material::setTransparency(float transparency) {
    this->transparency = transparency;
}

float Material::getRoughness() {
    return m_roughness;
}

int Material::getLumParam() const {
    return lumParam;
}

void Material::setLumParam(int lumParam) {
    this->lumParam = lumParam;
}

glm::vec3 Material::reflect(const glm::vec3& v, const glm::vec3& n) {
    return v - 2.f * glm::dot(v, n) * n;
}

glm::vec3 Material::randomInUnitSphere() {
//TODO understand why both method give point on unit sphere but resuts of lambertian are different
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<float> dist(0.f, 1.f);
    std::uniform_real_distribution<float> distU(-1.f, 1.f);

    float u = distU(rng);
    float x = dist(rng);
    float y = dist(rng);
    float z = dist(rng);

    float mag = glm::sqrt(x * x + y * y + z * z);
    x /= mag; y /= mag; z /= mag;
    float c = std::cbrt(u);
    x *= c; y *= c; z *= c;
    return glm::vec3(x, y, z);
}

glm::vec3 Material::eval(const Ray& , const Intersection& hit, glm::vec3, float& pdf) {

//    Material* mat = hit.getObjRef()->getMaterial();

    glm::vec3 color = hit.getColor();;

    pdf = 1.f;

    return color;
}

glm::vec3 Material::uniformSampleHemisphere(float r1, float r2) {
    float sinTheta = sqrtf(1 - r1 * r1);
    float phi = 2 * float(M_PI) * r2;
    float x = sinTheta * cosf(phi);
    float z = sinTheta * sinf(phi);
    return glm::vec3(x, r1, z);
}

void Material::createCoordinateSystem(const glm::vec3 &N, glm::vec3 &Nt, glm::vec3 &Nb) {
    if (std::fabs(N.x) > std::fabs(N.y))
        Nt = glm::vec3(N.z, 0.f, -N.x) / sqrtf(N.x * N.x + N.z * N.z);
    else
        Nt = glm::vec3(0.f, -N.z, N.y) / sqrtf(N.y * N.y + N.z * N.z);
    Nb = glm::cross(N, Nt);
}

glm::vec3 Material::getRandomDirection(glm::vec3 normal, glm::vec3 , glm::vec3 Nt, glm::vec3 Nb, glm::vec3, float& r1) {

    r1 = m_dist01(m_rng);
    float r2 = m_dist01(m_rng);
    glm::vec3 sample = uniformSampleHemisphere(r1, r2);
    glm::vec3 sampleWorld(sample.x * Nb.x + sample.y * normal.x + sample.z * Nt.x,
                          sample.x * Nb.y + sample.y * normal.y + sample.z * Nt.y,
                          sample.x * Nb.z + sample.y * normal.z + sample.z * Nt.z);
    sampleWorld = glm::normalize(sampleWorld);

    return sampleWorld;
}

Texture* Material::getTexture(uint i) {

    if (i >= m_textures.size())
        return nullptr;

    return m_textures[i];

}

uint Material::getTextureCount() {
    return m_textures.size();
}

void Material::addTexture(Texture* texture) {
    if (texture == nullptr)
        return;

    bool exist = false;
    for (uint i = 0; i < m_textures.size(); ++i) {
        if (m_textures[i]->getName() == texture->getName())
            exist = true;
    }

    if (!exist) {
        m_textures.push_back(texture);
        if (texture->getTextureType() == "texture_diffuse") {
            m_offsetDiffuseMap = m_textures.size()-1;
        }
        if (texture->getTextureType() == "texture_specular") {
            m_offsetSpecularMap = m_textures.size()-1;

        }
        if (texture->getTextureType() == "texture_normal") {
            m_offsetNormalMap = m_textures.size()-1;
        }
    }
}


Texture* Material::getNormalMap() {
    return m_textures[m_offsetNormalMap];
}

Texture* Material::getDiffuseMap() {
    return m_textures[m_offsetDiffuseMap];
}

Texture* Material::getSpecularMap() {
    return m_textures[m_offsetSpecularMap];
}

bool Material::hasDiffuseMap() {
    return m_offsetDiffuseMap != uint(-1);
}

bool Material::hasNormalMap() {
    bool hasNormalMap = m_offsetNormalMap != uint(-1);
    return (hasNormalMap) && (m_textures[m_offsetNormalMap]->getChannelCount() == 3);
}

bool Material::hasBumpMap() {
    bool hasNormalMap = m_offsetNormalMap != uint(-1);
    return (hasNormalMap) && (m_textures[m_offsetNormalMap]->getChannelCount() == 1);
}

bool Material::hasSpecularMap() {
    return m_offsetSpecularMap != uint(-1);
}

void Material::initShader() {
    Shader::ShaderNames files;
    std::string shaderName;

    files.vextexShader = Shader::vertexDir + "globalShading.vs";
    files.fragmentShader = Shader::fragmentDir + "blinnLighting.fs";
    shaderName = "Default shader";
    m_shader = Shader(files, shaderName);
}

void Material::reloadShader() {
    LOG("Reload shader : %n, from material : %n", m_shader.getName(), getName())
    m_shader.reload();
}

void Material::use() {
    m_shader.use();
}

Shader& Material::getShader() {
    return m_shader;
}