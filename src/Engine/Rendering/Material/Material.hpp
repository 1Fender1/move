//
// Created by jb on 02/03/18.
//

#ifndef HELLOOPENGL_MATERIAL_HPP
#define HELLOOPENGL_MATERIAL_HPP

#include <glm/glm.hpp>
#include <glm/geometric.hpp>
#include <iostream>
#include <Core/Math/RayTracing/Ray.hpp>
#include <random>
#include <Engine/Scenes/MoveObject.hpp>
#include <Engine/Rendering/Texture/Texture.hpp>
#include <Engine/Shader/Shader.hpp>
#include <Core/Types.hpp>
#include <Core/System/Logger.hpp>

//#include <Math/Intersection.hpp>
//#include <Rendering/RayTracing/ContextSceneEmbree.hpp>
class Intersection;
class ContextSceneEmbree;

class Material : public MoveObject {

public:

    enum materialType {
        NONE = 0,
        PRINCIPLED,
        LAMBERTIAN,
        MICROFACET,
        DIELECTRIC,
        METAL
    };

    enum specularType {
        VOID = 0,
        BLINNPHONG,
        GGX,
        BECKMANN
    };

    Material(std::string id);

    Material();
    Material(const Material& mat);
    Material& operator=(const Material& mat) {
        m_diffuseColor = mat.m_diffuseColor;
        m_emissiveColor = mat.m_emissiveColor;
        m_specularColor = mat.m_specularColor;
        specularExp = mat.specularExp;
        opticDensity = mat.opticDensity;
        lumParam = mat.lumParam;
        transparency = mat.transparency;
        m_rng = mat.m_rng;
        m_dist01 = mat.m_dist01;
        m_objType = mat.m_objType;
        m_pdf = mat.m_pdf;
        m_roughness = mat.m_roughness;
        m_objectName = mat.m_objectName;

        m_objType = mat.m_objType;

        m_materialType = mat.m_materialType;
        m_shader = mat.m_shader;
        m_specType = mat.m_specType;

        return *this;
    }

    virtual ~Material() {}

    virtual void draw(const Shader& shader);


    const glm::vec3 &getAmbianteColor() const;

    void setAmbianteColor(const glm::vec3 &ambianteColor);

    glm::vec4 &getDiffuseColor();

    void setDiffuseColor(const glm::vec4 &diffuseColor);

    glm::vec3 &getSpecularColor();

    void setSpecularColor(const glm::vec3 &specularColor);

    const glm::vec3 &getEmissiveColor() const;

    void setEmissiveColor(const glm::vec3 &emissiveColor);

    float getOpticDensity() const;

    void setOpticDensity(float opticDensity);

    int getSpecularExp() const;

    void setSpecularExp(int specularExp);

    float getTransparency() const;

    void setTransparency(float transparency);

    float getRoughness();

    int getLumParam() const;

    void setLumParam(int lumParam);

    glm::vec3 randomInUnitSphere();
    virtual glm::vec3 eval(const Ray& inRay, const Intersection& hit, glm::vec3 lightDir, float& pdf);
    virtual glm::vec3 getRandomDirection(glm::vec3 normal, glm::vec3 dir, glm::vec3 Nt, glm::vec3 Nb, glm::vec3, float&);
    void createCoordinateSystem(const glm::vec3 &N, glm::vec3 &Nt, glm::vec3 &Nb);
    glm::vec3 uniformSampleHemisphere(float r1, float r2);
    glm::vec3 reflect(const glm::vec3& v, const glm::vec3& n);

    materialType getMaterialType() {return m_materialType;}

    Texture* getTexture(uint i);
    uint getTextureCount();
    void addTexture(Texture*);

    Texture* getNormalMap();
    Texture* getDiffuseMap();
    Texture* getSpecularMap();

    bool hasDiffuseMap();
    bool hasNormalMap();
    bool hasBumpMap();
    bool hasSpecularMap();

    void reloadShader();
    void use();
    Shader& getShader();


    virtual float eval_BRDF_LTC(glm::vec3 , glm::vec3 , float , float &) const {
        LOG_ERROR("Base class function, does not be called");
        return 0.f;
    };

    virtual glm::vec3 sample_LTC(const glm::vec3& , float , float , float ) const {
        LOG_ERROR("Base class function, does not be called");
        return glm::vec3(0.f);
    }

    virtual float eval_fresnel_LTC(const glm::vec3&, const glm::vec3&) const {
        LOG_ERROR("Base class function, does not be called");
        return 0.f;
    }


protected:

    virtual void initShader();

    glm::vec3 m_ambianteColor;
    glm::vec4 m_diffuseColor;
    glm::vec3 m_specularColor;
    glm::vec3 m_emissiveColor;
    float m_roughness = 0.7f;
    float opticDensity;
    int specularExp;
    float transparency;
    int lumParam;
    std::random_device m_dev;
    std::mt19937 m_rng;
    std::uniform_real_distribution<float> m_dist01;

    materialType m_materialType;

    std::vector<Texture*> m_textures;

    uint m_offsetDiffuseMap = -1;
    uint m_offsetSpecularMap = -1;
    uint m_offsetNormalMap = -1;

    //is the probability density function
    //default material is a lambertian so pdf is cos(theta)/PI
    float m_pdf;

    specularType m_specType = BLINNPHONG;

    //GL shader
    Shader m_shader;

};

typedef std::unique_ptr<Material> Material_ptr;


#endif //HELLOOPENGL_MATERIAL_H
