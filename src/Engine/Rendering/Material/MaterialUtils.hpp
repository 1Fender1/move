//
// Created by jb on 08/05/2020.
//

#ifndef MOVE_MATERIALUTILS_HPP
#define MOVE_MATERIALUTILS_HPP

#include <glm/glm.hpp>


#pragma once
namespace MaterialUtils {

    namespace Utils {
        inline float IORtoF0(float ior) {
            float temp = (1.f - ior) / (1.f + ior);
            return temp * temp;
        }

        inline float f0ToIOR(float f0) {
            return (2.f / (1.f - glm::sqrt(f0))) - 1.f;
        }

        //https://fr.wikipedia.org/wiki/Luminance || UIT-R BT 709
        inline float colorToLuma(glm::vec3 color) {

            return 0.2126f * color.r + 0.7152f * color.g + 0.0722f * color.b;

        }

    }


    namespace Diffuse {

        //TODO evaluate currently used mapping in production
        inline float diffuseRoughnessMapping(float diffuseRoughness) {
            return diffuseRoughness;
        }


        inline glm::vec4 computeLambertian(glm::vec3 normal, glm::vec3 lightDir, glm::vec4 baseColor, float metalness = 0.f) {
            glm::vec4 diffuseColor = (1.f - metalness) * baseColor;
            return diffuseColor * glm::clamp(glm::dot(normal, lightDir), 0.f, 1.f) / float(M_PI);
        }

        inline glm::vec4 computeOrenNayar(glm::vec3 normal, glm::vec3 lightDir, glm::vec3 camDir, glm::vec4 baseColor, float diffuseRoughness = 0.f, float metalness = 0.f) {
            float alpha = MaterialUtils::Diffuse::diffuseRoughnessMapping(diffuseRoughness);
            float alpha2 = alpha * alpha;

            float A = 1.f - (alpha2/(2.f*(alpha2 + 0.33f)));
            float B = (0.45f * alpha2) / (alpha2 + 0.09f);

            float LdotV = glm::dot(lightDir, camDir);
            float NdotL = glm::dot(lightDir, normal);
            float NdotV = glm::dot(normal, camDir);

            float s = LdotV - NdotL * NdotV;
            float t = glm::mix(1.f, glm::max(NdotL, NdotV), glm::step(0.f, s));

            glm::vec4 diffuseColor = (1.f - metalness) * baseColor;

            return diffuseColor * glm::max(0.f, NdotL) * (A + B * s / t) / float(M_PI);
        }

        //Default diffuse is orenNayar, if no diffuse roughness is set, result is a lambertian
        inline glm::vec4 computeDiffuse(glm::vec3 normal, glm::vec3 lightDir, glm::vec3 camDir, glm::vec4 baseColor, float diffuseRoughness = 0.f, float metalness = 0.f) {
            return computeOrenNayar(normal, lightDir, camDir, baseColor, diffuseRoughness, metalness);
        }



    }

    namespace Specular {

        inline float roughnessMapping(float roughness) {
            return roughness * roughness;
        }


        inline glm::vec4 computeBlinnPhong(glm::vec3 normal, glm::vec3 lightDir, glm::vec3 camDir, glm::vec3 specularColor, float roughness) {

            glm::vec3 reflectDir = reflect(-lightDir, normal);
            float spec = glm::pow(glm::max(glm::dot(camDir, reflectDir), 0.f), 32);

            return glm::vec4(spec * (1.f - roughness) * glm::vec4(specularColor, 1.f));
        }

        //http://graphicrants.blogspot.com/2013/08/specular-brdf-reference.html && https://learnopengl.com/PBR/Theory

        //Normal distrubution function
        inline float DGGX(float NdotH, float a) {
            float a2 = a*a;
            float NdotH2 = NdotH*NdotH;

//            float nom = a2;
            float temp = NdotH2 * (a2 - 1.f) + 1.f;
            temp = M_PI * temp * temp;

            return a2 / temp;
        }

        //Geometric function
        inline float GeometrySchlickGGX(float NdotX, float k) {
            return NdotX / (NdotX * (1.f - k) + k);
        }

        inline float GeometrySmith(float NdotV, float NdotL, float k) {
            float ggx1 = GeometrySchlickGGX(NdotV, k);
            float ggx2 = GeometrySchlickGGX(NdotL, k);

            return ggx1 * ggx2;
        }

        inline float GeometryGGX(float NdotV, float NdotL, float k) {
            float G_V = NdotV + glm::sqrt((NdotV - NdotV * k) * NdotV + k);
            float G_L = NdotL + glm::sqrt((NdotL - NdotL * k) * NdotL + k);
            return G_V * G_L;
        }

        //Fresnel function, is Schick approximation
        inline glm::vec3 fresnel(float HdotV, glm::vec3 f0) {
            return f0 + (1.f - f0) * glm::pow(1.f - HdotV, 5.f);
        }


        inline glm::vec4 computeGGX(glm::vec3 normal, glm::vec3 lightDir, glm::vec3 camDir, glm::vec4 diffuseColor, float metalness, float roughnessT, float ior, float& pdf) {

            float roughness = roughnessMapping(roughnessT);

            glm::vec3 h = normalize(camDir + lightDir);
            float NdotH = glm::max(0.f, glm::dot(normal, h));
            float VdotH = glm::max(0.f, glm::dot(camDir, h));
            float NdotV = glm::max(0.f, glm::dot(normal, camDir));
            float NdotL = glm::max(0.f, glm::dot(normal, lightDir));

            glm::vec3 f0;
            if (metalness > 0.001f) {
                glm::vec4 colorTmpRfl = diffuseColor * metalness;
                glm::vec3 colorTmp = glm::vec3(diffuseColor.x, diffuseColor.y, diffuseColor.z);
                glm::vec3 reflectance = glm::vec3(colorTmpRfl.x, colorTmpRfl.y, colorTmpRfl.z);
                f0 = 0.16f * reflectance * reflectance * (1.f - metalness) + colorTmp * metalness;
            } else {
                f0 = glm::vec3(MaterialUtils::Utils::IORtoF0(ior));
            }


            float D = DGGX(NdotH, roughness);
            float G = /*GeometrySmith*/GeometryGGX(NdotV, NdotL, roughness/2.f);
            glm::vec3 F = fresnel(VdotH, f0);

            glm::vec3 dfg = D * F * G;
            //pdf = glm::abs(4.f * NdotV * NdotL);
            pdf = D/(4.f * NdotV);


            if (4.f * NdotV * NdotL < 0.f) {
                float a = 0.f;
                a++;
            }

            glm::vec3 energieCompensation = glm::vec3(1.f);// 1.f + f0 * (1.f / dfg.y - 1.f);

            glm::vec4 result = glm::vec4(dfg * energieCompensation, 1.f);

            return result;

        }


    }

}



#endif //MOVE_MATERIALUTILS_HPP
