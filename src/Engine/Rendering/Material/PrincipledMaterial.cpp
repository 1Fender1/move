//
// Created by jb on 27/04/2020.
//
#include <Core/Math/RayTracing/Intersection.hpp>

#include "PrincipledMaterial.hpp"
#include <Engine/Rendering/Material/MaterialUtils.hpp>
#include <Core/System/Logger.hpp>

PrincipledMaterial::PrincipledMaterial(String name) : Material(name) {

    m_diffuseColor = glm::vec4(1.f);
    m_diffuseRoughness = 1.f;
    m_metalness = 0.f;
    m_ior = 1.5f;

    m_emissiveColor = glm::vec3(0.f);
    m_ambianteColor = glm::vec3(0.f);
    m_specularColor = glm::vec3(0.f);

    m_materialType = materialType::PRINCIPLED;
    m_specType = specularType::GGX;
    initShader();
}

PrincipledMaterial::PrincipledMaterial(const PrincipledMaterial& mat) : Material(mat) {
    m_metalness = mat.m_metalness;
    m_roughness = mat.m_roughness;
    m_diffuseRoughness = mat.m_diffuseRoughness;
    m_ior = mat.m_ior;
    m_specType = specularType::GGX;
}

void PrincipledMaterial::draw(const Shader& shader) {

    shader.setBool("hasNormalMap", hasNormalMap());
    shader.setBool("hasBumpMap", hasNormalMap());
    shader.setBool("hasSpecularMap", hasSpecularMap());
    shader.setBool("hasDiffuseMap", hasDiffuseMap());
    shader.setFloatV4("material.diffuseColor", getDiffuseColor());
    shader.setFloat("material.transparency", getTransparency());
    shader.setFloatV3("material.specularColor", getSpecularColor());
    shader.setFloatV3("material.emissiveColor", getEmissiveColor());
    shader.setFloat("material.roughness", m_roughness);
    shader.setFloat("material.diffuseRoughness", m_diffuseRoughness);
    shader.setFloat("material.metalness", m_metalness);
    shader.setFloat("material.ior", m_ior);
    shader.setInt("material.specType", (int)m_specType);

}

void PrincipledMaterial::initShader() {
        Shader::ShaderNames files;
        std::string shaderName;

        files.vextexShader = Shader::vertexDir + "globalShading.vs";
        files.fragmentShader = Shader::fragmentDir + "principledMaterial.fs";
        shaderName = "Default shader";
        m_shader = Shader(files, shaderName);
}


glm::vec3 PrincipledMaterial::eval(const Ray& inRay, const Intersection& hit, glm::vec3 lightDir, float& pdf) {
    glm::vec3 normal = hit.getNormal();
    glm::vec3 v = -glm::normalize(inRay.getDirection());
    glm::vec3 l = -glm::normalize(lightDir);

    glm::vec4 baseColor = glm::vec4(hit.getColor(), 1.f);
    float roughness = hit.getSpecularity();
    glm::vec4 diffuse = MaterialUtils::Diffuse::computeDiffuse(normal, l, v, baseColor, m_diffuseRoughness, m_metalness);
    glm::vec4 specular = MaterialUtils::Specular::computeGGX(normal, l, v, baseColor, m_metalness, roughness, m_ior, pdf);

    glm::vec4 resTemp = diffuse + specular;
    resTemp.a = 1.f;

    return glm::vec3(resTemp.r, resTemp.g, resTemp.b);
}

float roughnessToAlpha(float roughness) {

    //return 2.f/(roughness*roughness)-2.f;
    //return roughness*0.7f;
   // return roughness;
   return roughness * roughness;

}


//TODO put on a math lib
void makeOrthonormals(const glm::vec3 N, glm::vec3 &a, glm::vec3 &b) {
    if (N.x != N.y || N.x != N.z)
        a = glm::vec3(N.z - N.y, N.x - N.z, N.y - N.x);  //(1,1,1)x N
    else
        a = glm::vec3(N.z - N.y, N.x + N.z, -N.y - N.x);  //(-1,1,1)x N

    a = glm::normalize(a);
    b = glm::cross(N, a);
}


glm::vec3 PrincipledMaterial::getRandomDirection(glm::vec3 normal, glm::vec3 dir, glm::vec3 , glm::vec3 , glm::vec3, float& ) {


   // return glm::reflect(-dir, normal);

    float U1 = m_dist01(m_rng);
    U1 = (U1 == 1.f) ? U1 - 0.00001f : U1;
    float U2 = m_dist01(m_rng);
    U2 = (U2 == 1.f) ? U2 - 0.00001f : U2;

    float alphaX = MaterialUtils::Specular::roughnessMapping(m_roughness);
    float alphaY = MaterialUtils::Specular::roughnessMapping(m_roughness);//Isotropic case only for the moment

    glm::vec3 X, Y, Z = normal;
    makeOrthonormals(Z, X, Y);

    glm::vec3 V_ = glm::vec3(glm::dot(X, dir), glm::dot(Y, dir), glm::dot(normal, dir));

    // stretch view
    glm::vec3 V = glm::normalize(glm::vec3(alphaX * V_.x, alphaY * V_.y, V_.z));

    // orthonormal basis
    glm::vec3 T1 = (V.z < 0.9999f) ? glm::normalize(glm::cross(V, glm::vec3(0.f,0.f,1.f))) : glm::vec3(1.f,0.f,0.f);
    glm::vec3 T2 = glm::cross(T1, V);

    // sample point with polar coordinates (r, phi)
    float a = 1.f / (1.f + V.z);
    float r = glm::sqrt(U1);
    float phi = (U2<a) ? U2/a * float(M_PI) : float(M_PI) + (U2-a)/(1.f-a) * float(M_PI);
    float P1 = r*glm::cos(phi);
    float P2 = r*glm::sin(phi)*((U2<a) ? 1.f : V.z);

    // compute normal
    glm::vec3 N = P1*T1 + P2*T2 + glm::sqrt(glm::max(0.f, 1.f - P1*P1 - P2*P2)) * V;

    // unstretch
    N = glm::normalize(glm::vec3(alphaX*N.x, alphaY*N.y, glm::max(0.f, N.z)));

    glm::vec3 m = glm::normalize(X*N.x + Y*N.y + Z*N.z);

    glm::vec3 outDir = 2.f * dot(m, dir) * m - dir;

    if (glm::any(glm::isnan(outDir)))
        LOG_ERROR("Error nan values")

    return outDir;
}


float PrincipledMaterial::eval_BRDF_LTC(glm::vec3 V, glm::vec3 L, float alpha, float &pdf) const {

    if(V.z <= 0.f) {
        pdf = 0.f;
        return 0.f;
    }

    // masking
    const float a_V = 1.f / alpha / tanf(acosf(V.z));
    const float LambdaV = (V.z < 1.f) ? 0.5f * (-1.f + sqrtf(1.f + 1.f/a_V/a_V)) : 0.f;
//    const float G1 = 1.f / (1.f + LambdaV);

    // shadowing
    float G2;
    if(L.z <= 0.f) {
        G2 = 0.f;
    } else {
        const float a_L = 1.f / alpha / tanf(acosf(L.z));
        const float LambdaL = (L.z < 1.f) ? 0.5f * (-1.f + sqrtf(1.f + 1.f/a_L/a_L)) : 0.f;
        G2 = 1.f / (1.f + LambdaV + LambdaL);
    }

    // D
    const glm::vec3 H = glm::normalize(V+L);
    const float slopex = H.x/H.z;
    const float slopey = H.y/H.z;
    float D = 1.f / (1.f + (slopex*slopex+slopey*slopey)/alpha/alpha);
    D = D * D;
    D = D / (float(M_PI) * alpha * alpha * H.z*H.z*H.z*H.z);

    pdf = fabsf(D * H.z / 4.0f / glm::dot(V,H));
    float res = D * G2 / 4.0f / V.z;

    return res;
/*
    glm::vec3 normal = glm::vec3(0.f, 0.f, 1.f);
    float roughnessT = m_roughness;

    float roughness = MaterialUtils::Specular::roughnessMapping(roughnessT);

    glm::vec3 h = normalize(V + L);
    float NdotH = glm::max(0.f, glm::dot(normal, h));
    float VdotH = glm::max(0.f, glm::dot(V, h));
    float NdotV = glm::max(0.f, glm::dot(normal, V));
    float NdotL = glm::max(0.f, glm::dot(normal, L));

    float D = MaterialUtils::Specular::DGGX(NdotH, roughness);
    float G = MaterialUtils::Specular::GeometrySmith(NdotV, NdotL, roughness/2.f);


    pdf = glm::abs(D * h.z / 4.f / glm::dot(V,h));
    //pdf = 1.f;
    float dg = D * G / 4.0f / V.z;
    //float dg = D * G;

  //  glm::vec3 energieCompensation = glm::vec3(1.f);// 1.f + f0 * (1.f / dfg.y - 1.f);

    return dg;
*/
}

glm::vec3 PrincipledMaterial::sample_LTC(const glm::vec3& V, float alpha, float U1, float U2) const {
    const float phi = 2.f* float(M_PI) * U1;
    const float r = alpha*sqrtf(U2/(1.0f-U2));
    const glm::vec3 N = glm::normalize(glm::vec3(r*cosf(phi), r*sinf(phi), 1.f));
    const glm::vec3 L = -V + 2.f * N * glm::dot(N, V);
    return L;
}

float PrincipledMaterial::eval_fresnel_LTC(const glm::vec3& V, const glm::vec3& L) const {

    glm::vec3 H = glm::normalize(L+V);
    float HdotV = glm::dot(H, V);

    return pow(1.f - HdotV, 5.f);
}



void PrincipledMaterial::setRoughness(float roughness) {
    if (roughness < glm::epsilon<float>())
        roughness = 0.01f;
    if (roughness > 1.f-glm::epsilon<float>())
        roughness = 0.99f;
    m_roughness = roughness;
}

void PrincipledMaterial::setDiffuseRoughness(float diffuseRoughness) {
    m_diffuseRoughness = diffuseRoughness;
}
void PrincipledMaterial::setMetalness(float metalness) {
    m_metalness = metalness;
}
void PrincipledMaterial::setIOR(float ior) {
    m_ior = ior;
}

float PrincipledMaterial::getRoughness() {
    return m_roughness;
}

float PrincipledMaterial::getDiffuseRoughness() {
    return m_diffuseRoughness;
}
float PrincipledMaterial::getMetalness() {
    return m_metalness;
}
float PrincipledMaterial::getIOR() {
    return m_ior;
}