//
// Created by jb on 27/04/2020.
//

#ifndef MOVE_PRINCIPLEDMATERIAL_HPP
#define MOVE_PRINCIPLEDMATERIAL_HPP

#include <Engine/Rendering/Material/Material.hpp>

class PrincipledMaterial : public Material {

public :
    PrincipledMaterial(String name = "defaultMaterial");
    PrincipledMaterial(const PrincipledMaterial& mat);
    PrincipledMaterial& operator=(const PrincipledMaterial& mat) {

        if (&mat == this) {
            return *this;
        }

        Material::operator=(mat);
        m_metalness = mat.m_metalness;
        m_roughness = mat.m_roughness;
        m_diffuseRoughness = mat.m_diffuseRoughness;
        m_ior = mat.m_ior;

        return *this;
    }

    void draw(const Shader& shader) override;

    glm::vec3 eval(const Ray& inRay, const Intersection& hit, glm::vec3 lightDir, float& pdf);
    glm::vec3 getRandomDirection(glm::vec3 normal, glm::vec3 dir, glm::vec3 Nt, glm::vec3 Nb, glm::vec3, float&);

    void setRoughness(float);
    void setDiffuseRoughness(float);
    void setMetalness(float);
    void setIOR(float);

    float getRoughness();
    float getDiffuseRoughness();
    float getMetalness();
    float getIOR();

    float eval_BRDF_LTC(glm::vec3 V, glm::vec3 L, float alpha, float &pdf) const override;

    glm::vec3 sample_LTC(const glm::vec3& V, float alpha, float U1, float U2) const override;

    float eval_fresnel_LTC(const glm::vec3& V, const glm::vec3& L) const override;

private:

    void initShader() override;


    float m_diffuseRoughness = 0.f;
    float m_metalness = 0.f;
    float m_ior = 0.f;


};


#endif //MOVE_PRINCIPLEDMATERIAL_HPP
