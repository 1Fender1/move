//
// Created by jb on 07/04/19.
//

#include "ContextSceneEmbree.hpp"


ContextSceneEmbree::ContextSceneEmbree() {
    std::string config = "hugepages=1";
     //       "threads=0, isa=sse4.2, set_affinity=1, hugepages=1, frequency_level=simd128";
    m_device = rtcNewDevice(config.c_str());
    m_mainScene = rtcNewScene(m_device);
    rtcSetSceneBuildQuality(m_mainScene, RTC_BUILD_QUALITY_REFIT);
    rtcSetSceneFlags(m_mainScene, RTC_SCENE_FLAG_DYNAMIC);

    m_objCount = 0;
}

ContextSceneEmbree::~ContextSceneEmbree() {

    m_embreeObjects.clear();

    rtcReleaseDevice(m_device);
    rtcReleaseScene(m_mainScene);
}

void ContextSceneEmbree::addMesh(Mesh* mesh) {
    RTCGeometry embMesh = rtcNewGeometry(m_device, RTC_GEOMETRY_TYPE_TRIANGLE);

    m_embreeObjects[mesh->getID()] = std::make_unique<EmbreeObject>(m_device, m_mainScene, embMesh, mesh);

    m_objCount++;
    rtcCommitScene(m_mainScene);
}

void ContextSceneEmbree::removeObj(Mesh* meshToRemove) {

    m_embreeObjects.erase(meshToRemove->getID());

}

void ContextSceneEmbree::remapReferences(MoveObject* obj) {

    for (uint i = 0; i < obj->getChildCount(); ++i) {
        MoveObject* child = obj->getChild(i);
        if (m_embreeObjects.find(child->getID()) != m_embreeObjects.end()) {
            m_embreeObjects[child->getID()]->updateMeshPtr((Mesh*)child);
        }
    }

}

void ContextSceneEmbree::getTransformedHitData(const glm::mat4& model, const Vertex& v1, const Vertex& v2, const Vertex& v3, float u, float v, Vertex& vOut) {

    float w = 1.f - u - v;
    glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(model)));

    glm::vec3 position = w * v1.position + u * v2.position + v * v3.position;
    glm::vec3 normal = w * v1.normal + u * v2.normal + v * v3.normal;
    glm::vec2 texCoord = w * v1.texCoords + u * v2.texCoords + v * v3.texCoords;
    glm::vec3 tangent = w * v1.tangent + u * v2.tangent + v * v3.tangent;
    glm::vec3 bitangent = w * v1.bitangent + u * v2.bitangent + v * v3.bitangent;

    position = glm::vec3(model * glm::vec4(position, 1.f));
    normal = glm::normalize(normalMatrix * normal);
    tangent = glm::normalize(normalMatrix * tangent);
    bitangent = glm::normalize(normalMatrix * bitangent);

    vOut.position = position;
    vOut.normal = normal;
    vOut.texCoords = texCoord;
    vOut.tangent = tangent;
    vOut.bitangent = bitangent;
}

Intersection ContextSceneEmbree::intersectRay(const Ray& ray) {
    RTCIntersectContext context;
    rtcInitIntersectContext(&context);

    RTCRayHit embreeRay;
    embreeRay.ray.dir_x = ray.getDirection().x;
    embreeRay.ray.dir_y = ray.getDirection().y;
    embreeRay.ray.dir_z = ray.getDirection().z;
    embreeRay.ray.org_x = ray.getOrigin().x;
    embreeRay.ray.org_y = ray.getOrigin().y;
    embreeRay.ray.org_z = ray.getOrigin().z;
    embreeRay.ray.mask = -1;
    embreeRay.ray.tnear = 0.001f;
    embreeRay.ray.tfar = 1000000.f;
    embreeRay.ray.time = 0.f;
    embreeRay.hit.geomID = RTC_INVALID_GEOMETRY_ID;
    embreeRay.hit.primID = RTC_INVALID_GEOMETRY_ID;
    embreeRay.hit.instID[0] = RTC_INVALID_GEOMETRY_ID;

    rtcIntersect1(m_mainScene, &context, &embreeRay);

    if(embreeRay.hit.geomID == RTC_INVALID_GEOMETRY_ID)
        return Intersection();

    float u = embreeRay.hit.u;
    float v = embreeRay.hit.v;
//    float t = embreeRay.ray.tfar;
    EmbreeObject* obj = getEmbreeObject(embreeRay.hit.geomID);
    Vertex v1, v2, v3;
    if (obj)
        obj->getMesh()->getTriangle(embreeRay.hit.primID, v1, v2, v3);
    else
        return Intersection();

    glm::mat4 model = obj->getMesh()->getModel();

    Vertex vHit;
    getTransformedHitData(model, v1, v2, v3, u, v, vHit);
    return Intersection(obj->getMesh(), vHit.position, vHit.normal, vHit.texCoords, vHit.tangent, vHit.bitangent);
}

void ContextSceneEmbree::intersectRayStream(const Ray* rays, uint nbRays, Intersection* intersections, uint flag = uint(RTC_INTERSECT_CONTEXT_FLAG_INCOHERENT)) {
    RTCRayHit* rays_aligned = static_cast<RTCRayHit*>(aligned_alloc(16, sizeof(RTCRayHit) * nbRays));

    RTCIntersectContext context;
    context.flags = RTCIntersectContextFlags(flag);
    rtcInitIntersectContext(&context);

    for (uint i = 0; i < nbRays; ++i) {
        rays_aligned[i].ray.dir_x = rays[i].getDirection().x;
        rays_aligned[i].ray.dir_y = rays[i].getDirection().y;
        rays_aligned[i].ray.dir_z = rays[i].getDirection().z;
        rays_aligned[i].ray.org_x = rays[i].getOrigin().x;
        rays_aligned[i].ray.org_y = rays[i].getOrigin().y;
        rays_aligned[i].ray.org_z = rays[i].getOrigin().z;
        rays_aligned[i].ray.mask = -1;
        rays_aligned[i].ray.tnear = 0.001f;
        rays_aligned[i].ray.tfar = 1000000.f;
        rays_aligned[i].ray.time = 0.f;
        rays_aligned[i].hit.geomID = RTC_INVALID_GEOMETRY_ID;
        rays_aligned[i].hit.primID = RTC_INVALID_GEOMETRY_ID;
        rays_aligned[i].hit.instID[0] = RTC_INVALID_GEOMETRY_ID;
    }

    rtcIntersect1M(m_mainScene, &context, (RTCRayHit*)&rays_aligned[0], nbRays, sizeof(RTCRayHit));
    //rtcIntersectNM(m_mainScene, &context, (RTCRayHit*)&rays_aligned[0], 64, nbRays, sizeof(RTCRayHit));

    for (uint i = 0; i < nbRays; ++i) {

        if (rays_aligned[i].hit.geomID == RTC_INVALID_GEOMETRY_ID) {
            intersections[i] = Intersection();
            continue;
        }

        float u = rays_aligned[i].hit.u;
        float v = rays_aligned[i].hit.v;
        //float t = rays_aligned[i].ray.tfar;
        EmbreeObject* obj = getEmbreeObject(rays_aligned[i].hit.geomID);
        Vertex v1, v2, v3;
        if (obj)
            obj->getMesh()->getTriangle(rays_aligned[i].hit.primID, v1, v2, v3);

        glm::mat4 model = obj ? obj->getMesh()->getModel() : glm::mat4(1.f);

        Vertex vHit;
        getTransformedHitData(model, v1, v2, v3, u, v, vHit);
        if (!obj) vHit.position = glm::vec3(infinity);
        intersections[i] = obj ? Intersection(obj->getMesh(), vHit.position, vHit.normal, vHit.texCoords, vHit.tangent, vHit.bitangent) : Intersection();
    }
    free(rays_aligned);
}

void ContextSceneEmbree::intersectRayStream(const std::vector<Ray>& rays, std::vector<Intersection>& intersections, uint flag = uint(RTC_INTERSECT_CONTEXT_FLAG_INCOHERENT)) {
    intersectRayStream(&rays[0], rays.size(), &intersections[0], flag);
}

std::vector<bool> ContextSceneEmbree::intersectRayOccludedStream(const std::vector<Ray>& ) {
    //RTCRay* rays_aligned = static_cast<RTCRay*>(aligned_alloc(16, sizeof(RTCRay) *rays.size()));
    LOG_ERROR("intersectRayOccludedStream not implemented")
    return std::vector<bool>();
}

bool ContextSceneEmbree::intersectRayOccluded(const Ray& ray) {
    RTCIntersectContext context;
    rtcInitIntersectContext(&context);
    RTCRay embreeRay;
    embreeRay.dir_x = ray.getDirection().x;
    embreeRay.dir_y = ray.getDirection().y;
    embreeRay.dir_z = ray.getDirection().z;
    embreeRay.org_x = ray.getOrigin().x;
    embreeRay.org_y = ray.getOrigin().y;
    embreeRay.org_z = ray.getOrigin().z;
    embreeRay.mask = -1;
    embreeRay.tnear = 0.001f;
    embreeRay.tfar = 0.1000000f;
    embreeRay.time = 0.f;
    embreeRay.id = RTC_INVALID_GEOMETRY_ID;
    rtcOccluded1(m_mainScene, &context, &embreeRay);

    return (embreeRay.tfar > 0.f);

}

void ContextSceneEmbree::updateContext() {
    for (auto it = m_embreeObjects.begin(); it != m_embreeObjects.end(); ++it) {
        EmbreeObject* obj = it->second.get();
        Mesh* mesh = obj->getMesh();
        if (mesh->isDirty()) {
            glm::mat4 model = mesh->getModel();
            float* model_ptr = &model[0][0];
            rtcSetGeometryTransform(obj->getEmbreeGeomId(), 0, RTC_FORMAT_FLOAT4X4_COLUMN_MAJOR, model_ptr);
            rtcCommitGeometry(obj->getEmbreeGeomId());
            m_isUpdatedContext = true;
        }
    }

    if (m_isUpdatedContext)
        rtcCommitScene(m_mainScene);

}


bool ContextSceneEmbree::handleUpdate() {
    bool needUpdate = m_isUpdatedContext;
    m_isUpdatedContext = false;
    return needUpdate;
}

EmbreeObject* ContextSceneEmbree::getEmbreeObject(uint geomID) {
    if (geomID == RTC_INVALID_GEOMETRY_ID)
        return nullptr;

    if (m_embreeObjects.find(geomID) != m_embreeObjects.end()) {
        return m_embreeObjects[geomID].get();
    }

    return nullptr;
}