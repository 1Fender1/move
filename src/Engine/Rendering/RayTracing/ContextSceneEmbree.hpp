//
// Created by jb on 07/04/19.
//

#ifndef MOVE_CONTEXTSCENEEMBREE_HPP
#define MOVE_CONTEXTSCENEEMBREE_HPP

#include <embree3/rtcore.h>
#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Rendering/RayTracing/EmbreeObject.hpp>
#include <map>
#include <Core/Math/RayTracing/Intersection.hpp>
#include <Core/Math/RayTracing/Ray.hpp>

class ContextSceneEmbree {

public :
    ContextSceneEmbree();
    ~ContextSceneEmbree();
    //void buildScene(RenderData& scene);
    void updateScene() {}
    Intersection intersectRay(const Ray& ray);
    bool intersectRayOccluded(const Ray& ray);

    void intersectRayStream(const std::vector<Ray>& ray, std::vector<Intersection>& intersections, uint flag);
    void intersectRayStream(const Ray* rays, uint nbRays, Intersection* intersections, uint flag);
    std::vector<bool> intersectRayOccludedStream(const std::vector<Ray>& ray);

    static const uint NO_INTERSECTION = RTC_INVALID_GEOMETRY_ID;

    void addMesh(Mesh* mesh);

    void updateContext();
    bool handleUpdate();

    void removeObj(Mesh* mesh);
    void remapReferences(MoveObject* obj);

private :

    EmbreeObject* getEmbreeObject(uint geomID);

    void getTransformedHitData(const glm::mat4& model, const Vertex& v1, const Vertex& v2, const Vertex& v3, float u, float v, Vertex& vOut);

    RTCScene m_mainScene;
    RTCDevice m_device;
    uint m_objCount = 0;
    std::map<uint, EmbreeObject_ptr> m_embreeObjects;
    bool m_isUpdatedContext = false;


};


#endif //MOVE_CONTEXTSCENEEMBREE_HPP
