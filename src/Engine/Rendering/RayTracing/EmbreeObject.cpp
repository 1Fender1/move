//
// Created by jb on 10/04/19.
//

#include "EmbreeObject.hpp"

EmbreeObject::EmbreeObject(RTCDevice device, RTCScene mainScene, RTCGeometry  geometry, Mesh* mesh) {
    m_scene = rtcNewScene(device);
    m_geomID = mesh->getID();
    m_mesh = mesh;
    m_geometry = geometry;
    m_mainScene = mainScene;

    m_instID = rtcNewGeometry(device, RTC_GEOMETRY_TYPE_INSTANCE);

    m_vertices = (EmbreeObject::EmbreeVertex*)rtcSetNewGeometryBuffer(m_geometry, RTC_BUFFER_TYPE_VERTEX, 0, RTC_FORMAT_FLOAT3, sizeof(EmbreeObject::EmbreeVertex), mesh->getVertices().size());
    m_triangles = (EmbreeObject::EmbreeTriangle*)rtcSetNewGeometryBuffer(m_geometry, RTC_BUFFER_TYPE_INDEX, 0, RTC_FORMAT_UINT3, sizeof(EmbreeObject::EmbreeTriangle), mesh->getIndices().size()/3);
    if (!m_vertices || !m_triangles) {
        LOG_ERROR("Bad allocation for embree geometry")
        return;
    }

    for (uint i = 0; i < mesh->getVertices().size(); i++) {
        const Vertex& vTemp = mesh->getVertices()[i];
        const glm::vec3& pos = vTemp.position;
        m_vertices[i].x = pos.x;
        m_vertices[i].y = pos.y;
        m_vertices[i].z = pos.z;
    }
    uint cpt = 0;
    for (uint i = 0; i < mesh->getIndices().size(); i+=3) {
        m_triangles[cpt].v1 = mesh->getIndices()[i];
        m_triangles[cpt].v2 = mesh->getIndices()[i+1];
        m_triangles[cpt].v3 = mesh->getIndices()[i+2];
        cpt++;
    }
    rtcCommitGeometry(m_geometry);
    rtcAttachGeometryByID(m_scene, m_geometry, m_geomID);
    rtcCommitScene(m_scene);
   // rtcReleaseGeometry(geometry);

    rtcSetGeometryInstancedScene(m_instID, m_scene);
    rtcSetGeometryTimeStepCount(m_instID, 1);

   // mesh->getModel() *= 2.f;
    rtcSetGeometryTransform(m_instID, 0, RTC_FORMAT_FLOAT4X4_COLUMN_MAJOR, &mesh->getModel()[0][0]);

    rtcCommitGeometry(m_instID);
    rtcAttachGeometryByID(mainScene, m_instID, m_geomID+0);

    rtcReleaseGeometry(m_instID);
    rtcReleaseGeometry(m_geometry);
    rtcReleaseScene(m_scene);

}

EmbreeObject::~EmbreeObject() {
    rtcDetachGeometry(m_scene, m_geomID);
    rtcDetachGeometry(m_mainScene, m_geomID);

    rtcCommitScene(m_mainScene);
}


RTCScene EmbreeObject::getScene() const {
    return m_scene;
}

uint EmbreeObject::getGeomId() const {
    return m_geomID;
}

Mesh *EmbreeObject::getMesh() const {
    return m_mesh;
}

void EmbreeObject::updateMeshPtr(Mesh* mesh) {
    m_mesh = mesh;
}

RTCGeometry EmbreeObject::getEmbreeGeomId() const {
    //The reason I return the instID is that all object render in the scene are instances
    //Because I want to be able to transform (afine) objects without destroy and re-commit geometry
    return m_instID;
}
