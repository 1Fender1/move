//
// Created by jb on 10/04/19.
//

#ifndef MOVE_EMBREEOBJECT_HPP
#define MOVE_EMBREEOBJECT_HPP

#include <embree3/rtcore.h>
#include <Engine/Modeling/Mesh/Mesh.hpp>

class EmbreeObject {

public:

    static const uint embreeInstanceOffset = 1000000000;

    struct EmbreeVertex {
        float x;
        float y;
        float z;
    };

    struct EmbreeTriangle {
        uint v1;
        uint v2;
        uint v3;
    };

    EmbreeObject(RTCDevice device, RTCScene mainScene, RTCGeometry  geometry, Mesh* mesh);
    EmbreeObject() {}
    ~EmbreeObject();

    RTCScene getScene() const;

    uint getGeomId() const;

    RTCGeometry getEmbreeGeomId() const;

    Mesh *getMesh() const;
    void updateMeshPtr(Mesh* mesh);


private :
    RTCScene m_scene;
    RTCScene m_mainScene;
    RTCGeometry  m_geometry;
    RTCGeometry m_instID;
    uint m_geomID;
    Mesh* m_mesh;
    EmbreeVertex* m_vertices;
    EmbreeTriangle* m_triangles;

};

typedef std::unique_ptr<EmbreeObject> EmbreeObject_ptr;

#endif //MOVE_EMBREEOBJECT_HPP
