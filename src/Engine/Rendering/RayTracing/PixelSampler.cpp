//
// Created by jb on 15/09/2019.
//

#include <Core/System/Logger.hpp>
#include "PixelSampler.hpp"
PixelSampler::PixelSampler(PixelSampler::SamplingType type, uint samplesPerPixel, std::mt19937* mersenne) {
    m_samplesPerPixel = samplesPerPixel;
    m_type = type;
    m_sampleNumber = 0;
    m_rng = mersenne;
    m_samples.clear();

    if (!mersenne)
        return;

    initializeSamples();
}

PixelSampler::PixelSampler(const PixelSampler& ps) {
    m_samplesPerPixel = ps.m_samplesPerPixel;
    m_type = ps.m_type;
    m_sampleNumber = 0;
    m_rng = ps.m_rng;
    m_samples.clear();

    if (!m_rng)
        return;

    initializeSamples();

}


/*
PixelSampler::PixelSampler() {
    m_samplesPerPixel = 8;
    m_type = SamplingType::Stratified;
    m_sampleNumber = 0;
    m_rng = nullptr;
    m_samples.clear();

    initializeSamples();
}
 */
/*
PixelSampler PixelSampler::operator=(const PixelSampler& ps) {
    m_samplesPerPixel = ps.m_samplesPerPixel;
    m_type = ps.m_type;
    m_sampleNumber = 0;
    m_rng = std::mt19937(m_dev());
    m_samples.clear();
    initializeSamples();
}
*/


PixelSampler::~PixelSampler() {
    m_samples.clear();
}


void PixelSampler::initializeSamples() {
    m_samples.clear();
    m_sampleNumber = 0;
    m_samples.resize(m_samplesPerPixel * m_samplesPerPixel);

    switch(m_type) {
        case PixelSampler::SamplingType::RandomUniform :
            computeRandomUniform();
            break;
        case PixelSampler::SamplingType::Uniform :
            computeUniform();
            break;
        case PixelSampler::SamplingType::Stratified :
            computeStratified();
            break;
        default: {
            LOG_ERROR("Case unknow, default is uniform random");
            break;
            }
    }

}


void PixelSampler::computeUniform() {
    for (uint i = 0; i < m_samplesPerPixel; ++i)
        for (uint j = 0; j < m_samplesPerPixel; ++j)
            m_samples[i * m_samplesPerPixel + j] = sampleInterval(0.5f, 0.5f, 0.5f, 0.5f);
}

void PixelSampler::computeRandomUniform() {
    float intervalX = 1.f / m_samplesPerPixel;
    float intervalY = 1.f / m_samplesPerPixel;
    float x = 0.f, y = 0.f;

    for (uint i = 0; i < m_samplesPerPixel; ++i) {
        for (uint j = 0; j < m_samplesPerPixel; ++j) {
            m_samples[i * m_samplesPerPixel + j] = sampleInterval(x, x + intervalX, y, y + intervalY);
            y += intervalY;
        }
        x += intervalX;
    }
}

void PixelSampler::shuffle() {
    for (uint i = 0; i < m_samples.size(); ++i) {
        m_distInt = std::uniform_int_distribution<uint>(0, m_samples.size()-1 - i);
        int other = i + m_distInt(*m_rng);
        std::swap(m_samples[i], m_samples[other]);
    }
}

void PixelSampler::computeStratified() {
    float intervalX = 1.f / m_samplesPerPixel;
    float intervalY = 1.f / m_samplesPerPixel;
    float x = 0.f, y = 0.f;

    for (uint i = 0; i < m_samplesPerPixel; ++i) {
        for (uint j = 0; j < m_samplesPerPixel; ++j) {
            m_samples[i * m_samplesPerPixel + j] = sampleInterval(x, x + intervalX, y, y + intervalY);
            y += intervalY;
        }
        y = 0.f;
        x += intervalX;
    }

    for (uint i = 0; i < m_samplesPerPixel * m_samplesPerPixel; ++i)
        shuffle();
}


void PixelSampler::getSamplePos(float& x, float& y) {

    m_distX = std::uniform_real_distribution<float>(m_samples[m_sampleNumber].xMin, m_samples[m_sampleNumber].xMax);
    m_distY = std::uniform_real_distribution<float>(m_samples[m_sampleNumber].yMin, m_samples[m_sampleNumber].yMax);

    x = m_distX(*m_rng);
    y = m_distY(*m_rng);

    m_sampleNumber = (m_sampleNumber + 1) % m_samples.size();
}