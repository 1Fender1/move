//
// Created by jb on 15/09/2019.
//

#ifndef MOVE_PIXELSAMPLER_HPP
#define MOVE_PIXELSAMPLER_HPP

#include <iostream>
#include <vector>
#include <random>

class PixelSampler {

public :
    enum SamplingType {
        Uniform,
        RandomUniform,
        Stratified
    };

    PixelSampler(PixelSampler::SamplingType type = PixelSampler::SamplingType::Stratified, uint samplesPerPixel = 1, std::mt19937* mersenne = nullptr);
    PixelSampler(const PixelSampler& ps);

    PixelSampler& operator=(const PixelSampler& px) {

        if (this == &px)
            return *this;

        m_distX = px.m_distX;
        m_distY = px.m_distY;
        m_distInt = px.m_distInt;
        m_rng = px.m_rng;
        m_sampleNumber = px.m_sampleNumber;
        m_samplesPerPixel = px.m_samplesPerPixel;
        m_type = px.m_type;

        m_samples.clear();
        m_samples.resize(px.m_samples.size());
        for (uint i = 0; i < px.m_samples.size(); ++i) {
            m_samples[i] = px.m_samples[i];
        }


        return *this;

    }

    ~PixelSampler();
    //PixelSampler operator=(const PixelSampler& ps);



   // PixelSampler();


    void initializeSamples();
    void computeUniform();
    void computeRandomUniform();
    void computeStratified();

    void getSamplePos(float& x, float& y);


private :

    struct sampleInterval {
        float xMin;
        float xMax;
        float yMin;
        float yMax;

        sampleInterval(float minX = 0.f, float maxX = 0.f, float minY = 0.f, float maxY = 0.f) {
            xMin = minX;
            xMax = maxX;
            yMin = minY;
            yMax = maxY;
        }
    };

    void shuffle();
    void LatinHypercube();

    SamplingType m_type;
    uint m_samplesPerPixel;
    uint m_sampleNumber;
    std::mt19937* m_rng = nullptr;

    std::uniform_real_distribution<float> m_distX;
    std::uniform_real_distribution<float> m_distY;

    std::uniform_int_distribution<uint> m_distInt;

    std::vector<sampleInterval> m_samples;
};


#endif //MOVE_PIXELSAMPLER_HPP
