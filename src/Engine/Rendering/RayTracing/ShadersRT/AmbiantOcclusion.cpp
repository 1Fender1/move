//
// Created by jb on 11/06/19.
//

#include "AmbiantOcclusion.hpp"
#include <random>


AmbiantOcclusion::AmbiantOcclusion() : ShaderRT() {

}

glm::vec3 AmbiantOcclusion::eval(Ray &ray, ContextSceneEmbree &context) {

    float occlusion = 0.f;
    Intersection hit = context.intersectRay(ray);
    if (!hit.isHit())
        return glm::vec3(0.f);
    for (uint i = 0; i < m_samples; ++i) {
        glm::vec3 Nt, Nb;
        float r1;
        glm::vec3 normal = hit.getNormal();
        createCoordinateSystem(normal, Nt, Nb);
        glm::vec4 tmpNormal = glm::vec4(normal, 0.f);
        normal = glm::vec3(tmpNormal.x, tmpNormal.y, tmpNormal.z);
        glm::vec3 randDir = getRandomDirection(normal, ray.getDirection(), Nt, Nb, ray.getDirection(), r1);

        Ray rayOcc = Ray(hit.getPosition(), randDir);

        if (context.intersectRayOccluded(rayOcc))
           occlusion++;
    }
    occlusion /= float(m_samples);
    return glm::vec3(occlusion);
}

void AmbiantOcclusion::setSamples(uint samples) {
    m_samples = samples;
}