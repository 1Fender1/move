//
// Created by jb on 11/06/19.
//

#ifndef MOVE_AMBIANTOCCLUSION_HPP
#define MOVE_AMBIANTOCCLUSION_HPP
#include <Core/Math/RayTracing/Ray.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/ShaderRt.hpp>


class AmbiantOcclusion : public ShaderRT {

        public:

            AmbiantOcclusion();
            glm::vec3 eval(Ray &ray, ContextSceneEmbree &context);


        private:

            void setSamples(uint samples);

            uint m_samples = 16;
    };


#endif //MOVE_AMBIANTOCCLUSION_HPP
