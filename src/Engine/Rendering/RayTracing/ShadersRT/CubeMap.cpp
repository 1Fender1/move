//
// Created by jb on 12/09/2019.
//

#include "CubeMap.hpp"

//This function take a direction and give uv m_fbTexture coordinates and m_fbTexture index
void CubeMap::getCoords(const glm::vec3& direction, float& u, float& v, uint& index) {
    glm::vec3 normalizedDirection = glm::normalize(direction);
    float x = normalizedDirection.x;
    float y = normalizedDirection.y;
    float z = normalizedDirection.z;

    float absX = std::fabs(x);
    float absY = std::fabs(y);
    float absZ = std::fabs(z);

    int isXPositive = x > 0 ? 1 : 0;
    int isYPositive = y > 0 ? 1 : 0;
    int isZPositive = z > 0 ? 1 : 0;

    //TODO change dirty remapping

    float maxAxis = 0.f, uc = 0.f, vc = 0.f;
    index = 0;
    // POSITIVE X
    if (isXPositive && absX >= absY && absX >= absZ) {
        // u (0 to 1) goes from +z to -z
        // v (0 to 1) goes from -y to +y
        maxAxis = absX;
        uc = -z;
        vc = y;
        //index = 0;
        index = 0;
    }
    // NEGATIVE X
    if (!isXPositive && absX >= absY && absX >= absZ) {
        // u (0 to 1) goes from -z to +z
        // v (0 to 1) goes from -y to +y
        maxAxis = absX;
        uc = z;
        vc = y;
        //index = 1;
        index = 1;
    }
    // POSITIVE Y
    if (isYPositive && absY >= absX && absY >= absZ) {
        // u (0 to 1) goes from -x to +x
        // v (0 to 1) goes from +z to -z
        maxAxis = absY;
        uc = x;
        vc = -z;
        //index = 2;
        index = 2;
    }
    // NEGATIVE Y
    if (!isYPositive && absY >= absX && absY >= absZ) {
        // u (0 to 1) goes from -x to +x
        // v (0 to 1) goes from -z to +z
        maxAxis = absY;
        uc = x;
        vc = z;
        //index = 3;
        index = 3;
    }
    // POSITIVE Z
    if (isZPositive && absZ >= absX && absZ >= absY) {
        // u (0 to 1) goes from -x to +x
        // v (0 to 1) goes from -y to +y
        maxAxis = absZ;
        uc = x;
        vc = y;
        //index = 4;
        index = 4;
    }
    // NEGATIVE Z
    if (!isZPositive && absZ >= absX && absZ >= absY) {
        // u (0 to 1) goes from +x to -x
        // v (0 to 1) goes from -y to +y
        maxAxis = absZ;
        uc = -x;
        vc = y;
        //index = 5;
        index = 5;
    }

    // Convert range from -1 to 1 to 0 to 1
    u = 0.5f * (uc / maxAxis + 1.0f);
    v = 0.5f * (vc / maxAxis + 1.0f);

}

glm::vec3 CubeMap::evalEnvironment(float u, float v, uint index, CubeMapLoader& cubemap) {
    glm::vec3 color;
    const unsigned char* texture = cubemap.getTexture(index);
    uint width, height;
    cubemap.getSize(width, height);

    u = glm::mod(u, 1.f);
    v = glm::mod(v, 1.f);

    uint posWidht = uint((u)*float((width-1)));
    uint posHeight = uint((1.f-v)*float((height-1)));

    unsigned char r = texture[3 * (posHeight * width + posWidht)];
    unsigned char g = texture[3 * (posHeight * width + posWidht) + 1];
    unsigned char b = texture[3 * (posHeight * width + posWidht) + 2];

    color = glm::vec3(float(r) / 255.f, float(g) / 255.f, float(b) / 255.f);

    return color;
}

