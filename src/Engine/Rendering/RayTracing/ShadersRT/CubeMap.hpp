//
// Created by jb on 12/09/2019.
//

#ifndef MOVE_CUBEMAP_HPP
#define MOVE_CUBEMAP_HPP

#include <glm/glm.hpp>
#include <Engine/FileLoader/CubeMapLoader.hpp>

class CubeMap {

public :
    static void getCoords(const glm::vec3& direction, float& u, float& v, uint& index);
    static glm::vec3 evalEnvironment(float u, float v, uint index, CubeMapLoader& cubemap);


};


#endif //MOVE_CUBEMAP_HPP
