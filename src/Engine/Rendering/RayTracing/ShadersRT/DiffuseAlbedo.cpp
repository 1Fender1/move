//
// Created by jb on 10/05/2020.
//

#include "DiffuseAlbedo.hpp"

DiffuseAlbedo::DiffuseAlbedo() : ShaderRT() {

}

glm::vec4 DiffuseAlbedo::eval(Ray ray, ContextSceneEmbree &intersectionContext) {

    Intersection hit = intersectionContext.intersectRay(ray);

    glm::vec4 color  = glm::vec4(0.f, 0.f, 0.f, 1.f);
    if (!hit.isHit())
        return color;

    glm::vec3 tempColor = hit.getColor();

    return glm::vec4(tempColor[0], tempColor[1], tempColor[2], 1.f);

}