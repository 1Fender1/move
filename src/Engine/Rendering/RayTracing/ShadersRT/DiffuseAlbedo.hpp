//
// Created by jb on 10/05/2020.
//

#ifndef MOVE_DIFFUSEALBEDO_HPP
#define MOVE_DIFFUSEALBEDO_HPP

#include <Core/Math/RayTracing/Ray.hpp>
#include <Core/Math/RayTracing/Intersection.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/ShaderRt.hpp>

class DiffuseAlbedo : public ShaderRT {

public:
    DiffuseAlbedo();

    glm::vec4 eval(Ray ray, ContextSceneEmbree &intersectionContext);

private:



};


#endif //MOVE_DIFFUSEALBEDO_HPP
