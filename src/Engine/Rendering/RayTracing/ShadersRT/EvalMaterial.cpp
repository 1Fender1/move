//
// Created by jb on 08/06/19.
//

#include <Engine/Rendering/RayTracing/ShadersRT/CubeMap.hpp>
#include "EvalMaterial.hpp"

EvalMaterial::EvalMaterial() : ShaderRT() {

}

glm::vec3 EvalMaterial::castRayBis(Intersection hit, Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap) {
    return castRayRecBis(hit, ray, context, lights, environmentMap, 0);
}

glm::vec3 EvalMaterial::castRayRecBis(Intersection hit, Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap, uint bounces = 0) {
    if (bounces > MAX_BOUNCES)
        return glm::vec3(0.f);

    glm::vec3 hitColor = glm::vec3(0.f);
    if (hit.isHit()) {
        glm::vec3 position = hit.getPosition();
        glm::vec3 normal = hit.getNormal();
        Mesh* obj = hit.getObjRef();
        glm::vec3 rayDir = ray.getDirection();

        glm::vec3 directLighting = glm::vec3(0.f);
        for (uint i = 0; i < lights->size(); ++i) {
            Light& light = *lights->at(i);

            for (uint j = 0; j < light.getSampleCount(); ++j) {
                glm::vec3 lightDir;
                float lightPdf, matPdf;
                glm::vec3 lightRadiance = light.eval(normal, position + normal * 0.01f, context, lightDir, lightPdf);
                glm::vec3 materialColor = obj->getMaterial()->eval(ray, hit, lightDir, matPdf);
                glm::vec3 color = (lightRadiance * materialColor) / (/*matPdf*/1.f); //!! result is not correct do corrent importance sampling and MIS
                if (!glm::any(glm::isnan(color)))
                    directLighting += color /** glm::dot(normal, -ray.getDirection())*/;

            }
            directLighting /= float( light.getSampleCount());
        }
        directLighting += obj->getMaterial()->getEmissiveColor();

        glm::vec3 indirectLighting = glm::vec3(0.f);
        uint N = MAX_SAMPLES;

        Ray *rays = new Ray[N];
        Intersection *intersections = new Intersection[N];
        float* randoms = new float[N];
        for (uint i = 0; i < N; ++i) {
            glm::vec3 Nt, Nb;
            float r1;
            obj->getMaterial()->createCoordinateSystem(normal, Nt, Nb);
            glm::vec4 tmpNormal = hit.getObjRef()->getModel(0) * glm::vec4(normal, 0.f);
            normal = glm::vec3(tmpNormal.x, tmpNormal.y, tmpNormal.z);
            glm::vec3 randDir = obj->getMaterial()->getRandomDirection(glm::vec3(glm::vec4(normal, 0.f) * obj->getModel()), -rayDir, Nt, Nb, ray.getDirection(), r1);
            rays[i] = Ray(position, randDir);
            randoms[i] = r1;
        }
        context.intersectRayStream(rays, N, intersections, RTC_INTERSECT_CONTEXT_FLAG_INCOHERENT);
        for (uint i = 0; i < N; ++i)
            indirectLighting += /*randoms[i] **/ castRayRecBis(intersections[i], rays[i], context, lights, environmentMap, bounces+1);

        delete [] rays;
        delete [] intersections;
        delete [] randoms;

        indirectLighting /= float(N);
        //TODO let M_PI ?
        hitColor = indirectLighting + (directLighting);
    } else {
        float u, v;
        uint index;
        CubeMap::getCoords(ray.getDirection(), u, v, index);
       // hitColor = CubeMap::evalEnvironment(u, v, index, environmentMap);
        hitColor = glm::vec3(0.f);
    }

    return hitColor;
}


glm::vec3 EvalMaterial::castRay(Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap) {
    return castRayRec(ray, context, lights, environmentMap, 0);
}

glm::vec3 EvalMaterial::castRayRec(Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap, uint bounces = 0) {
    if (bounces > MAX_BOUNCES)
        return glm::vec3(0.f);

    glm::vec3 hitColor = glm::vec3(0.f);
    Intersection hit = context.intersectRay(ray);
    if (hit.isHit()) {
        glm::vec3 position = hit.getPosition();
        glm::vec3 normal = hit.getNormal();
        Mesh* obj = hit.getObjRef();
        glm::vec3 rayDir = ray.getDirection();

        glm::vec3 directLighting = glm::vec3(0.f);
        for (uint i = 0; i < lights->size(); ++i) {
            PointLight* light = static_cast<PointLight*>(lights->at(i).get());
            glm::vec3 lightDir = light->getPosition() - position;
            float pdf;
            glm::vec3 color = obj->getMaterial()->eval(ray, hit, -lightDir, pdf);
            directLighting += (light->sample(context, ray, pdf) * color) * std::max(0.f, glm::dot(normal, -lightDir));
        }

        glm::vec3 indirectLighting = glm::vec3(0.f);
        uint N = MAX_SAMPLES;
        //TODO use ray stream
        for (uint i = 0; i < N; ++i) {
            glm::vec3 Nt, Nb;
            float r1;
            obj->getMaterial()->createCoordinateSystem(normal, Nt, Nb);
            glm::vec3 randDir = obj->getMaterial()->getRandomDirection(normal, rayDir, Nt, Nb, ray.getDirection(), r1);
            Ray newRay = Ray(position, randDir);

            indirectLighting += r1 * castRayRec(newRay, context, lights, environmentMap, bounces+1);
        }
        indirectLighting /= float(N);
        //TODO let M_PI ?
        hitColor = (directLighting /*/ float(M_PI)*/) + 2.f * indirectLighting;

    } else {
        float u, v;
        uint index;
        CubeMap::getCoords(ray.getDirection(), u, v, index);
        hitColor = CubeMap::evalEnvironment(u, v, index, environmentMap);
    }

    return hitColor;
}


uint EvalMaterial::getRayDepth() {
    return MAX_BOUNCES;
}

void EvalMaterial::setRayDepth(uint depth) {
    MAX_BOUNCES = depth;
}

uint EvalMaterial::getGISamplesCount() {
    return MAX_SAMPLES;
}

void EvalMaterial::setGISamplesCount(uint sampleCount) {
    MAX_SAMPLES = sampleCount;
}