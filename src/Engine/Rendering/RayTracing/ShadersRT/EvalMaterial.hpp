//
// Created by jb on 08/06/19.
//

#ifndef MOVE_EVALMATERIAL_HPP
#define MOVE_EVALMATERIAL_HPP

#include <Engine/Rendering/Light/Lights.hpp>
#include <Core/Math/RayTracing/Ray.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>
#include <Engine/FileLoader/CubeMapLoader.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/ShaderRt.hpp>

    class EvalMaterial : public ShaderRT {
        public :

            EvalMaterial();

            //Need more test to evaluates two functions efficiency
            glm::vec3 castRayBis(Intersection hit, Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap);
            glm::vec3 castRay(Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap);


            uint getRayDepth();
            void setRayDepth(uint depth);

            uint getGISamplesCount();
            void setGISamplesCount(uint sampleCount);

        private :

            uint MAX_BOUNCES = 8;
            uint MAX_SAMPLES = 1;


            glm::vec3 castRayRecBis(Intersection hit, Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap, uint bounces);
            glm::vec3 castRayRec(Ray ray, ContextSceneEmbree& context, std::vector<Light_ptr>* lights, CubeMapLoader& environmentMap, uint bounces);

    };



#endif //MOVE_EVALMATERIAL_HPP
