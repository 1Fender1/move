//
// Created by jb on 07/06/19.
//

#include "Normals.hpp"

Normals::Normals() : ShaderRT() {

}


glm::vec3 Normals::eval(Ray ray, ContextSceneEmbree& intersectionContext) {
    Intersection hit = intersectionContext.intersectRay(ray);

    glm::vec3 normal  = glm::vec3(0.f);
    if (!hit.isHit())
        return normal;

    Material* mat = hit.getObjRef()->getMaterial();

    if (/*mat->hasNormalMap()*/false) {
        normal = mat->getNormalMap()->getNormal(hit.getTexCoords().x, hit.getTexCoords().y, hit.getTangent(), hit.getBiTangent(), hit.getNormal(), hit.getObjRef()->getModel());
        normal = glm::vec3(normal * 0.5f + 0.5f);
    } else {
        normal = glm::vec3(glm::normalize(hit.getNormal()) * 0.5f + 0.5f);
    }
   // if (normal == glm::vec3(0.f))
      //  normal = glm::vec3(glm::normalize(hit.getNormal()) * 0.5f + 0.5f);

    return normal;
}