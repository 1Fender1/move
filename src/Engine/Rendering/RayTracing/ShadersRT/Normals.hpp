//
// Created by jb on 07/06/19.
//

#ifndef MOVE_NORMALS_HPP
#define MOVE_NORMALS_HPP
#include <Core/Math/RayTracing/Ray.hpp>
#include <Core/Math/RayTracing/Intersection.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/ShaderRt.hpp>

    class Normals : public ShaderRT {

    public:

        Normals();

        glm::vec3 eval(Ray ray, ContextSceneEmbree &intersectionContext);

    };


#endif //MOVE_NORMALS_HPP
