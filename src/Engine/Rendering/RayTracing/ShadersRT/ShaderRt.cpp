//
// Created by jb on 07/12/2019.
//

#include "ShaderRt.hpp"

ShaderRT::ShaderRT() {
    m_rng = std::mt19937(m_dev());
    m_dist01 = std::uniform_real_distribution<float>(0.f, 1.f);
}

glm::vec3 ShaderRT::randPointOnHemisphere() {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<float> dist(-1.f, 0.9999999f);
    //TODO verify
    float u = dist(rng);
    float x1 = dist(rng);
    float x2 = dist(rng);
    float x3 = dist(rng);

    float mag = glm::sqrt(x1*x1 + x2*x2 + x3*x3);
    x1 /= mag; x2 /= mag; x3 /= mag;

    float c = std::cbrt(u);

    return glm::vec3(x1*c, x2*c, x3*c);

}

glm::vec3 ShaderRT::getRandomDirection(glm::vec3 normal, glm::vec3, glm::vec3 Nt, glm::vec3 Nb, glm::vec3, float& r1) {

    r1 = m_dist01(m_rng);
    float r2 = m_dist01(m_rng);
    glm::vec3 sample = uniformSampleHemisphere(r1, r2);
    glm::vec3 sampleWorld(sample.x * Nb.x + sample.y * normal.x + sample.z * Nt.x,
                          sample.x * Nb.y + sample.y * normal.y + sample.z * Nt.y,
                          sample.x * Nb.z + sample.y * normal.z + sample.z * Nt.z);

    return sampleWorld;
}

void ShaderRT::createCoordinateSystem(const glm::vec3 &N, glm::vec3 &Nt, glm::vec3 &Nb) {
    if (std::fabs(N.x) > std::fabs(N.y))
        Nt = glm::vec3(N.z, 0.f, -N.x) / sqrtf(N.x * N.x + N.z * N.z);
    else
        Nt = glm::vec3(0.f, -N.z, N.y) / sqrtf(N.y * N.y + N.z * N.z);
    Nb = glm::cross(N, Nt);
}

glm::vec3 ShaderRT::uniformSampleHemisphere(float r1, float r2) {
    float sinTheta = sqrtf(1 - r1 * r1);
    float phi = 2 * float(M_PI) * r2;
    float x = sinTheta * cosf(phi);
    float z = sinTheta * sinf(phi);
    return glm::vec3(x, r1, z);
}
