//
// Created by jb on 07/12/2019.
//

#ifndef MOVE_SHADERRT_HPP
#define MOVE_SHADERRT_HPP

#include <glm/glm.hpp>
#include <random>


class ShaderRT {

    public :
        ShaderRT();

        glm::vec3 eval() {return glm::vec3(0.f);};

    protected :
        void createCoordinateSystem(const glm::vec3 &N, glm::vec3 &Nt, glm::vec3 &Nb);

        glm::vec3 uniformSampleHemisphere(float r1, float r2);

        glm::vec3 getRandomDirection(glm::vec3 normal, glm::vec3 dir, glm::vec3 Nt, glm::vec3 Nb, glm::vec3, float &r1);

        glm::vec3 randPointOnHemisphere();

        std::mt19937 m_rng;
        std::uniform_real_distribution<float> m_dist01;
        std::random_device m_dev;


};

#endif //MOVE_SHADERRT_HPP
