//
// Created by jb on 06/04/19.
//

#include <Engine/Rendering/RaytracerRenderer.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/ShadersRT.hpp>
#include <Core/glAssert.hpp>
#include <Core/Math/RayTracing/Ray.hpp>
#include <omp.h>
#include <random>
#include <thread>
#include <nmmintrin.h> //SSE4.2


RaytracerRenderer::RaytracerRenderer(uint w, uint h) : Renderer(w, h) {

    m_width = w;
    m_height = h;

    m_currentWidth = getDividedWidth();
    m_currentHeight = getDividedHeight();

    imgData = new float[m_currentWidth*m_currentHeight*m_channels];

    sampleSates = new ushort[m_currentWidth*m_currentHeight];

    m_rand =  std::mt19937(m_randomDev());


    m_samplingType = PixelSampler::SamplingType::Stratified;
    m_samplingSize = 8;

    m_pixelSamplers = new PixelSampler[m_currentWidth*m_currentHeight];
    uint sizePX = m_currentWidth*m_currentHeight;
    for (uint i = 0; i < sizePX; ++i) {
        m_pixelSamplers[i] = PixelSampler(m_samplingType, m_samplingSize, &m_rand);
    }


    initBuffer();

    initializeGL(m_currentWidth, m_currentHeight);
    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
    _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);
    m_currentTile = 0;

    loadCubeMap(std::string("seaBlur"), std::string(".jpg"));
    //m_shaderType = ShaderEnum::AMBIANT_OCCLUSION;
    //m_shaderType = ShaderEnum::NORMALS;
    //m_shaderType = ShaderEnum::DIFFUSE;
}

RaytracerRenderer::~RaytracerRenderer() {

    if (frameBuffer)
        delete frameBuffer;

    if (m_pixelSamplers)
        delete [] m_pixelSamplers;

    if (sampleSates)
        delete [] sampleSates;

    if (imgData)
        delete [] imgData;



}

uint RaytracerRenderer::getDividedWidth() {
    uint w = m_width / m_currentResolutionDivider;
    w = (w == 0) ? 1 : w;

    return w;
}


uint RaytracerRenderer::getDividedHeight() {
    uint h = m_height / m_currentResolutionDivider;
    h = (h == 0) ? 1 : h;

    return h;
}

glm::vec3 RaytracerRenderer::eval_shader(Ray& ray, Intersection& intersection) {

    if (!m_renderData)
        return glm::vec3(0.f);

    switch(m_shaderType) {
        case ShaderEnum::PATH_TRACER :
            return m_pathTracer.castRayBis(intersection, ray, *m_rayTracerContext, m_renderData->getLights(), m_environmentMap);
        case ShaderEnum::AMBIANT_OCCLUSION :
            return m_AOShader.eval(ray, *m_rayTracerContext);
        case ShaderEnum::NORMALS :
            return m_normalShader.eval(ray, *m_rayTracerContext);
        case ShaderEnum::DIFFUSE :
            return m_diffuseShader.eval(ray, *m_rayTracerContext);
    }

    return glm::vec3(0.f);
}

void RaytracerRenderer::initializeGL(uint w, uint h) {
    m_height = h;
    m_width = w;
    frameBuffer = new FrameBuffer(m_currentWidth, m_currentHeight);
    m_tempBuffer = new RaytracerBuffer(m_currentWidth, m_currentHeight);
    m_tempBuffer->setTextureFromData(imgData);
}

uint RaytracerRenderer::draw() {

    if (m_isChangeRes) {
        m_isChangeRes = false;
      //  frameBuffer->resize(m_width, m_height);
        m_tempBuffer->resize(m_currentWidth, m_currentHeight);
        m_tempBuffer->setTextureFromData(imgData);
    }
    else {
        //m_tempBuffer->resize(m_currentWidth, m_currentHeight);
        m_tempBuffer->updateTextureFromData(imgData);
    }

    //glViewport(0, 0, m_width, m_height);

    GLint defaultBuffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &defaultBuffer);

    frameBuffer->bind();

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    screenShader->use();

    screenShader->setFloat("exposure", m_outExposure);


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_tempBuffer->getTexture());
    screenShader->setInt("texture_diffuse1", 0);

    frameBuffer->drawGeom(screenShader);

    glBindFramebuffer(GL_FRAMEBUFFER, defaultBuffer);

    GL_CHECK_ERROR

    return frameBuffer->texId();
}


void RaytracerRenderer::computeImage() {
    if (m_adaptRes) {
        m_adaptRes = false;
        resize(m_width, m_height);
        m_isChangeRes = true;
    }

    m_rayTracerContext->updateContext();

    if (m_rayTracerContext->handleUpdate()) {
        resize(m_width, m_height);
        m_isChangeRes = true;
    }

    uint nbThreads = omp_get_max_threads();
    if (m_tiles.size() - m_currentTile < nbThreads)
        nbThreads = m_tiles.size() - m_currentTile;

    #pragma omp parallel default(none) num_threads(nbThreads)
    {
        Tile& currentTile = m_tiles[m_currentTile + omp_get_thread_num()];

        uint tileWidth = currentTile.maxY - currentTile.minY;
        uint tileHeight = currentTile.maxX - currentTile.minX;

        //Rays preparation
        std::vector<Ray> rays = std::vector<Ray>(tileWidth * tileHeight);
        for (uint i = 0; i < tileHeight; ++i) {
            for (uint j = 0; j < tileWidth; ++j) {
                uint posX = currentTile.minY + j;
                uint posY = currentTile.minX + i;

                float sampleX, sampleY;
                PixelSampler &ps = m_pixelSamplers[posX, posY];
                ps.getSamplePos(sampleX, sampleY);

                glm::vec3 direction =
                        glm::unProject(glm::vec3(float(posY) + sampleX, float(posX) + sampleY, m_camera->getFrustum().getNear()),
                                       m_camera->getView(), m_camera->getProjection(),
                                       glm::vec4(0, 0, m_currentWidth, m_currentHeight)) - m_camera->getPosition();
                direction = glm::normalize(direction);
                rays[i * tileWidth + j] = Ray(m_camera->getPosition(), direction);
            }
        }

        //Intersection computing
        std::vector<Intersection> intersections = std::vector<Intersection>(tileWidth * tileHeight);
        m_rayTracerContext->intersectRayStream(rays, intersections, RTC_INTERSECT_CONTEXT_FLAG_COHERENT);

        //Screen color computing
        glm::vec3 color = glm::vec3(0.f);
        for (uint i = 0; i < tileHeight; ++i) {
            for (uint j = 0; j < tileWidth; ++j) {
                uint colorPos = (currentTile.minY + j) * m_currentWidth * m_channels + (currentTile.minX + i) * m_channels;
                uint r = colorPos;
                uint g = colorPos + 1;
                uint b = colorPos + 2;
                uint a = colorPos + 3;
                uint posInImage = (currentTile.minY + j) * m_currentWidth + (currentTile.minX + i);
                uint posInTile = i*tileWidth + j;

                //Intersection &hit = intersections[posInTile];
                //Ray &ray = rays[posInTile];

                color = eval_shader(rays[posInTile], intersections[posInTile]);

                imgData[r] =
                        ((imgData[r] * float(sampleSates[posInImage])) + color.r) / (float(sampleSates[posInImage]) + 1);
                imgData[g] =
                        ((imgData[g] * float(sampleSates[posInImage])) + color.g) / (float(sampleSates[posInImage]) + 1);
                imgData[b] =
                        ((imgData[b] * float(sampleSates[posInImage])) + color.b) / (float(sampleSates[posInImage]) + 1);
                imgData[a] = 1.f;
                ++sampleSates[posInImage];
            }
        }
    }

    m_currentTile += nbThreads;
    if (m_currentTile >= m_tiles.size() - 1) {
        m_currentTile = 0;
        m_nbRenders++;
    }

    if (m_nbRenders >= 2 && m_currentResolutionDivider != 1) {
        m_currentResolutionDivider -= 3;
        if (m_currentResolutionDivider < 1)
            m_currentResolutionDivider = 1;

        m_adaptRes = true;
        m_nbRenders = 0;
    }

}

void RaytracerRenderer::resize(uint w, uint h) {
    m_height = h;
    m_width = w;

    m_currentWidth = getDividedWidth();
    m_currentHeight = getDividedHeight();

    if (imgData != nullptr) {
        delete [] imgData;
        imgData = new float[m_currentWidth * m_currentHeight * m_channels];
    }

    if (sampleSates != nullptr) {
        delete [] sampleSates;
        sampleSates = new ushort[m_currentWidth * m_currentHeight];
    }
/*
    if (m_pixelSamplers != nullptr) {
        delete [] m_pixelSamplers;
        m_pixelSamplers = nullptr;
        m_pixelSamplers = new PixelSampler[m_currentWidth * m_currentHeight];
    }
*/
    initBuffer();
    initTiles();


    frameBuffer->resize(m_width, m_height);

    //m_tempBuffer->bind();
    m_tempBuffer->resize(m_currentWidth, m_currentHeight);
    m_tempBuffer->setTextureFromData(imgData);
    //m_tempBuffer->updateTextureFromData(imgData);
    //m_tempBuffer->unbind();

    glViewport(0, 0, m_width, m_height);
    //initializeGL(w, h);
}



void RaytracerRenderer::refresh() {

    initBuffer();
    m_currentResolutionDivider = m_defaultResolutionDivider;

    m_currentWidth = getDividedWidth();
    m_currentHeight = getDividedHeight();
    resize(m_width, m_height);


    //initBuffer();
    //resize(m_width, m_height);

    m_toRefresh = false;
}

void RaytracerRenderer::initBuffer() {
    for (uint i = 0; i < m_currentHeight * m_currentWidth * m_channels; ++i)
        imgData[i] = 0.f;

    for (uint i = 0; i < m_currentHeight * m_currentWidth; ++i)
        sampleSates[i] = 0;


    m_intersections.resize(m_tilesDim*m_tilesDim);
    m_rays.resize(m_tilesDim*m_tilesDim);

}


void RaytracerRenderer::initTiles() {
    m_currentTile = 0;
    uint nbTiles = ((m_currentWidth/m_tilesDim)+1)*((m_currentHeight/m_tilesDim)+1);
    m_tiles.resize(nbTiles);
    uint cptTiles = 0;

    switch (m_tilingType) {
        case TilingEnum::Horizontal : {
            for (uint i = 0; i < m_currentWidth; i+= m_tilesDim) {
                for (uint j = 0; j < m_currentHeight; j += m_tilesDim) {
                    m_tiles[cptTiles] = Tile(i, std::min(i+m_tilesDim, m_currentWidth), j, std::min(j+m_tilesDim, m_currentHeight));
                    ++cptTiles;
                }
            }
        }
        break;
        case TilingEnum::Vertical : {
            for (uint i = 0; i < m_currentHeight; i+= m_tilesDim) {
                for (uint j = 0; j < m_currentWidth; j += m_tilesDim) {
                    m_tiles[cptTiles] = Tile(j, std::min(j+m_tilesDim, m_currentWidth), i, std::min(i+m_tilesDim, m_currentHeight));
                    ++cptTiles;
                }
            }
        }
        break;
        case TilingEnum::Hilbert : {

        }
        break;
        case TilingEnum::Morton : {

        }
        break;
        case TilingEnum::Spiral : {
            int posX, posY;
            m_tiles.clear();
            posX = (int(m_currentWidth) / 2) - m_tilesDim;
            posY = (int(m_currentHeight) / 2) - m_tilesDim;
            posX = (posX < 0) ? 0 : posX;
            posY = (posY < 0) ? 0 : posY;

            m_tiles.emplace_back(Tile(posX, glm::min(posX + m_tilesDim, m_currentWidth-1), posY, glm::min(posY + m_tilesDim, m_currentHeight-1)));
            cptTiles++;
            // (di, dj) is a vector - direction in which we move right now
            int di = 1;
            int dj = 0;
            // length of current segment
            int segment_length = 1;

            int segment_passed = 0;
            uint maxTiles = glm::max(m_currentWidth, m_currentHeight);
            maxTiles = ((maxTiles/m_tilesDim) + 1) * ((maxTiles/m_tilesDim) + 1);
            for (uint k = 0; k < (maxTiles - 1) * 2; ++k) {
                // make a step, add 'direction' vector (di, dj)
                posX += di * int(m_tilesDim);
                posY += dj * int(m_tilesDim);

                int posXmax = posX + int(m_tilesDim);
                int posYmax = posY + int(m_tilesDim);
                if ((((posX + int(m_tilesDim)) >= 0) && (posX <= int(m_currentWidth-1))) && (((posY + int(m_tilesDim) >= 0)) && (posY <= int((m_currentHeight-1))))) {
                    int newPosX = (posX < 0) ? 0 : posX;
                    int newPosY = (posY < 0) ? 0 : posY;
                    posXmax = (posXmax >= int(m_currentWidth)) ? m_currentWidth : posXmax;
                    posYmax = (posYmax >= int(m_currentHeight)) ? m_currentHeight : posYmax;
                    m_tiles.emplace_back(Tile(newPosX, posXmax, newPosY, posYmax));
                }

                ++segment_passed;

                if (segment_passed == segment_length) {
                    // done with current segment
                    segment_passed = 0;

                    // 'rotate' directions
                    int buffer = di;
                    di = -dj;
                    dj = buffer;

                    // increase segment length if necessary
                    if (dj == 0) {
                        ++segment_length;
                    }
                }
            }

        }
        break;
    }


}


void  RaytracerRenderer::toRefresh() {
    m_toRefresh = true;
}

void RaytracerRenderer::loadCubeMap(std::string path, std::string type) {
    m_environmentMap.loadCubeMap(path, type);
}

void RaytracerRenderer::setRaytraceContext(ContextSceneEmbree* context) {
    m_rayTracerContext = context;
}

void RaytracerRenderer::setShader(Shader* shader) {
    screenShader = shader;
}

EvalMaterial* RaytracerRenderer::getPathTracer() {
    return &m_pathTracer;
}