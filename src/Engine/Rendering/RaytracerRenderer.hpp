//
// Created by jb on 06/04/19.
//

#ifndef MOVE_RAYTRACERRENDERER_HPP
#define MOVE_RAYTRACERRENDERER_HPP

#include <csignal>
#include <Engine/Rendering/Renderer.hpp>

#include <Engine/Rendering/RayTracing/ShadersRT/ShadersRT.hpp>

#include <Engine/Rendering/FrameBuffer/RaytracerBuffer.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>

#include <Engine/Modeling/Mesh/Mesh.hpp>
#include <Engine/FileLoader/CubeMapLoader.hpp>
#include <Engine/Rendering/RayTracing/PixelSampler.hpp>
/*
#include <Engine/Rendering/RayTracing/ShadersRT/ShaderRt.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/Normals.hpp>
#include <Engine/Rendering/RayTracing/ShadersRT/EvalMaterial.hpp>
 */


class RaytracerRenderer : public Renderer {

    enum ShaderEnum {
        PATH_TRACER,
        NORMALS,
        AMBIANT_OCCLUSION,
        DIFFUSE
    };

    struct Tile {
        uint minX;
        uint maxX;
        uint minY;
        uint maxY;

        Tile(uint _minX = 0, uint _maxX = 0, uint _minY = 0, uint _maxY = 0) {
            minX = _minX;
            maxX = _maxX;
            minY = _minY;
            maxY = _maxY;
        }
    };

    enum TilingEnum {
        Horizontal,
        Vertical,
        Spiral,
        Morton,
        Hilbert
    };

public :

    RaytracerRenderer(uint w, uint h);
    ~RaytracerRenderer();
    void initializeGL(uint w, uint h);
    uint draw();
    void computeImage();
    void resize(uint w, uint h);
    void refresh();
    void toRefresh();
    void setRaytraceContext(ContextSceneEmbree* context);

    void loadCubeMap(std::string path, std::string type);

    void setShader(Shader* shader);

    EvalMaterial* getPathTracer();


private :

    void initBuffer();
    void initTiles();

    uint getDividedWidth();
    uint getDividedHeight();

    glm::vec3 eval_shader(Ray& ray, Intersection& intersection);


    const uint m_defaultResolutionDivider = 10;
    const uint m_tilesDim = 16; //16x16
    uint m_currentResolutionDivider = 10;

    FrameBuffer* frameBuffer = nullptr;
    RaytracerBuffer* m_tempBuffer = nullptr;

    Shader* screenShader;
    float* imgData = nullptr;
    ushort *sampleSates = nullptr;
    ContextSceneEmbree* m_rayTracerContext;

    //Tiles has an arbitrary size (m_tilesDim x m_tilesDim here)
    std::vector<Tile> m_tiles;
    //Used to render the tile number m_currentTile
    uint m_currentTile = 0;

    //Intersections and rays for all framebuffer
    std::vector<Intersection> m_intersections;
    std::vector<Ray> m_rays;

    bool m_refreshing = false;
    bool m_adaptRes = false;
    bool m_isChangeRes = false;

    uint m_currentWidth;
    uint m_currentHeight;
    const uint m_channels = 4;
    uint m_nbRenders = 0;

    PixelSampler::SamplingType m_samplingType;
    uint m_samplingSize;
    PixelSampler *m_pixelSamplers = nullptr;

    TilingEnum m_tilingType = TilingEnum::Spiral;

    CubeMapLoader m_environmentMap;

    //Shaders
    Normals m_normalShader;
    EvalMaterial m_pathTracer;
    AmbiantOcclusion m_AOShader;
    DiffuseAlbedo m_diffuseShader;


    ShaderEnum m_shaderType = ShaderEnum::PATH_TRACER;

    std::random_device m_randomDev;
    std::mt19937 m_rand;


};


#endif //MOVE_RAYTRACERRENDERER_HPP
