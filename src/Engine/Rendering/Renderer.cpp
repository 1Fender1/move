#include "Renderer.hpp"

#include "Core/glAssert.hpp"


Renderer::Renderer(uint w, uint h) {
    m_width = w;
    m_height = h;
}

Renderer::~Renderer() {


}


void Renderer::setRenderData(RenderData* renderData) {
    m_renderData = renderData;
}