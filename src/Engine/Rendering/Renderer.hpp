#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <cstdio>
#include <iostream>
#include <vector>

#include <Engine/Modeling/Mesh/Mesh.hpp>
#include <Engine/Rendering/FrameBuffer/FrameBuffer.hpp>
#include <Engine/Rendering/FrameBuffer/Gbuffer.hpp>
#include <Engine/Rendering/FrameBuffer/Ssao.hpp>
#include <Engine/Rendering/FrameBuffer/Blur.hpp>
#include <Engine/Rendering/FrameBuffer/Fxaa.hpp>
#include <Engine/FileLoader/CubeMapLoader.hpp>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>

#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Camera/Camera.hpp>
#include <Engine/Rendering/Light/Lights.hpp>
#include <Ui/Gizmos/Gizmo.hpp>


class Renderer
{

public:
    Renderer(uint w, uint h);
    Renderer() {}
    virtual ~Renderer();


    virtual uint draw() {return 0;}

    virtual void resize(unsigned int, unsigned int) {}

    virtual void setMainShader(Shader*) {}

    virtual void setFrameShader(Shader*) {}
    virtual void setGBufShader(Shader*) {}
    virtual void setSSAOShader(Shader*) {}
    virtual void setSSAOBlurBuffer(Shader*) {}
    virtual void setFXAAShader(Shader*) {}
    virtual void setCubeMapShader(Shader*) {}
    virtual void addObj(Object*) {}
    virtual void setGizmo(Gizmo*) {}
    virtual void toRefresh() {}

    float getExposure() {return m_outExposure;}
    void setExposure(float exposure) {m_outExposure = (exposure > 0.f) ? exposure : m_outExposure;};

    void setIsFillGeometry(bool fill) {
        m_isFillGeometry = fill;
    }

    void setRenderData(RenderData* renderData);

    bool m_isFillGeometry = true;
    Camera* m_camera = nullptr;

protected :

    uint m_width;
    uint m_height;
    bool m_toRefresh = false;

    float m_outExposure = 1.f;

    RenderData* m_renderData = nullptr;

};

#endif // RENDERER_H
