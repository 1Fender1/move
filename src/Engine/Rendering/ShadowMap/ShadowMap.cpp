//
// Created by jb on 22/09/2019.
//

#include "ShadowMap.hpp"

ShadowMap::ShadowMap(uint width, uint height) {


    m_lightProjection = glm::mat4(1.f);
    m_lightView = glm::mat4(1.f);
    m_lightSpaceMatrix = glm::mat4(1.f);
    m_shader = nullptr;
    m_shaderStandard = nullptr;
    m_shaderPoint = nullptr;

    m_width = width;
    m_height = height;

    m_lightType = Light::LightType::UNKNOW;
}

ShadowMap::ShadowMap(const ShadowMap& sm) {
    m_lightProjection = sm.m_lightProjection;
    m_lightView = sm.m_lightView;
    m_lightSpaceMatrix = sm.m_lightSpaceMatrix;
    m_shader = nullptr;
    m_shaderStandard = nullptr;
    m_shaderPoint = nullptr;

    m_width = sm.m_width;
    m_height = sm.m_width;

    m_lightType = sm.m_lightType;
}

ShadowMap::ShadowMap() {

}

void ShadowMap::computeFrustum(Light* light) {

    setShader(light);

    m_lightType = light->getLightType();

    switch (m_lightType) {

        case Light::LightType::POINT : {
            computePointLightFrustum(static_cast<PointLight*>(light));
        }
            break;
        case Light::LightType::SPOT : {
            computeSpotLightFrustum(static_cast<SpotLight*>(light));
        }
            break;
        case Light::LightType::DIR : {
            computeDirLightFrustum(static_cast<DirectionalLight*>(light));
        }
            break;
        default :
            break;

    }


}


void ShadowMap::computeSpotLightFrustum(SpotLight* light) {
    glm::vec3 position = light->getPosition();
    glm::vec3 direction = light->getDirection();
    float angle = light->getOAngle();
    m_lightProjection = glm::perspective<float>(angle, float(m_width) / float(m_height), 1.f, 20.f);
    m_lightView = glm::lookAt(position, position + direction, glm::vec3(0.f, 0.f, 1.f));
    m_lightSpaceMatrix = m_lightProjection * m_lightView;

}

void ShadowMap::computeDirLightFrustum(DirectionalLight* light) {

    const glm::vec3& lightDir = light->getDirection();
    glm::vec3 dir = glm::vec3(0.f, -1.f, 0.f);

    glm::vec3 position = dir * -10.f;


    m_lightProjection = glm::ortho(-10.f, 10.f, -10.f, 10.f, 0.1f, 20.f);
    glm::vec3 center =  position + lightDir;
    glm::vec3 up =  glm::vec3(0.f, 0.f, 1.f);
    m_lightView = glm::lookAt(position, center, up);
    m_lightSpaceMatrix = m_lightProjection * m_lightView;


}

void ShadowMap::computePointLightFrustum(PointLight* light) {
    glm::vec3 position = light->getPosition();
    m_lightProjection = glm::perspective<float>(glm::radians(90.f), float(m_width)/float(m_height), 0.01f, 20.f);

    m_lightViews[0] = m_lightProjection * glm::lookAt(position, position + glm::vec3( 1.f, 0.f, 0.f), glm::vec3(0.f, -1.f, 0.f));
    m_lightViews[1] = m_lightProjection * glm::lookAt(position, position + glm::vec3(-1.f, 0.f, 0.f), glm::vec3(0.f, -1.f, 0.f));
    m_lightViews[2] = m_lightProjection * glm::lookAt(position, position + glm::vec3(0.f, 1.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
    m_lightViews[3] = m_lightProjection * glm::lookAt(position, position + glm::vec3(0.f, -1.f, 0.f), glm::vec3(0.f, 0.f, -1.f));
    m_lightViews[4] = m_lightProjection * glm::lookAt(position, position + glm::vec3(0.f, 0.f, 1.f), glm::vec3(0.f, -1.f, 0.f));
    m_lightViews[5] = m_lightProjection * glm::lookAt(position, position + glm::vec3(0.f, 0.f,-1.f), glm::vec3(0.f,-1.f, 0.f));

}


void ShadowMap::setShader(Light* light) {
    m_shader = m_shaderStandard;

    if (light->getLightType() == Light::LightType::POINT)
        m_shader = m_shaderPoint;
    else
        m_shader = m_shaderStandard;

    m_lightType = light->getLightType();

}

void ShadowMap::setShaderPoint(Shader* shader) {
    m_shaderPoint = shader;
}

void ShadowMap::setShaderStandard(Shader* shader) {
    m_shaderStandard = shader;
}