//
// Created by jb on 22/09/2019.
//

#ifndef MOVE_SHADOWMAP_HPP
#define MOVE_SHADOWMAP_HPP

#include <Engine/Scenes/RenderData.hpp>

class ShadowMap {

public:

    ShadowMap(uint width, uint height);
    ShadowMap(const ShadowMap& sm);

    ShadowMap();

    ShadowMap& operator=(const ShadowMap& sm) {

        if (this == &sm)
            return *this;

        m_lightProjection = sm.m_lightProjection;
        m_lightView = sm.m_lightView;
        m_lightSpaceMatrix = sm.m_lightSpaceMatrix;
        m_shader = nullptr;
        m_shaderStandard = nullptr;
        m_shaderPoint = nullptr;

        m_width = sm.m_width;
        m_height = sm.m_width;

        m_lightType = sm.m_lightType;

        return *this;
    }

    virtual void computeShadowMap(Light* light, RenderData* renderData) = 0;
    virtual void draw(Shader* shader) = 0;

    void setShaderPoint(Shader* shader);
    void setShaderStandard(Shader* shader);

protected:

    void computeFrustum(Light* light);

    void computeSpotLightFrustum(SpotLight* light);
    void computeDirLightFrustum(DirectionalLight* light);
    void computePointLightFrustum(PointLight* light);

    void setShader(Light* light);

    uint m_width;
    uint m_height;


    Shader* m_shader = nullptr;
    Shader* m_shaderStandard = nullptr;
    Shader* m_shaderPoint = nullptr;

    uint m_lightType;

    glm::mat4 m_lightProjection;
    glm::mat4 m_lightView;
    glm::mat4 m_lightSpaceMatrix;

    //For point lights only
    glm::mat4 m_lightViews[6];

};


#endif //MOVE_SHADOWMAP_HPP
