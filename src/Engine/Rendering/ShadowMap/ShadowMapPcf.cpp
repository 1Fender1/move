//
// Created by jb on 22/09/2019.
//

#include "ShadowMapPcf.hpp"
#include <Core/glAssert.hpp>

ShadowMapPCF::ShadowMapPCF(uint w, uint h) : ShadowMap(w, h) {

    m_buffer = new ShadowMapPCFBuffer(w, h);
}

ShadowMapPCF::ShadowMapPCF(const ShadowMapPCF& sm) : ShadowMap(sm) {
    m_buffer = new ShadowMapPCFBuffer(m_width, m_height);
}


ShadowMapPCF::~ShadowMapPCF() {
    if (m_buffer)
        delete m_buffer;
}

void ShadowMapPCF::draw(Shader* shader) {

   // glClear(GL_DEPTH_BUFFER_BIT);

    //glViewport(0, 0, m_width, m_height);

    if (m_lightType == Light::LightType::POINT) {
        shader->setBool("isShadowMap", false);
    }
    else {
        shader->setBool("isShadowMap", true);
        shader->setMat4("lightSpaceMatrix", m_lightSpaceMatrix);
    }

}

void ShadowMapPCF::computeShadowMap(Light* light, RenderData* renderData) {

    m_lightType = light->getLightType();

    //m_buffer->resize(m_width, m_height);
    m_buffer->bind();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_FRONT);

    computeFrustum(light);

    m_shader = m_shaderStandard;
    m_shader->use();
    this->draw(m_shader);

    uint nbObjects = renderData->getObjCount();
    for (uint i = 0; i < nbObjects; ++i) {
        Object& object = *renderData->getObject(i);
        for (uint j = 0; j < object.getMeshCount(); ++j) {
            Mesh* mesh = object.getMeshAt(j);
            const glm::mat4 &model = mesh->getModel(0);
            m_shader->setMat4("model", model);
            //printMat4(m_lightSpaceMatrix);
            mesh->draw(*m_shader);
        }
    }

    m_buffer->unbind();
    glCullFace(GL_BACK);
}

ShadowMapPCFBuffer* ShadowMapPCF::getSMBuffer() {
    return m_buffer;
}

void ShadowMapPCF::resize(uint w, uint h) {
    if (w != m_width || h != m_height)
        m_isResized = false;

    if (m_isResized)
        return;

    m_buffer->resize(w, h);
    m_isResized = true;
}


uint ShadowMapPCF::getTexID() {
    return m_buffer->texId();
}