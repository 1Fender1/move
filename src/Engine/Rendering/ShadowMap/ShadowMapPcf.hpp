//
// Created by jb on 22/09/2019.
//

#ifndef MOVE_SHADOWMAPPCF_HPP
#define MOVE_SHADOWMAPPCF_HPP

#include <Engine/Rendering/ShadowMap/ShadowMap.hpp>
#include <Engine/Rendering/FrameBuffer/ShadowMapPCFBuffer.hpp>

class ShadowMapPCF final : public ShadowMap {

public:

    ShadowMapPCF(uint w, uint h);

    ShadowMapPCF(const ShadowMapPCF& sm);
    ShadowMapPCF& operator=(const ShadowMapPCF& sm) {

        if (this == &sm)
            return *this;

        ShadowMap::operator=(sm);
        m_buffer = new ShadowMapPCFBuffer(m_width, m_height);

        return *this;

    }

    ~ShadowMapPCF();

    void computeShadowMap(Light* light, RenderData* renderData) override;
    void draw(Shader* shader) override;

    ShadowMapPCFBuffer* getSMBuffer();

    //void bind();

    void resize(uint w, uint h);

    uint getTexID();

private:

    ShadowMapPCFBuffer* m_buffer = nullptr;

    bool m_isResized = false;



};


#endif //MOVE_SHADOWMAPPCF_HPP
