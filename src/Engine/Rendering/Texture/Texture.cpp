//
// Created by jb on 23/03/2020.
//

#include "Texture.hpp"
#include <iostream>
#include <Core/System/Logger.hpp>

#include <stb/stb_image.h>

Texture::Texture() : MoveObject() {

}

Texture::Texture(std::string filename) {

    m_path = filename;


    glGenTextures(1, &m_id);
    m_selfGeneratedTex = true;
    int width, height, nrComponents = 0;
    uchar *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
    m_channels = nrComponents;
    m_width = width;
    m_height = height;
    m_size = m_width * m_height * m_channels;
    LOG("Load %n", filename);
    if (data) {
        GLuint format;
        switch (nrComponents) {
            case  1 :
                format = GL_RED;
                break;
            case 3 :
                format = GL_RGB;
                break;
            case 4 :
                format = GL_RGBA;
                break;
            default :
                format = GL_SRGB;
                break;
        }

        m_format = format;

        m_data = new uchar[m_size];
        for (uint i = 0; i < m_size; ++i) {
            m_data[i] = data[i];
        }

        bind();

        glTexImage2D(GL_TEXTURE_2D, 0, m_format, m_width, m_height, 0, m_format, GL_UNSIGNED_BYTE, m_data);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindTexture(GL_TEXTURE_2D, 0);


        stbi_image_free(data);

    }
    else {
        LOG_WARNING("Texture failed to load at path : %n", filename)
    }


}

Texture::Texture(Texture& texture) : MoveObject(texture) {

    m_width = texture.m_width;
    m_height = texture.m_height;
    m_channels = texture.m_channels;
    m_size = texture.m_size;
   // m_id = m_fbTexture.m_id;
    m_format = texture.m_format;
    m_textureType = texture.m_textureType;
    m_path = texture.m_path;
    //m_selfGeneratedTex = m_fbTexture.m_selfGeneratedTex;

    m_data = new uchar[m_size];
    for (uint i = 0; i < m_size; ++i) {
        m_data[i] = texture.m_data[i];
    }

/*
    glDeleteTextures(1, &m_id);
    glGenTextures(1, &m_id);
    */
    glGenTextures(1, &m_id);

    bind();

    glTexImage2D(GL_TEXTURE_2D, 0, m_format, m_width, m_height, 0, m_format, GL_UNSIGNED_BYTE, m_data);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

}

Texture::Texture(uint width, uint height, uint channels, std::string name): MoveObject(name) {

    m_width = width;
    m_height = height;
    m_channels = channels;
    m_size = m_width * m_height * m_channels;

    m_data = new uchar[m_size];

}


Texture::~Texture() {
    //TODO add a destroy method
//return;
    //if (m_selfGeneratedTex) {
        if (m_data)
            delete [] m_data;

        glDeleteTextures(1, &m_id);
    //}

}


glm::vec4 Texture::getColor(float x, float y) {

    uint l = uint(x * m_width);
    uint c = uint(y * m_height);
    return getColor(l, c);

}

glm::vec4 Texture::getColor(uint l, uint c) {

    if (m_data == nullptr)
        return glm::vec4(0.f);

    float u = float(l) / float(m_width);
    float v = float(c) / float(m_height);


    u = glm::mod(u, 1.f);
    v = glm::mod(v, 1.f);

    uint x = uint((u)*float((m_width-1)));
    uint y = uint((v)*float((m_height-1)));

    uchar r = m_data[m_channels * (y * m_width + x)];
    uchar g = m_data[m_channels * (y * m_width + x) + 1];
    uchar b = m_data[m_channels * (y * m_width + x) + 2];

    return glm::vec4(float(r)/255.f, float(g)/255.f, float(b)/255.f, 1.f);
}


glm::vec3 Texture::computeNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4& model) {
    if (is3ChannelsNormal())
        return compute3ChannelsNormal(u, v, t, b, n, model);
    else
        return compute1ChannelNormal(u, v, t, b, n, model);
}

glm::vec3 Texture::compute3ChannelsNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4&) {
    if (m_data == nullptr) {
        return n;
    }

//    glm::mat4 normalMatrix = glm::transpose(glm::inverse(model));
    glm::vec3 tangent = t;//normalize(glm::vec3(normalMatrix * glm::vec4(t, 0.f)));
    glm::vec3 normal = n;//normalize(glm::vec3(normalMatrix * glm::vec4(n, 0.f)));
    glm::vec3 bitangent = b;//normalize(glm::vec3(normalMatrix * glm::vec4(b, 0.f)));


    tangent = glm::normalize(tangent - normal * glm::dot(normal, tangent));

    bitangent = -bitangent;

    glm::mat3 TBN = glm::mat3(tangent, bitangent, normal);

    u = glm::mod(u, 1.f);
    v = glm::mod(v, 1.f);

    uint x = uint(u * float(m_width-1));
    uint y = uint(v * float(m_height-1));

    uint bufferPos = m_channels * (y * m_width + x);
    uchar nx = m_data[bufferPos];
    uchar ny = m_data[bufferPos + 1];
    uchar nz = m_data[bufferPos + 2];

    glm::vec3 texNormal = glm::normalize(glm::vec3(float(nx)/255.f, float(ny)/255.f, float(nz)/255.f));
    texNormal = glm::normalize(texNormal * 2.f - 1.f);
    glm::vec3 outNormal = glm::normalize(TBN * texNormal);

    return outNormal;
}

glm::vec3 Texture::compute1ChannelNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4&) {
    if (m_data == nullptr) {
        return n;
    }

   // glm::mat4 normalMatrix = glm::transpose(glm::inverse(model));
    glm::vec3 tangent = t;//normalize(glm::vec3(normalMatrix * glm::vec4(t, 0.f)));
    glm::vec3 normal = n;//normalize(glm::vec3(normalMatrix * glm::vec4(n, 0.f)));
    glm::vec3 bitangent = b;//normalize(glm::vec3(normalMatrix * glm::vec4(b, 0.f)));


    tangent = glm::normalize(tangent - normal * glm::dot(normal, tangent));

    bitangent = -bitangent;

    glm::mat3 TBN = glm::mat3(tangent, bitangent, normal);

    u = glm::mod(u, 1.f);
    v = glm::mod(v, 1.f);

    uint x = uint(u * float(m_width-1));
    uint y = uint(v * float(m_height-1));

    uint bufferPos = m_channels * (y * m_width + x);
    uchar texValue = m_data[bufferPos];

    uint xPos;
    if (x < m_width)
        xPos = m_channels * (y * m_width + x + 1);
    else
        xPos = m_channels * (y * m_width + x - 1);
    uint yPos;
    if (y < m_height)
        yPos = m_channels * ((y+1) * m_width + x + 1);
    else
        yPos = m_channels * ((y-1) * m_width + x + 1);

    float center = float(texValue)/255.f;
    float xOffset = float(m_data[xPos])/255.f;
    float yOffset = float(m_data[yPos])/255.f;
    glm::vec3 texNormal = glm::normalize(glm::vec3(xOffset-center, yOffset-center, 0.01f));

    texNormal = glm::normalize(texNormal);
    glm::vec3 outNormal = glm::normalize(TBN * texNormal);

    return outNormal;
}

bool Texture::is3ChannelsNormal() {

    bool compTest = m_data[0] != m_data[2] && m_data[1] != m_data[2];
    return (m_channels == 3) && compTest;

}

glm::vec3 Texture::getNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4& model) {
    return computeNormal(u, v, t, b, n, model);
}

float Texture::getSpecularity(float u, float v) {
    if (m_data == nullptr)
        return -1.f;

    u = glm::mod(u, 1.f);
    v = glm::mod(v, 1.f);

    uint x = uint((u)*float((m_width-1)));
    uint y = uint((v)*float((m_height-1)));


    uchar s = m_data[m_channels * (y * m_width + x)];

    float spec = float(s) / 255.f;

    return spec;
}

void Texture::bind() {
    glBindTexture(GL_TEXTURE_2D, m_id);
}

uchar* Texture::getData() {
    return m_data;
}