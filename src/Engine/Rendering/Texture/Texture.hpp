//
// Created by jb on 23/03/2020.
//

#ifndef MOVE_TEXTURE_HPP
#define MOVE_TEXTURE_HPP

#include <Engine/Scenes/MoveObject.hpp>
#include <glm/glm.hpp>
#include <Ui/libs_ui.hpp>

typedef unsigned char uchar;

class Texture : public MoveObject {

public :

    const std::string UNDEFINED_MAP = "texture_undefined";
    const std::string DIFFUSE_MAP = "texture_diffuse";
    const std::string SPECULAR_MAP = "texture_specular";
    const std::string NORMAL_MAP = "texture_normal";

    Texture();
    Texture(std::string name);
    Texture(Texture& texture);
    Texture(uint width, uint height, uint channels, std::string name);

    Texture& operator=(const Texture& texture) {

        if (this == &texture)
            return *this;

        m_width = texture.m_width;
        m_height = texture.m_height;
        m_channels = texture.m_channels;
        m_size = texture.m_size;
        // m_id = m_fbTexture.m_id;
        m_format = texture.m_format;
        m_textureType = texture.m_textureType;
        m_path = texture.m_path;
        //m_selfGeneratedTex = m_fbTexture.m_selfGeneratedTex;

        m_data = new uchar[m_size];
        for (uint i = 0; i < m_size; ++i) {
            m_data[i] = texture.m_data[i];
        }
/*
    glDeleteTextures(1, &m_id);
    glGenTextures(1, &m_id);
    */
        glGenTextures(1, &m_id);

        bind();

        glTexImage2D(GL_TEXTURE_2D, 0, m_format, m_width, m_height, 0, m_format, GL_UNSIGNED_BYTE, m_data);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindTexture(GL_TEXTURE_2D, 0);

        return *this;
    }

    ~Texture();

    glm::vec4 getColor(uint x, uint y);
    glm::vec4 getColor(float x, float y);

    //This function return correct normal after TBN transformation
    glm::vec3 getNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4& model);

    float getSpecularity(float u, float v);

    void setTextureType(std::string type) { m_textureType = type; }

    void setPath(std::string path) { m_path = path; }

    std::string getPath() { return m_path; }

    std::string getTextureType() { return m_textureType; }

    uint getID() { return m_id; }

    void setID(uint id) { m_id = id; }

    void bind();

    uint getChannelCount() {return m_channels;}

    uchar* getData();


private :

    inline glm::vec3 computeNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4& model);
    inline glm::vec3 compute3ChannelsNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4& model);
    inline glm::vec3 compute1ChannelNormal(float u, float v, const glm::vec3& t, const glm::vec3& b, const glm::vec3& n, const glm::mat4& model);
    inline bool is3ChannelsNormal();

    uint m_id = 0;

    uint m_width = 0;
    uint m_height = 0;
    uint m_channels = 0;

    uint m_size = 0;

    uchar* m_data = nullptr;

    std::string m_textureType = UNDEFINED_MAP;
    std::string m_path = "";

    bool m_selfGeneratedTex = false;

    uint m_format = 0;

};

typedef std::unique_ptr<Texture> Texture_ptr;


#endif //MOVE_TEXTURE_HPP
