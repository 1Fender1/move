//
// Created by jb on 01/12/2019.
//

#include "MoveObject.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <Core/System/Logger.hpp>

MoveObject::MoveObject(std::string objectName, MoveObject* parent) {
    if (objectName != "")
        m_objectName = objectName;
    else
        m_objectName = "Object";
    m_parent = parent;
    m_models.push_back(glm::mat4(1.f));
}

MoveObject::MoveObject(const MoveObject& obj) {
    m_objectName = obj.m_objectName;
    if (m_objectName == "")
        m_objectName = "Object";
    m_objType = obj.m_objType;
    m_children = obj.m_children;
    m_ID = obj.m_ID;

    m_models.clear();
    m_models.resize(obj.m_models.size());
    for (uint i = 0; i < obj.m_models.size(); ++i) {
        m_models[i] = obj.m_models[i];
    }

    m_parent = obj.m_parent;
}

MoveObject::~MoveObject() {

}

void MoveObject::setName(const std::string& objectName) {
    if (objectName.empty())
        m_objectName = "Object";
    else
        m_objectName = objectName;
}

std::string MoveObject::getName() {
    return m_objectName;
}

void MoveObject::addChild(MoveObject* child) {
    m_children.push_back(child);
    child->setParent(this);
}

MoveObject* MoveObject::getChild(uint index) {
    if (index >= m_children.size())
        return nullptr;

    return m_children[index];
}

MoveObject* MoveObject::getChild(std::string name) {
    for (auto obj : m_children)
        if (obj)
            if (obj->getName() == name)
                return obj;

    return nullptr;
}

uint MoveObject::getChildCount() {
    return m_children.size();
}

void MoveObject::setParent(MoveObject* parent) {
    m_parent = parent;
}

MoveObject* MoveObject::getParent() {
    return m_parent;
}

const glm::mat4& MoveObject::getModel(uint i) {
    if (i >= m_models.size())
        return m_models[0];
    return m_models[i];
}

uint MoveObject::getInstanceCount() {
    return m_models.size();
}


void MoveObject::transform(const glm::mat4& t, uint instanceID) {
    if (instanceID >= getInstanceCount())
        return;

    m_isDirty = true;

    m_models[instanceID] = t * m_models[instanceID];

    for (auto& m : m_children) {
        glm::mat4 transform = t;
        transform = glm::inverse(m_models[instanceID])*t;
        m->transform(m_models[instanceID], instanceID);
    }
}

void MoveObject::setTransform(const glm::mat4& t, uint instanceID) {
    if (instanceID >= getInstanceCount())
        return;
    m_isDirty = true;
    m_models[instanceID] = t;

    for (auto& m : m_children) {
        glm::mat4 transform = t;
        transform = glm::inverse(m_models[instanceID])*t;
        m->setTransform(m_models[instanceID], instanceID);
    }
}

void MoveObject::scale(const glm::vec3& scale, uint instanceID) {

    if (instanceID >= getInstanceCount())
        return;
    m_isDirty = true;

    m_models[instanceID] = glm::scale(m_models[instanceID], scale);
}


bool MoveObject::isLeaf() {
    return m_children.empty();
}

bool MoveObject::isDirty() {
    return m_isDirty;
}

bool MoveObject::isDirtyChildren() {

    if (m_isDirty)
        return true;

    for (auto& child : m_children) {
        if (child != nullptr)
            return m_isDirty || child->isDirty();
    }

    return m_isDirty;
}

void MoveObject::undirty() {
    m_isDirty = false;
}

void MoveObject::undirtyChildren() {

    m_isDirty = false;

    for (auto& child : m_children)
        if (child)
            child->undirtyChildren();

}

bool MoveObject::isSelected() {
    return m_isSelected;
}

void MoveObject::setSelected(bool selected) {
    m_isSelected = selected;
}

void MoveObject::setID(MoveObjectID id) {
    m_ID = id;
}

MoveObjectID MoveObject::getID() {
    return m_ID;
}