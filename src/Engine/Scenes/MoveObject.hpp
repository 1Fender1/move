//
// Created by jb on 01/12/2019.
//

#ifndef MOVE_MOVEOBJECT_HPP
#define MOVE_MOVEOBJECT_HPP

#include <string>
#include <vector>
#include <Core/Math/Math.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <Core/System/Logger.hpp>

typedef uint64_t MoveObjectID;
#define NO_MOVE_OBJECT_ID MoveObjectID(-1)

class MoveObject {

public :

    enum objectType {
        NONE,
        MESH,
        OBJECT,
        LIGHT,
        MATERIAL,
        CAMERA
    };


    MoveObject(std::string objectName = "", MoveObject* parent = nullptr);
    MoveObject(const MoveObject& obj);
    MoveObject& operator=(const MoveObject& obj) {


       // if (&obj == this)
         //   return *this;

        m_objectName = obj.m_objectName;
        if (m_objectName == "")
            m_objectName = "Object";
        m_objType = obj.m_objType;
        m_children = obj.m_children;
        m_ID = obj.m_ID;


        m_models.clear();
        m_models.resize(obj.m_models.size());
        for (uint i = 0; i < obj.m_models.size(); ++i)
            m_models[i] = obj.m_models[i];

        m_parent = obj.m_parent;

        return *this;
    }


    ~MoveObject();

    void setName(const std::string& objectName);
    std::string getName();

    void addChild(MoveObject* child);
    MoveObject* getChild(uint index);
    MoveObject* getChild(std::string name);
    uint getChildCount();

    void setParent(MoveObject* parent);
    MoveObject* getParent();

    objectType getType() {return m_objType;}

    bool isLeaf();

    const glm::mat4& getModel(uint i = 0);

    //Return 1 if no instance (mean we only have the original object), it's the number of matrix model available
    uint getInstanceCount();

    void transform(const glm::mat4& t, uint instanceID = 0);
    void setTransform(const glm::mat4& t, uint instanceID = 0);
    void scale(const glm::vec3& scale, uint instanceID = 0);

    bool isDirty();
    bool isDirtyChildren();
    void undirty();
    void undirtyChildren();

    bool isSelected();
    void setSelected(bool selected);

    void setID(MoveObjectID id);
    MoveObjectID getID();

protected :

    MoveObject* m_parent = nullptr;
    std::vector<MoveObject*> m_children;
    std::string m_objectName = "default";

    objectType m_objType = objectType::NONE;

    bool m_isDirty = false;
    bool m_isSelected = false;


private :
    //we only want manipulate m_models with MoveObjects functions
    std::vector<glm::mat4> m_models;
    MoveObjectID m_ID = NO_MOVE_OBJECT_ID;


};


#endif //MOVE_MOVEOBJECT_HPP
