
#include "ObjVis.hpp"
#include "Core/glAssert.hpp"
#include <Core/Math/RayCaster.hpp>
#include <Engine/Rendering/ForwardRenderer.hpp>
#include <Engine/Rendering/Material/Materials.hpp>

#include <Engine/Rendering/Light/LightUtils/computeLTC_GGX.hpp>
#include <Core/System/Logger.hpp>


ObjVis::ObjVis(int width, int height) : Scene(width, height) {

    m_stopDrawing = false;
    m_isRaytrace = false;
    m_isMousePressed = false;
    m_fileLoader = new FileLoader(&m_renderData);

    m_raytracerRenderer = new RaytracerRenderer(width, height);
    m_raytracerRenderer->setRenderData(&m_renderData);
    m_raytracerRenderer->setRaytraceContext(&(m_renderData.getRaytracerContext()));

    m_renderer = new ForwardRenderer(width, height, &m_renderData);
    m_renderer->m_isFillGeometry = &m_drawfill;
    m_renderer->setRenderData(&m_renderData);

    //Create base camera
    CameraFree cam = CameraFree(glm::vec3(2.f, 2.f, 4.f), m_width, m_height);
    cam.setTarget(glm::vec3(0.f));
    cam.setName("Camera");
    m_camera = m_renderData.addObject(cam);

    intializeShaders();

    m_renderer->m_camera = m_camera;
    m_raytracerRenderer->m_camera = m_camera;

    if (m_gizmo) {
        m_renderer->setGizmo(m_gizmo);
        m_gizmo->setCamera(m_camera);
    }


    /*
     *
     * Init temp
     *
     */

    PrincipledMaterial matPlane;
    matPlane.setName("mtl_plane");
    m_renderData.addObject(matPlane);

    PrincipledMaterial matCube;
    matCube.setName("mtl_cube");
    m_renderData.addObject(matCube);

    Object plane = BaseObjects::quad(100.f, 100.f);
    plane.getMeshAt(0)->setMaterial(m_renderData.getMaterial("mtl_plane"));
    m_renderData.addObject(plane);

    Object cube = BaseObjects::cube(1.f, 1.f, 1.f);
    glm::mat4 transformCube = glm::mat4(1.f);
    transformCube = glm::translate(transformCube, glm::vec3(0.f, 1.f, 0.f));
    cube.setTransform(transformCube);
    cube.getMeshAt(0)->setMaterial(m_renderData.getMaterial("mtl_cube"));
    m_renderData.addObject(cube);

    PointLight light = PointLight();
    light.setPosition(glm::vec3(4.f, 4.f, 4.f));
    light.setIntensity(50);
    m_renderData.addObject(light);

}


ObjVis::~ObjVis() {

    delete m_fileLoader;
    delete m_renderer;
    delete m_raytracerRenderer;
}

void ObjVis::resize(int width, int height) {

    if (m_width == width && m_height == height)
        return;

    Scene::resize(width, height);
    m_camera->resize(width, height);
    m_renderer->m_camera = m_camera;

}

void ObjVis::mouseRelease(int button, float xpos, float ypos) {
    m_isMousePressed = false;
    m_camera->processMouseRelease(button, xpos, ypos);
    if (m_gizmo)
        m_gizmo->mouseRelease(glm::vec2(xpos, ypos));
}


uint ObjVis::draw() {
    Scene::draw();

    if (m_isRaytrace) {
        m_raytracerRenderer->computeImage();
        m_raytracerTexID = m_raytracerRenderer->draw();
    }

    if (m_isForward) {
        m_forwardTexID = m_renderer->draw();
    }


    return 0;
}

void ObjVis::drawRT() {
    if (m_raytracerRenderer)
        m_raytracerRenderer->draw();
}

void ObjVis::mouseclick(int button, float xpos, float ypos) {

    m_isMousePressed = true;
    m_button = button;
    m_mousex = xpos;
    m_mousey = ypos;
    m_camera->processMouseClick(m_button, xpos, ypos);
    if (button == 0) {
        if (m_gizmo) {
            m_gizmo->mouseClick(glm::vec2(xpos, ypos));
        }
    }

}



void ObjVis::mouseDoubleClick(int button, float xpos, float ypos) {
    m_button = button;
    m_mousex = xpos;
    m_mousey = ypos;

    Ray ray = Ray(m_camera->getPosition(),m_camera->getRayFromScreen(xpos, ypos));
    Intersection hit = m_renderData.getRaytracerContext().intersectRay(ray);
    if (m_button == 0) {
        if (hit.isHit()) {
            if (m_gizmo) {
                m_gizmo->setObjectRef((Object *) hit.getObjRef());
                m_selectedObject = hit.getObjRef();
                unselectScene();
                m_selectedObject->setSelected(true);
            }
        }
        else {
            if (m_gizmo) {
                m_gizmo->setObjectRef(nullptr);
                unselectScene();
                m_selectedObject = nullptr;
            }
        }
    }


}

void ObjVis::wheelScroll(int delta) {
    m_camera->processMouseScroll(GLfloat(delta));
    if (m_gizmo)
        m_gizmo->wheelScroll(delta);
    if (m_raytracerRenderer) {
        m_raytracerRenderer->refresh();
        m_raytracerRenderer->toRefresh();
    }

}

void ObjVis::mousemove(float xpos, float ypos, float deltaTime) {

    if (m_camera != nullptr)
        m_camera->processMouseMovement(m_button, xpos, ypos, true, deltaTime);

    if (m_gizmo != nullptr) {
        if (xpos < m_width && xpos >= 0.f && ypos < m_height && ypos >= 0.f)
            m_gizmo->mouseMove(*m_camera, glm::vec2(xpos, ypos), true);
    }
    if (m_button == 1 && m_isMousePressed) {
        if (m_raytracerRenderer)
            m_raytracerRenderer->refresh();
    }
    if (m_raytracerRenderer)
        m_raytracerRenderer->toRefresh();

}


void ObjVis::keyboardmove(int key, double time) {
    m_camera->processKeyboard(key, time);
    if (m_gizmo)
        m_gizmo->keyboardmove();
    if (m_raytracerRenderer) {
        m_raytracerRenderer->refresh();
        m_raytracerRenderer->toRefresh();
    }
    //raise(1);
}

bool ObjVis::keyboard(unsigned char ) {
    return true;
}


void ObjVis::loadFile(std::string name) {
    m_fileLoader->clear();

    m_stopDrawing = true;
    m_fileLoader->objLoader(name);

    LOG("%n loaded.", name);

    glFinish();

    m_stopDrawing = false;

}

void ObjVis::computeRaytraceImage() {
    if (m_raytracerRenderer)
        m_raytracerRenderer->computeImage();
}

Camera* &ObjVis::getCamera() {
    return m_camera;
}
