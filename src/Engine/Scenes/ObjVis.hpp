//
// Created by jb on 05/03/18.
//

#ifndef OBJVIS_HPP
#define OBJVIS_HPP

#include <Engine/Scenes/Scene.hpp>

#include <Engine/Camera/Cameras.hpp>
#include <Engine/FileLoader/FileLoader.hpp>
#include <Engine/Shader/ShaderLoader.hpp>
#include <Engine/Modeling/Geometry/b_spline.hpp>

#include <memory>
#include <iostream>
#include <Engine/Modeling/Geometry/openMeshConverter.hpp>
#include <Engine/Modeling/Geometry/Meshtools.hpp>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>
#include <Engine/Rendering/Light/Lights.hpp>
#include <Engine/Rendering/Renderer.hpp>
#include <Engine/Rendering/RaytracerRenderer.hpp>
#include <Engine/Rendering/FrameBuffer/FrameBuffer.hpp>

#include <Core/Math/Math.hpp>

class ObjVis : public Scene {

public:
    explicit ObjVis(int width, int height);
    ~ObjVis();

    void resize(int width, int height) override;
    uint draw() override;
    void drawRT();

    void mouseclick(int button, float xpos, float ypos) override;
    void mouseDoubleClick(int button, float xpos, float ypos) override;
    void mousemove(float xpos, float ypos, float deltaTime) override;
    void keyboardmove(int key, double time) override;
    void wheelScroll(int delta) override;
    void mouseRelease(int button, float xpos, float ypos) override;
    bool keyboard(unsigned char key) override;
    void loadFile(std::string name) override;


    void computeRaytraceImage();


private:


    FileLoader *m_fileLoader = nullptr;

    uint m_nbObjects;
    int m_numObj;

    bool m_isMousePressed;

public:

    Camera* &getCamera();

private:

    //std::vector<Light*> lights;

    // for mouse management
    int m_button = 3; // 0 --> left. 1 --> right. 2 --> middle. 3 --> other
    float m_mousex{0};
    float m_mousey{0};



    float m_t;
    bool m_stopDrawing;

    //TEMP
    glm::mat4 m_rotationLBS = glm::mat4(1);
    bool m_GPU = false;
    glm::vec3 m_axisRotation = glm::vec3(0.f, 0.f, 1.f);


};


#endif //HELLOOPENGL_HELLOOBJ_H
