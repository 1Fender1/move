//
// Created by jb on 29/02/2020.
//

#include "RenderData.hpp"


RenderData::RenderData() {

}

RenderData::~RenderData() {
    clearScene();
}

//TODO Set meshes ids
Object* RenderData::addObject(Object& obj) {
    m_objects.push_back(std::make_unique<Object>(obj));
    setIDs(m_objects.back().get());

    //TODO can be opitimize add mesh commit for scene each addMesh call
    if (obj.isRaytracerVisible()) {
        for (uint i = 0; i < obj.getMeshCount(); ++i) {
            Mesh *mesh = m_objects.back()->getMeshAt(i);
            m_raytraceContext.addMesh(/*mesh*/(Mesh*)m_objects.back()->getChild(i));
        }
    }

    return m_objects.back().get();
}

Light* RenderData::addObject(Light& obj) {

    switch(obj.getLightType()) {
        case Light::POINT : {
            PointLight* pLight = reinterpret_cast<PointLight*>(&obj);
            if (pLight) {
                m_lights.push_back(std::make_unique<PointLight>(*pLight));
                setIDs(m_lights.back().get());
                return m_lights.back().get();
            }
        }
        break;
        case Light::DIR : {
            DirectionalLight* dLight = reinterpret_cast<DirectionalLight*>(&obj);
            if (dLight) {
                m_lights.push_back(std::make_unique<DirectionalLight>(*dLight));
                setIDs(m_lights.back().get());
                return m_lights.back().get();
            }
        }
        break;
        case Light::SPOT : {
            SpotLight* sLight = reinterpret_cast<SpotLight*>(&obj);
            if (sLight) {
                m_lights.push_back(std::make_unique<SpotLight>(*sLight));
                setIDs(m_lights.back().get());
                return m_lights.back().get();
            }
        }
        break;
        case Light::AREA : {
            AreaLight* aLight = reinterpret_cast<AreaLight*>(&obj);
            if (aLight) {
                m_lights.push_back(std::make_unique<AreaLight>(*aLight));
                setIDs(m_lights.back().get());
                return m_lights.back().get();
            }
        }
        break;
            default : {
                LOG_ERROR("RenderData : Light type unknown")
        }
        break;
    }

    return nullptr;
}


Material* RenderData::addObject(Material& obj) {


    switch(obj.getMaterialType()) {

        case Material::materialType::PRINCIPLED : {
            PrincipledMaterial* mPrincipled = dynamic_cast<PrincipledMaterial*>(&obj);
            if (mPrincipled) {
                m_materials.push_back(std::make_unique<PrincipledMaterial>(*mPrincipled));
                setIDs(m_materials.back().get());
                return m_materials.back().get();
            }
        }
        break;
        default : {
            LOG_WARNING("Add material, add none type")
        }
        break;
    }

    return nullptr;
}

Texture* RenderData::addObject(Texture& obj) {
    m_textures.push_back(std::make_unique<Texture>(obj));
    return m_textures.back().get();
}

Camera* RenderData::addObject(Camera& obj) {
    Camera* camera = nullptr;
    switch(obj.getCameraType()) {

        case Camera::cameraType::FREE : {
            CameraFree* mCamFree = dynamic_cast<CameraFree*>(&obj);
            m_cameras.push_back(std::make_unique<CameraFree>(*mCamFree));
            camera = m_cameras.back().get();
        } break;
        case Camera::cameraType::ORBIT : {
            CameraOrbit* mCamOrbit = dynamic_cast<CameraOrbit*>(&obj);
            m_cameras.push_back(std::make_unique<CameraOrbit>(*mCamOrbit));
            camera = m_cameras.back().get();
        } break;
        default : {
            LOG_ERROR("Camera type unknown")
        } break;
    }

    if (camera == nullptr)
        return nullptr;

    setIDs(camera);

    return camera;
}



void  RenderData::clearScene() {
    clearObjects();
    clearLights();
    clearMaterials();
    clearCameras();
    clearTextures();
    clearShaders();
}

void  RenderData::clearObjects() {
    m_objects.clear();
}

void  RenderData::clearLights() {
    m_lights.clear();
}

void  RenderData::clearMaterials() {
    m_materials.clear();
}

void  RenderData::clearCameras() {
    m_cameras.clear();
}

void RenderData::clearTextures() {
    m_textures.clear();
}

void  RenderData::clearObject(uint i) {
    if (i > m_objects.size()-1)
        return;

    m_objects.erase(m_objects.begin()+i);
}
/*
void  RenderData::clearMaterial(uint i) {
    if (i > m_materials.size()-1)
        return;

    m_materials.erase(m_materials.begin()+i);
}

void  RenderData::clearLight(uint i) {
    if (i > m_lights.size()-1)
        return;

    m_lights.erase(m_lights.begin()+i, m_lights.begin()+i);
}

void  RenderData::clearCamera(uint i) {
    if (i > m_cameras.size()-1)
        return;

    m_cameras.erase(m_cameras.begin()+i, m_cameras.begin()+i);
}
*/

Object* RenderData::getObject(uint i) {
    if (i > m_objects.size()-1)
        return nullptr;

    return m_objects[i].get();
}

Light* RenderData::getLight(uint i) {
    if (i > m_lights.size()-1)
        return nullptr;

    return m_lights[i].get();
}

Material* RenderData::getMaterial(uint i) {
    if (i > m_materials.size()-1)
        return nullptr;

    return m_materials[i].get();
}

Camera* RenderData::getCamera(uint i) {
    if (i > m_cameras.size()-1)
        return nullptr;

    return m_cameras[i].get();
}

Texture* RenderData::getTexture(uint id) {
    if (id >= m_textures.size())
        return nullptr;

    return m_textures[id].get();
}

Object* RenderData::getObject(std::string name) {

    for (uint i = 0; i < m_objects.size(); ++i)
        if (m_objects[i].get()->getName() == name)
            return m_objects[i].get();

    return nullptr;
}

Light* RenderData::getLight(std::string name) {
    for (uint i = 0; i < m_lights.size(); ++i)
        if (m_lights[i].get()->getName() == name)
            return m_lights[i].get();

    return nullptr;
}

Material* RenderData::getMaterial(std::string name) {
    for (uint i = 0; i < m_materials.size(); ++i)
        if (m_materials[i].get()->getName() == name)
            return m_materials[i].get();

    return nullptr;
}

Camera* RenderData::getCamera(std::string name) {
    for (uint i = 0; i < m_cameras.size(); ++i)
        if (m_cameras[i].get()->getName() == name)
            return m_cameras[i].get();

    return nullptr;
}

Texture* RenderData::getTexture(std::string name) {
    for (uint i = 0; i < m_textures.size(); ++i)
        if (m_textures[i].get()->getName() == name)
            return m_textures[i].get();

    return nullptr;
}


uint RenderData::getObjCount() {
    return m_objects.size();
}

uint RenderData::getLightCount() {
    return m_lights.size();
}

uint RenderData::getMaterialCount() {
    return m_materials.size();
}

uint RenderData::getCameraCount() {
    return m_cameras.size();
}

uint RenderData::getTextureCount() {
    return m_textures.size();
}

std::vector<Light_ptr>* RenderData::getLights() {
    return &m_lights;
}

std::vector<Object_ptr>* RenderData::getObjects() {
    return &m_objects;
}


bool RenderData::isExistMaterial(Material* mat) {
    if (mat == nullptr)
        return false;
    for (uint i = 0; i < m_materials.size(); ++i)
        if (mat->getName() == m_materials[i]->getName())
            return true;

    return false;
}

bool RenderData::isExistTexture(Texture* texture) {

    if (texture == nullptr)
        return false;

    for (uint i = 0; i < m_textures.size(); ++i) {
        if (texture->getName() == m_textures[i]->getName())
            return true;
    }

    return false;
}

bool RenderData::isExistTexture(std::string name) {
    for (uint i = 0; i < m_textures.size(); ++i) {
        if (name == m_textures[i]->getName())
            return true;
    }

    return false;
}

uint RenderData::getTextureID(std::string name) {
    for (uint i = 0; i < m_textures.size(); ++i) {
        if (name == m_textures[i]->getName())
            return i;
    }

    return uint(-1);
}

ContextSceneEmbree& RenderData::getRaytracerContext() {
    return m_raytraceContext;
}


Shader* RenderData::addShader(Shader& shader) {

    String name = shader.getName();
    for (uint i = 0; i < m_shaders.size(); ++i)
        if (m_shaders[i]->getName() == name)
            return m_shaders[i].get();

    m_shaders.push_back(std::make_unique<Shader>(shader));
    return m_shaders.back().get();
}

void RenderData::clearShaders() {
    m_shaders.clear();
}

Shader* RenderData::getShader(uint index) {

    if (index >= m_shaders.size())
        return nullptr;

    return m_shaders[index].get();
}


Shader* RenderData::getShader(String name) {

    for (uint i = 0; i < m_shaders.size(); ++i)
        if (m_shaders[i]->getName() == name)
            return m_shaders[i].get();

    return nullptr;
}

uint RenderData::getShaderCount() {
    return m_shaders.size();
}

bool RenderData::isDirtyScene() {
    for (auto& obj : m_objects) {
        if (obj->isDirtyChildren()) {
            isSceneDirty = true;
            return true;
        }
    }

    for (auto& light : m_lights) {
        if (light->isDirtyChildren()) {
            isSceneDirty = true;
            return true;
        }
    }

    for (auto& cam : m_cameras) {
        if (cam->isDirtyChildren()) {
            isSceneDirty = true;
            return true;
        }
    }

    for (auto& material : m_materials) {
        if (material->isDirtyChildren()) {
            isSceneDirty = true;
            return true;
        }
    }


    return false;
}

void RenderData::unDirtyScene() {
    for (auto& obj : m_objects)
        obj->undirtyChildren();

    for (auto& light : m_lights)
        light->undirtyChildren();

    for (auto& cam : m_cameras)
        cam->undirtyChildren();

    for (auto& material : m_materials)
        material->undirtyChildren();

}


void RenderData::removeObject(MoveObject* obj) {
    if (!obj)
        return;
    switch (obj->getType()) {

        case MoveObject::objectType::OBJECT : {

            for (uint i = 0; i < m_objects.size(); ++i) {
                Object* currObj = m_objects[i].get();
                if (currObj->getID() == obj->getID()) {

                    for (uint j = 0; j < currObj->getMeshCount(); ++j) {
                        Mesh* currMesh = currObj->getMeshAt(j);
                        m_raytraceContext.removeObj(currMesh);
                        currObj->removeMeshAt(j);
                        m_raytraceContext.remapReferences(obj);
                    }

                    m_objects.erase(m_objects.begin()+i);
                    break;
                }
            }

        }break;

        case MoveObject::objectType::MESH : {

            for (uint i = 0; i < m_objects.size(); ++i) {
                Object* object = m_objects[i].get();
                for (uint j = 0; j < object->getMeshCount(); ++j) {
                    if (object->getMeshAt(j)->getID() == obj->getID()) {
                        m_raytraceContext.removeObj(object->getMeshAt(j));
                        object->removeMeshAt(j);
                        m_raytraceContext.remapReferences(object);
                        break;
                    }
                }
                if (object->getMeshCount() == 0) {
                    m_objects.erase(m_objects.begin()+i);
                    break;
                }
            }

        }break;

        case MoveObject::objectType::LIGHT : {

            for (uint i = 0; i < m_lights.size(); ++i) {
                if (m_lights[i]->getID() == obj->getID()) {
                    m_lights.erase(m_lights.begin()+i);
                    break;
                }
            }

        }break;

        case MoveObject::objectType::CAMERA : {

            if (m_cameras.size() == 1) {
                LOG_WARNING("Move need at least one camera to render. If you want remove this camera, please, create another one before.")
                return;
            }

            for (uint i = 0; i < m_cameras.size(); ++i) {
                if (m_cameras[i].get()->getID() == obj->getID()) {
                    m_cameras.erase(m_cameras.begin()+i);
                    break;
                }
            }

        }break;

        case MoveObject::objectType::MATERIAL : {
            for (uint i = 0; i < m_materials.size(); ++i) {
                if (m_materials[i]->getID() == obj->getID()) {
                    nullifyMaterial((Material*)obj);
                    m_materials.erase(m_materials.begin()+i);
                    break;
                }
            }
        }break;

    }

}


void RenderData::nullifyMaterial(Material* mat) {

    for (uint i = 0; i < m_objects.size(); ++i) {
        Object* currObj = m_objects[i].get();
        for (uint j = 0; j < currObj->getMeshCount(); ++j) {
            Mesh* currMesh = currObj->getMeshAt(j);
            if (currMesh->getMaterial()->getID() == mat->getID())
                currMesh->setMaterial(nullptr);
        }
    }

}

MoveObjectID RenderData::allocID() {
    MoveObjectID outID = m_idCount;
    ++m_idCount;
    return outID;
}

void RenderData::setIDs(MoveObject* obj) {

    if (!obj)
        return;

    obj->setID(allocID());
    for (uint i = 0; i < obj->getChildCount(); ++i) {
        setIDs(obj->getChild(i));
    }

}