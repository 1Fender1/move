//
// Created by jb on 29/02/2020.
//

#ifndef MOVE_RENDERDATA_HPP
#define MOVE_RENDERDATA_HPP

#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Modeling/Mesh/Object.hpp>
#include <Engine/Rendering/Light/Lights.hpp>
#include <Engine/Camera/Cameras.hpp>
#include <Engine/Rendering/Material/Materials.hpp>
#include <Engine/Rendering/Texture/Texture.hpp>
#include <Engine/Rendering/RayTracing/ContextSceneEmbree.hpp>
#include <Engine/Shader/Shader.hpp>
#include <memory>


class RenderData {

public :

    RenderData();
    ~RenderData();

    Object* addObject(Object& obj);
    Light* addObject(Light& obj);
    Material* addObject(Material& obj);
    Texture* addObject(Texture& obj);
    Camera* addObject(Camera& obj);

    Shader* addShader(Shader& shader);

    void clearScene();

    void clearObjects();
    void clearLights();
    void clearMaterials();
    void clearCameras();
    void clearTextures();
    void clearShaders();

    void removeObject(MoveObject* obj);

    void clearObject(uint);
    void clearMaterial(uint);
    void clearLight(uint);
    void clearCamera(uint);

    Object* getObject(uint);
    Light* getLight(uint);
    Material* getMaterial(uint);
    Camera* getCamera(uint);
    Texture* getTexture(uint);
    Shader* getShader(uint);

    Object* getObject(String);
    Light* getLight(String);
    Material* getMaterial(String);
    Camera* getCamera(String);
    Texture* getTexture(String);
    Shader* getShader(String);

    bool isExistMaterial(Material* mat);
    bool isExistTexture(Texture* texture);
    bool isExistTexture(std::string name);

    uint getObjCount();
    uint getLightCount();
    uint getMaterialCount();
    uint getCameraCount();
    uint getTextureCount();
    uint getShaderCount();

    uint getTextureID(std::string name);

    ContextSceneEmbree& getRaytracerContext();

    std::vector<Light_ptr>* getLights();
    std::vector<Object_ptr>* getObjects();

    bool isDirtyScene();
    void unDirtyScene();


private :

    //Set at null all mesh material with mat ref;
    void nullifyMaterial(Material* mat);
    MoveObjectID allocID();
    void setIDs(MoveObject* obj);


    std::vector<Object_ptr> m_objects;

    std::vector<Light_ptr> m_lights;

    std::vector<Material_ptr> m_materials;

    std::vector<Texture_ptr> m_textures;

    std::vector<Camera_ptr> m_cameras;

    std::vector<Shader_ptr> m_shaders;

    ContextSceneEmbree m_raytraceContext;

    bool isSceneDirty = false;

    MoveObjectID m_idCount = 0;

};


#endif //MOVE_RENDERDATA_HPP
