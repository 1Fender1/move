#include "Scene.hpp"
#include <iostream>

Scene::Scene(int width, int height) : m_width(width), m_height(height), m_drawfill(true) {

    glEnable(GL_DEPTH_TEST);
   // glViewport(0, 0, m_width, m_height);
    m_gizmo = new Gizmo();

    m_camera = nullptr;
}

Scene::~Scene() {
    if (m_gizmo)
        delete m_gizmo;

}

void Scene::resize(int width, int height) {
    m_width = width;
    m_height = height;
   //glViewport(0, 0, m_width, m_height);
}

void Scene::unselectScene() {

    for (uint i = 0; i < m_renderData.getObjCount(); ++i) {
        Object& obj = *m_renderData.getObject(i);
        obj.setSelected(false);
        for (uint j = 0; j < obj.getMeshCount(); ++j) {
            Mesh& mesh = *obj.getMeshAt(j);
            mesh.setSelected(false);
        }
    }
    for (uint i = 0; i < m_renderData.getCameraCount(); ++i) {
        Camera& cam = *m_renderData.getCamera(i);
        cam.setSelected(false);
    }

    for (uint i = 0; i < m_renderData.getLightCount(); ++i) {
        Light& light = *m_renderData.getLight(i);
        light.setSelected(false);
    }
}

uint Scene::draw() {
    glClearColor(0.2f, 0.3f, 0.3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    if (m_gizmo)
        m_gizmo->setObjectRef(m_selectedObject);

    return 0;
}

void Scene::intializeShaders() {

    Shader::ShaderNames files;
    std::string shaderName;
    Shader* shader = nullptr;

    files.vextexShader = Shader::vertexDir + "globalShading.vs";
    files.fragmentShader = Shader::fragmentDir + "blinnLighting.fs";
    shaderName = "Default shader";
    Shader blinnPhong =  Shader(files, shaderName);
    shader = m_renderData.addShader(blinnPhong);
    m_renderer->setMainShader(shader);

    files.vextexShader = Shader::vertexDir + "renderToTexture.vs";
    files.fragmentShader = Shader::fragmentDir + "toneMapping.fs";
    shaderName = "Render to Texture shader";
    Shader toneMapping = Shader(files, shaderName);
    shader = m_renderData.addShader(toneMapping);
    m_renderer->setFrameShader(shader);
    m_raytracerRenderer->setShader(shader);

    files.vextexShader = Shader::vertexDir + "GBuffer.vs";
    files.fragmentShader = Shader::fragmentDir + "GBuffer.fs";
    shaderName = "GBuffer shader";
    Shader GBufferShader = Shader(files, shaderName);
    shader = m_renderData.addShader(GBufferShader);
    m_renderer->setGBufShader(shader);

    files.vextexShader = Shader::vertexDir + "renderToTexture.vs";
    files.fragmentShader = Shader::fragmentDir + "SSAO.fs";
    shaderName = "SSAO shader";
    Shader ssao = Shader(files, shaderName);
    shader = m_renderData.addShader(ssao);
    m_renderer->setSSAOShader(shader);

    files.vextexShader = Shader::vertexDir + "renderToTexture.vs";
    files.fragmentShader = Shader::fragmentDir + "SSAOBlur.fs";
    shaderName = "SSAO blur shader";
    Shader ssaoBlur = Shader(files, shaderName);
    shader = m_renderData.addShader(ssaoBlur);
    m_renderer->setSSAOBlurBuffer(shader);

    files.vextexShader = Shader::vertexDir + "renderToTexture.vs";
    files.fragmentShader = Shader::fragmentDir + "FXAA.fs";
    shaderName = "FXAA shader";
    Shader fxaa = Shader(files, shaderName);
    shader = m_renderData.addShader(fxaa);
    m_renderer->setFXAAShader(shader);

    files.vextexShader = Shader::vertexDir + "cubeMap.vs";
    files.fragmentShader = Shader::fragmentDir + "cubeMap.fs";
    shaderName = "Cube map shader";
    Shader cubeMap = Shader(files, shaderName);
    shader = m_renderData.addShader(cubeMap);
    m_renderer->setCubeMapShader(shader);

    files.vextexShader = Shader::vertexDir + "SMDepth.vs";
    files.fragmentShader = Shader::fragmentDir + "SMDepth.fs";
    shaderName = "Depth buffer shader";
    Shader depthBuffer = Shader(files, shaderName);
    shader = m_renderData.addShader(depthBuffer);
    m_renderer->setDepthMapStandardShader(shader);


}

void Scene::mouseclick(int , float , float ) {

}

void Scene::mouseDoubleClick(int, float, float) {}

void Scene::mousemove(float , float, float) {

}

void Scene::mouseRelease(int, float, float) {}

void Scene::keyboardmove(int , double ) {

}

void Scene::wheelScroll(int ) {
}

bool Scene::keyboard(unsigned char ) {
    return false;
}

void Scene::toggledrawmode() {
    m_drawfill = !m_drawfill;
}

void Scene::loadFile(std::string) {
}

void Scene::reloadShaders(uint i) {

    if (i == uint(-1)) {
        for (uint j = 0; j < m_renderData.getShaderCount(); ++j)
            m_renderData.getShader(j)->reload();

        for (uint j = 0; j < m_renderData.getMaterialCount(); ++j)
            m_renderData.getMaterial(j)->reloadShader();

        for (uint j = 0; j < m_renderData.getObjCount(); ++j) {
            Object& obj = *m_renderData.getObject(j);
            for (uint k = 0; k < obj.getMeshCount(); ++k) {
                Mesh& mesh = *obj.getMeshAt(k);
                if (mesh.isDefaultMaterial()) {
                    mesh.getMaterial()->reloadShader();
                }
            }
        }
    } else {
        if (i > m_renderData.getShaderCount()-1)
            return;
        m_renderData.getShader(i)->reload();
    }

}


void Scene::setCamera(Camera* cam) {

    m_camera = cam;
    m_camera->resize(m_width, m_height);

    //m_camera.reset(new CameraOrbit(glm::vec3(0.f, 0.f, 4.f), m_width, m_height));

    m_renderer->m_camera = m_camera;
    if (m_gizmo)
        m_gizmo->setCamera(m_camera);
    if (m_raytracerRenderer) {
        m_raytracerRenderer->m_camera = m_camera;
        m_raytracerRenderer->toRefresh();
        m_raytracerRenderer->refresh();
    }

}

Camera* Scene::getCamera() {
    return m_camera;
}

void Scene::setIsForwardDraw(bool isDraw) {
        m_isForward = isDraw;
}

void Scene::setIsPathTracerDraw(bool isDraw) {
    m_isRaytrace = isDraw;
}

void Scene::setSelectedObject(MoveObject* obj) {
    m_selectedObject = obj;
    if (obj)
        obj->setSelected(true);
}

MoveObject* Scene::getSelectedObject() {
    return m_selectedObject;
}

ForwardRenderer* Scene::getGLRenderer() {
    return m_renderer;
}
RaytracerRenderer* Scene::getPTRenderer() {
    return m_raytracerRenderer;
}

Gizmo* Scene::getGizmo() {
    return m_gizmo;
}