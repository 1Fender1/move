#ifndef SCENE_HPP
#define SCENE_HPP


#include <vector>
#include <string>
#include <Core/libsGL.hpp>
#include <src/Ui/Gizmos/Gizmo.hpp>
#include <Engine/Rendering/Renderer.hpp>
#include <Engine/Rendering/RaytracerRenderer.hpp>
#include <Engine/Rendering/ForwardRenderer.hpp>
#include <Engine/Scenes/RenderData.hpp>

/*
 * Simple class to manage a scene
 */
class Scene {

public:
    explicit Scene(int width, int height);
    virtual ~Scene();

    virtual void resize(int width, int height);
    virtual uint draw();

    virtual void mouseclick(int button, float xpos, float ypos);
    virtual void mousemove(float xpos, float ypos, float deltaTime);
    virtual void keyboardmove(int key, double time);
    virtual bool keyboard(unsigned char k);
    virtual void wheelScroll(int delta);
    virtual void mouseDoubleClick(int button, float xpos, float ypos);
    virtual void mouseRelease(int button, float xpos, float ypos);

    Gizmo* getGizmo();

    void toggledrawmode();

    virtual void loadFile(std::string name);

    void updateGraphSceneUI();

    RenderData* getRenderData() {
        return &m_renderData;
    }

    void reloadShaders(uint i = -1);

    uint getTexForward() {
        return m_forwardTexID;
    }

    uint getTexPathTracer() {
        return m_raytracerTexID;
    }

    void setIsForwardDraw(bool);
    void setIsPathTracerDraw(bool);

    void setSelectedObject(MoveObject* obj);
    MoveObject* getSelectedObject();

    ForwardRenderer* getGLRenderer();
    RaytracerRenderer* getPTRenderer();

    void unselectScene();

    void setCamera(Camera* cam);
    Camera* getCamera();



protected:

    void intializeShaders();

    // Shader program for rendering
    ForwardRenderer* m_renderer = nullptr;
    RaytracerRenderer* m_raytracerRenderer = nullptr;

    RenderData m_renderData;

    // Width and heigth of the viewport
    int m_width;
    int m_height;
    bool m_drawfill;
    Gizmo *m_gizmo = nullptr;

    uint m_forwardTexID = 0;
    uint m_raytracerTexID = 0;


    bool m_isRaytrace = false;
    bool m_isForward = false;

    MoveObject* m_selectedObject = nullptr;

    // Main camera
    Camera* m_camera;

};


#endif // SCENE_H
