#include "Shader.hpp"
#include <Core/glAssert.hpp>

const std::string Shader::vertexDir = "../Shaders/VertexShaders/";
const std::string Shader::fragmentDir = "../Shaders/FragmentShaders/";
const std::string Shader::tcsDir = "../Shaders/VertexShaders/TessellationControlShaders/";
const std::string Shader::tesDir = "../Shaders/VertexShaders/TessellationEvaluationShaders/";
const std::string Shader::geometryDir = "../Shaders/VertexShaders/GeometryShaders/";
const std::string Shader::utilsDir = "../Shaders/Utils/";


Shader::Shader(const ShaderNames& files, const std::string& stringID) {

    //m_files = files;
    m_files.vextexShader = files.vextexShader;
    m_files.fragmentShader = files.fragmentShader;
    m_files.geometryShader = files.geometryShader;
    m_files.tessellationControlShader = files.tessellationControlShader;
    m_files.tessellationEvaluationShader = files.tessellationEvaluationShader;

    m_program = NO_ID;
    m_vertexShaderID = NO_ID;
    m_fragmentShaderID = NO_ID;
    m_geometryShaderID = NO_ID;
    m_tcsShaderID = NO_ID;
    m_tesShaderID = NO_ID;
    m_stringID = stringID;

}

Shader::Shader(const Shader& shader) {

    m_files = shader.m_files;

    //m_program = shader.m_program;
    /*
    m_vertexShaderID = shader.m_vertexShaderID;
    m_fragmentShaderID = shader.m_fragmentShaderID;
    m_geometryShaderID = shader.m_geometryShaderID;
    m_tcsShaderID = shader.m_tcsShaderID;
    m_tesShaderID = shader.m_tesShaderID;
     */
    m_program = NO_ID;
    m_vertexShaderID = NO_ID;
    m_fragmentShaderID = NO_ID;
    m_geometryShaderID = NO_ID;
    m_tcsShaderID = NO_ID;
    m_tesShaderID = NO_ID;
    m_stringID = shader.m_stringID;


}

void Shader::reload() {

    deleteProgram();

    createProgram();

    GL_CHECK_ERROR
}

Shader::~Shader() {

    deleteProgram();

    GL_CHECK_ERROR
}
void Shader::use() {
    //std::cout << m_stringID << " : \n" << m_files.vextexShader << "\n" << m_files.fragmentShader << std::endl;

    if (!m_isInitShader) {
        createProgram();
        m_isInitShader = true;
    }

    glUseProgram(m_program);
}

std::string Shader::getName() {
    return m_stringID;
}

GLuint Shader::compileSource(const char* source , Shader::ShaderType type) {

    if (source == nullptr)
        return NO_ID;

    GLuint shaderTYPE;
    std::string typeName = "";
    std::string shaderName = "";
    switch (type) {
        case Shader::ShaderType::VERTEX : {
                shaderTYPE = GL_VERTEX_SHADER;
                typeName = "VERTEX_SHADER";
                shaderName = m_files.vextexShader;
            }
            break;
        case Shader::ShaderType::FRAGMENT : {
                shaderTYPE = GL_FRAGMENT_SHADER;
                typeName = "FRAGMENT_SHADER";
                shaderName = m_files.fragmentShader;
        }
            break;
        case Shader::ShaderType::GEOMETRY : {
                shaderTYPE = GL_GEOMETRY_SHADER;
                typeName = "GEOMETRY_SHADER";
                shaderName = m_files.geometryShader;
        }
            break;
        case Shader::ShaderType::TCS : {
                shaderTYPE = GL_TESS_CONTROL_SHADER;
                typeName = "TESS_CONTROL_SHADER";
                shaderName = m_files.tessellationControlShader;
        }
            break;
        case Shader::ShaderType::TES : {
                shaderTYPE = GL_TESS_EVALUATION_SHADER;
                typeName = "TESS_EVALUATION_SHADER";
                shaderName = m_files.tessellationEvaluationShader;
        }
            break;
        default :
            std::cerr << "Error, shader type unknow for compilation" << std::endl;
            return NO_ID;
    }

    if (shaderName.empty())
        return NO_ID;

    GLint success;
    GLuint shaderID = glCreateShader(shaderTYPE);

    glShaderSource(shaderID, 1, &source, NULL);
    glCompileShader(shaderID);
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

    if(success == GL_FALSE) {
        GLint logSize = 0;
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logSize);
        GLchar* infoLog = (GLchar*) malloc(sizeof(GLchar)*logSize);
        glGetShaderInfoLog(shaderID, logSize, &logSize, infoLog);
        std::cerr << "ERROR::" + typeName + "::COMPILATION_FAILED : " << shaderName << std::endl << infoLog << std::endl;

        free(infoLog);
    }

    return shaderID;
}

void Shader::deleteProgram() {
    glUseProgram(0);

    if (m_vertexShaderID != NO_ID)
        glDeleteShader(m_vertexShaderID);
    if (m_fragmentShaderID != NO_ID)
        glDeleteShader(m_fragmentShaderID);
    if (m_geometryShaderID != NO_ID)
        glDeleteShader(m_geometryShaderID);
    if (m_tcsShaderID != NO_ID)
        glDeleteShader(m_tcsShaderID);
    if (m_tesShaderID != NO_ID)
        glDeleteShader(m_tesShaderID);

    if (m_program != NO_ID)
        glDeleteProgram(m_program);
}

void Shader::createProgram() {

    GLint success;
    ShaderLoader loader = ShaderLoader();

    String vertexSource = loader.loadShader(m_files.vextexShader);
    String fragmentSource = loader.loadShader(m_files.fragmentShader);

    m_vertexShaderID = compileSource(vertexSource.c_str(), Shader::ShaderType::VERTEX);
    m_fragmentShaderID = compileSource(fragmentSource.c_str(), Shader::ShaderType::FRAGMENT);
    if (m_vertexShaderID == NO_ID || m_fragmentShaderID == NO_ID) {
        std::cerr << "No vertex shader or fragment shader set" << std::endl;
        return;
    }

    String geometrySource = loader.loadShader(m_files.geometryShader);
    String tcsSource = loader.loadShader(m_files.tessellationControlShader);
    String tesSource = loader.loadShader(m_files.tessellationEvaluationShader);

    m_geometryShaderID = compileSource(geometrySource.c_str(), Shader::ShaderType::GEOMETRY);
    m_tcsShaderID = compileSource(tcsSource.c_str(), Shader::ShaderType::TCS);
    m_tesShaderID = compileSource(tesSource.c_str(), Shader::ShaderType::TES);

    m_program = glCreateProgram();

    glAttachShader(m_program, m_vertexShaderID);
    glAttachShader(m_program, m_fragmentShaderID);
    if (m_geometryShaderID != NO_ID)
        glAttachShader(m_program, m_geometryShaderID);
    if (m_tcsShaderID != NO_ID)
        glAttachShader(m_program, m_tcsShaderID);
    if (m_tesShaderID != NO_ID)
        glAttachShader(m_program, m_tesShaderID);


    glLinkProgram(m_program);

    glGetProgramiv(m_program, GL_LINK_STATUS, &success);
    if(success == GL_FALSE) {
        GLint logSize = 0;
        glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logSize);
        GLchar* infoLog = (GLchar*) malloc(sizeof(GLchar)*logSize);
        glGetProgramInfoLog(m_program, logSize, NULL, infoLog);
        std::cerr << "ERROR::SHADER::LINK_FAILED\n" << infoLog << std::endl;
        free(infoLog);
    }

    GL_CHECK_ERROR

    glDetachShader(m_program, m_vertexShaderID);
    glDetachShader(m_program, m_fragmentShaderID);
    if (m_geometryShaderID != NO_ID)
        glDetachShader(m_program, m_geometryShaderID);
    if (m_tcsShaderID != NO_ID)
        glDetachShader(m_program, m_tcsShaderID);
    if (m_tesShaderID != NO_ID)
        glDetachShader(m_program, m_tesShaderID);


}

void Shader::setBool(std::string name, bool val) const {
    int shaderValue = val ? 1 : 0;
    glUniform1i(glGetUniformLocation(m_program, name.c_str()), shaderValue);
}

void Shader::setFloat(std::string name, float val) const {
    glUniform1f(glGetUniformLocation(m_program, name.c_str()), val);
}

void Shader::setFloatV2(std::string name, const glm::vec2 &vec) const {
    glUniform2fv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setFloatV3(std::string name, const glm::vec3 &vec) const {
    glUniform3fv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setFloatV4(std::string name, const glm::vec4 &vec) const {
    glUniform4fv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setInt(std::string name, int val) const {
    glUniform1i(glGetUniformLocation(m_program, name.c_str()), val);
}

void Shader::setIntV2(std::string name, const glm::ivec2 &vec) const {
    glUniform2iv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setIntV3(std::string name, const glm::ivec3 &vec) const {
    glUniform3iv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setIntV4(std::string name, const glm::ivec4 &vec) const {
    glUniform4iv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setMat2(std::string name, const glm::mat2 &mat) const {
    glUniformMatrix2fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat3(std::string name, const glm::mat3 &mat) const {
    glUniformMatrix3fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat4(std::string name, const glm::mat4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

