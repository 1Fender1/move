#ifndef SHADER_H
#define SHADER_H

#include <cstdio>
#include <string>
#include <Core/libsGL.hpp>
//#include <Core/Math/Math.hpp>
#include <glm/glm.hpp>
#include <Engine/Shader/ShaderLoader.hpp>

class Shader {

public:

    static const std::string vertexDir;
    static const std::string fragmentDir;
    static const std::string tcsDir;
    static const std::string tesDir;
    static const std::string geometryDir;
    static const std::string utilsDir;
    static const GLuint NO_ID = GLuint(-1);


    struct ShaderNames {
        std::string vextexShader = ""; //.vs files
        std::string fragmentShader = ""; //.fs files
        std::string tessellationControlShader = ""; //.tcs files
        std::string tessellationEvaluationShader = ""; //.tes tiles
        std::string geometryShader = ""; //.gs files
        std::string computeShader = ""; //.cs files

        ShaderNames() {
            vextexShader = std::string("");
            fragmentShader = std::string("");
            tessellationControlShader = std::string("");
            tessellationEvaluationShader = std::string("");
            geometryShader = std::string("");
            computeShader = std::string("");
        }

        ShaderNames& operator=(const ShaderNames& sn) {
            if (&sn == this)
                return *this;

            vextexShader = sn.vextexShader;
            fragmentShader = sn.fragmentShader;
            tessellationControlShader = sn.tessellationControlShader;
            tessellationEvaluationShader = sn.tessellationEvaluationShader;
            geometryShader = sn.geometryShader;
            computeShader = sn.computeShader;
            return *this;
        }

    };

    Shader() {
        m_files = ShaderNames();
        m_program = NO_ID;
        m_vertexShaderID = NO_ID;
        m_fragmentShaderID = NO_ID;
        m_geometryShaderID = NO_ID;
        m_tcsShaderID = NO_ID;
        m_tesShaderID = NO_ID;
        m_computeShaderID = NO_ID;
        m_stringID = "";
    };

    Shader(const ShaderNames& files, const std::string& stringID);


    Shader(const Shader& shader);

    ~Shader();

    Shader& operator=(const Shader& shader) {

        if (&shader == this)
            return *this;

        m_files = shader.m_files;
        m_stringID = shader.m_stringID;

        return *this;
    }


    void reload();
    void use();

    std::string getName();

    /*
     *
     *  Uniform setting
     *
     *
     */

    void setBool(std::string name, bool val) const;
    void setFloat(std::string name, float val) const;
    void setFloatV2(std::string name, const glm::vec2 &vec) const;
    void setFloatV3(std::string name, const glm::vec3 &vec) const;
    void setFloatV4(std::string name, const glm::vec4 &vec) const;

    void setInt(std::string name, int val) const;
    void setIntV2(std::string name, const glm::ivec2 &vec) const;
    void setIntV3(std::string name, const glm::ivec3 &vec) const;
    void setIntV4(std::string name, const glm::ivec4 &vec) const;

    void setMat2(std::string name, const glm::mat2 &mat) const;
    void setMat3(std::string name, const glm::mat3 &mat) const;
    void setMat4(std::string name, const glm::mat4 &mat) const;


private :

    enum ShaderType {
        VERTEX,
        FRAGMENT,
        GEOMETRY,
        TCS,
        TES,
        COMPUTE
    };

    GLuint compileSource(const char* source , ShaderType type);
    void createProgram();
    void deleteProgram();

    GLuint m_program;

    GLuint m_vertexShaderID;
    GLuint m_fragmentShaderID;
    GLuint m_geometryShaderID;
    GLuint m_tcsShaderID;
    GLuint m_tesShaderID;
    GLuint m_computeShaderID;

    ShaderNames m_files;
    std::string m_stringID;

    bool m_isInitShader = false;

};

typedef std::unique_ptr<Shader> Shader_ptr;


#endif // SHADER_H
