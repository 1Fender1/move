//
// Created by jb on 31/01/18.
//

#include "ShaderLoader.hpp"


#include <istream>
#include <fstream>
#include <sstream>
#include <regex>
#include <cstdlib>
#include <climits>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <algorithm>
#include <Core/System/Logger.hpp>

ShaderLoader::ShaderLoader() {
}

String ShaderLoader::loadShader(std::string name = "") {

    String nullStr = String("");

    std::string shader = "";
    if (name.empty()) {
        //std::cerr << "ERROR::LOADING SHADER : NO_NAME" << std::endl;
        return nullStr;
    }

    // std::cout << "Loading of " << name << std::endl;
    std::string path = std::string("./");
    std::ifstream shaderFile(path.append(name), std::ios::in);
    std::string ligne = "";
    if (shaderFile) {
        while (std::getline(shaderFile, ligne)) {
            shader.append(ligne).append(1, '\n');
        }
        shaderFile.close();
        shader.append(1, '\0');
    } else {
        std::cerr << "ERROR::LOADING SHADER : CANNOT OPEN FILE" << std::endl;
        return nullStr;
    }
    char *out = new char[shader.size()];
    for (uint i = 0; i < shader.size(); i++)
        out[i] = shader[i];

    std::string outTemp = std::string(out);
    outTemp = preprocessIncludes(outTemp);
    outTemp.append(1, '\0');
    if (out) {
        delete [] out;
        out = new char[outTemp.size()];
    }

    for (unsigned int i = 0; i < outTemp.size(); i++)
        out[i] = outTemp[i];

    String strOut = String(out);

    delete [] out;

    return strOut;
}


std::string ShaderLoader::readFile(std::string& name) {


    std::string shader = "";
    std::string path = "";
    std::ifstream shaderFile(path.append(name), std::ios::in);
    std::string ligne = "";
    if (shaderFile) {
        while (std::getline(shaderFile, ligne)) {
            shader.append(ligne).append(1, '\n');
        }
        shaderFile.close();
    }
    else {
        std::cerr << "ERROR::readFile cannot find file  " << name << std::endl;
        return "";
    }

    return shader;
}


std::string ShaderLoader::preprocessIncludes(std::string& shaderName) {
    shaderName.erase(std::remove(shaderName.begin(), shaderName.end(), '\0'), shaderName.end());

    std::string result = "";
    std::vector<std::string> finalStrings;
    std::vector<std::string> shaderLines;

    std::istringstream f(shaderName);
    std::string s;
    while (getline(f, s, '\n')) {
        shaderLines.push_back(s);
    }

    finalStrings.reserve(shaderLines.size());

    static const std::regex reg("^[ ]*#[ ]*include[ ]+[\"<](.*)[\">].*");

    for (const auto& l : shaderLines) {
        std::string line = l;
        std::smatch match;
        if (std::regex_search(l, match, reg)) {

            char full[10000];
            std::string fileName = "" + match[1].str();
            fileName = shaderDir + fileName;
            char* res = realpath(fileName.c_str(), full);
            if (res == nullptr) {
                LOG_ERROR("Absolute path cannot be found : %n", fileName)
            }
            std::string fullPath = std::string(full);
            std::string includeShader = readFile(fullPath);

            if (includeShader.empty()) {
                LOG_ERROR("Cannot open included file : %n", match[1].str())
            } else {
                line = line;
                line = preprocessIncludes(includeShader);
            }
        }

        finalStrings.push_back(line);
    }

    for (const auto& l : finalStrings) {
        result.append(l);
        result.append(1, '\n');
    }

    return result;
}
