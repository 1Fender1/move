//
// Created by jb on 31/01/18.
//

#ifndef HELLOOPENGL_SHADERLOADER_HPP
#define HELLOOPENGL_SHADERLOADER_HPP
#include <iostream>
#include <fstream>
#include <Core/Types.hpp>

class ShaderLoader {

public:
    ShaderLoader();
    //Take file name
    String loadShader(std::string name);


    const std::string FRAGMENT_SHADER = "fragmentShader.glsl";
    const std::string FRAGMENT_SHADER_NORMAL = "fragmentShaderNormal.glsl";
    const std::string VERTEX_SHADER = "vertexShader.glsl";


private :
    //Permit to include files to glsl with #include "shaderToInclude.glsl"
    std::string preprocessIncludes(std::string& shaderName);
    std::string readFile(std::string& name);
    const std::string shaderDir = "../Shaders/";



};


#endif //HELLOOPENGL_SHADERLOADER_H
