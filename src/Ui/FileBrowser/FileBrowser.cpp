

#include <Ui/FileBrowser/FileBrowser.hpp>
#include <Ui/imgui/imgui.h>
#include <Ui/Font/ShortCutIcons.hpp>
#include <Core/System/Logger.hpp>

FileBrowser::FileBrowser() {
    m_windowFlags = ImGuiWindowFlags_None | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoCollapse;
    m_selectableFlags = ImGuiSelectableFlags_None | ImGuiSelectableFlags_AllowDoubleClick;// | ImGuiSelectableFlags_AllowItemOverlap;
}


void FileBrowser::open() {

    m_isOpen = true;

}

void FileBrowser::show() {
    m_userValidPath.clear();
    if (!m_isOpen)
        return;

    if (ImGui::Begin("File browser", &m_isOpen, m_windowFlags)) {

        if (ImGui::IsKeyPressed(GLFW_KEY_BACKSPACE)) {
            goBack();
        }
        if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE)) {
            close();
            return;
        }

        drawPath();

        ImGui::Separator();

        drawMenuBarUp();

        ImGui::Separator();

        drawList();

        ImGui::Separator();

        drawMenuBarDown();

        ImGui::End();
    }



}

void FileBrowser::close() {
    m_isOpen = false;
}


void FileBrowser::drawPath() {
    std::vector<String> pathTokens;
    Core::String::splitPath(m_parser.getCurrentPath(), pathTokens);

    //remove empty tokens, corresponds to //
    for (uint i = 0; i < pathTokens.size(); ++i) {
        if (pathTokens[i].empty()) {
            pathTokens.erase(pathTokens.begin() + i);
            i--;
        }
    }

    String pathLabel = Icon::edit + "##FileBrowserEditPath";
    if (ImGui::Button(pathLabel.c_str())) {
        m_isPathEditMode = true;
    }

    ImGui::SameLine(0.f, 16.f);


    if (!m_isPathEditMode) {

        for (uint i = 0; i < pathTokens.size(); ++i) {
            if (ImGui::Button(pathTokens[i].c_str())) {

                String newPath = generatePath(pathTokens, i);
                m_currentPath = newPath;
                m_parser.setCurrentPath(newPath);
                break;
            }
            ImGui::SameLine();
            ImGui::Text("/");
            if (i < pathTokens.size() - 1)
                ImGui::SameLine();
        }



    }
    else {
        String inputPathLabel = "##PathTextEdit";
        String generatedPath = generatePath(pathTokens, pathTokens.size());
        char input[1024];
        ImGuiInputTextFlags flags = ImGuiInputTextFlags_AutoSelectAll | ImGuiInputTextFlags_EnterReturnsTrue;
        for (uint i = 0; i < generatedPath.size(); ++i) {
            input[i] = generatedPath[i];
        }
        input[generatedPath.size()] = '\0';
        if (ImGui::InputText(inputPathLabel.c_str(), &input[0], 1024, flags)) {
            String editedPath = String(input);
            m_currentPath = editedPath;
            m_parser.setCurrentPath(m_currentPath);
            m_isPathEditMode = false;
        }
        ImGui::SetItemDefaultFocus();

    }

    ImGui::SameLine();


}
void FileBrowser::drawMenuBarUp() {
    //TODO not finished, for proto
    char str[150];
    if (!m_filter.empty()) {
        for (uint i = 0; i < m_filter.size(); ++i) {
            str[i] = m_filter[i];
        }
        str[m_filter.size()] = '\0';
    }
    else {
        str[0] = '\0';
    }

    String fileBrowserSearchLabel = Icon::search + "## fileBrowserSearch";
    if (ImGui::InputText(fileBrowserSearchLabel.c_str(), str, 150)) {
        m_filter = String(str);
    }

    ImGui::SameLine();


    String homeButtonLabel = Icon::home + "##fileBrowserHome";
    if (ImGui::Button(homeButtonLabel.c_str())) {
        m_parser.setCurrentPath(m_parser.getHome());
        m_currentPath = m_parser.getCurrentPath();
    }

    ImGui::SameLine();
    String leftArrowButtonLabel = Icon::arrowLeft + "##fileBrowserLeftArrow";

    if (ImGui::Button(leftArrowButtonLabel.c_str())) {
        goBack();
    }

    ImGui::SameLine();
    String rightArrowButtonLabel = Icon::arrowRight + "##fileBrowserRightArrow";
    if (ImGui::Button(rightArrowButtonLabel.c_str())) {
        goFront();
    }

    String hideButtonLabel;
    ImGui::SameLine();
    if (m_isHiden)
        hideButtonLabel += Icon::eyeSlash + "##fileBrowserHideFiles";
    else
        hideButtonLabel += Icon::eyeOpen + "##fileBrowserHideFiles";

    if (ImGui::Button(hideButtonLabel.c_str())) {
        m_isHiden = !m_isHiden;
    }


}


void FileBrowser::drawMenuBarDown() {

    if (ImGui::Button("Cancel")) {
        m_curentSelectedFile = nullptr;
        m_currentFileList.clear();
        m_currentPath = "";
        m_userValidPath.clear();
        close();
    }
    ImGui::SameLine();
    if (ImGui::Button("Valid")) {
       // if (!m_curentSelectedFile)
         //   return;
        onValidButton();
    }

}


void FileBrowser::drawList() {

    bool newPath = m_currentPath != m_parser.getCurrentPath();
    if (newPath) {
        m_currentPath = m_parser.getCurrentPath();
        m_currentFileList.clear();
    }

    ImVec2 boxSize = ImVec2(ImGui::GetContentRegionAvail().x * (3.f/4.f), ImGui::GetContentRegionAvail().y * (3.f/4.f));

    bool isListBoxOpen = ImGui::ListBoxHeader("##Files list", boxSize);

    m_currentFileList.clear();
    m_parser.listCurrentPath(m_currentFileList);


    std::vector<File*> tempFiles;
    for (uint i = 0; i < m_currentFileList.size(); ++i)
        tempFiles.push_back(&m_currentFileList[i]);

    Core::File::filter(tempFiles, m_filter);

    for (uint i = 0; i < tempFiles.size(); ++i) {
        File &file = *tempFiles[i];
        String filename;
        if (file.type == FileType::Directory)
            filename = Icon::folder + " ";
        else
            filename = Icon::defaultFile + " ";
        filename += file.name;
        if ((!file.isHide) || (file.isHide && !m_isHiden)) {
            bool is_selected = m_curentSelectedFile && m_curentSelectedFile->name == file.name;
            ImGui::Selectable(filename.c_str(), &is_selected, m_selectableFlags);

            if (ImGui::IsItemHovered() && ImGui::IsMouseClicked(0)) {
                m_curentSelectedFile = tempFiles[i];
            }
            if (ImGui::IsItemHovered() && (ImGui::IsMouseDoubleClicked(0) || ImGui::IsKeyPressed(GLFW_KEY_ENTER))) {
                m_curentSelectedFile = tempFiles[i];
                onValidButton();
            }
        }
    }
    if (isListBoxOpen)
        ImGui::ListBoxFooter();
}

String FileBrowser::getUserPath() {
    return m_userValidPath;
}

String FileBrowser::generatePath(const std::vector<String>& pathTokens, uint index) {
    String newPath;
    for (uint j = 0; j <= index; ++j)
        newPath += "/" + pathTokens[j];

    return newPath;
}

void FileBrowser::onValidButton() {
    if (m_curentSelectedFile && m_curentSelectedFile->type != FileType::Directory) {
        m_userValidPath = m_curentSelectedFile->fullPath + m_curentSelectedFile->name;
        m_curentSelectedFile = nullptr;
        m_currentFileList.clear();
        //m_currentPath = "";
        //m_userValidPath.clear();

        close();
    } else {

        if (m_curentSelectedFile) {
            m_parser.setCurrentPath(m_curentSelectedFile->fullPath);
            m_currentPath = m_curentSelectedFile->fullPath;
        }
        m_curentSelectedFile = nullptr;
        m_currentFileList.clear();
        m_userValidPath.clear();
        m_userValidPath = "";
    }
    m_filter.clear();
}


void FileBrowser::goBack() {
    String newPath = m_currentPath;
    std::vector<String> splitedPath;
    Core::String::splitPath(newPath, splitedPath);
    if (splitedPath.empty() || (splitedPath[0].empty() && splitedPath.size() == 1)) {
        return;
    }
    newPath = generatePath(splitedPath, splitedPath.size()-2);
    LOG_DEBUG("go back, current : %n, new : %n", m_currentPath, newPath);
    m_frontPath.push_back(m_currentPath);
    m_currentPath = newPath;
    m_parser.setCurrentPath(m_currentPath);
}

void FileBrowser::goFront() {
    if (m_frontPath.empty())
        return;

    m_currentPath = m_frontPath.back();
    m_frontPath.erase(m_frontPath.end());
    m_parser.setCurrentPath(m_currentPath);
}