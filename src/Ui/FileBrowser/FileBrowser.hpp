//
// Created by jb on 02/04/2020.
//

#ifndef MOVE_FILEBROWSER_HPP
#define MOVE_FILEBROWSER_HPP

#include <Ui/libs_ui.hpp>
#include <Core/System/SystemParser.hpp>

class FileBrowser {

public :

    FileBrowser();

    void open();

    void show();

    void close();

    String getUserPath();



private :

    void drawPath();
    void drawMenuBarUp();
    void drawMenuBarDown();
    void drawList();

    void onValidButton();
    String generatePath(const std::vector<String>& pathTokens, uint index);

    //Call when return key is pressed or left arrow is clicked
    void goBack();
    void goFront();

    bool m_isOpen = false;
    bool m_isPathEditMode = false;
    bool m_isHiden = true;

    SystemParser m_parser;

    std::vector<File> m_currentFileList;
    std::vector<String> m_frontPath;

    String m_currentPath = "";
    String m_filter;

    File* m_curentSelectedFile = nullptr;

    String m_userValidPath;

    ImGuiWindowFlags m_windowFlags;
    ImGuiSelectableFlags m_selectableFlags;


};


#endif //MOVE_FILEBROWSER_HPP
