//
// Created by jb on 10/04/2020.
//

#ifndef MOVE_SHORTCUTICONS_HPP
#define MOVE_SHORTCUTICONS_HPP

#include <Ui/Font/IconsFontAF.hpp>

namespace Icon {

    static const String folder = ICON_FA_FOLDER;
    static const String defaultFile = ICON_FA_FILE;
    static const String home = ICON_FA_HOME;
    static const String search = ICON_FA_SEARCH;
    static const String edit = ICON_FA_EDIT;
    static const String arrowLeft = ICON_FA_ARROW_LEFT;
    static const String arrowRight = ICON_FA_ARROW_RIGHT;
    static const String eyeOpen = ICON_FA_EYE;
    static const String eyeSlash = ICON_FA_EYE_SLASH;
    static const String mouse_pointer = ICON_FA_MOUSE_POINTER;
    static const String translation = ICON_FA_ARROWS_ALT;
    static const String scale = ICON_FA_EXPAND_ALT;
    static const String rotation = ICON_FA_SYNC_ALT;

}


#endif //MOVE_SHORTCUTICONS_HPP
