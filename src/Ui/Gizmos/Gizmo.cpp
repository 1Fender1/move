//
// Created by jb on 29/08/2020.
//

#include "Gizmo.hpp"
#include <Core/Math/RayTracing/Primitives.hpp>
#include <Engine/Modeling/Geometry/BaseObjects.hpp>
#include <Engine/FileLoader/FileLoader.hpp>
#include <Core/System/Logger.hpp>

Gizmo::Gizmo() {
    Shader::ShaderNames shaderName;
    shaderName.vextexShader = Shader::vertexDir + "globalShading.vs";
    shaderName.fragmentShader = Shader::fragmentDir + "lineDebug.fs";
    std::string name = std::string("Gizmo line shader");
    m_shaderLines = new Shader(shaderName, name);

    shaderName.vextexShader = Shader::vertexDir + "globalShading.vs";
    shaderName.fragmentShader = Shader::fragmentDir + "Gizmo.fs";
    m_shader = new Shader(shaderName, name);

    generateMeshes();

}


Gizmo::~Gizmo() {
    delete m_shaderLines;
    delete m_shader;
}

Shader* Gizmo::getShader() {
    return m_shaderLines;
}

void Gizmo::draw() {
    if (!m_camera || !m_objectRef)
        return;

    if (m_currentMode == Gizmo::GTransformMode::TRANSFORM_COUNT)
        return;

    generateGizmoLines();
    if (!m_gizmoLines)
        return;

    m_shaderLines->use();
    /*
    m_shaderLines->setFloatV3("camPos", m_camera->getPosition());
    m_shaderLines->setMat4("view", m_camera->getView());
    m_shaderLines->setMat4("projection", (m_camera->getProjection()));
*/
    m_gizmoLines->draw(*m_shaderLines);

    drawMesh();
}

void Gizmo::drawMesh() {

    if (!m_gizmo || !m_shader)
        return;

    m_shader->use();
    m_shader->setFloatV3("camPos", m_camera->getPosition());
    m_shader->setMat4("view", m_camera->getView());
    m_shader->setMat4("projection", (m_camera->getProjection()));

    Object& gizmo = *m_gizmo;
    for (uint i = 0; i < gizmo.getMeshCount(); ++i) {
        Mesh& mesh = *gizmo.getMeshAt(i);
        m_shader->setBool("isPointed", isMeshPointed(mesh));
        mesh.draw(*m_shader);
    }
}

bool Gizmo::isMeshPointed(Mesh& mesh) {
    bool res = false;
    const std::string name = mesh.getName();
    if (m_currentMode == GTransformMode::Translation) {
        if (m_currentAxis == axis::X)
            if (name == "CylinderX" || name == "ConeX")
                res = true;

        if (m_currentAxis == axis::Y)
            if (name == "CylinderY" || name == "ConeY")
                res = true;

        if (m_currentAxis == axis::Z)
            if (name == "CylinderZ" || name == "ConeZ")
                res = true;

        if (m_currentAxis == axis::Center)
            if (name == "SphereCenter")
                res = true;

       if (m_currentAxis == axis::XY)
           if (name == "PlaneXY")
               res = true;

        if (m_currentAxis == axis::XZ)
            if (name == "PlaneXZ")
                res = true;

        if (m_currentAxis == axis::YZ)
            if (name == "PlaneYZ")
                res = true;

        return res;
    }

    if (m_currentMode == GTransformMode::Scale) {
        if (m_currentAxis == axis::X)
            if (name == "CylinderX" || name == "CubeX")
                res = true;

        if (m_currentAxis == axis::Y)
            if (name == "CylinderY" || name == "CubeY")
                res = true;

        if (m_currentAxis == axis::Z)
            if (name == "CylinderZ" || name == "CubeZ")
                res = true;

        if (m_currentAxis == axis::Center)
            if (name == "CubeCenter")
                res = true;

        if (m_currentAxis == axis::XY)
            if (name == "PlaneXY")
                res = true;

        if (m_currentAxis == axis::XZ)
            if (name == "PlaneXZ")
                res = true;

        if (m_currentAxis == axis::YZ)
            if (name == "PlaneYZ")
                res = true;

        return res;
    }

    if (m_currentMode == GTransformMode::Rotation) {
        if (m_currentAxis == axis::X)
            if (mesh.getName() == "TorusX")
                res = true;

        if (m_currentAxis == axis::Y)
            if (mesh.getName() == "TorusY")
                res = true;

        if (m_currentAxis == axis::Z)
            if (mesh.getName() == "TorusZ")
                res = true;

        if (m_currentAxis == axis::Center)
            if (mesh.getName() == "SphereCenter")
                res = true;

        return res;
    }

    return res;

}


void Gizmo::generateTranslationGizmoLines() {
    if (m_isClicked && (m_currentAxis != Gizmo::axis::AXIS_COUNT)) {
        glm::vec3 axis;
        glm::vec3 color;
        switch (m_currentAxis) {
            case Gizmo::axis::X : {
                axis = m_Xaxis;
                color = m_XColor;
            } break;
            case Gizmo::axis::Y : {
                axis = m_Yaxis;
                color = m_YColor;
            } break;
            case Gizmo::axis::Z : {
                axis = m_Zaxis;
                color = m_ZColor;
            } break;
            default : axis = glm::vec3(0.f); break;
        }
        std::vector<Polyline::Vertex> lineVertex = {{axis * -m_axisSize}, {axis * m_axisSize}};
        Polyline line = Polyline(lineVertex);
        line.setColor(color);
        m_gizmoLines = new ObjectLine({line});
    } else {
        std::vector<Polyline::Vertex> xAxisVertex = {{glm::vec3(0.f)}, {glm::vec3(1.f, 0.f, 0.f)}};
        Polyline xAxis = Polyline(xAxisVertex);
        xAxis.setColor(glm::vec3(1.f, 0.f, 0.f));
        std::vector<Polyline::Vertex> yAxisVertex = {{glm::vec3(0.f)}, {glm::vec3(0.f, 1.f, 0.f)}};
        Polyline yAxis = Polyline(yAxisVertex);
        yAxis.setColor(glm::vec3(0.f, 1.f, 0.f));
        std::vector<Polyline::Vertex> zAxisVertex = {{glm::vec3(0.f)}, {glm::vec3(0.f, 0.f, 1.f)}};
        Polyline zAxis = Polyline(zAxisVertex);
        zAxis.setColor(glm::vec3(0.f, 0.f, 1.f));

        //std::vector<Polyline::Vertex> debug1V = {{p0Debug}, {p1Debug}};
        //std::vector<Polyline::Vertex> debug2V = {{p2Debug}, {p3Debug}};
        //Polyline debug1 = Polyline(debug1V);
        //Polyline debug2 = Polyline(debug2V);


        switch (m_currentAxis) {
            case Gizmo::axis::X : xAxis.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            case Gizmo::axis::Y : yAxis.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            case Gizmo::axis::Z : zAxis.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            default : break;
        }
        m_gizmoLines = new ObjectLine({xAxis, yAxis, zAxis/*, debug1*/});
    }

    if (m_objectRef)
        m_model[3] = m_objectRef->getModel()[3];
    if (m_gizmoLines)
        m_gizmoLines->setTransform(m_model);
    if (m_gizmo)
        m_gizmo->setTransform(m_model);

}

void Gizmo::generateScaleGizmoLines() {
    if (m_isClicked && (m_currentAxis != Gizmo::axis::AXIS_COUNT)) {
        glm::vec3 axis;
        glm::vec3 color;
        switch (m_currentAxis) {
            case Gizmo::axis::X : {
                axis = m_Xaxis;
                color = m_XColor;
            } break;
            case Gizmo::axis::Y : {
                axis = m_Yaxis;
                color = m_YColor;
            } break;
            case Gizmo::axis::Z : {
                axis = m_Zaxis;
                color = m_ZColor;
            } break;
            default : axis = glm::vec3(0.f); break;
        }
        std::vector<Polyline::Vertex> lineVertex = {{axis * -m_axisSize}, {axis * m_axisSize}};
        Polyline line = Polyline(lineVertex);
        line.setColor(color);
        m_gizmoLines = new ObjectLine({line});
    } else {
        std::vector<Polyline::Vertex> xAxisVertex = {{glm::vec3(0.f)}, {glm::vec3(1.f, 0.f, 0.f)}};
        Polyline xAxis = Polyline(xAxisVertex);
        xAxis.setColor(glm::vec3(1.f, 0.f, 0.f));
        std::vector<Polyline::Vertex> yAxisVertex = {{glm::vec3(0.f)}, {glm::vec3(0.f, 1.f, 0.f)}};
        Polyline yAxis = Polyline(yAxisVertex);
        yAxis.setColor(glm::vec3(0.f, 1.f, 0.f));
        std::vector<Polyline::Vertex> zAxisVertex = {{glm::vec3(0.f)}, {glm::vec3(0.f, 0.f, 1.f)}};
        Polyline zAxis = Polyline(zAxisVertex);
        zAxis.setColor(glm::vec3(0.f, 0.f, 1.f));

        std::vector<Polyline::Vertex> debug1V = {{p0Debug}, {p1Debug}};
        //std::vector<Polyline::Vertex> debug2V = {{p2Debug}, {p3Debug}};
        Polyline debug1 = Polyline(debug1V);
        //Polyline debug2 = Polyline(debug2V);


        switch (m_currentAxis) {
            case Gizmo::axis::X : xAxis.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            case Gizmo::axis::Y : yAxis.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            case Gizmo::axis::Z : zAxis.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            default : break;
        }
        m_gizmoLines = new ObjectLine({xAxis, yAxis, zAxis, debug1});
    }

    if (m_objectRef)
        m_model[3] = m_objectRef->getModel()[3];
    if (m_gizmoLines)
        m_gizmoLines->setTransform(m_model);
    if (m_gizmo)
        m_gizmo->setTransform(m_model);
}

void Gizmo::generateRotationGizmoLines() {


    if (m_isClicked && (m_currentAxis != Gizmo::axis::AXIS_COUNT)) {
        glm::vec3 axis;
        glm::vec3 color;
        switch (m_currentAxis) {
            case Gizmo::axis::X : {
                axis = m_Xaxis;
                color = m_XColor;
            } break;
            case Gizmo::axis::Y : {
                axis = m_Yaxis;
                color = m_YColor;
            } break;
            case Gizmo::axis::Z : {
                axis = m_Zaxis;
                color = m_ZColor;
            } break;
            default : break;
        }

        //std::vector<Polyline::Vertex> lineVertex = {{axis * -m_axisSize}, {axis * m_axisSize}};
        Polyline circleP = BaseObjects::circleLine(glm::vec3(0.f), axis, 2.f, 64);
        Polyline circle = Polyline(circleP);
        circle.setColor(color);
        m_gizmoLines = new ObjectLine({circle});

    } else {

        Polyline circleXP = BaseObjects::circleLine(glm::vec3(0.f), m_Xaxis, 2.f, 64);
        Polyline circleX = Polyline(circleXP);
        circleX.setColor(m_XColor);

        Polyline circleYP = BaseObjects::circleLine(glm::vec3(0.f), m_Yaxis, 2.f, 64);
        Polyline circleY = Polyline(circleYP);
        circleY.setColor(m_YColor);

        Polyline circleZP = BaseObjects::circleLine(glm::vec3(0.f), m_Zaxis, 2.f, 64);
        Polyline circleZ = Polyline(circleZP);
        circleZ.setColor(m_ZColor);

        switch (m_currentAxis) {
            case Gizmo::axis::X : circleX.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            case Gizmo::axis::Y : circleY.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            case Gizmo::axis::Z : circleZ.setColor(glm::vec3(1.f, 0.3686f, 0.0039f)); break;
            default : break;
        }
/*
        std::vector<Polyline::Vertex> debug1V = {{p0Debug}, {p1Debug}};
        std::vector<Polyline::Vertex> debug2V = {{p2Debug}, {p3Debug}};
        Polyline debug1 = Polyline(debug1V);
        debug1.setColor(glm::vec3(1.f, 0.3686f, 0.0039f));
*/
        m_gizmoLines = new ObjectLine({circleX, circleY, circleZ});

    }

    if (m_objectRef)
        m_model[3] = m_objectRef->getModel()[3];
    if (m_gizmoLines)
        m_gizmoLines->setTransform(m_model);
    if (m_gizmo)
        m_gizmo->setTransform(m_model);
}

void Gizmo::generateGizmoLines() {

    if (m_gizmoLines)
        delete m_gizmoLines;

    if (m_currentMode == Gizmo::GTransformMode::Translation) {
        generateTranslationGizmoLines();
        return;
    }

    if (m_currentMode == Gizmo::GTransformMode::Scale) {
        generateScaleGizmoLines();
        return;
    }

    if (m_currentMode == Gizmo::GTransformMode::Rotation) {
        generateRotationGizmoLines();
        return;
    }
}


Plane computeBestPlane(glm::vec3 gizmoPos, glm::vec3 xAxis, glm::vec3 yAxis, glm::vec3 zAxis, Camera* camera, glm::vec3 currentAxis) {

    glm::vec3 gizmoPosition = gizmoPos;

    Plane planeX = Plane(xAxis, gizmoPosition);
    Plane planeY = Plane(yAxis, gizmoPosition);
    Plane planeZ = Plane(zAxis, gizmoPosition);

    // Set-up what is essentially the camera's front-facing vector.
    //Ray pickRay = Ray(camera->getPosition(), camera->getDirection());
    Ray pickRay = Ray(camera->getPosition(), camera->getDirection());

    // Project on to each on the object's potential
    // translation planes (local axes)

    Intersection hitPX, hitPY, hitPZ;
    intersectPlaneLine(pickRay, planeX,hitPX);
    intersectPlaneLine(pickRay, planeY,hitPY);
    intersectPlaneLine(pickRay, planeZ,hitPZ);

    glm::vec3 vX = hitPX.isHit() ? hitPX.getPosition() - pickRay.getOrigin() : glm::vec3(infinity);
    glm::vec3 vY = hitPY.isHit() ? hitPY.getPosition() - pickRay.getOrigin() : glm::vec3(infinity);
    glm::vec3 vZ = hitPZ.isHit() ? hitPZ.getPosition() - pickRay.getOrigin() : glm::vec3(infinity);

    float dotRx = glm::abs(glm::dot(glm::normalize(vX), planeX.orientation));
    float dotRy = glm::abs(glm::dot(glm::normalize(vY), planeY.orientation));
    float dotRz = glm::abs(glm::dot(glm::normalize(vZ), planeZ.orientation));
    float min = glm::min(dotRx, glm::min(dotRy, dotRz));

    if (currentAxis == xAxis) {
        min = glm::min(dotRy, dotRz);
    }
    if (currentAxis == yAxis) {
        min = glm::min(dotRx, dotRz);
    }
    if (currentAxis == zAxis) {
        min = glm::min(dotRx, dotRy);
    }


    if (min == dotRx) {
        return planeX;
    }
    if (min == dotRy) {
        return planeY;
    }
    if (min == dotRz) {
        return planeZ;
    }
    return planeX;
}

float Gizmo::closestHitBetweenLines(Ray& ray1, Ray& ray2) {

    glm::vec3 u = ray1.getDirection();
    glm::vec3 v = ray2.getDirection();
    glm::vec3 w = ray1.getOrigin() - ray2.getOrigin();
    float a = glm::dot(u,u);         // always >= 0
    float b = glm::dot(u,v);
    float c = glm::dot(v,v);         // always >= 0
    float d = glm::dot(u,w);
    float e = glm::dot(v,w);
    float D = a * c - b * b;        // always >= 0
    float sc, tc;

    // compute the line parameters of the two closest points
    if (D < FLT_MIN) {          // the lines are almost parallel
        sc = 0.f;
        tc = (b > c ? d/b : e/c);    // use the largest denominator
    } else {
        sc = (b * e - c * d) / D;
        tc = (a * e - b * d) / D;
    }

    // get the difference of the two closest points
    glm::vec3 dP = w + (sc * u) - (tc * v);  // =  L1(sc) - L2(tc)

    return glm::length(dP);   // return the closest distance
}


void Gizmo::setCamera(Camera* cam) {
    m_camera = cam;
    m_currentAxis = Gizmo::axis::AXIS_COUNT;
    m_currentMouseCoords = glm::vec2(0.f);
    m_prevMouseCoords = glm::vec2(0.f);
}

void Gizmo::setObjectRef(MoveObject* obj) {
    m_objectRef = obj;
    if (m_objectRef)
        computeGizmoScale();
}

void Gizmo::computeTranslation() {

    glm::vec3 delta;
    if (m_currentAxis == axis::Center) {
        delta = computeCameraOrientedDelta();
    } else {
        delta = computeDeltaWorldSpace();
    }

    glm::mat4 transform = glm::mat4(1.f);
    transform = glm::translate(transform, delta);
    if (m_objectRef && m_isClicked) {
        const glm::mat4& model = m_objectRef->getModel();
        glm::mat4 finalTransform = transform * model;
        m_objectRef->setTransform(finalTransform);
        m_model = glm::translate(m_model, delta);
    }

}

void Gizmo::computeScale() {

    glm::vec3 delta;
    if (m_currentAxis == axis::Center) {
        delta = computeCameraOrientedDelta();
        glm::vec3 gizmoPos = m_gizmo->getModel()[3];
        Frustum& frustum = m_camera->getFrustum();
        glm::vec4 viewport = glm::vec4(0.f, 0.f, float(frustum.getWidth()), float(frustum.getHeight()));
        glm::vec3 proj = glm::project(glm::vec3( 0.f),m_camera->getView()*m_gizmo->getModel(),m_camera->getProjection(), viewport);
        glm::vec3 screenGizmoPos = proj;
        if (m_currentMouseCoords.x < screenGizmoPos.x) {
            delta = -delta;
        }
        delta = glm::vec3(1.f-delta);

    } else {
        delta = computeDeltaWorldSpace();
        delta = glm::vec3(1.f-delta);
    }

    glm::mat4 transform = glm::mat4(1.f);

    transform = glm::scale(transform, delta);

    if (m_objectRef && m_isClicked) {
        const glm::mat4& model = m_objectRef->getModel();
        glm::mat4 finalTransform = glm::inverse(transform * glm::inverse(model));
        m_objectRef->setTransform(finalTransform);
    }

}

void Gizmo::computeRotation() {

    if (!m_objectRef || !m_isClicked)
        return;

    glm::mat4 transform = glm::mat4(1.f);
    const glm::mat4& model = m_objectRef->getModel();


    if (m_currentAxis == axis::Center) {
        transform = computeTrackBallRotation();
    } else {
        float angle = computeDeltaCircularWorldSpace();
        glm::mat3 model3 = model;
        transform = glm::rotate(transform, angle, -glm::inverse(model3)*m_currentAxisVector);
    }
    transform = glm::inverse(transform * glm::inverse(model));


    m_objectRef->setTransform(transform);

}

glm::vec3 closestPointOnLine(glm::vec3 orig, glm::vec3 dir, glm::vec3 point) {
    dir = glm::normalize(dir);
    glm::vec3 v = point - orig;
    float d = glm::dot(v, dir);
    return orig + dir * d;
}

float Gizmo::computeDeltaCircularWorldSpace() {

    if (m_currentAxis == Gizmo::AXIS_COUNT)
        return 0.f;

    glm::vec3 gizmoPos = glm::vec3(m_model[3].x, m_model[3].y, m_model[3].z);

    Plane plane = Plane(m_currentAxisVector, gizmoPos);

    glm::vec3 rayDirCurr = m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y);
    glm::vec3 rayDirPrev = m_camera->getRayFromScreen(m_prevMouseCoords.x, m_prevMouseCoords.y);
    Ray rayCurr = Ray(m_camera->getPosition(), rayDirCurr);
    Ray rayPrev = Ray(m_camera->getPosition(), rayDirPrev);


    Intersection hitPCurr, hitPPrev;
    intersectPlaneLine(rayCurr, plane, hitPCurr);
    intersectPlaneLine(rayPrev, plane, hitPPrev);

    if (hitPCurr.isHit() && hitPPrev.isHit()) {

        glm::vec3 dirCurr = glm::normalize(hitPCurr.getPosition() - gizmoPos);
        glm::vec3 dirPrev = glm::normalize(hitPPrev.getPosition() - gizmoPos);

        float dot = glm::dot(dirCurr, dirPrev);

        dot = (dot > 1.f-epsilon) ? 1.f-epsilon : dot;
        dot = (dot < -1.f+epsilon) ? -1.f+epsilon : dot;

        float angle = glm::acos(dot);

        float sign = -glm::sign(glm::dot(m_currentAxisVector, glm::cross(dirCurr, dirPrev)));
        angle *= sign;

        return angle;
    }
    else {
        return 0.f;
    }


}


glm::vec3 Gizmo::projectPos(const glm::vec2& mousePos) {

    float width = m_camera->getFrustum().getWidth();
    float height = m_camera->getFrustum().getHeight();
    glm::vec3 projectedVector;

    glm::vec2 mouseCurrent = mousePos;

    float d, a;
    projectedVector.x = (2.f*mouseCurrent.x - width) / width;
    projectedVector.y = (height - 2.f*mouseCurrent.y) / height;
    d = std::sqrt(projectedVector.x*projectedVector.x + projectedVector.y*projectedVector.y);
    projectedVector.z = std::cos((float(M_PI)/2.f) * ((d < 1.f) ? d : 1.f));
    a = 1.f / std::sqrt(projectedVector.x*projectedVector.x + projectedVector.y*projectedVector.y + projectedVector.z * projectedVector.z);
    projectedVector.x *= a;
    projectedVector.y *= a;
    projectedVector.z *= a;

    return projectedVector;


}


glm::mat4 Gizmo::computeTrackBallRotation() {

    glm::vec3 projCurr = projectPos(m_currentMouseCoords);
    glm::vec3 projPrev = projectPos(m_prevMouseCoords);
    glm::mat4 transform = glm::mat4(1.f);
    float dx, dy, dz;

    dx = projCurr.x - projPrev.x;
    dy = projCurr.y - projPrev.y;
    dz = projCurr.z - projPrev.z;
    if (dx != 0.f || dy != 0.f || dz != 0.f) {
        float angle = 90.f * std::sqrt(dx * dx + dy * dy + dz * dz);
        glm::vec3 axis;
        axis.x = projPrev.y * projCurr.z - projPrev.z * projCurr.y;
        axis.y = projPrev.z * projCurr.x - projPrev.x * projCurr.z;
        axis.z = projPrev.x * projCurr.y - projPrev.y * projCurr.x;

        axis = -axis;
        glm::mat3 model = m_objectRef->getModel();

        transform = glm::rotate(transform, glm::radians(angle), glm::normalize(glm::inverse(model)*axis));
    }


    return transform;

}


glm::vec3 Gizmo::computeDeltaWorldSpace() {
    if (m_currentAxis == Gizmo::AXIS_COUNT)
        return glm::vec3(0.f);

    //glm::vec3 prevDirection = m_camera->getRayFromScreen(m_prevMouseCoords.x, m_prevMouseCoords.y);
    //glm::vec3 curDirection = m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y);

  //  Ray prevRay = Ray(m_camera->getPosition(), prevDirection);
//    Ray curRay = Ray(m_camera->getPosition(), curDirection);

    Plane planeToIntersect = computeBestPlane(m_model[3], m_Xaxis, m_Yaxis, m_Zaxis, m_camera, m_currentAxisVector);

    Ray pickRayCurr = Ray(m_camera->getPosition(), m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y));
    Ray pickRayPrev = Ray(m_camera->getPosition(), m_camera->getRayFromScreen(m_prevMouseCoords.x, m_prevMouseCoords.y));

    Intersection intersectionCurr, intersectionPrev;
    intersectPlaneLine(pickRayCurr, planeToIntersect, intersectionCurr);
    intersectPlaneLine(pickRayPrev, planeToIntersect, intersectionPrev);

    //Case for double axis
    glm::vec3 projectedPosCurr, projectedPosPrev;
    switch (m_currentAxis) {
            case Gizmo::axis::XY : {
                Plane planeXY = Plane(glm::vec3(0.f, 0.f, 1.f), m_model[3]);
                Intersection intersectionXYCurr, intersectionXYPrev;
                intersectPlaneLine(pickRayCurr, planeXY, intersectionXYCurr);
                intersectPlaneLine(pickRayPrev, planeXY, intersectionXYPrev);
                if (!intersectionXYCurr.isHit() || !intersectionXYPrev.isHit())
                    break;
                glm::vec3 projectedPosCurrX = closestPointOnLine(m_camera->getPosition(), m_Xaxis,
                                                                 intersectionXYCurr.getPosition());
                glm::vec3 projectedPosPrevX = closestPointOnLine(m_camera->getPosition(), m_Xaxis,
                                                                 intersectionXYPrev.getPosition());
                glm::vec3 projectedPosCurrY = closestPointOnLine(m_camera->getPosition(), m_Yaxis,
                                                                 intersectionXYCurr.getPosition());
                glm::vec3 projectedPosPrevY = closestPointOnLine(m_camera->getPosition(), m_Yaxis,
                                                                 intersectionXYPrev.getPosition());
                projectedPosCurr = projectedPosCurrX + projectedPosCurrY;
                projectedPosPrev = projectedPosPrevX + projectedPosPrevY;
                glm::vec3 delta = projectedPosCurr - projectedPosPrev;

                return delta;
            } break;
            case Gizmo::axis::XZ : {
                Plane planeXZ = Plane(glm::vec3(0.f, 1.f, 0.f), m_model[3]);
                Intersection intersectionXZCurr, intersectionXZPrev;
                intersectPlaneLine(pickRayCurr, planeXZ, intersectionXZCurr);
                intersectPlaneLine(pickRayPrev, planeXZ, intersectionXZPrev);
                if (!intersectionXZCurr.isHit() || !intersectionXZPrev.isHit())
                    break;
                glm::vec3 projectedPosCurrX = closestPointOnLine(m_camera->getPosition(), m_Xaxis,
                                                                 intersectionXZCurr.getPosition());
                glm::vec3 projectedPosPrevX = closestPointOnLine(m_camera->getPosition(), m_Xaxis,
                                                                 intersectionXZPrev.getPosition());
                glm::vec3 projectedPosCurrZ = closestPointOnLine(m_camera->getPosition(), m_Zaxis,
                                                                 intersectionXZCurr.getPosition());
                glm::vec3 projectedPosPrevZ = closestPointOnLine(m_camera->getPosition(), m_Zaxis,
                                                                 intersectionXZPrev.getPosition());
                projectedPosCurr = projectedPosCurrX + projectedPosCurrZ;
                projectedPosPrev = projectedPosPrevX + projectedPosPrevZ;
                glm::vec3 delta = projectedPosCurr - projectedPosPrev;

                return delta;
            } break;
            case Gizmo::axis::YZ : {
                Plane planeYZ = Plane(glm::vec3(1.f, 0.f, 0.f), m_model[3]);
                Intersection intersectionYZCurr, intersectionYZPrev;
                intersectPlaneLine(pickRayCurr, planeYZ, intersectionYZCurr);
                intersectPlaneLine(pickRayPrev, planeYZ, intersectionYZPrev);
                if (!intersectionYZCurr.isHit() || !intersectionYZPrev.isHit())
                    break;
                glm::vec3 projectedPosCurrY = closestPointOnLine(m_camera->getPosition(), m_Yaxis,
                                                                 intersectionYZCurr.getPosition());
                glm::vec3 projectedPosPrevY = closestPointOnLine(m_camera->getPosition(), m_Yaxis,
                                                                 intersectionYZPrev.getPosition());
                glm::vec3 projectedPosCurrZ = closestPointOnLine(m_camera->getPosition(), m_Zaxis,
                                                                 intersectionYZCurr.getPosition());
                glm::vec3 projectedPosPrevZ = closestPointOnLine(m_camera->getPosition(), m_Zaxis,
                                                                 intersectionYZPrev.getPosition());
                projectedPosCurr = projectedPosCurrY + projectedPosCurrZ;
                projectedPosPrev = projectedPosPrevY + projectedPosPrevZ;
                glm::vec3 delta = projectedPosCurr - projectedPosPrev;

                return delta;
            } break;
            default :break;


        }


    if (intersectionCurr.isHit() && intersectionPrev.isHit()) {

        /*glm::vec3 delta = (intersectionCurr.getPosition() - intersectionPrev.getPosition()) * m_currentAxisVector;*/
        glm::vec3 projectedPosCurr, projectedPosPrev;
        switch (m_currentAxis) {
            case Gizmo::axis::X : {
                projectedPosCurr = closestPointOnLine(m_camera->getPosition(), m_Xaxis, intersectionCurr.getPosition());
                projectedPosPrev = closestPointOnLine(m_camera->getPosition(), m_Xaxis, intersectionPrev.getPosition());
            } break;
            case Gizmo::axis::Y : {
                projectedPosCurr = closestPointOnLine(m_camera->getPosition(), m_Yaxis, intersectionCurr.getPosition());
                projectedPosPrev = closestPointOnLine(m_camera->getPosition(), m_Yaxis, intersectionPrev.getPosition());
            } break;
            case Gizmo::axis::Z : {
                projectedPosCurr = closestPointOnLine(m_camera->getPosition(), m_Zaxis, intersectionCurr.getPosition());
                projectedPosPrev = closestPointOnLine(m_camera->getPosition(), m_Zaxis, intersectionPrev.getPosition());
            } break;
            default : break;
        }

        glm::vec3 delta = projectedPosCurr - projectedPosPrev;

        return delta;
    }


    return glm::vec3(0.f);

}


glm::vec3 Gizmo::computeCameraOrientedDelta() {

    const glm::vec3& gizmoPos = m_gizmo->getModel()[3];
    Plane plane = Plane(-m_camera->getDirection(), gizmoPos);
    Ray pickRayCurr = Ray(m_camera->getPosition(), m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y));
    Ray pickRayPrev = Ray(m_camera->getPosition(), m_camera->getRayFromScreen(m_prevMouseCoords.x, m_prevMouseCoords.y));

    Intersection intersectionCurr, intersectionPrev;
    intersectPlaneLine(pickRayCurr, plane, intersectionCurr);
    intersectPlaneLine(pickRayPrev, plane, intersectionPrev);

    if (intersectionCurr.isHit() && intersectionPrev.isHit()) {

        if (m_currentMode == GTransformMode::Translation) {
            return intersectionCurr.getPosition() - intersectionPrev.getPosition();
        } else {
            float length = glm::length(intersectionCurr.getPosition() - intersectionPrev.getPosition());
            float lenCurr = glm::length(intersectionCurr.getPosition() - gizmoPos);
            float lenPrev = glm::length(intersectionPrev.getPosition() - gizmoPos);
            float sign = lenCurr > lenPrev ? 1.f : -1.f;
            return glm::vec3(length * sign);
        }

    }
    return glm::vec3(0.f);

}


void Gizmo::computeSelectedAxisRotation() {

    glm::vec3 gizmoPos = glm::vec3(m_model[3].x, m_model[3].y, m_model[3].z);

    Plane planeX = Plane(m_Xaxis, gizmoPos);
    Plane planeY = Plane(m_Yaxis, gizmoPos);
    Plane planeZ = Plane(m_Zaxis, gizmoPos);

    glm::vec3 rayDir = m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y);
    Ray ray = Ray(m_camera->getPosition(), rayDir);

    Intersection hitPX, hitPY, hitPZ;
    intersectPlaneLine(ray, planeX, hitPX);
    intersectPlaneLine(ray, planeY, hitPY);
    intersectPlaneLine(ray, planeZ, hitPZ);


    //Test center sphere
    Sphere sphereCenter = Sphere(gizmoPos, m_gizmoScaleFactor*0.15f);
    Intersection interectionCenter;
    intersectSphere(ray, sphereCenter, interectionCenter);
    if (interectionCenter.isHit()) {
        m_currentAxis = Gizmo::axis::Center;
        m_currentAxisVector = m_Xaxis;
        return;
    }


    glm::vec3 reprojectedPX = hitPX.isHit() ? gizmoPos + glm::normalize(hitPX.getPosition() - gizmoPos) * (m_gizmoScaleFactor) : glm::vec3(infinity);
    glm::vec3 reprojectedPY = hitPY.isHit() ? gizmoPos + glm::normalize(hitPY.getPosition() - gizmoPos) * (m_gizmoScaleFactor) : glm::vec3(infinity);
    glm::vec3 reprojectedPZ = hitPZ.isHit() ? gizmoPos + glm::normalize(hitPZ.getPosition() - gizmoPos) * (m_gizmoScaleFactor) : glm::vec3(infinity);

    float lx = glm::length(reprojectedPX - hitPX.getPosition());
    float ly = glm::length(reprojectedPY - hitPY.getPosition());
    float lz = glm::length(reprojectedPZ - hitPZ.getPosition());

    float min = glm::min(lx, glm::min(ly, lz));

    if (min == lx) {
        m_currentAxis = Gizmo::axis::X;
        m_currentAxisVector = m_Xaxis;
        //p0Debug = reprojectedPX;
        //p1Debug = hitPX.getPosition();

        return;
    }
    if (min == ly) {
        m_currentAxis = Gizmo::axis::Y;
        m_currentAxisVector = m_Yaxis;
        //p0Debug = reprojectedPY;
        //p1Debug = hitPY.getPosition();
        return;
    }
    if (min == lz) {
        m_currentAxis = Gizmo::axis::Z;
        m_currentAxisVector = m_Zaxis;
        //p0Debug = reprojectedPZ;
        //p1Debug = hitPZ.getPosition();
        return;
    }

}

void Gizmo::computeSelectedAxisScale() {

//    glm::vec3 dirPrev = m_camera->getRayFromScreen(m_prevMouseCoords.x, m_prevMouseCoords.y);
    glm::vec3 dirCurr = m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y);

//    Ray rayPrev = Ray(m_camera->getPosition(), dirPrev);
    Ray rayCur = Ray(m_camera->getPosition(), dirCurr);

//    float t = closestHitBetweenLines(rayPrev, rayCur);

    Ray xRay = Ray(m_model[3], m_Xaxis);
    Ray yRay = Ray(m_model[3], m_Yaxis);
    Ray zRay = Ray(m_model[3], m_Zaxis);

    float tx = closestHitBetweenLines(xRay, rayCur);
    float ty = closestHitBetweenLines(yRay, rayCur);
    float tz = closestHitBetweenLines(zRay, rayCur);
    float minT = glm::min(tx, glm::min(ty, tz));
    glm::vec3 gizmoPos = m_model[3];


    //Test center cube
    Box cubeCenter = Box(gizmoPos - 0.1f*m_gizmoScaleFactor, gizmoPos + 0.1f*m_gizmoScaleFactor);
    Intersection intersectionBox;
    intersectBox(rayCur, cubeCenter, intersectionBox);
    if (intersectionBox.isHit()) {
        m_currentAxis = Gizmo::axis::Center;
        m_currentAxisVector = m_Xaxis;
        return;
    }

    //Test XY, XZ, YZ axis
    Plane planeXY = Plane(glm::vec3(0.f, 0.f, 1.f), gizmoPos);
    Plane planeXZ = Plane(glm::vec3(0.f, 1.f, 0.f), gizmoPos);
    Plane planeYZ = Plane(glm::vec3(1.f, 0.f, 0.f), gizmoPos);
    Intersection intersectionPXY, intersectionPXZ, intersectionPYZ;
    intersectPlaneLine(rayCur, planeXY, intersectionPXY);
    intersectPlaneLine(rayCur, planeXZ, intersectionPXZ);
    intersectPlaneLine(rayCur, planeYZ, intersectionPYZ);
    float radius = 2.f*0.1f * m_gizmoScaleFactor;

    if (intersectionPXY.isHit()) {
        glm::vec3 quadCenter = glm::vec3((0.4f+0.1f)*m_gizmoScaleFactor, (0.4f+0.1f)*m_gizmoScaleFactor, 0.f);
        glm::vec3 v = intersectionPXY.getPosition() - (gizmoPos + quadCenter);
        if (glm::length(v) < radius) {
            m_currentAxis = Gizmo::axis::XY;
            m_currentAxisVector = m_Xaxis;
            return;
        }
    }

    if (intersectionPYZ.isHit()) {
        glm::vec3 quadCenter = glm::vec3(0.f, (0.4f+0.1f)*m_gizmoScaleFactor, (0.4f+0.1f)*m_gizmoScaleFactor);
        glm::vec3 v = intersectionPYZ.getPosition() - (gizmoPos + quadCenter);
        if (glm::length(v) < radius) {
            m_currentAxis = Gizmo::axis::YZ;
            m_currentAxisVector = m_Xaxis;
            return;
        }
    }

    if (intersectionPXZ.isHit()) {
        glm::vec3 quadCenter = glm::vec3((0.4f+0.1f)*m_gizmoScaleFactor, 0.f, (0.4f+0.1f)*m_gizmoScaleFactor);
        glm::vec3 v = intersectionPXZ.getPosition() - (gizmoPos + quadCenter);
        if (glm::length(v) < radius) {
            m_currentAxis = Gizmo::axis::XZ;
            m_currentAxisVector = m_Xaxis;
            return;
        }
    }


    if (minT == tx) {
        m_currentAxis = Gizmo::axis::X;
        m_currentAxisVector = m_Xaxis;
        return;
    }
    if (minT == ty) {
        m_currentAxis = Gizmo::axis::Y;
        m_currentAxisVector = m_Yaxis;
        return;
    }
    if (minT == tz) {
        m_currentAxis = Gizmo::axis::Z;
        m_currentAxisVector = m_Zaxis;
        return;
    }
    m_currentAxis = Gizmo::axis::AXIS_COUNT;
}

void Gizmo::computeSelectedAxisTranslation() {

//    glm::vec3 dirPrev = m_camera->getRayFromScreen(m_prevMouseCoords.x, m_prevMouseCoords.y);
    glm::vec3 dirCurr = m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y);

 //   Ray rayPrev = Ray(m_camera->getPosition(), dirPrev);
    Ray rayCur = Ray(m_camera->getPosition(), dirCurr);

//    float t = closestHitBetweenLines(rayPrev, rayCur);

    Ray xRay = Ray(m_model[3], m_Xaxis);
    Ray yRay = Ray(m_model[3], m_Yaxis);
    Ray zRay = Ray(m_model[3], m_Zaxis);

    float tx = closestHitBetweenLines(xRay, rayCur);
    float ty = closestHitBetweenLines(yRay, rayCur);
    float tz = closestHitBetweenLines(zRay, rayCur);
    float minT = glm::min(tx, glm::min(ty, tz));


    //Test center sphere
    glm::vec3 pos = m_camera->getPosition() + rayCur.getDirection() * minT;
    glm::vec3 gizmoPos = m_gizmo->getModel()[3];
    Sphere sphereCenter = Sphere(gizmoPos, m_gizmoScaleFactor*0.15f);
    Intersection interectionCenter;
    intersectSphere(rayCur, sphereCenter, interectionCenter);
    if (interectionCenter.isHit()) {
        m_currentAxis = Gizmo::axis::Center;
        m_currentAxisVector = m_Xaxis;
        return;
    }

    //Test XY, XZ, YZ axis
    Plane planeXY = Plane(glm::vec3(0.f, 0.f, 1.f), gizmoPos);
    Plane planeXZ = Plane(glm::vec3(0.f, 1.f, 0.f), gizmoPos);
    Plane planeYZ = Plane(glm::vec3(1.f, 0.f, 0.f), gizmoPos);
    Intersection intersectionPXY, intersectionPXZ, intersectionPYZ;
    intersectPlaneLine(rayCur, planeXY, intersectionPXY);
    intersectPlaneLine(rayCur, planeXZ, intersectionPXZ);
    intersectPlaneLine(rayCur, planeYZ, intersectionPYZ);
    float radius = 2.f*0.1f * m_gizmoScaleFactor;
    if (intersectionPXY.isHit()) {
        glm::vec3 quadCenter = glm::vec3((0.4f+0.1f)*m_gizmoScaleFactor, (0.4f+0.1f)*m_gizmoScaleFactor, 0.f);
        glm::vec3 v = intersectionPXY.getPosition() - (gizmoPos + quadCenter);
        if (glm::length(v) < radius) {
            m_currentAxis = Gizmo::axis::XY;
            m_currentAxisVector = m_Xaxis;
            return;
        }
    }

    if (intersectionPYZ.isHit()) {
        glm::vec3 quadCenter = glm::vec3(0.f, (0.4f+0.1f)*m_gizmoScaleFactor, (0.4f+0.1f)*m_gizmoScaleFactor);
        glm::vec3 v = intersectionPYZ.getPosition() - (gizmoPos + quadCenter);
        if (glm::length(v) < radius) {
            m_currentAxis = Gizmo::axis::YZ;
            m_currentAxisVector = m_Xaxis;
            return;
        }
    }

    if (intersectionPXZ.isHit()) {
        glm::vec3 quadCenter = glm::vec3((0.4f+0.1f)*m_gizmoScaleFactor, 0.f, (0.4f+0.1f)*m_gizmoScaleFactor);
        glm::vec3 v = intersectionPXZ.getPosition() - (gizmoPos + quadCenter);
        if (glm::length(v) < radius) {
            m_currentAxis = Gizmo::axis::XZ;
            m_currentAxisVector = m_Xaxis;
            return;
        }
    }




    //Test XYZ axis
    if (minT == tx) {
        m_currentAxis = Gizmo::axis::X;
        m_currentAxisVector = m_Xaxis;
        return;
    }
    if (minT == ty) {
        m_currentAxis = Gizmo::axis::Y;
        m_currentAxisVector = m_Yaxis;
        return;
    }
    if (minT == tz) {
        m_currentAxis = Gizmo::axis::Z;
        m_currentAxisVector = m_Zaxis;
        return;
    }
    m_currentAxis = Gizmo::axis::AXIS_COUNT;
}

void Gizmo::computeSelectedAxis() {

    switch (m_currentMode) {
        case Gizmo::GTransformMode::Translation : {
            computeSelectedAxisTranslation();
        } break;
        case Gizmo::GTransformMode::Scale : {
            computeSelectedAxisScale();
        } break;
        case Gizmo::GTransformMode::Rotation : {
            computeSelectedAxisRotation();
        } break;
        default : {
            m_currentAxis = Gizmo::axis::AXIS_COUNT;
            m_currentAxisVector = m_Xaxis;
        }
            break;
    }

    glm::vec3 gizmoPos = m_model[3];
    Plane plane = computeBestPlane(gizmoPos, m_Xaxis, m_Yaxis, m_Zaxis, m_camera, m_currentAxisVector);
    Ray ray = Ray(m_camera->getPosition(), m_camera->getRayFromScreen(m_currentMouseCoords.x, m_currentMouseCoords.y));
    Intersection intersection;
    intersectPlaneLine(ray, plane, intersection);
    if (intersection.isHit()) {
        float dist = glm::length(intersection.getPosition() - gizmoPos);
        if (dist > m_gizmoScaleFactor * 2.f) {
            m_currentAxis = Gizmo::axis::AXIS_COUNT;
            m_currentAxisVector = m_Xaxis;
        }
    }

}

void Gizmo::computeTransform() {

    if (m_currentMode == Gizmo::GTransformMode::TRANSFORM_COUNT)
        return;

    if (!m_isClicked) {
        computeSelectedAxis();
        return;
    }

    if (m_currentAxis == Gizmo::axis::AXIS_COUNT)
        return;

    if (m_currentMouseCoords == m_prevMouseCoords)
        return;

    if (!m_isClicked)
        return;

    switch (m_currentMode) {
        case Gizmo::GTransformMode::Translation : computeTranslation(); break;
        case Gizmo::GTransformMode::Rotation : computeRotation(); break;
        case Gizmo::GTransformMode::Scale : computeScale(); break;
        default : LOG_ERROR("This gizmo transform mode does not exist : %n", m_currentMode); break;
    }

    m_axisSize = m_camera->getFrustum().getFar() - m_camera->getFrustum().getNear();
}

glm::mat4 Gizmo::mouseMove(Camera& , glm::vec2 nextXY, bool ) {

    computeGizmoScale();

    if (m_currentMouseCoords == glm::vec2(infinity)) {
        m_currentMouseCoords = nextXY;
        m_prevMouseCoords = nextXY;
        return glm::mat4(1.f);
    }


    computeTransform();
    m_prevMouseCoords = m_currentMouseCoords;
    m_currentMouseCoords = nextXY;

    return glm::mat4(1.f);
}

void Gizmo::updateTransform(const glm::mat4 &, const glm::mat4& ) {
    //LOG_ERROR("Gizmo updateTransform not implemented");
}

void Gizmo::mouseClick(glm::vec2 ) {
    m_isClicked = true;
    computeGizmoScale();
}

void Gizmo::mouseRelease(glm::vec2 ) {
    m_isClicked = false;
    computeGizmoScale();

}

void Gizmo::wheelScroll(int ) {
    computeGizmoScale();
}

void Gizmo::keyboardmove() {
    computeGizmoScale();
}

void Gizmo::setTransformType(GTransformMode mode) {
    m_currentMode = mode;
    if (m_currentMode < GTransformMode::TRANSFORM_COUNT)
        m_gizmo = m_gizmoData.getObject(m_currentMode);
    else
        m_gizmo = nullptr;
}

void Gizmo::setSpaceType(GSpaceMode space) {
    m_currentSpace = space;
}

void Gizmo::computeGizmoScale() {
    if (!m_camera)
        return;
    if (!m_objectRef)
        return;

    glm::mat4 model = m_model;
    glm::vec3 gizmoPos = glm::vec3(model[3][0], model[3][1], model[3][2]);
    float z = glm::length(gizmoPos - m_camera->getPosition());
    //float scaleValue = 1.f/z;
    float scaleValue = glm::tan(m_camera->getFrustum().getFOV()) * z * (50.f / m_camera->getFrustum().getWidth());
    m_gizmoScaleFactor =scaleValue;


    m_model[0][0] = m_gizmoScaleFactor;
    m_model[1][1] = m_gizmoScaleFactor;
    m_model[2][2] = m_gizmoScaleFactor;
}


void Gizmo::generateTranslationMesh() {

    FileLoader loader = FileLoader(&m_gizmoData);

    loader.objLoader("../Assets/Gizmo/gizmoTranslation.obj");

}

void Gizmo::generateScaleMesh() {
    FileLoader loader = FileLoader(&m_gizmoData);

    loader.objLoader("../Assets/Gizmo/gizmoScale.obj");
}

void Gizmo::generateRotationMesh() {
    FileLoader loader = FileLoader(&m_gizmoData);

    loader.objLoader("../Assets/Gizmo/gizmoRotation.obj");
}

void Gizmo::generateMeshes() {
    generateTranslationMesh();
    generateRotationMesh();
    generateScaleMesh();
}