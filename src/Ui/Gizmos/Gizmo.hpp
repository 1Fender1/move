//
// Created by jb on 29/08/2020.
//

#ifndef MOVE_GIZMO_HPP
#define MOVE_GIZMO_HPP


#include <Engine/Modeling/Mesh/Object.hpp>
#include <glm/glm.hpp>
#include <Engine/Camera/Camera.hpp>
#include <Engine/Shader/Shader.hpp>
#include <Engine/Scenes/RenderData.hpp>


//Gizmo implementation based on https://nelari.us/post/gizmos/

class Gizmo {

public :
    enum GTransformMode {
        Translation,
        Rotation,
        Scale,
        TRANSFORM_COUNT
    };

    enum GSpaceMode {
        Local,
        World,
        SPACE_COUNT
    };


public :
    Gizmo();
    ~Gizmo();
    Shader* getShader();
    void draw();

    glm::mat4 mouseMove(Camera& cam, glm::vec2 nextXY, bool stepped);
    void updateTransform(const glm::mat4 &worldTo, const glm::mat4& t);
    void mouseClick(glm::vec2 pos);
    void mouseRelease(glm::vec2 pos);
    void wheelScroll(int delta);
    void keyboardmove();

    void setCamera(Camera* cam);
    void setObjectRef(MoveObject* obj);

    void setTransformType(GTransformMode mode);
    void setSpaceType(GSpaceMode space);


private :
    float closestHitBetweenLines(Ray& ray1, Ray& ray2);
    glm::vec3 computeDeltaWorldSpace();
    glm::vec3 computeCameraOrientedDelta(); //Use for center translation gizmo
    float computeDeltaCircularWorldSpace();

    glm::vec3 projectPos(const glm::vec2& mousePos);
    glm::mat4 computeTrackBallRotation();

    void generateTranslationGizmoLines();
    void generateScaleGizmoLines();
    void generateRotationGizmoLines();
    void generateGizmoLines();

    void generateTranslationMesh();
    void generateScaleMesh();
    void generateRotationMesh();
    void generateMeshes();

    void computeScale();
    void computeRotation();
    void computeTranslation();
    void computeTransform();

    void computeSelectedAxisRotation();
    void computeSelectedAxisScale();
    void computeSelectedAxisTranslation();
    void computeSelectedAxis();

    void computeGizmoScale();

    void drawMesh();
    bool isMeshPointed(Mesh& mesh);


    enum axis {
        X,
        Y,
        Z,
        XY,
        XZ,
        YZ,
        Center,
        AXIS_COUNT
    };

    Shader *m_shaderLines = nullptr;
    Shader *m_shader = nullptr;
    MoveObject* m_objectRef = nullptr;
    ObjectLine* m_gizmoLines = nullptr;

    Object* m_gizmo = nullptr;

    bool m_isClicked = false;

    glm::mat4 m_model = glm::mat4(1.f);

    Camera* m_camera = nullptr;
    axis m_currentAxis = AXIS_COUNT;
    glm::vec3 m_currentAxisVector = glm::vec3(0.f);

    GTransformMode m_currentMode = TRANSFORM_COUNT;
    GSpaceMode m_currentSpace = SPACE_COUNT;

    RenderData m_gizmoData;
    ContextSceneEmbree m_rtContext;

    glm::vec2 m_prevMouseCoords = glm::vec2(infinity);
    glm::vec2 m_currentMouseCoords = glm::vec2(infinity);

    const glm::vec3 m_Xaxis = glm::vec3(1.f, 0.f, 0.f);
    const glm::vec3 m_Yaxis = glm::vec3(0.f, 1.f, 0.f);
    const glm::vec3 m_Zaxis = glm::vec3(0.f, 0.f, 1.f);

    const glm::vec3 m_XYaxis = glm::vec3(1.f, 1.f, 0.f);
    const glm::vec3 m_XZaxis = glm::vec3(1.f, 0.f, 1.f);
    const glm::vec3 m_YZaxis = glm::vec3(0.f, 1.f, 1.f);

    const glm::vec3 m_XColor = glm::vec3(1.f, 0.033105f, 0.084376f);
    const glm::vec3 m_YColor = glm::vec3(0.258183f, 0.715694f, 0.f);
    const glm::vec3 m_ZColor = glm::vec3(0.021219f, 0.278894f, 1.f);

    glm::vec3 p0Debug = glm::vec3(infinity);
    glm::vec3 p1Debug = glm::vec3(infinity);
    glm::vec3 p2Debug = glm::vec3(infinity);
    glm::vec3 p3Debug = glm::vec3(infinity);

    float m_axisSize = 0.f;

    float m_gizmoScaleFactor = 1.f;

    const float m_gizmoMouseMargin = 0.1f;


};


#endif //MOVE_GIZMO_HPP
