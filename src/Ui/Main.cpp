
#include <Core/System/Logger.hpp>
#include <Core/System/Utility/StringUtility.hpp>
#include <Core/System/SystemParser.hpp>
#include <glm/gtx/io.hpp>

#include <Ui/MainWindow.hpp>
#include <omp.h>
#include <xmmintrin.h>
#include <pmmintrin.h>
#include <unistd.h>
#include <Core/System/Utility/Chrono.hpp>

int main(int /*argc*/, char**/* *argv[]*/) {

    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
    _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);

    //Hack to set OMP_CANCELLATION to true, the program restart with environment set
/*
    char *hasCancel = getenv("OMP_CANCELLATION");
    if (hasCancel == nullptr) {
        setenv("OMP_CANCELLATION", "true", 1);
        execvp(argv[0], argv);
        exit(1);
    }
*/



    MainWindow window;
    window.show();

    return 0;
}