//
// Created by jb on 23/02/2020.
//

#include "MainWindow.hpp"
#include <Ui/Font/ShortCutIcons.hpp>

#include <Engine/Rendering/ShadowMap/ShadowMapPcf.hpp>
#include <Ui/MoveObjectAttributes/PerformancesUI.hpp>
#include <fstream>

static void glfw_error_callback(int error, const char* description) {
    LOG_ERROR("Glfw Error : %n : %d", error, description);
}

MainWindow::MainWindow() {

    m_width = 800;
    m_height = 600;
}


MainWindow::~MainWindow() {

    if (m_scene)
        delete m_scene;

    if (m_menuBarUI)
        delete m_menuBarUI;

    if (m_mainIcon)
        delete m_mainIcon;

}


bool MainWindow::g_isDragAndDrop;
std::vector<String> MainWindow::g_dragNDropFiles;

void dragDropCallBack(GLFWwindow* , int pathCount, const char* paths[]) {

    if (pathCount == 0)
        return;

    MainWindow::g_isDragAndDrop = true;

    LOG_DEBUG("Drag and drop call");
    uint uPathCount = uint(pathCount);
    for (uint i = 0; i < uPathCount; ++i) {
        String fileName = String(paths[i]);
        LOG_DEBUG("File : %n", fileName);
        MainWindow::g_dragNDropFiles.push_back(fileName);
    }

}

void MainWindow::renderWindow() {
    int display_w, display_h;
    glfwGetFramebufferSize(m_window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(m_clearColor.x, m_clearColor.y, m_clearColor.z, m_clearColor.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    GLFWwindow* backup_current_context = glfwGetCurrentContext();
    ImGui::UpdatePlatformWindows();
    ImGui::RenderPlatformWindowsDefault();
    glfwMakeContextCurrent(backup_current_context);

    glfwSwapBuffers(m_window);
}

void MainWindow::cleanup() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(m_window);
    glfwTerminate();
}

void MainWindow::initMainWindow() {

    initDragDropData();

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit()) {
        LOG_FATAL("Error glfw is not initialized")
        exit(-1);
    }

    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    int xPos, yPos, monitorWidth, monitorHeight;

    glfwGetMonitorWorkarea(monitor, &xPos, &yPos, &monitorWidth, &monitorHeight);
    m_width = std::max(uint(150), uint(float(monitorWidth - xPos) * (3.f/4.f)));
    m_height = std::max(uint(150), uint(float(monitorHeight - yPos) * (3.f/4.f)));

    const char* glsl_version = "#version 410";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    // Create window with graphics context
    m_window = glfwCreateWindow((int)m_width, (int)m_height, "Move", (GLFWmonitor*)nullptr, (GLFWwindow *)nullptr);

    glfwSetWindowSizeLimits(m_window, 150, 150, GLFW_DONT_CARE, GLFW_DONT_CARE);

    if (m_window == nullptr) {
        LOG_FATAL("Main window is not intialized")
        exit(-1);
    }

    glfwMakeContextCurrent(m_window);
    glfwSwapInterval(0); // Disable vsync

    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    m_io = &io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableSetMousePos;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    io.ConfigWindowsMoveFromTitleBarOnly = true;

    ImGuiStyle& style = ImGui::GetStyle();
    style.WindowMinSize = ImVec2(50.f, 50.f);
    style.WindowPadding = ImVec2(5.f, 5.f);


    m_io->IniFilename = nullptr;

    configTheme(io);


    // Setup Dear ImGui style
    //ImGui::StyleColorsDark();
    //ImGui::LoadIniSettingsFromMemory()

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    setApplicationIcon(m_window);

    m_clearColor = ImVec4(0.11f, 0.15f, 0.17f, 1.f);

    m_scene = new ObjVis(m_width, m_height);

    m_renderui = RenderUI(m_scene, &io, RenderUI::FORWARD);
    m_treeSceneGraph = TreeSceneWindowUI(m_scene->getRenderData(), &io);
    m_treeSceneGraph.setScene(m_scene);
    m_RendererUI = RendererUI(m_scene);
    m_RendererUI.setForwardRenderer(m_scene->getGLRenderer());
    m_RendererUI.setPathTracerRenderer(m_scene->getPTRenderer());
    m_params.setScene(m_scene);
    m_menuBarUI = new MenuBarUI(&io);
    m_menuBarUI->setScene(m_scene);
    m_menuBarUI->setRenderData(m_scene->getRenderData());
    m_menuBarUI->setRenderUI(&m_renderui);

    m_windowFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    m_windowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    m_windowFlags |= ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

    std::ifstream f("./imgui.ini");
    m_configFileExist = f.good();

}

void MainWindow::loadUiConfig() {
    if (!m_configFileExist)
        ImGui::LoadIniSettingsFromDisk("../Assets/ressources/Views/default.ini");
    else
        ImGui::LoadIniSettingsFromDisk("./imgui.ini");
}

void MainWindow::saveUiConfig() {
    double time = ImGui::GetTime();
    if (((time - m_lastSave) > m_uiConfigRefreshRate) || (m_lastSave == 0.f)) {
        m_lastSave = time;
        ImGui::SaveIniSettingsToDisk("./imgui.ini");
    }
}


void MainWindow::show() {

   // remove("./imgui.ini");
    initMainWindow();

    loadUiConfig();

    MoveObject *newSelectedObject = nullptr;
    MoveObject *prevSelectedObject = nullptr;

    PerformancesUI perfWindow;

    // Main loop
    while (!glfwWindowShouldClose(m_window)) {
        //glfwSetWindowSize(m_window, (int)155, (int)155);

        glfwPollEvents();

        glfwGetWindowSize(m_window, (int*)&m_width, (int*)&m_height);

        glViewport(0, 0, m_width, m_height);

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::PushFont(m_mainFont);

        (*m_io).WantCaptureKeyboard = true;
        (*m_io).WantCaptureMouse = true;

        bool is_open = true;
        ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);
        ImGui::SetNextWindowViewport(viewport->ID);


        if (ImGui::Begin("Main dockSpace", &is_open, m_windowFlags)) {
            ImGuiID mainDockSpaceID = ImGui::GetID("MyDockSpace");


            ImGui::DockSpace(mainDockSpaceID, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_None);

            handleDragDrop(m_window);

            m_menuBarUI->show();
            m_RendererUI.show();

            prevSelectedObject = newSelectedObject;
            MoveObject *selectedObject = m_treeSceneGraph.createUI();
            newSelectedObject = selectedObject;

            m_renderui.manageInputs();

            if (m_params.getSelectedItem() && (newSelectedObject == prevSelectedObject)) {
                selectedObject = m_params.getSelectedItem();
            } else {
                selectedObject = newSelectedObject;
                m_params.setSelectedItem(nullptr);
            }
            if (m_renderui.getSelectedItem())
                m_params.setSelectedItem(m_renderui.getSelectedItem());


            m_params.show(selectedObject);


            if (m_params.isNeedUpdatePathTracer() || m_RendererUI.needUpdatePathTracer())
                m_scene->getPTRenderer()->refresh();

            glFinish();
            m_renderui.show();

            ImGui::End();
        }
        ImGui::PopFont();
        ImGui::Render();

        // Rendering
        renderWindow();
        saveUiConfig();
    }

    // Cleanup
    cleanup();

}







void MainWindow::manageInputs(ImGuiIO& ) {




}



void MainWindow::configTheme(ImGuiIO& io) {

    ImFontConfig config;

    m_mainFont = io.Fonts->AddFontFromFileTTF("../Assets/ressources/Fonts/Roboto/OpenSans-Regular.ttf", 18.0f, nullptr, io.Fonts->GetGlyphRangesDefault());
    ImGui::GetStyle().FrameRounding = 4.0f;
    ImGui::GetStyle().GrabRounding = 4.0f;


    //Merge icons to the current font

    static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
    ImFontConfig icons_config; icons_config.MergeMode = true; icons_config.PixelSnapH = true;
    io.Fonts->AddFontFromFileTTF( FONT_ICON_FILE_NAME_FAS, 16.0f, &icons_config, icons_ranges );

    ImVec4* colors = ImGui::GetStyle().Colors;

    colors[ImGuiCol_Text] = ImVec4(0.95f, 0.96f, 0.98f, 1.f);
    colors[ImGuiCol_TextDisabled] = ImVec4(0.36f, 0.42f, 0.47f, 1.f);
    colors[ImGuiCol_WindowBg] = ImVec4(0.11f, 0.15f, 0.17f, 1.f);
    colors[ImGuiCol_ChildBg] = ImVec4(0.15f, 0.18f, 0.22f, 1.f);
    colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
    colors[ImGuiCol_Border] = ImVec4(0.08f, 0.1f, 0.12f, 1.f);
    colors[ImGuiCol_BorderShadow] = ImVec4(0.f, 0.f, 0.f, 0.f);
    colors[ImGuiCol_FrameBg] = ImVec4(0.2f, 0.25f, 0.29f, 1.f);
    colors[ImGuiCol_FrameBgHovered] = ImVec4(0.12f, 0.2f, 0.28f, 1.f);
    colors[ImGuiCol_FrameBgActive] = ImVec4(0.09f, 0.12f, 0.14f, 1.f);
    colors[ImGuiCol_TitleBg] = ImVec4(0.09f, 0.12f, 0.14f, 0.65f);
    colors[ImGuiCol_TitleBgActive] = ImVec4(0.08f, 0.10f, 0.12f, 1.f);
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.f, 0.f, 0.f, 0.51f);
    colors[ImGuiCol_MenuBarBg] = ImVec4(0.15f, 0.18f, 0.22f, 1.f);
    colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.39f);
    colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.2f, 0.25f, 0.29f, 1.f);
    colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.18f, 0.22f, 0.25f, 1.f);
    colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.09f, 0.21f, 0.31f, 1.f);
    colors[ImGuiCol_CheckMark] = ImVec4(0.28f, 0.56f, 1.f, 1.f);
    colors[ImGuiCol_SliderGrab] = ImVec4(0.28f, 0.56f, 1.f, 1.f);
    colors[ImGuiCol_SliderGrabActive] = ImVec4(0.37f, 0.61f, 1.f, 1.f);
    colors[ImGuiCol_Button] = ImVec4(0.20f, 0.25f, 0.29f, 1.f);
    colors[ImGuiCol_ButtonHovered] = ImVec4(0.28f, 0.56f, 1.f, 1.f);
    colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.f);
    //colors[ImGuiCol_Header] = ImVec4(0.2f, 0.25f, 0.29f, 0.55f);
    colors[ImGuiCol_Header] = ImVec4(0.1f, 0.1f, 0.1f, 0.8f);
    colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.8f);
    colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.f);
    colors[ImGuiCol_Separator] = ImVec4(0.2f, 0.25f, 0.29f, 1.f);
    colors[ImGuiCol_SeparatorHovered] = ImVec4(0.1f, 0.4f, 0.75f, 0.78f);
    colors[ImGuiCol_SeparatorActive] = ImVec4(0.1f, 0.4f, 0.75f, 1.f);
    colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
    colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
    colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
    colors[ImGuiCol_Tab] = ImVec4(0.11f, 0.15f, 0.17f, 1.f);
    colors[ImGuiCol_TabHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.8f);
    colors[ImGuiCol_TabActive] = ImVec4(0.2f, 0.25f, 0.29f, 1.f);
    colors[ImGuiCol_TabUnfocused] = ImVec4(0.11f, 0.15f, 0.17f, 1.f);
    colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.11f, 0.15f, 0.17f, 1.f);
    //colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.f);
    colors[ImGuiCol_PlotLines] = ImVec4(1.f, 0.3686f, 0.0039f, 1.f);
    colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.f, 0.43f, 0.35f, 1.f);
    colors[ImGuiCol_PlotHistogram] = ImVec4(0.9f, 0.7f, 0.f, 1.f);
    colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.f, 0.6f, 0.f, 1.f);
    colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
    colors[ImGuiCol_DragDropTarget] = ImVec4(1.f, 1.f, 0.f, 0.9f);
    colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.f);
    colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.f, 1.f, 1.f, 0.7f);
    colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.8f, 0.8f, 0.8f, 0.2f);
    colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.8f, 0.8f, 0.8f, 0.35f);
    colors[ImGuiCol_DockingPreview] = ImVec4(0.95f, 0.96f, 0.98f, 0.3f);
    colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.95f, 0.96f, 0.98f, 0.3f);

    colors[ImGuiCol_ResizeGrip] = ImVec4(0.f, 0.f, 0.f, 0.f);
}


void MainWindow::initDragDropData() {
    g_isDragAndDrop = false;
    g_dragNDropFiles.clear();
}


void MainWindow::handleDragDrop(GLFWwindow* window) {

    if (!m_scene)
        return;

    glfwSetDropCallback(window, dragDropCallBack);


    for (uint i = 0; i < MainWindow::g_dragNDropFiles.size(); ++i) {
        m_scene->loadFile(MainWindow::g_dragNDropFiles[i]);
    }

    deleteDragDropData();

}

void MainWindow::deleteDragDropData() {
    MainWindow::g_isDragAndDrop = false;
    MainWindow::g_dragNDropFiles.clear();
}

void MainWindow::setApplicationIcon(GLFWwindow* window) {

    m_mainIcon = new Texture("../Assets/ressources/Icons/main.png");
    m_mainIconGLFW = {64, 64, m_mainIcon->getData()};
    glfwSetWindowIcon(window, 1, &m_mainIconGLFW);

}
