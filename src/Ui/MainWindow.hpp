//
// Created by jb on 23/02/2020.
//

#ifndef MOVE_MAINWINDOW_HPP
#define MOVE_MAINWINDOW_HPP

#include <Ui/MoveObjectAttributes/MoveObjectParamsUI.hpp>
#include <Ui/MoveObjectAttributes/RendererUi.hpp>
#include <Ui/MoveObjectAttributes/RenderUi.hpp>
#include <Ui/libs_ui.hpp>
#include <Engine/Scenes/Scene.hpp>
#include <Engine/Scenes/ObjVis.hpp>
#include <Ui/TreeSceneWindowUi.hpp>
#include <Ui/MenuBar/MenuBarUi.hpp>
#include <chrono>

#include <iostream>

#include <sys/types.h>
#include <Core/System/Logger.hpp>

class MainWindow {

public :

    static bool g_isDragAndDrop;
    static std::vector<String> g_dragNDropFiles;

    MainWindow();
    ~MainWindow();

    void show();

private :

    void configTheme(ImGuiIO& io);
    void manageInputs(ImGuiIO& io);

    void initDragDropData();
    void deleteDragDropData();
    void handleDragDrop(GLFWwindow* window);

    void setApplicationIcon(GLFWwindow* window);

    void initMainWindow();

    void renderWindow();

    void cleanup();

    void loadUiConfig();
    void saveUiConfig();

    uint m_width;
    uint m_height;

    MoveObjectParamsUI m_params;
    RendererUI m_RendererUI;
    RenderUI m_renderui;
    TreeSceneWindowUI m_treeSceneGraph;
    MenuBarUI* m_menuBarUI = nullptr;

    ImFont* m_mainFont = nullptr;

    Scene* m_scene = nullptr;

    Texture* m_mainIcon = nullptr;
    GLFWimage m_mainIconGLFW;

    GLFWwindow* m_window = nullptr;

    ImGuiIO* m_io = nullptr;

    ImVec4 m_clearColor;

    ImGuiWindowFlags m_windowFlags = ImGuiWindowFlags_None;

    bool m_configFileExist = false;

    float m_uiConfigRefreshRate = 1.f; //In seconds
    double m_lastSave = 0.f;


};


#endif //MOVE_MAINWINDOW_HPP
