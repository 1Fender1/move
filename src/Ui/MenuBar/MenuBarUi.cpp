//
// Created by jb on 01/03/2020.
//

#include <iostream>
#include "MenuBarUi.hpp"


MenuBarUI::MenuBarUI() {
}

MenuBarUI::MenuBarUI(ImGuiIO* io) {
    m_io = io;
    //ImGui::FileBrowser fileDialog;
}

MenuBarUI::~MenuBarUI() {

}

void MenuBarUI::show() {
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            if (ImGui::MenuItem("Open")) {
                m_fileNavigatorDialog.open();
            }

            ImGui::MenuItem("Clear");

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Create")) {
            if (ImGui::BeginMenu("Light")) {
                if (ImGui::MenuItem("Point")) {
                    PointLight light;
                    light.setName("PointLight");
                    m_renderData->addObject(light);
                }
                if (ImGui::MenuItem("Spot")) {
                    SpotLight light;
                    light.setPosition(glm::vec3(0.f));
                    light.setDirection(glm::vec3(0.f, -1.f, 0.f));
                    light.setName("SpotLight");
                    m_renderData->addObject(light);
                }
                if (ImGui::MenuItem("Directional")) {
                    DirectionalLight light = DirectionalLight(glm::normalize(glm::vec3(0.5f, -0.5f, 0.f)), glm::vec3(1.f));
                    light.setName("DirectionalLight");
                    m_renderData->addObject(light);
                }
                if (ImGui::MenuItem("Area")) {
                    AreaLight light = AreaLight();
                    light.setName("AreaLight");
                    m_renderData->addObject(light);
                }
                if (ImGui::MenuItem("Environment")) {
                    LOG_ERROR("Environment light not implemented yet")
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Mesh")) {

                if (ImGui::MenuItem("Sphere")) {
                    Object sphere = BaseObjects::icoSphere(1.f, 5);
                    sphere.setName("Sphere");
                    m_renderData->addObject(sphere);
                }
                if (ImGui::MenuItem("Cylinder")) {
                    Object cylinder = BaseObjects::cylinder(2.f, 0.5f);
                    cylinder.setName("Cylinder");
                    m_renderData->addObject(cylinder);
                }
                if (ImGui::MenuItem("Cube")) {
                    Object cube = BaseObjects::cube(1.f, 1.f, 1.f);
                    cube.setName("Cube");
                    m_renderData->addObject(cube);
                }
                if (ImGui::MenuItem("Plane")) {
                    Object plane = BaseObjects::quad(1.f, 1.f);
                    plane.setName("Plane");
                    m_renderData->addObject(plane);
                }

                if (ImGui::MenuItem("Cone")) {
                    Object cone = BaseObjects::cone(1.f, 0.5f, 64);
                    cone.setName("Cone");
                    m_renderData->addObject(cone);
                }

                if (ImGui::MenuItem("Torus")) {
                    Object torus = BaseObjects::torus(0.5f, 0.1f);
                    torus.setName("Torus");
                    m_renderData->addObject(torus);
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Material")) {
                if (ImGui::MenuItem("Principled")) {
                    PrincipledMaterial material;
                    material.setName("Material");
                    m_renderData->addObject(material);
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Camera")) {
                if (ImGui::MenuItem("Free")) {
                    CameraFree camera = CameraFree(glm::vec3(2.f, 2.f, 4.f), 400.f, 600.f);
                    camera.setTarget(glm::vec3(0.f));
                    camera.setName("Free camera");
                    m_renderData->addObject(camera);
                }

                if (ImGui::MenuItem("Orbit")) {
                    CameraOrbit camera = CameraOrbit(glm::vec3(2.f, 2.f, 4.f), 400.f, 600.f);
                    camera.setTarget(glm::vec3(0.f));
                    camera.setName("Orbit camera");
                    m_renderData->addObject(camera);
                }
                ImGui::EndMenu();
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Tools")) {
            ImGui::PushItemFlag(ImGuiItemFlags_SelectableDontClosePopup, true);
            if (ImGui::MenuItem("Performances window", nullptr, m_isMesurePerf)) {
                if (m_renderUI)
                    m_renderUI->setMesuring(!m_isMesurePerf);
                m_isMesurePerf = !m_isMesurePerf;
            }
            ImGui::PopItemFlag();
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    m_fileNavigatorDialog.show();


    handleEvents();
}


void MenuBarUI::handleEvents() {

    String fileToLoad = m_fileNavigatorDialog.getUserPath();
    if (m_scene && !fileToLoad.empty())
        m_scene->loadFile(fileToLoad);

}

void MenuBarUI::setScene(Scene *scene) {
    m_scene = scene;
}