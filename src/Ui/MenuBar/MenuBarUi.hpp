//
// Created by jb on 01/03/2020.
//

#ifndef MOVE_MENUBARUI_HPP
#define MOVE_MENUBARUI_HPP

#include <Ui/libs_ui.hpp>
#include <Ui/FileBrowser/FileBrowser.hpp>
#include <Engine/Scenes/Scene.hpp>
#include <Ui/MoveObjectAttributes/RenderUi.hpp>

class MenuBarUI {


public :
    MenuBarUI();
    MenuBarUI(ImGuiIO*);

    ~MenuBarUI();

    void show();
    void handleEvents();
    void setScene(Scene* scene);

    void setRenderData(RenderData* renderData) {
        m_renderData = renderData;
    }

    void setRenderUI(RenderUI* renderUI) {
        m_renderUI = renderUI;
    }


private :


    ImGuiIO* m_io = nullptr;

    RenderData* m_renderData = nullptr;
    FileBrowser m_fileNavigatorDialog;
    std::string m_currentPath = "~/";

    Scene* m_scene = nullptr;

    RenderUI* m_renderUI = nullptr;
    bool m_isMesurePerf = false;
};


#endif //MOVE_MENUBARUI_HPP
