//
// Created by jb on 25/02/2020.
//

#include "MoveObjectParamsUI.hpp"
#include <glm/ext.hpp>
#include <glm/matrix.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>
#include <limits>

MoveObjectParamsUI::MoveObjectParamsUI(ImGuiIO* io, RenderData* renderData) {

    m_baseInPutFlags = ImGuiInputTextFlags_AllowTabInput | ImGuiInputTextFlags_EnterReturnsTrue;
    m_baseColorFlags = ImGuiColorEditFlags_Float | ImGuiColorEditFlags_RGB | ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_PickerHueWheel | ImGuiColorEditFlags_NoInputs;
    m_io = io;
    m_renderData = renderData;

    m_items.push_back("Physical");
    m_items.push_back("Custom");
    m_comboLightAttenuationSelected = &m_items[0][0];
}


void MoveObjectParamsUI::show(MoveObject* obj) {



    m_isNeedRefreshPathTracer = false;

    MoveObject::objectType type = obj ? obj->getType() : MoveObject::objectType::NONE;

    ImGui::Separator();

    if (ImGui::Begin("Attributes")) {

        //if (obj)
          //  ImGui::SetWindowFocus();
        if (obj) {
            std::string name = processString(obj->getName());
            if (!name.empty())
                obj->setName(name);
        }

        switch (type) {
            case MoveObject::objectType::LIGHT : {
                Light *light = (Light*)(obj);
                createLightUI(light);
            }
                break;
            case MoveObject::objectType::OBJECT : {
                Object *object = static_cast<Object*>(obj);
                createObjectUI(object);
            }
            break;
            case MoveObject::objectType::MESH : {
                Mesh* mesh = static_cast<Mesh*>(obj);
                createMeshUI(mesh);
            }
                break;
            case MoveObject::objectType::MATERIAL : {
                Material* material = (Material*)(obj);
                createMaterialUI(material);
            }
                break;
            case MoveObject::objectType::CAMERA : {
                Camera* camera = static_cast<Camera*>(obj);
                createCameraUI(camera);
            }
            break;
            default :
                break;

        }
        ImGui::End();
    }
}


glm::vec3 MoveObjectParamsUI::toDegreeDirection(glm::vec3 direction) {

    float x = glm::acos(direction.x);
    float y = glm::acos(direction.y);
    float z = glm::acos(direction.z);

    glm::vec3 dir = glm::degrees(glm::vec3(x, y, z));

    return dir;
}

glm::vec3 MoveObjectParamsUI::toNormalizedDirection(glm::vec3 direction) {

    glm::vec3 radDir = glm::radians(direction);
    float x = glm::cos(radDir.x);
    float y = glm::cos(radDir.y);
    float z = glm::cos(radDir.z);

    glm::vec3 outDir = glm::normalize(glm::vec3(x, y, z));

    return outDir;
}


std::string MoveObjectParamsUI::processString(std::string name) {
    uint nameSize = name.size();
    for (uint i = 0; i < nameSize; ++i)
        m_name[i] = name[i];
    m_name[nameSize] = '\0';
    if (ImGui::InputText("Name", &m_name[0], 64, ImGuiInputTextFlags_EnterReturnsTrue))
        return std::string(m_name);

    return std::string("");

}

void MoveObjectParamsUI::createLightUI(Light* light) {


//    bool isOpen = true;
    ImGuiWindowFlags flags = ImGuiWindowFlags_None;
    flags |= ImGuiWindowFlags_DockNodeHost;

    if (light) {

        glm::vec3 lightColor = light->getColor();
        if (ImGui::ColorEdit3("Light color", &lightColor[0], m_baseColorFlags)) {
            light->setColor(lightColor);
            m_isNeedRefreshPathTracer = true;
        }

        float lightIntensity = light->getIntensity();
        if (ImGui::DragFloat("Itensity", &lightIntensity, 0.1f, 0.f, 100.f)) {
            light->setIntensity(lightIntensity);
            m_isNeedRefreshPathTracer = true;
        }

        switch (light->getLightType()) {
            case Light::LightType::POINT : {
                createPointLightUI(static_cast<PointLight *>(light));
            }
            break;
            case Light::LightType::DIR : {
                createDirLightUI(static_cast<DirectionalLight *>(light));
            }
            break;
            case Light::LightType::SPOT : {
                createSpotLightUI(static_cast<SpotLight*>(light));
            }
            break;
            case Light::LightType::AREA : {
                createAreaLightUI(static_cast<AreaLight*>(light));
            }
            break;
            default : {
                LOG_ERROR("ParamsUI : Light type unkwnown")
            }
            break;
        }
    }

    if (light->getLightType() != Light::LightType::DIR) {
        ImGui::Separator();

        if (ImGui::BeginCombo("Attenuation", m_comboLightAttenuationSelected)) {
            for (uint n = 0; n < m_items.size(); n++) {
                bool is_selected = (m_comboLightAttenuationSelected == m_items[n]);
                if (ImGui::Selectable(m_items[n].c_str(), is_selected)) {
                    if (m_items[n] != std::string(m_comboLightAttenuationSelected)) {
                        m_isNeedRefreshPathTracer = true;
                    }
                    m_comboLightAttenuationSelected = &m_items[n][0];
                }
                if (is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        if (m_comboLightAttenuationSelected == m_items[0].c_str()) {
            if (!light->isAttenuationPhysical())
                m_isNeedRefreshPathTracer = true;
            light->setAttenuationPhysical(true);
        } else if (m_comboLightAttenuationSelected == m_items[1].c_str()) {
            if (light->isAttenuationPhysical())
                m_isNeedRefreshPathTracer = true;
            light->setAttenuationPhysical(false);

            float attConstant = light->getConstantAttenuation();
            float attLinear = light->getLinearAttenuation();
            float attQuad = light->getQuadraticAttenuation();

            if (ImGui::DragFloat("Constant", &attConstant, 0.1f, 0.f, 100.f)) {
                light->setConstantAttenuation(attConstant);
            }
            if (ImGui::DragFloat("Linear", &attLinear, 0.1f, 0.f, 100.f)) {
                light->setLinearAttenuation(attLinear);
            }
            if (ImGui::DragFloat("Quadratic", &attQuad, 0.1f, 0.f, 100.f)) {
                light->setQuadraticAttenuation(attQuad);
            }
        }
    }

    ImGui::Separator();

    if (ImGui::CollapsingHeader("Raytracer")) {

        int samplesCount = int(light->getSampleCount());
        if (ImGui::DragInt("Samples", &samplesCount, 1, 1, 1024)) {
            samplesCount = glm::max(0, samplesCount);
            light->setSampleCount(uint(samplesCount));
            m_isNeedRefreshPathTracer = true;
        }

    }

}

void MoveObjectParamsUI::createPointLightUI(PointLight* light) {


    glm::vec3 position = light->getPosition();

    if (ImGui::InputFloat3("Translation", &position[0], m_baseInPutFlags)) {
        light->setPosition(position);
        m_isNeedRefreshPathTracer = true;
    }
}



void MoveObjectParamsUI::createDirLightUI(DirectionalLight* light) {

    glm::vec3 direction = light->getDirection();
    if (ImGui::InputFloat3("Direction", &direction[0], m_baseInPutFlags)) {
        direction = glm::normalize(direction);
        light->setDirection(direction);
        m_isNeedRefreshPathTracer = true;
    }

}

void MoveObjectParamsUI::createSpotLightUI(SpotLight* light) {

    if (light == nullptr) {
        LOG_ERROR("Error light is null")
        return;
    }

    glm::vec3 position = light->getPosition();
    if (ImGui::InputFloat3("Position##SpotLight", &position[0], m_baseInPutFlags)) {
        light->setPosition(position);
        m_isNeedRefreshPathTracer = true;
    }

    glm::vec3 direction = light->getDirection();
    if (ImGui::InputFloat3("Direction##SpotLight", &direction[0], m_baseInPutFlags)) {
        light->setDirection(normalize(direction));
        m_isNeedRefreshPathTracer = true;
    }


    float outerAngle = light->getOAngle();
    float degreeOuterAngle = glm::degrees(outerAngle) * 2.f;
    if (ImGui::DragFloat("Angle##SpotLight", &degreeOuterAngle, 1.f, 1.f, 180.f)) {

        if (degreeOuterAngle == 180.f)
            degreeOuterAngle = 179.0f;

        float rad = glm::radians(degreeOuterAngle) / 2.f;

        m_isNeedRefreshPathTracer = true;

        float blend = glm::degrees(light->getIAngle()) / glm::degrees(light->getOAngle());

        light->setOAngle(rad);

        light->setIAngle(light->getOAngle() * blend);
    }

    float blend = glm::degrees(light->getIAngle()) / glm::degrees(light->getOAngle());
    if (ImGui::DragFloat("Blend##SpotLight", &blend, 0.01f, 0.f, 1.f)) {
        m_isNeedRefreshPathTracer = true;

        light->setIAngle(light->getOAngle() * blend);
    }


}

void MoveObjectParamsUI::createAreaLightUI(AreaLight* light) {

    //glm::vec3 direction = light->getDirection();

    //glm::vec3 degDir = toDegreeDirection(direction);
/*
    if (ImGui::InputFloat3("Direction", &degDir[0], m_baseInPutFlags)) {
        direction = toNormalizedDirection(degDir);
        light->setDirection(direction);
        m_isNeedRefreshPathTracer = true;
    }
*/
    glm::vec3 position = light->getPosition();
    if (ImGui::InputFloat3("Position", &position[0], m_baseInPutFlags)) {
        light->setPosition(position);
        m_isNeedRefreshPathTracer = true;
    }

    float width = light->getWidth();
    if (ImGui::InputFloat("Width", &width, m_baseInPutFlags)) {
        light->setWidth(width);
        m_isNeedRefreshPathTracer = true;
    }

    float height = light->getHeight();
    if (ImGui::InputFloat("Height", &height, m_baseInPutFlags)) {
        light->setHeight(height);
        m_isNeedRefreshPathTracer = true;
    }

    bool doubleSided = light->isDoubleSided();
    if (ImGui::Checkbox("Double sided", &doubleSided)) {
        light->setDoubleSided(doubleSided);
        m_isNeedRefreshPathTracer = true;
    }
}

bool equalEps(glm::mat4 mat1, glm::mat4 mat2, float eps) {
    for (uint i = 0; i < 4; ++i) {
        glm::vec4 v1 = mat1[i];
        glm::vec4 v2 = mat2[i];
        for (uint j = 0; j < 4; ++j) {
            if (glm::abs(v1[j] - v2[j]) > eps)
                return false;
        }
    }
    return true;
}


void MoveObjectParamsUI::createObjectUI(Object* obj) {

    glm::mat4 model = obj->getModel(0);
    glm::mat4 newMat = transformMatrixUI(model);
    float eps = 0.001f;
    if (!equalEps(newMat, model, eps)) {
        //obj->setModel(newMat);
        obj->setTransform(newMat);
    }
}

//TODO copy this code to a math utils file
void decompose(glm::mat4 mat, glm::vec3& translation, glm::vec3& scale, glm::mat4& rotation) {
    translation = mat[3];

    float scaleX = glm::length(mat[0]);
    float scaleY = glm::length(mat[1]);
    float scaleZ = glm::length(mat[2]);

    scale = glm::vec3(scaleX, scaleY, scaleZ);

    rotation[0] = mat[0] / scaleX;
    rotation[1] = mat[1] / scaleY;
    rotation[2] = mat[2] / scaleZ;
}


glm::mat4 MoveObjectParamsUI::transformMatrixUI(const glm::mat4& mat) {

    glm::vec3 translation;
    glm::mat4 rotation;
    glm::vec3 scale;

    decompose(mat, translation, scale, rotation);

    ImGuiInputTextFlags flag = ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_AutoSelectAll | ImGuiInputTextFlags_Multiline;

    if (ImGui::InputFloat3("Translation", &translation[0]), flag) {
    }
    else {
    }

    glm::vec3 angles;
    glm::extractEulerAngleXYZ(rotation, angles.x, angles.y, angles.z);
    glm::vec3 degreeAngles = glm::degrees(angles);
    glm::mat4 rotMat = rotation;
    if (ImGui::InputFloat3("Rotation", &degreeAngles[0]), flag) {
        angles = glm::radians(degreeAngles);
        rotMat = glm::eulerAngleXYZ(angles[0], angles[1], angles[2]);
    }
    else {
    }

    if (ImGui::InputFloat3("Scale", &scale[0]), flag) {
        float eps = 0.01f;
        if (scale.x < eps)
            scale.x = eps;
        if (scale.y < eps)
            scale.y = eps;
        if (scale.z < eps)
            scale.z = eps;
    }
    else {
    }

//    glm::mat4 newMat = glm::mat4(1.f);
    glm::mat4 scaleMat = glm::scale(glm::mat4(1.f), scale);
    glm::mat4 translateMat = glm::translate(glm::mat4(1.f), translation);

    return translateMat * rotMat * scaleMat;
}

void MoveObjectParamsUI::createMeshUI(Mesh* mesh) {

    glm::mat4 model = mesh->getModel(0);
    glm::mat4 newMat = transformMatrixUI(model);
    float eps = 0.001f;
    if (!equalEps(newMat, model, eps)) {
        //mesh->setModel(newMat);
        mesh->setTransform(newMat);
    }


    uint materialCount = m_renderData->getMaterialCount();
    std::string *items = new std::string[materialCount];

    for (uint i = 0; i < materialCount; ++i) {
        items[i] = m_renderData->getMaterial(i)->getName();
    }
/*
    std::string itemCurrent = mesh->getMaterial()->getName();
    if (ImGui::BeginCombo("Material", itemCurrent.c_str())) {

    }
*/


    std::string item_current = mesh->getMaterial()->getName();
    if (ImGui::BeginCombo("Material", item_current.c_str())) {
        for (uint i = 0; i < m_renderData->getMaterialCount(); ++i) {

            bool is_selected = (item_current == items[i]);
            if (ImGui::Selectable(items[i].c_str(), is_selected))
                item_current = items[i];
            if (is_selected)
                ImGui::SetItemDefaultFocus();
        }
        ImGui::EndCombo();
    }
    if (item_current != mesh->getMaterial()->getName())
        mesh->setMaterial(m_renderData->getMaterial(item_current));

    if (ImGui::Button("Goto Material")) {
        m_selectedItem = m_renderData->getMaterial(item_current);
    }/*
    else {
        m_selectedItem = nullptr;
    }*/

    delete [] items;
}

void MoveObjectParamsUI::createMaterialUI(Material* mat) {

    switch (mat->getMaterialType()) {

        case Material::materialType::PRINCIPLED : {
            createPrincipledMatUI(reinterpret_cast<PrincipledMaterial*>(mat));
        }
        break;
        default:
        break;

    }

}


void MoveObjectParamsUI::createPrincipledMatUI(PrincipledMaterial* mat) {


    if (ImGui::CollapsingHeader("Diffuse")) {

        glm::vec4 baseColor = mat->getDiffuseColor();
        if (ImGui::ColorEdit4("Diffuse color", &baseColor[0], m_baseColorFlags)) {
            mat->setDiffuseColor(baseColor);
            m_isNeedRefreshPathTracer = true;
        }

        float diffuseRoughness = mat->getDiffuseRoughness();
        if (ImGui::DragFloat("Diffuse roughness", &diffuseRoughness, 0.01f, 0.f, 1.f)) {
            if (diffuseRoughness < 0.0001f)
                diffuseRoughness = 0.0001f;
            mat->setDiffuseRoughness(diffuseRoughness);
            m_isNeedRefreshPathTracer = true;
        }
    }

    ImGui::Separator();

    if (ImGui::CollapsingHeader("Specular")) {

        float roughness = mat->getRoughness();
        if (ImGui::DragFloat("Roughness", &roughness, 0.01f, 0.f, 1.f)) {
            if (roughness < 0.0001f)
                roughness = 0.0001f;
            mat->setRoughness(roughness);
            m_isNeedRefreshPathTracer = true;
        }

        float metalness = mat->getMetalness();
        if (ImGui::DragFloat("Metalness", &metalness, 0.01f, 0.f, 1.f)) {
            mat->setMetalness(metalness);
            m_isNeedRefreshPathTracer = true;
        }

    }

    ImGui::Separator();

    if (ImGui::CollapsingHeader("Emissive")) {

        glm::vec3 emissiveColor = mat->getEmissiveColor();
        if (ImGui::ColorEdit3("Emissive color", &emissiveColor[0], m_baseColorFlags)) {
            mat->setEmissiveColor(emissiveColor);
            m_isNeedRefreshPathTracer = true;
        }

       // float emissiveIntensity =

    }




    ImGui::Separator();


    float ior = mat->getIOR();
    if (ImGui::DragFloat("IOR", &ior, 0.1f, 0.f, 10.f)) {
        mat->setIOR(ior);
        m_isNeedRefreshPathTracer = true;
    }


}


void MoveObjectParamsUI::createCameraFreeUI(CameraFree* ) {

}

void MoveObjectParamsUI::createCameraOrbitUI(CameraOrbit* cam) {

    float scrollSpeed = cam->getScrollSpeed();
    if (ImGui::DragFloat("Scrool speed", &scrollSpeed, 1.f, 0.01f, 100.f)) {
        if (scrollSpeed > 0.f) {
            cam->setScrollSpeed(scrollSpeed);
        }
    }

}


void MoveObjectParamsUI::createCameraUI(Camera* cam) {

    if (!cam)
        return;

    Frustum& frustum = cam->getFrustum();
    float velocity = cam->getSpeed();
    if (ImGui::DragFloat("Speed", &velocity, 0.5f, 0.f, 1000.f)) {
        if (velocity > 0.f)
            cam->setSpeed(velocity);
    }

    ImGui::LabelText("##Frustum Camera", "Frustum");

    float minDist = cam->getFrustum().getNear();
    if (ImGui::InputFloat("Near distance", &minDist, 1.f, 100.f)) {
            frustum.setNear(minDist);
    }

    float maxDist = frustum.getFar();
    if (ImGui::InputFloat("Far distance", &maxDist, 1.f, 100.f)) {
            frustum.setFar(maxDist);
    }

    float fov = frustum.getFOV();
    if (ImGui::DragFloat("FOV", &fov, 1.f, 10.f, 150.f)) {
        if (fov > 0.f)
            frustum.setFOV(fov);
        m_isNeedRefreshPathTracer = true;
    }

    switch (cam->getCameraType()) {

        case Camera::cameraType::ORBIT : {
            createCameraOrbitUI(reinterpret_cast<CameraOrbit*>(cam));
        }
        break;
        case Camera::cameraType::FREE : {
            createCameraFreeUI(reinterpret_cast<CameraFree*>(cam));
        }
        break;
        default : {

        }
        break;

    }

    if (ImGui::Button("Set main")) {
        m_scene->setCamera(cam);
    }

}


void MoveObjectParamsUI::setScene(Scene* scene) {
    m_scene = (ObjVis*)scene;
    m_renderData = m_scene->getRenderData();
}

MoveObject* MoveObjectParamsUI::getSelectedItem() {
    return m_selectedItem;

}

void MoveObjectParamsUI::setSelectedItem(MoveObject* obj) {
    m_selectedItem = obj;
}