//
// Created by jb on 25/02/2020.
//

#ifndef MOVE_LIGHTSUI_HPP
#define MOVE_LIGHTSUI_HPP

#include <Ui/libs_ui.hpp>

#include <Engine/Rendering/Light/Lights.hpp>
#include <Engine/Camera/Camera.hpp>
#include <Engine/Scenes/RenderData.hpp>
#include <Engine/Scenes/ObjVis.hpp>

class MoveObjectParamsUI {


public :
    MoveObjectParamsUI(ImGuiIO* io = nullptr, RenderData* renderData = nullptr);
    void show(MoveObject* obj);
    void setScene(Scene* scene);
    bool isNeedUpdatePathTracer() {return m_isNeedRefreshPathTracer;}

    MoveObject* getSelectedItem();
    void setSelectedItem(MoveObject* obj);

private :

    void createLightUI(Light* light);
    void createPointLightUI(PointLight* light);
    void createDirLightUI(DirectionalLight* light);
    void createSpotLightUI(SpotLight* light);
    void createAreaLightUI(AreaLight* light);

    void createObjectUI(Object* obj);

    void createMeshUI(Mesh* mesh);

    void createMaterialUI(Material* mat);
    void createPrincipledMatUI(PrincipledMaterial* mat);


    void createCameraUI(Camera* cam);
    void createCameraFreeUI(CameraFree* cam);
    void createCameraOrbitUI(CameraOrbit* cam);


    glm::mat4 transformMatrixUI(const glm::mat4& mat);
    std::string processString(std::string name);

    glm::vec3 toDegreeDirection(glm::vec3 direction);
    glm::vec3 toNormalizedDirection(glm::vec3 direction);


    ImGuiInputTextFlags m_baseInPutFlags;
    ImGuiColorEditFlags  m_baseColorFlags;

    RenderData* m_renderData = nullptr;
    ObjVis* m_scene = nullptr;

    std::string m_currentMaterial = "";

    ImGuiIO* m_io = nullptr;

    char m_name[64];

    bool m_isNeedRefreshPathTracer = false;


    std::vector<String> m_items;
    char* m_comboLightAttenuationSelected = nullptr;

    MoveObject* m_selectedItem = nullptr;



};



#endif //MOVE_LIGHTS_HPP
