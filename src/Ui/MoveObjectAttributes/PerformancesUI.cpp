//
// Created by jb on 29/11/2020.
//

#include "PerformancesUI.hpp"
#include <Core/Math/Random/SamplerMT.hpp>

PerformancesUI::PerformancesUI() {

    m_GPUTimes.resize(m_maxTimesSaved, 0.f);
    m_CPUTimes.resize(m_maxTimesSaved, 0.f);
    m_totalTimes.resize(m_maxTimesSaved, 0.f);
}


void PerformancesUI::show() {

    ImGui::Begin("Performances");

    float minGPUTime, maxGPUTime, minCPUTime, maxCPUTime, minTotal, maxTotal;
    getMinMax(m_GPUTimes, minGPUTime, maxGPUTime);
    getMinMax(m_CPUTimes, minCPUTime, maxCPUTime);
    getMinMax(m_totalTimes, minTotal, maxTotal);

    float fps = 1.f/(m_totalTimes.back() / 1000.f);
    String fpsStr = "FPS : " + std::to_string(fps);
    ImGui::LabelText("##Label FPS", fpsStr.c_str());



    ImGui::PlotLines("GPU##label GPU Time", m_GPUTimes.data(), m_GPUTimes.size(), 5, "", minGPUTime, maxGPUTime, ImVec2(0.f, 80.f));
    String GPUTime = std::to_string(m_GPUTimes.back());
    GPUTime += " ms";
    ImGui::LabelText("##Label GPUTime", GPUTime.c_str());

    ImGui::PlotLines("CPU##label CPU Time", m_CPUTimes.data(), m_CPUTimes.size(), 5, "", minCPUTime, maxCPUTime, ImVec2(0.f, 80.f));
    String CPUTime = std::to_string(m_CPUTimes.back());
    CPUTime += " ms";
    ImGui::LabelText("##Label GPUTime", CPUTime.c_str());

    ImGui::PlotLines("Frame##label total Time", m_totalTimes.data(), m_totalTimes.size(), 5, "", minTotal, maxTotal, ImVec2(0.f, 80.f));
    String totalTime = std::to_string(m_totalTimes.back());
    totalTime += " ms";
    ImGui::LabelText("##Label frame time", totalTime.c_str());



    ImGui::End();
}


void PerformancesUI::querryGPUStart() {
    glGenQueries(1, &m_timeQuerry);
    glBeginQuery(GL_TIME_ELAPSED, m_timeQuerry);
}

void PerformancesUI::querryGPUEnd() {
    glEndQuery(GL_TIME_ELAPSED);
    GLint time;
    glGetQueryObjectiv(m_timeQuerry, GL_QUERY_RESULT, &time);
    addValue(m_GPUTimes, float(time) / 1000000.f);
}


void PerformancesUI::querryCPUStart() {
    m_cpuChrono.start();
}

void PerformancesUI::querryCPUEnd() {
    float time = m_cpuChrono.finish();
    addValue(m_CPUTimes, time*1000.f);
}

void PerformancesUI::querryTotalTimeStart() {
    m_querryChrono.start();
}

void PerformancesUI::querryTotalTimeEnd() {
    m_lastTotalTime = m_querryChrono.finish();
    addValue(m_totalTimes, m_lastTotalTime*1000.f);
}

void PerformancesUI::shift(std::vector<float>& vec) {
    for (uint i = 0; i < m_maxTimesSaved-1; ++i)
        vec[i] = vec[i+1];
}

void PerformancesUI::addValue(std::vector<float>& vec, float value) {

    if (m_timeAcc < m_refreshTime)
        return;

    shift(vec);
    vec[m_maxTimesSaved-1] = value;
}

void PerformancesUI::getMinMax(const std::vector<float>& vec, float& min, float& max) {
    float minTemp = std::numeric_limits<float>::max();
    float maxTemp = 0;
    uint size = vec.size();
    for (uint i = 0; i < size; ++i) {
        minTemp = minTemp > vec[i] ? vec[i] : minTemp;
        maxTemp = maxTemp < vec[i] ? vec[i] : maxTemp;
    }
    min = minTemp;
    max = maxTemp;
}


void PerformancesUI::querryTimeStart() {
    querryTotalTimeStart();
    querryCPUStart();
    querryGPUStart();
}

void PerformancesUI::querryTimeEnd() {
    querryCPUEnd();
    glFlush();
    querryGPUEnd();
    querryTotalTimeEnd();
    if (m_timeAcc > m_refreshTime)
        m_timeAcc = 0.f;
    m_timeAcc += m_lastTotalTime;
}