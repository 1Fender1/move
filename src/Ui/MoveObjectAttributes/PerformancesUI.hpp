//
// Created by jb on 29/11/2020.
//

#ifndef MOVE_PERFORMANCESUI_HPP
#define MOVE_PERFORMANCESUI_HPP
#include <Ui/libs_ui.hpp>
#include <stack>
#include <Core/System/Utility/Chrono.hpp>

class PerformancesUI {

public :
    PerformancesUI();
    void show();

    void querryTimeStart();
    void querryTimeEnd();

private :

    void querryGPUStart();
    void querryGPUEnd();

    void querryCPUStart();
    void querryCPUEnd();

    void querryTotalTimeStart();
    void querryTotalTimeEnd();

    void shift(std::vector<float>& vec);
    void addValue(std::vector<float>& vec, float value);
    void getMinMax(const std::vector<float>& vec, float& min, float& max);

    //Times in ms
    std::vector<float> m_GPUTimes;
    std::vector<float> m_CPUTimes;

    std::vector<float> m_totalTimes;
    float m_lastTotalTime = 0.f;

    static constexpr uint m_maxTimesSaved = 100;


    //uint m_GPUTimesIndex = m_maxTimesSaved-1;
    //uint m_CPUTimesIndex = m_maxTimesSaved-1;
    Chrono m_querryChrono;
    float m_timeAcc = 0.f;
    static constexpr float m_refreshTime = 0.1f;//In seconds

    Chrono m_cpuChrono;
    GLuint m_timeQuerry;


};


#endif //MOVE_PERFORMANCESUI_HPP
