//
// Created by jb on 29/02/2020.
//

#include "RenderUi.hpp"
#include <Core/glAssert.hpp>

RenderUI::RenderUI() {

    m_windowFlags = ImGuiWindowFlags_None | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar;

}

RenderUI::RenderUI(Scene* scene, ImGuiIO* io, RenderType renderType) : RenderUI() {
    m_scene = scene;
    m_io = io;
    m_renderType = renderType;

}


bool RenderUI::isShouldResizeGL(uint w, uint h) {
    if (w != m_widthGL || h != m_heightGL) {
        m_widthGL = w;
        m_heightGL = h;
        return true;
    }
    return false;
}

bool RenderUI::isShouldResizePT(uint w, uint h) {
    if (w != m_widthPT || h != m_heightPT) {
        m_widthPT = w;
        m_heightPT = h;
        return true;
    }
    return false;
}

void RenderUI::show() {

    RenderData* renderData = m_scene->getRenderData();
    if (renderData->isDirtyScene()) {
        m_scene->getPTRenderer()->refresh();
        renderData->unDirtyScene();
    }

    showForward();
    showPathtracer();
}

bool RenderUI::acceptInputs(ImVec2 mousePos) {

    ImVec2 windowSize = ImGui::GetWindowSize();

    if (mousePos.x >= 0.f && mousePos.x <= windowSize.x) {
        if (mousePos.y >= 0.f && mousePos.y <= windowSize.y) {
            return true;
        }
    }
    return false;
}


void RenderUI::manageInputs() {

    if (!ImGui::IsWindowFocused())
        return;

    ImVec2 currentPos = m_io->MousePos;
    currentPos.x = m_io->MousePos.x - ImGui::GetCursorScreenPos().x;
    currentPos.y = m_io->MousePos.y - ImGui::GetCursorScreenPos().y;

    float widthT, heightT;
    computeWindowSize(widthT, heightT);
    m_scene->resize(widthT, heightT);

//    bool isClicked = m_io->MouseDown[ImGuiMouseButton_Right] | m_io->MouseDown[ImGuiMouseButton_Left] | m_io->MouseDown[ImGuiMouseButton_Middle];
    bool acceptPos = acceptInputs(currentPos);

    m_scene->mousemove(currentPos.x, currentPos.y, m_io->DeltaTime);

    if ((m_io->MouseDelta.x > 0.f || m_io->MouseDelta.y > 0.f) /*&& acceptPos*/) {
        //m_scene->mousemove(currentPos.x, currentPos.y);
    }

    if (m_io->MouseClicked[ImGuiMouseButton_Right]) {
        m_scene->mouseclick(1, currentPos.x, currentPos.y);
    }

    if (m_io->MouseClicked[ImGuiMouseButton_Left]) {
        m_scene->mouseclick(0, currentPos.x, currentPos.y);
    }

    if (m_io->MouseReleased[ImGuiMouseButton_Left]) {
        m_scene->mouseRelease(0, currentPos.x, currentPos.y);
    }
    if (m_io->MouseReleased[ImGuiMouseButton_Right]) {
        m_scene->mouseRelease(1, currentPos.x, currentPos.y);
    }

    if (m_io->MouseWheel != 0.f) {
        m_scene->wheelScroll(m_io->MouseWheel);
    }


    if (m_io->MouseDoubleClicked[ImGuiMouseButton_Left] && acceptPos) {
        float x = currentPos.x;
        float y = currentPos.y;
        float widthTmp, heightTmp;
        computeWindowSize(widthTmp, heightTmp);
        m_scene->resize(widthTmp, heightTmp);

        m_scene->mouseDoubleClick(0, x, y);
        m_selectedItem = m_scene->getSelectedObject();
    }


    float deltaTime = m_io->DeltaTime;
/*
    if (!ImGui::IsWindowFocused())
        return;
*/
    if (!acceptPos)
        return;

    //TODO do correct mapping qwerty/azerty
    if (m_io->KeysDown[GLFW_KEY_W]) {
        m_scene->keyboard(GLFW_KEY_Z);
        m_scene->keyboardmove(GLFW_KEY_Z, deltaTime);
    }

    if (m_io->KeysDown[GLFW_KEY_A]) {
        m_scene->keyboard(GLFW_KEY_Q);
        m_scene->keyboardmove(GLFW_KEY_Q, deltaTime);
    }

    if (m_io->KeysDown[GLFW_KEY_S]) {
        m_scene->keyboard(GLFW_KEY_S);
        m_scene->keyboardmove(GLFW_KEY_S, deltaTime);
    }

    if (m_io->KeysDown[GLFW_KEY_D]) {
        m_scene->keyboard(GLFW_KEY_D);
        m_scene->keyboardmove(GLFW_KEY_D, deltaTime);
    }


}


MoveObject* RenderUI::getSelectedItem() {
    return m_selectedItem;
}

void RenderUI::showForward() {

    m_selectedItem = nullptr;
//    bool isOpen = true;
    float width = 0;
    float height = 0;

    m_prevTime = m_startTime;
    m_startTime = clock();
    m_ellapse += m_startTime-m_prevTime;
    float ellapse = float(m_ellapse) / float(CLOCKS_PER_SEC);

    m_scene->setIsForwardDraw(true);
    m_scene->setIsPathTracerDraw(false);


    if (ImGui::Begin("RenderGL", nullptr, m_windowFlags)) {

        showMenuBar();

        computeWindowSize(width, height);

        manageInputs();
        glViewport(0, 0, width, height);
        m_scene->resize(width, height);


        if (m_scene != nullptr) {
            if (isShouldResizeGL(width, height)) {
                m_scene->resize(uint(width), uint(height));
                m_scene->getGLRenderer()->resize(width, height);
            }

            if (ellapse >= ((1.f/float(m_frameRate)))) {

                drawGL();

                m_lastTexID = m_scene->getTexForward();

                 m_ellapse = m_ellapse - clock_t(float(CLOCKS_PER_SEC) * float(ellapse - (1.f/float(m_frameRate)) ));
                ImGui::Image((void *)m_lastTexID, ImVec2(width, height), ImVec2(0, 1), ImVec2(1, 0));
            }
            else {
                ImVec2 cursorPos = ImGui::GetCursorScreenPos();

                m_lastTexID = m_scene->getTexForward();

                ImGui::Image((void *)m_lastTexID, cursorPos,
                             ImVec2(width, height));
                 }
        }
    }

    ImGui::End();

    m_scene->setIsForwardDraw(false);
}

void RenderUI::showPathtracer() {

//    bool isOpen = true;
    float width = 0;
    float height = 0;

    m_prevTime = m_startTime;
    m_startTime = clock();
    m_ellapse += m_startTime-m_prevTime;
    float ellapse = float(m_ellapse) / float(CLOCKS_PER_SEC);
    m_scene->setIsPathTracerDraw(true);
    m_scene->setIsForwardDraw(false);


    if (ImGui::Begin("RenderPathTracer", nullptr, m_windowFlags)) {
        computeWindowSize(width, height);
        manageInputs();

        showMenuBar();

        m_scene->resize(width, height);

        if (m_scene != nullptr) {
//            uint oldWidth = m_widthPT;
//            uint oldHeight = m_heightPT;
            if (isShouldResizePT(width, height)) {
                m_scene->resize(width, height);
                m_scene ->getPTRenderer()->resize(width, height);
                m_scene->getPTRenderer()->refresh();
            }
            glViewport(0, 0, width, height);

//            ImVec2 cursorPos = ImGui::GetCursorScreenPos();
            //m_scene->setIsForwardDraw(false);
            m_scene->draw();
            m_lastTexID = m_scene->getTexPathTracer();
/*
            ImGui::GetWindowDrawList()->AddImage(
                    (void *)m_lastTexID, cursorPos,
                    ImVec2(cursorPos.x + m_width, cursorPos.y + m_height), ImVec2(0, 1), ImVec2(1, 0));
            */
            ImGui::Image((void *)m_lastTexID, ImVec2(width, height), ImVec2(0, 1), ImVec2(1, 0));

            m_ellapse = m_ellapse - clock_t(float(CLOCKS_PER_SEC) * float(ellapse - (1.f/float(m_frameRate)) ));

        }

    }
    ImGui::End();
    m_scene->setIsPathTracerDraw(false);

}

void RenderUI::computeWindowBoundingBox(float& minX, float& minY, float& maxX, float& maxY) {

    ImVec2 vMin = ImGui::GetWindowContentRegionMin();
    ImVec2 vMax = ImGui::GetWindowContentRegionMax();

    vMin.x += ImGui::GetWindowPos().x;
    vMin.y += ImGui::GetWindowPos().y;
    vMax.x += ImGui::GetWindowPos().x;
    vMax.y += ImGui::GetWindowPos().y;

    minX = vMin.x;
    minY = vMin.y;
    maxX = vMax.x;
    maxY = vMax.y;

}

void RenderUI::computeWindowSize(float& width, float& height) {

    ImVec2 windowBboxMin, windowBboxMax;
    computeWindowBoundingBox(windowBboxMin.x, windowBboxMin.y, windowBboxMax.x, windowBboxMax.y);

    width = glm::abs(windowBboxMax.x - windowBboxMin.x);
    height = glm::abs(windowBboxMax.y - windowBboxMin.y);
}

void RenderUI::showMenuBar() {
    if (ImGui::BeginMenuBar()) {
        String mousePointerIcon = Icon::mouse_pointer + "## renderUI_mouse-pointer";
        Gizmo* gizmo = m_scene->getGizmo();

        ImVec4 activeColor = ImVec4(0.28f, 0.56f, 1.f, 1.f);

        if (m_gizmoState == RenderUI::MenuBarEnum::SELECTION)
            ImGui::PushStyleColor(ImGuiCol_Button, activeColor);
        else
            ImGui::PushStyleColor(ImGuiCol_Button, ImGuiCol_Button);

        if (ImGui::Button(mousePointerIcon.c_str())) {
            gizmo->setTransformType(Gizmo::GTransformMode::TRANSFORM_COUNT);
            m_gizmoState = RenderUI::MenuBarEnum::SELECTION;
        }
        ImGui::PopStyleColor(1);

        if (m_gizmoState == RenderUI::MenuBarEnum::GIZMO_TRANSLATION)
            ImGui::PushStyleColor(ImGuiCol_Button, activeColor);
        else
            ImGui::PushStyleColor(ImGuiCol_Button, ImGuiCol_Button);
        String translationGizmoIcon = Icon::translation + "## renderUI_translation";
        if (ImGui::Button(translationGizmoIcon.c_str())) {
            gizmo->setTransformType(Gizmo::GTransformMode::Translation);
            m_gizmoState = RenderUI::MenuBarEnum::GIZMO_TRANSLATION;
        }

        ImGui::PopStyleColor(1);

        if (m_gizmoState == RenderUI::MenuBarEnum::GIZMO_SCALE)
            ImGui::PushStyleColor(ImGuiCol_Button, activeColor);
        else
            ImGui::PushStyleColor(ImGuiCol_Button, ImGuiCol_Button);
        String scaleGizmoIcon = Icon::scale + "## renderUI_scale";
        if (ImGui::Button(scaleGizmoIcon.c_str())) {
            gizmo->setTransformType(Gizmo::GTransformMode::Scale);
            m_gizmoState = RenderUI::MenuBarEnum::GIZMO_SCALE;
        }
        ImGui::PopStyleColor(1);

        if (m_gizmoState == RenderUI::MenuBarEnum::GIZMO_ROTATION)
            ImGui::PushStyleColor(ImGuiCol_Button, activeColor);
        else
            ImGui::PushStyleColor(ImGuiCol_Button, ImGuiCol_Button);
        String rotateGizmoIcon = Icon::rotation + "## renderUI_rotation";
        if (ImGui::Button(rotateGizmoIcon.c_str())) {
            gizmo->setTransformType(Gizmo::GTransformMode::Rotation);
            m_gizmoState = RenderUI::MenuBarEnum::GIZMO_ROTATION;
        }
        ImGui::PopStyleColor(1);

        ImGui::EndMenuBar();
    }
}

void RenderUI::setMesuring(bool mesure) {
    m_isMesurePerformances = mesure;
}

void RenderUI::drawGL() {

    if (m_isMesurePerformances) {
        m_performancesUi.querryTimeStart();
    }
    m_scene->draw();
    if (m_isMesurePerformances) {
        m_performancesUi.querryTimeEnd();
        m_performancesUi.show();
    }
}