//
// Created by jb on 29/02/2020.
//

#ifndef MOVE_RENDERUI_HPP
#define MOVE_RENDERUI_HPP

#include <Ui/libs_ui.hpp>
#include <Engine/Scenes/Scene.hpp>
#include <Engine/Scenes/ObjVis.hpp>
#include <ctime>
#include <Ui/Font/ShortCutIcons.hpp>
#include <Ui/MoveObjectAttributes/PerformancesUI.hpp>

#define ImDrawCallback_ResetState reinterpret_cast<ImDrawCallback>(-1)

class RenderUI {

public :

    enum RenderType {

        FORWARD,
        PATHTRACER,

    };

    RenderUI();
    RenderUI(Scene* scene, ImGuiIO* io, RenderType renderType);
    void show();
    void showForward();
    void showPathtracer();

    bool isRendered();

    MoveObject* getSelectedItem();

    void manageInputs();

    void setMesuring(bool);

private :

    enum MenuBarEnum {

        SELECTION,
        GIZMO_TRANSLATION,
        GIZMO_SCALE,
        GIZMO_ROTATION,
        MENUBAR_COUNT

    };

    bool acceptInputs(ImVec2 mousePos);

    static void draw(const ImDrawList* , const ImDrawCmd* cmd) {

        ObjVis* scene = (ObjVis*)cmd->UserCallbackData;
        scene->draw();

    }

    bool isShouldResizeGL(uint w, uint h);
    bool isShouldResizePT(uint w, uint h);


    void computeWindowBoundingBox(float& minX, float& minY, float& maxX, float& maxY);
    void computeWindowSize(float& width, float& height);

    void showMenuBar();

    void drawGL();

    Scene *m_scene = nullptr;
    ImGuiIO* m_io = nullptr;

    uint m_widthGL = 0;
    uint m_heightGL = 0;
    uint m_widthPT = 0;
    uint m_heightPT = 0;

    ImGuiWindowFlags m_windowFlags = ImGuiWindowFlags_None;

    uint m_frameRate = 30; //30 fps
    bool m_isRendered = false;
    bool m_isMesurePerformances = false;

    clock_t m_startTime = 0;
    clock_t m_prevTime = 0;
    clock_t m_ellapse = 0;

    GLint m_lastTexID = 0;

    RenderType m_renderType;

    MenuBarEnum m_gizmoState = MenuBarEnum::SELECTION;

    MoveObject* m_selectedItem = nullptr;

    PerformancesUI m_performancesUi;

};


#endif //MOVE_RENDERUI_HPP
