//
// Created by jb on 27/02/2020.
//

#include "RendererUi.hpp"

RendererUI::RendererUI(Scene* scene) {

    m_scene = scene;
    m_currentItemRenderer = m_ComboRenderText[0];
    m_isUpdatePathTracer = false;

}


void RendererUI::setForwardRenderer(ForwardRenderer* renderer) {
    m_forwardRenderer = renderer;
}
void RendererUI::setPathTracerRenderer(RaytracerRenderer* renderer) {
    m_ptRenderer = renderer;
}

void RendererUI::show() {

    if (ImGui::Begin("Renderer parameters")) {


        ImGui::LabelText("##Global parameters RendererUI", "Global parameters");
        float exposure = m_scene->getGLRenderer()->getExposure();
        if (ImGui::DragFloat("Exposure", &exposure, 0.1f, 0.01f, 100.f)) {

            if (exposure > 0.f) {
                m_scene->getGLRenderer()->setExposure(exposure);
                m_scene->getPTRenderer()->setExposure(exposure);
            }

        }


        m_isUpdatePathTracer = false;

        const char *items[m_rendererCount];

        for (uint i = 0; i < m_rendererCount; ++i) {
            items[i] = m_ComboRenderText[i].c_str();
        }

        static const char *item_current = items[0];
        if (ImGui::BeginCombo("Renderer", item_current)) {
            for (uint i = 0; i < m_rendererCount; i++) {
                bool is_selected = (item_current == items[i]);
                if (ImGui::Selectable(items[i], is_selected))
                    item_current = items[i];
                if (is_selected) {
                    ImGui::SetItemDefaultFocus();
                   // m_currentItemRenderer = item_current;
                }
                m_currentItemRenderer = item_current;

            }
            ImGui::EndCombo();
        }


        if (m_currentItemRenderer == m_ComboRenderText[0]) {
            createForwardUI();
        } else if (m_currentItemRenderer == m_ComboRenderText[1]) {
            createPathTracerUI();
        }
        ImGui::End();
    }



}


void RendererUI::createPathTracerUI() {

    //int maxSamples = 32;
    //ImGui::InputInt("Max samples", &maxSamples);

    EvalMaterial* pathTracer = m_ptRenderer->getPathTracer();

    int rayDepth = int(pathTracer->getRayDepth());
    if (ImGui::InputInt("Max depth", &rayDepth, 1, 4)) {
        rayDepth = rayDepth >= 0 ? rayDepth : 0;
        pathTracer->setRayDepth(uint(rayDepth));
        m_isUpdatePathTracer = true;
    }


    int giSamples = int(pathTracer->getGISamplesCount());
    if (ImGui::InputInt("GI samples", &giSamples, 1, 4)) {
        giSamples = giSamples >= 1 ? giSamples : 1;
        pathTracer->setGISamplesCount(uint(giSamples));
        m_isUpdatePathTracer = true;
    }


}

void RendererUI::createForwardUI() {


    if (ImGui::Button("Reload Shaders")) {
        ObjVis* scene = static_cast<ObjVis*>(m_scene);
        scene->reloadShaders();
    }

    if (ImGui::Checkbox("Wireframe", &m_isWireframe)) {
        ObjVis* scene = static_cast<ObjVis*>(m_scene);
        scene->getGLRenderer()->setIsFillGeometry(!m_isWireframe);
    }


}

bool RendererUI::needUpdatePathTracer() {
    return m_isUpdatePathTracer;
}