//
// Created by jb on 27/02/2020.
//

#ifndef MOVE_RENDERERUI_HPP
#define MOVE_RENDERERUI_HPP

#include <Ui/libs_ui.hpp>
#include <Engine/Rendering/Renderer.hpp>
#include <Engine/Rendering/ForwardRenderer.hpp>
#include <Engine/Rendering/RaytracerRenderer.hpp>
#include <Engine/Scenes/Scene.hpp>
#include <Engine/Scenes/ObjVis.hpp>

class RendererUI {

public :
    RendererUI(Scene* scene = nullptr);

    void show();

    bool needUpdatePathTracer();

    void setForwardRenderer(ForwardRenderer*);
    void setPathTracerRenderer(RaytracerRenderer*);

private :


    void createPathTracerUI();
    void createForwardUI();

    static const uint m_rendererCount = 2;

    ImGuiInputTextFlags m_baseInPutFlags;

    std::string m_ComboRenderText[m_rendererCount] = {"Forward", "Pathtracer"};


    RaytracerRenderer* m_ptRenderer = nullptr;
    ForwardRenderer* m_forwardRenderer = nullptr;

    std::string m_currentItemRenderer = "";
    Scene* m_scene = nullptr;

    bool m_isWireframe = false;

    bool m_isUpdatePathTracer = false;

};


#endif //MOVE_RENDERERUI_HPP
