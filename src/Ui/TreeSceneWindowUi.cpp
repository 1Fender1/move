//
// Created by jb on 01/03/2020.
//

#include "TreeSceneWindowUi.hpp"


TreeSceneWindowUI::TreeSceneWindowUI(RenderData* renderData, ImGuiIO* io) {
    m_renderData = renderData;
    m_selectedObject = nullptr;
    m_io = io;
    m_flags = ImGuiWindowFlags_None | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar;
    m_treeFlags = ImGuiTreeNodeFlags_None | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;

}

MoveObject* TreeSceneWindowUI::createUI() {
    if (m_isFirstLaunch) {
       // ImGui::SetWindowFocus("Scene graph");
    }

    if (ImGui::Begin("Scene graph")/* || m_isFirstLaunch*/) {
      //  m_isFirstLaunch = false;

        bool shouldGetSceneSelectedObject = false;
        shouldGetSceneSelectedObject = (m_scene->getSelectedObject() != nullptr);
        shouldGetSceneSelectedObject &= m_selectedObject && (m_selectedObject->getType() != MoveObject::MATERIAL);

        if (shouldGetSceneSelectedObject) {
            m_selectedObject = m_scene->getSelectedObject();
            if (m_selectedObject) {
            }
        }

        showCameras();
        showLights();
        showObjects();
        showMaterials();

        if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE)) {
            m_selectedObject = nullptr;
            unselectObjects();
            m_scene->setSelectedObject(m_selectedObject);
        }

        if (ImGui::IsKeyPressed(GLFW_KEY_DELETE)) {
            bool isRemovable = true;
            if (m_selectedObject && (m_selectedObject->getType() == MoveObject::CAMERA)) {
                if (m_selectedObject == m_scene->getCamera()) {
                    LOG_WARNING("Cannot remove main camera %n. Please create another one and switch to remove this one.", m_selectedObject->getName())
                    isRemovable = false;
                }
            }
            if (isRemovable) {
                m_renderData->removeObject(m_selectedObject);
                m_selectedObject = nullptr;
                unselectObjects();
                m_scene->setSelectedObject(m_selectedObject);
            }
        }

        ImGui::End();
    }

    return m_selectedObject;
}

void TreeSceneWindowUI::showCameras() {
    if ((m_renderData == nullptr) || (m_renderData->getCameraCount() == 0))
        return;

    bool isCameraOpen = false;
    uint cameraCount = m_renderData->getCameraCount();
    for (uint i = 0; i < cameraCount; ++i) {
        isCameraOpen |= haveChildSelected(m_renderData->getCamera(i));
    }
    ImGuiTreeNodeFlags locFlags = m_treeNode;
    if (isCameraOpen) {
        locFlags = m_treeSelectedNode;
    }

    if (ImGui::TreeNodeEx("Cameras", locFlags)) {
        uint camCount = m_renderData->getCameraCount();
        for (uint i = 0; i < camCount; ++i) {
            Camera* cam = m_renderData->getCamera(i);

            ImGuiTreeNodeFlags localFlag = m_leaf;
            if (m_selectedObject == cam || haveChildSelectedAttrib(cam))
                localFlag = m_selectedLeaf;

            ImGui::TreeNodeEx(cam->getName().c_str(), localFlag);

            if (ImGui::IsItemClicked()) {
                unselectObjects();
                m_scene->setSelectedObject(cam);
                m_selectedObject = cam;
            }
            ImGui::TreePop();
        }
        ImGui::TreePop();
    }


}

void TreeSceneWindowUI::showLights() {
    if ((m_renderData == nullptr) || (m_renderData->getLightCount() == 0))
        return;

    bool isLightOpen = false;
    uint lightCount = m_renderData->getLightCount();
    for (uint i = 0; i < lightCount; ++i) {
        isLightOpen |= haveChildSelected(m_renderData->getLight(i));
    }
    ImGuiTreeNodeFlags locFlags = m_treeNode;
    if (isLightOpen) {
        locFlags = m_treeSelectedNode;
    }


    if (ImGui::TreeNodeEx("Lights", locFlags)) {
        uint lightCount = m_renderData->getLightCount();
        for (uint i = 0; i < lightCount; ++i) {
            Light* light = m_renderData->getLight(i);

            ImGuiTreeNodeFlags localFlags = m_leaf;
            if (m_selectedObject == light || haveChildSelectedAttrib(light))
                localFlags = m_selectedLeaf;

            ImGui::TreeNodeEx(light->getName().c_str(), localFlags);

            if (ImGui::IsItemClicked()) {
                m_selectedObject = light;
                unselectObjects();
                m_scene->setSelectedObject(light);
                light->setSelected(true);
            }
            ImGui::TreePop();
        }
        ImGui::TreePop();
    }


}

void TreeSceneWindowUI::unselectObjects() {
    //unselect lights
    /*
    uint lightSize = m_renderData->getLightCount();
    for (uint i = 0; i < lightSize; ++i) {
        m_renderData->getLight(i)->setSelected(false);
    }*/
    m_scene->unselectScene();
}

void TreeSceneWindowUI::showObjRec(MoveObject* obj) {

    if (obj == nullptr)
        return;

    if (obj->getChildCount() == 0) {
        Mesh* mesh = static_cast<Mesh*>(obj);

        ImGuiTreeNodeFlags locFlag = m_leaf;
        if (m_selectedObject == obj || haveChildSelectedAttrib(obj))
            locFlag = m_selectedLeaf;

        //bool isChildOpen = haveChildSelected(obj);

        ImGui::TreeNodeEx(mesh->getName().c_str(), locFlag);

        if (ImGui::IsItemClicked()) {
            m_selectedObject = mesh;
            unselectObjects();
            m_scene->setSelectedObject(obj);
        }
        ImGui::TreePop();


        return;
    }


    for (uint i = 0; i < obj->getChildCount(); ++i) {
        MoveObject *child = obj->getChild(i);
        if (child == nullptr) {
            continue;
        }

        if (child->getType() == MoveObject::objectType::NONE) {
            return;
        }

        Object *object = nullptr;
        Mesh *mesh = nullptr;

        if (child->getType() == MoveObject::objectType::OBJECT) {
            object = static_cast<Object*>(child);

            //bool isChildOpen = haveChildSelected(object);

            if (ImGui::TreeNodeEx(object->getName().c_str())) {

                if (object->getChildCount() != 0) {
                    showObjRec(object);
                } else {

                    ImGuiTreeNodeFlags locFlag = m_leaf;
                    if (m_selectedObject == mesh || haveChildSelectedAttrib(mesh))
                        locFlag = m_selectedLeaf;

                    //bool isChildOpen = haveChildSelected(object);

                    ImGui::TreeNodeEx(object->getName().c_str(), locFlag);
                    if (ImGui::IsItemClicked()) {
                        m_selectedObject = object;
                        unselectObjects();
                        m_scene->setSelectedObject(object);
                    }
                    ImGui::TreePop();
                }
                ImGui::TreePop();
            }
            continue;
        }
        if (child->getType() == MoveObject::objectType::MESH) {
            mesh = static_cast<Mesh*>(child);

            ImGuiTreeNodeFlags locFlag = m_leaf;
            if (m_selectedObject == mesh || haveChildSelectedAttrib(mesh))
                locFlag = m_selectedLeaf;

            //bool isChildOpen = haveChildSelected(child);

            ImGui::TreeNodeEx(mesh->getName().c_str(), locFlag);
            if (ImGui::IsItemClicked()) {
                m_selectedObject = mesh;
                unselectObjects();
                m_scene->setSelectedObject(mesh);
            }
            ImGui::TreePop();
        }
    }

}


void TreeSceneWindowUI::showObjects() {

    if ((m_renderData == nullptr) || (m_renderData->getObjCount() == 0))
        return;

    bool isMeshOpen = false;
    uint objCount = m_renderData->getObjCount();
    for (uint i = 0; i < objCount; ++i) {
        isMeshOpen |= haveChildSelected(m_renderData->getObject(i)) || haveChildSelectedAttrib(m_renderData->getObject(i));
    }
    ImGuiTreeNodeFlags locFlags = m_treeNode;
    if (isMeshOpen) {
        locFlags = m_treeSelectedNode;
    }

    if (ImGui::TreeNodeEx("Meshes", locFlags)) {
        uint objCount = m_renderData->getObjCount();
        for (uint i = 0; i < objCount; ++i) {
            MoveObject* obj = m_renderData->getObject(i);
            //ImGuiTreeNodeFlags localFlag = m_treeFlags;

            //m_selectedObject = m_scene->getSelectedObject();

            locFlags = m_treeNode;

            if (isMeshOpen && haveChildSelected(obj)) {
                locFlags = m_treeSelectedNode;
            }

            if (m_selectedObject == obj || haveChildSelectedAttrib(obj))
                locFlags |= ImGuiTreeNodeFlags_Selected;

            //bool isChildOpen = haveChildSelected(obj);

            bool isOpen = ImGui::TreeNodeEx(obj->getName().c_str(), locFlags);
            if (ImGui::IsItemClicked(0)) {
                m_selectedObject = obj;
                unselectObjects();
                m_scene->setSelectedObject(obj);
            }

            if (isOpen) {
                showObjRec(obj);
                ImGui::TreePop();
            }
        }
        ImGui::TreePop();
    }


}

void TreeSceneWindowUI::showMaterials() {
    if ((m_renderData == nullptr) || (m_renderData->getMaterialCount() == 0))
        return;

    bool isMatOpen = false;
    uint matCount = m_renderData->getMaterialCount();
    for (uint i = 0; i < matCount; ++i) {
        isMatOpen |= haveChildSelected(m_renderData->getMaterial(i));
    }
    ImGuiTreeNodeFlags locFlags = m_treeNode;
    if (isMatOpen) {
        locFlags = m_treeSelectedNode;
    }

    if (ImGui::TreeNodeEx("Materials", locFlags)) {

        uint  nbMaterials = m_renderData->getMaterialCount();
        for (uint i = 0; i < nbMaterials; ++i) {
            Material *mat = m_renderData->getMaterial(i);
            std::string matName = mat->getName();

            ImGuiTreeNodeFlags localFlags = m_leaf;
            if (m_selectedObject == mat)
                localFlags = m_selectedLeaf;

            ImGui::TreeNodeEx(matName.c_str(), localFlags);

            if (ImGui::IsItemClicked()) {
                m_selectedObject = mat;
                unselectObjects();
                m_scene->setSelectedObject(nullptr);
            }
            ImGui::TreePop();
        }
        ImGui::TreePop();
    }
}

MoveObject* TreeSceneWindowUI::getSelectedObject() {
    return m_selectedObject;
}

void TreeSceneWindowUI::setSelectedObject(MoveObject* obj) {
    m_selectedObject = obj;
}

void TreeSceneWindowUI::setScene(Scene* scene) {
    m_scene = scene;
}

bool TreeSceneWindowUI::haveChildSelected(MoveObject* obj) {
    bool res = false;
    if (!m_selectedObject || !obj)
        return false;
    haveChildSelectedRec(obj, res);
    return res;
}

void TreeSceneWindowUI::haveChildSelectedRec(MoveObject* obj, bool& res) {

    if (!obj) {
        res = false;
        return;
    }

    if (obj->isLeaf() && obj == m_selectedObject) {
        res = true;
        return;
    } else {
        uint objCount = obj->getChildCount();
        for (uint i = 0; i < objCount; ++i) {
            haveChildSelectedRec(obj->getChild(i), res);
        }
    }

}

bool TreeSceneWindowUI::haveChildSelectedAttrib(MoveObject* obj) {
    bool res = false;
    if (!obj)
        return false;
    haveChildSelectedAttribRec(obj, res);
    return res;
}

void TreeSceneWindowUI::haveChildSelectedAttribRec(MoveObject* obj, bool& res) {
    if (!obj) {
        res = false;
        return;
    }

    if (obj->isSelected()) {
        res = true;
        return;
    } else {
        uint objCount = obj->getChildCount();
        for (uint i = 0; i < objCount; ++i) {
            haveChildSelectedAttribRec(obj->getChild(i), res);
        }
    }
}