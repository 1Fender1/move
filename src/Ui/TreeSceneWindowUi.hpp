//
// Created by jb on 01/03/2020.
//

#ifndef MOVE_TREESCENEWINDOWUI_HPP
#define MOVE_TREESCENEWINDOWUI_HPP

#include <Engine/Scenes/RenderData.hpp>
#include <Engine/Scenes/Scene.hpp>

class TreeSceneWindowUI {

public :

    TreeSceneWindowUI(RenderData* renderData = nullptr, ImGuiIO* io = nullptr);
    MoveObject* createUI();

    //If an object is selected, return a pointer to this, object. return nullptr else.
    MoveObject* getSelectedObject();
    void setSelectedObject(MoveObject*);

    bool selectObject();
    void setScene(Scene* scene);

private :

    void showCameras();
    void showLights();
    void showObjects();
    void showMaterials();

    void unselectObjects();

    void showObjRec(MoveObject* obj);

    bool haveChildSelected(MoveObject* obj);
    void haveChildSelectedRec(MoveObject* obj, bool& res);

    bool haveChildSelectedAttrib(MoveObject* obj);

    void haveChildSelectedAttribRec(MoveObject* obj, bool& res);

    RenderData* m_renderData;
    MoveObject* m_selectedObject = nullptr;
    ImGuiIO* m_io = nullptr;

    Scene* m_scene = nullptr;

    ImGuiWindowFlags m_flags;
    ImGuiTreeNodeFlags m_treeFlags;

    ImGuiTreeNodeFlags m_leaf = ImGuiTreeNodeFlags_Leaf;
    ImGuiTreeNodeFlags m_selectedLeaf = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Selected;
    ImGuiTreeNodeFlags m_treeNode = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
    ImGuiTreeNodeFlags m_treeSelectedNode = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_DefaultOpen;
    ImGuiCond m_openingCond = ImGuiCond_FirstUseEver;

    bool m_isFirstLaunch = true;

};


#endif //MOVE_TREESCENEWINDOWUI_HPP
