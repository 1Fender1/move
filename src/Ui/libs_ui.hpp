//
// Created by jb on 25/02/2020.
//

#ifndef MOVE_LIBS_UI_HPP
#define MOVE_LIBS_UI_HPP

#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <Ui/imgui/imgui.h>
#include <Ui/imgui/imgui_internal.h>
#include <Ui/imgui/imgui_impl_glfw.h>
#include <Ui/imgui/imgui_impl_opengl3.h>



#endif //MOVE_LIBS_UI_HPP
